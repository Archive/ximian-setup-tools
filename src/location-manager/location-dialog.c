/* -*- mode: c; style: linux; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */

/* location-dialog.c
 * Copyright (C) 2000 Ximian, Inc.
 *
 * Written by Bradford Hovinen <hovinen@helixcode.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include <bonobo.h>
#include <glade/glade.h>
#include "location-dialog.h"

#define WID(str) glade_xml_get_widget (dialog->p->dialog_data, str)

enum {
	ARG_0,
	ARG_TYPE,
	ARG_ARCHIVE,
	ARG_LOCATION
};

enum {
	OK_CLICKED_SIGNAL,
	LAST_SIGNAL
};

struct _LocationDialogPrivate 
{
	LocationDialogType       type;
	GladeXML                *dialog_data;

	ConfigArchiver_Archive   archive;
	ConfigArchiver_Location  location;
	LocationList            *list;
};

static GnomeDialogClass *parent_class;

static guint location_dialog_signals[LAST_SIGNAL] = { 0 };

static void location_dialog_init        (LocationDialog *location_dialog);
static void location_dialog_class_init  (LocationDialogClass *class);

static void location_dialog_set_arg     (GtkObject *object, 
					 GtkArg *arg, 
					 guint arg_id);
static void location_dialog_get_arg     (GtkObject *object, 
					 GtkArg *arg, 
					 guint arg_id);

static void location_dialog_destroy     (GtkObject *object);
static void location_dialog_finalize    (GtkObject *object);

static void ok_cb                       (GtkButton *button,
					 LocationDialog *dialog);
static void cancel_cb                   (GtkButton *button,
					 LocationDialog *dialog);

guint
location_dialog_get_type (void)
{
	static guint location_dialog_type = 0;

	if (!location_dialog_type) {
		GtkTypeInfo location_dialog_info = {
			"LocationDialog",
			sizeof (LocationDialog),
			sizeof (LocationDialogClass),
			(GtkClassInitFunc) location_dialog_class_init,
			(GtkObjectInitFunc) location_dialog_init,
			(GtkArgSetFunc) NULL,
			(GtkArgGetFunc) NULL
		};

		location_dialog_type = 
			gtk_type_unique (gnome_dialog_get_type (), 
					 &location_dialog_info);
	}

	return location_dialog_type;
}

static void
location_dialog_init (LocationDialog *dialog)
{
	dialog->p = g_new0 (LocationDialogPrivate, 1);

	gnome_dialog_append_buttons (GNOME_DIALOG (dialog),
				     GNOME_STOCK_BUTTON_OK,
				     GNOME_STOCK_BUTTON_CANCEL,
				     NULL);

	gnome_dialog_button_connect (GNOME_DIALOG (dialog), 0,
				     GTK_SIGNAL_FUNC (ok_cb), dialog);
	gnome_dialog_button_connect (GNOME_DIALOG (dialog), 1,
				     GTK_SIGNAL_FUNC (cancel_cb), dialog);
}

static void
location_dialog_class_init (LocationDialogClass *class) 
{
	GtkObjectClass *object_class;

	gtk_object_add_arg_type ("LocationDialog::type",
				 GTK_TYPE_INT,
				 GTK_ARG_READWRITE | GTK_ARG_CONSTRUCT_ONLY,
				 ARG_TYPE);

	gtk_object_add_arg_type ("LocationDialog::archive",
				 GTK_TYPE_POINTER,
				 GTK_ARG_READWRITE | GTK_ARG_CONSTRUCT_ONLY,
				 ARG_ARCHIVE);

	gtk_object_add_arg_type ("LocationDialog::location",
				 GTK_TYPE_POINTER,
				 GTK_ARG_READWRITE | GTK_ARG_CONSTRUCT_ONLY,
				 ARG_LOCATION);

	object_class = GTK_OBJECT_CLASS (class);
	object_class->destroy = location_dialog_destroy;
	object_class->finalize = location_dialog_finalize;
	object_class->set_arg = location_dialog_set_arg;
	object_class->get_arg = location_dialog_get_arg;

	location_dialog_signals[OK_CLICKED_SIGNAL] =
		gtk_signal_new ("ok-clicked", GTK_RUN_FIRST,
				object_class->type,
				GTK_SIGNAL_OFFSET (LocationDialogClass,
						   ok_clicked),
				gtk_signal_default_marshaller,
				GTK_TYPE_NONE, 0);

	gtk_object_class_add_signals (object_class, location_dialog_signals,
				      LAST_SIGNAL);

	parent_class = GNOME_DIALOG_CLASS
		(gtk_type_class (gnome_dialog_get_type ()));
}

static void
location_dialog_set_arg (GtkObject *object, GtkArg *arg, guint arg_id) 
{
	LocationDialog *dialog;
	gchar *str, *title;
	CORBA_Environment ev;
	CORBA_char *id;

	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_LOCATION_DIALOG (object));

	CORBA_exception_init (&ev);

	dialog = LOCATION_DIALOG (object);

	switch (arg_id) {
	case ARG_TYPE:
		dialog->p->type = GTK_VALUE_INT (*arg);

		switch (dialog->p->type) {
		case NEW_DIALOG:
			str = "new_location";
			title = _("Create new location");
			break;

		case COPY_DIALOG:
			str = "copy_location";
			title = _("Copy a location");
			break;

		case RENAME_DIALOG:
			str = "rename_location";
			title = _("Rename a location");
			break;

		default:
			g_warning ("Bad location dialog type specified");
			return;
			break;
		}

		dialog->p->dialog_data =
			glade_xml_new (INTERFACES_DIR "/location-manager.glade",
				       str);
		gtk_box_pack_start (GTK_BOX (GNOME_DIALOG (dialog)->vbox), 
				    WID (str), TRUE, TRUE, 0);

		gtk_window_set_title (GTK_WINDOW (dialog), title);

		break;

	case ARG_ARCHIVE:
		g_return_if_fail (GTK_VALUE_POINTER (*arg) != CORBA_OBJECT_NIL);

		dialog->p->archive = bonobo_object_dup_ref (GTK_VALUE_POINTER (*arg), &ev);

		dialog->p->list = LOCATION_LIST 
			(location_list_new (dialog->p->archive,
					    WID ("location_list")));
		break;

	case ARG_LOCATION:
		if (GTK_VALUE_POINTER (*arg) != CORBA_OBJECT_NIL) {
			dialog->p->location = bonobo_object_dup_ref (GTK_VALUE_POINTER (*arg), &ev);

			id = ConfigArchiver_Location__get_id (dialog->p->location, &ev);
			gtk_entry_set_text
				(GTK_ENTRY (WID ("name_entry")), id);
			CORBA_free (id);
		}

		break;

	default:
		g_warning ("Bad argument set");
		break;
	}

	CORBA_exception_free (&ev);
}

static void
location_dialog_get_arg (GtkObject *object, GtkArg *arg, guint arg_id) 
{
	LocationDialog *location_dialog;

	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_LOCATION_DIALOG (object));

	location_dialog = LOCATION_DIALOG (object);

	switch (arg_id) {
	case ARG_TYPE:
		GTK_VALUE_INT (*arg) = location_dialog->p->type;
		break;

	case ARG_ARCHIVE:
		GTK_VALUE_POINTER (*arg) = location_dialog->p->archive;
		break;

	case ARG_LOCATION:
		GTK_VALUE_POINTER (*arg) = location_dialog->p->location;
		break;

	default:
		g_warning ("Bad argument get");
		break;
	}
}

static void
location_dialog_destroy (GtkObject *object) 
{
	LocationDialog *location_dialog;

	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_LOCATION_DIALOG (object));

	location_dialog = LOCATION_DIALOG (object);

	gtk_object_destroy (GTK_OBJECT (location_dialog->p->list));
	gtk_object_destroy (GTK_OBJECT (location_dialog->p->dialog_data));

	if (location_dialog->p->archive != CORBA_OBJECT_NIL)
		bonobo_object_release_unref (location_dialog->p->archive, NULL);

	GTK_OBJECT_CLASS (parent_class)->destroy (object);
}

static void
location_dialog_finalize (GtkObject *object) 
{
	LocationDialog *location_dialog;

	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_LOCATION_DIALOG (object));

	location_dialog = LOCATION_DIALOG (object);

	g_free (location_dialog->p);

	GTK_OBJECT_CLASS (parent_class)->finalize (object);
}

GtkWidget *
location_dialog_new (LocationDialogType type,
		     ConfigArchiver_Archive archive,
		     ConfigArchiver_Location location)
{
	return gtk_widget_new (location_dialog_get_type (),
			       "type", type,
			       "archive", archive,
			       "location", location,
			       NULL);
}



static void
ok_cb (GtkButton *button, LocationDialog *dialog)
{
	gchar *name;
	ConfigArchiver_Location parent;
	CORBA_Environment ev;
	ConfigArchiver_Location loc;

	CORBA_exception_init (&ev);

	name = gtk_entry_get_text (GTK_ENTRY (WID ("name_entry")));

	if (name == NULL || *name == '\0') {
		gnome_ok_dialog (_("You must supply a name"));
		return;
	}

	loc = ConfigArchiver_Archive_getLocation (dialog->p->archive, name, &ev);

	if (!BONOBO_EX (&ev) || strcmp (ev._repo_id, "IDL:ConfigArchiver/Archive/LocationNotFound:1.0")) {
		gnome_ok_dialog (_("You already have a location by that name"));
		gnome_dialog_close (GNOME_DIALOG (dialog));
		bonobo_object_release_unref (loc, NULL);
		CORBA_exception_free (&ev);
		return;
	}

	switch (dialog->p->type) {
	case NEW_DIALOG:
		parent = ConfigArchiver_Archive_getLocation (dialog->p->archive, "default", &ev);
		loc = ConfigArchiver_Archive_createLocation (dialog->p->archive, name, name, parent, &ev);
		bonobo_object_release_unref (parent, NULL);
		bonobo_object_release_unref (loc, NULL);
		break;

	case COPY_DIALOG:
		loc = ConfigArchiver_Archive_createLocation (dialog->p->archive, name, name, dialog->p->location, &ev);
		bonobo_object_release_unref (loc, NULL);
		break;

	case RENAME_DIALOG:
		ConfigArchiver_Location__set_id (dialog->p->location, name, &ev);
		break;
	}

	gtk_signal_emit (GTK_OBJECT (dialog),
			 location_dialog_signals[OK_CLICKED_SIGNAL]);

	gnome_dialog_close (GNOME_DIALOG (dialog));

	CORBA_exception_free (&ev);
}

static void
cancel_cb (GtkButton *button, LocationDialog *dialog)
{
	gnome_dialog_close (GNOME_DIALOG (dialog));
}
