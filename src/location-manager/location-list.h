/* -*- mode: c; style: linux -*- */

/* location-list.h
 * Copyright (C) 2000-2001 Ximian, Inc.
 *
 * Written by Bradford Hovinen <hovinen@ximian.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */

#ifndef __LOCATION_LIST_H
#define __LOCATION_LIST_H

#include <gnome.h>

#include <config-archiver/archiver-client.h>

BEGIN_GNOME_DECLS

#define LOCATION_LIST(obj)          GTK_CHECK_CAST (obj, location_list_get_type (), LocationList)
#define LOCATION_LIST_CLASS(klass)  GTK_CHECK_CLASS_CAST (klass, location_list_get_type (), LocationListClass)
#define IS_LOCATION_LIST(obj)       GTK_CHECK_TYPE (obj, location_list_get_type ())

typedef struct _LocationList LocationList;
typedef struct _LocationListClass LocationListClass;
typedef struct _LocationListPrivate LocationListPrivate;

struct _LocationList 
{
	GtkObject parent;

	LocationListPrivate *p;
};

struct _LocationListClass 
{
	GtkObjectClass gtk_ctree_class;

	void (*location_changed) (LocationList *, ConfigArchiver_Location);
};

guint                     location_list_get_type                  (void);

GtkObject                *location_list_new                       (ConfigArchiver_Archive archive,
								   GtkWidget *widget);

gchar                    *location_list_get_selected_location_id  (LocationList *list);
ConfigArchiver_Location   location_list_get_selected_location     (LocationList *list);

void                      location_list_reread                    (LocationList *list);
void                      location_list_select                    (LocationList *list,
								   ConfigArchiver_Location location);

END_GNOME_DECLS

#endif /* __LOCATION_LIST_H */
