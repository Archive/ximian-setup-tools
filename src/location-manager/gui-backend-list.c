/* -*- mode: c; style: linux; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */

/* gui-backend-list.c
 * Copyright (C) 2000 Ximian, Inc.
 *
 * Written by Bradford Hovinen <hovinen@helixcode.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include <stdlib.h>
#include <dirent.h>
#include <errno.h>
#include <string.h>
#include <pthread.h>
#include <gdk-pixbuf/gdk-pixbuf.h>

#include <bonobo.h>
#include <config-archiver/archiver-client.h>

#include "gui-backend-list.h"

#include "checked.xpm"
#include "unchecked.xpm"

typedef struct _BackendNote BackendNote;

enum {
	ARG_0,
	ARG_ARCHIVE,
	ARG_CURRENT_LOCATION,
	ARG_WIDGET
};

struct _BackendNote 
{
	gchar             *backend_id;
	gchar             *label;
	gboolean           selected;
	gint               clist_row;
	GnomeDesktopEntry *dentry;
};

struct _GuiBackendListPrivate 
{
	GList                   *backends;
	BackendNote             *current_backend;
	ConfigArchiver_Archive   archive;
	ConfigArchiver_Location  current_location;
	GtkWidget               *widget;
};

static GtkObjectClass *parent_class;

static GdkPixmap *checked_pixmap;
static GdkPixmap *unchecked_pixmap;

static GdkBitmap *checked_mask;
static GdkBitmap *unchecked_mask;

static void gui_backend_list_init        (GuiBackendList *gui_backend_list);
static void gui_backend_list_class_init  (GuiBackendListClass *class);

static void gui_backend_list_set_arg     (GtkObject *object, 
					  GtkArg *arg, 
					  guint arg_id);
static void gui_backend_list_get_arg     (GtkObject *object, 
					  GtkArg *arg, 
					  guint arg_id);

static void gui_backend_list_destroy     (GtkObject *object);
static void gui_backend_list_finalize    (GtkObject *object);

static void append_entry                 (GuiBackendList *gui_list,
					  gchar *backend_id, 
					  GnomeDesktopEntry *entry);
static void append_entry_to_widget       (GuiBackendList *gui_list,
					  BackendNote *note);
static void populate_from_dir            (GuiBackendList *gui_list,
					  ConfigArchiver_BackendList list,
					  gchar *path,
					  CORBA_Environment *ev);
static void populate                     (GuiBackendList *dialog,
					  CORBA_Environment *ev);

static gchar *get_backend_id             (GnomeDesktopEntry *dentry,
					  ConfigArchiver_Archive archive,
					  CORBA_Environment *ev);

static void select_row_cb                (GtkCList *clist,
					  gint row, gint column,
					  GdkEvent *event,
					  GuiBackendList *list);

static void recheck_backends             (GuiBackendList *list,
					  CORBA_Environment *ev);

static void select_backend               (GuiBackendList *list,
					  BackendNote *note,
					  CORBA_Environment *ev);
static void unselect_backend             (GuiBackendList *list,
					  BackendNote *note,
					  CORBA_Environment *ev);

static void backend_note_destroy         (BackendNote *note);

guint
gui_backend_list_get_type (void)
{
	static guint gui_backend_list_type = 0;

	if (!gui_backend_list_type) {
		GtkTypeInfo gui_backend_list_info = {
			"GuiBackendList",
			sizeof (GuiBackendList),
			sizeof (GuiBackendListClass),
			(GtkClassInitFunc) gui_backend_list_class_init,
			(GtkObjectInitFunc) gui_backend_list_init,
			(GtkArgSetFunc) NULL,
			(GtkArgGetFunc) NULL
		};

		gui_backend_list_type = 
			gtk_type_unique (gtk_object_get_type (), 
					 &gui_backend_list_info);
	}

	return gui_backend_list_type;
}

static void
gui_backend_list_init (GuiBackendList *gui_backend_list)
{
	gui_backend_list->p = g_new0 (GuiBackendListPrivate, 1);
}

static void
gui_backend_list_class_init (GuiBackendListClass *class) 
{
	GtkObjectClass *object_class;
	GdkPixbuf *pixbuf;

	gtk_object_add_arg_type ("GuiBackendList::archive",
				 GTK_TYPE_POINTER,
				 GTK_ARG_READWRITE | GTK_ARG_CONSTRUCT_ONLY,
				 ARG_ARCHIVE);

	gtk_object_add_arg_type ("GuiBackendList::current-location",
				 GTK_TYPE_POINTER,
				 GTK_ARG_READWRITE,
				 ARG_CURRENT_LOCATION);

	gtk_object_add_arg_type ("GuiBackendList::widget",
				 GTK_TYPE_POINTER,
				 GTK_ARG_READWRITE | GTK_ARG_CONSTRUCT_ONLY,
				 ARG_WIDGET);

	object_class = GTK_OBJECT_CLASS (class);
	object_class->destroy = gui_backend_list_destroy;
	object_class->finalize = gui_backend_list_finalize;
	object_class->set_arg = gui_backend_list_set_arg;
	object_class->get_arg = gui_backend_list_get_arg;

	parent_class = GTK_OBJECT_CLASS
		(gtk_type_class (gtk_object_get_type ()));

	if (checked_pixmap == NULL) {
		pixbuf = gdk_pixbuf_new_from_xpm_data
			((const char **) checked_xpm);
		gdk_pixbuf_render_pixmap_and_mask (pixbuf, &checked_pixmap,
						   &checked_mask, 1);
		gdk_pixbuf_unref (pixbuf);
		gdk_pixmap_ref (checked_pixmap);
		gdk_bitmap_ref (checked_mask);
	}

	if (unchecked_pixmap == NULL) {
		pixbuf = gdk_pixbuf_new_from_xpm_data
			((const char **) unchecked_xpm);
		gdk_pixbuf_render_pixmap_and_mask (pixbuf, &unchecked_pixmap,
						   &unchecked_mask, 1);
		gdk_pixbuf_unref (pixbuf);
		gdk_pixmap_ref (unchecked_pixmap);
		gdk_bitmap_ref (unchecked_mask);
	}
}

static void
gui_backend_list_set_arg (GtkObject *object, GtkArg *arg, guint arg_id) 
{
	GuiBackendList *gui_backend_list;
	CORBA_Environment ev;

	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_GUI_BACKEND_LIST (object));

	gui_backend_list = GUI_BACKEND_LIST (object);

	CORBA_exception_init (&ev);

	switch (arg_id) {
	case ARG_ARCHIVE:
		if (GTK_VALUE_POINTER (*arg) == CORBA_OBJECT_NIL) return;

		gui_backend_list->p->archive =
			bonobo_object_dup_ref (GTK_VALUE_POINTER (*arg), &ev);

		if (gui_backend_list->p->widget != NULL &&
		    gui_backend_list->p->current_location != CORBA_OBJECT_NIL)
			populate (gui_backend_list, &ev);

		break;

	case ARG_CURRENT_LOCATION:
		if (GTK_VALUE_POINTER (*arg) == CORBA_OBJECT_NIL) return;

		if (gui_backend_list->p->current_location != CORBA_OBJECT_NIL)
			bonobo_object_release_unref
				(gui_backend_list->p->current_location, NULL);

		gui_backend_list->p->current_location = 
			bonobo_object_dup_ref (GTK_VALUE_POINTER (*arg), &ev);

		if (gui_backend_list->p->widget != NULL &&
		    gui_backend_list->p->archive != CORBA_OBJECT_NIL)
		{
			if (gui_backend_list->p->backends == NULL)
				populate (gui_backend_list, &ev);
			else
				recheck_backends (gui_backend_list, &ev);
		}

		break;

	case ARG_WIDGET:
		g_return_if_fail (GTK_VALUE_POINTER (*arg) == NULL || 
				  GTK_IS_CLIST (GTK_VALUE_POINTER (*arg)));

		if (GTK_VALUE_POINTER (*arg) == NULL) return;

		gui_backend_list->p->widget = GTK_VALUE_POINTER (*arg);

		gtk_object_ref (GTK_OBJECT (gui_backend_list->p->widget));

		if (gui_backend_list->p->archive != NULL &&
		    gui_backend_list->p->current_location != NULL)
			populate (gui_backend_list, &ev);

		gtk_signal_connect (GTK_OBJECT (gui_backend_list->p->widget),
				    "select-row",
				    GTK_SIGNAL_FUNC (select_row_cb),
				    gui_backend_list);

		gtk_clist_set_selection_mode
			(GTK_CLIST (gui_backend_list->p->widget),
			 GTK_SELECTION_BROWSE);

		gtk_clist_set_column_width
			(GTK_CLIST (gui_backend_list->p->widget), 0, 20);

		break;

	default:
		g_warning ("Bad argument set");
		break;
	}

	CORBA_exception_free (&ev);
}

static void
gui_backend_list_get_arg (GtkObject *object, GtkArg *arg, guint arg_id) 
{
	GuiBackendList *gui_backend_list;

	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_GUI_BACKEND_LIST (object));

	gui_backend_list = GUI_BACKEND_LIST (object);

	switch (arg_id) {
	case ARG_ARCHIVE:
		GTK_VALUE_POINTER (*arg) = gui_backend_list->p->archive;
		break;

	case ARG_CURRENT_LOCATION:
		GTK_VALUE_POINTER (*arg) =
			gui_backend_list->p->current_location;
		break;

	case ARG_WIDGET:
		GTK_VALUE_POINTER (*arg) = gui_backend_list->p->widget;
		break;

	default:
		g_warning ("Bad argument get");
		break;
	}
}

static void
gui_backend_list_destroy (GtkObject *object) 
{
	GuiBackendList *gui_backend_list;

	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_GUI_BACKEND_LIST (object));

	gui_backend_list = GUI_BACKEND_LIST (object);

	bonobo_object_release_unref (gui_backend_list->p->archive, NULL);
	bonobo_object_release_unref (gui_backend_list->p->current_location, NULL);
	gtk_object_unref (GTK_OBJECT (gui_backend_list->p->widget));

	g_list_foreach (gui_backend_list->p->backends,
			(GFunc) backend_note_destroy,
			NULL);
	g_list_free (gui_backend_list->p->backends);

	GTK_OBJECT_CLASS (parent_class)->destroy (object);
}

static void
gui_backend_list_finalize (GtkObject *object) 
{
	GuiBackendList *gui_backend_list;

	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_GUI_BACKEND_LIST (object));

	gui_backend_list = GUI_BACKEND_LIST (object);

	g_free (gui_backend_list->p);

	GTK_OBJECT_CLASS (parent_class)->finalize (object);
}

GtkObject *
gui_backend_list_new (ConfigArchiver_Archive archive, ConfigArchiver_Location current, GtkWidget *widget) 
{
	return gtk_object_new (gui_backend_list_get_type (),
			       "archive", archive,
			       "current-location", current,
			       "widget", widget,
			       NULL);
}

void
gui_backend_list_launch_current (GuiBackendList *list) 
{
	gchar *command, *args, *command_str, *backend_id;
	GnomeDesktopEntry *dentry;
	gint start;
	ConfigArchiver_Location active_location;
	CORBA_boolean res;
	CORBA_char *id;
	CORBA_Environment ev;

	g_return_if_fail (list != NULL);
	g_return_if_fail (IS_GUI_BACKEND_LIST (list));

	CORBA_exception_init (&ev);

	dentry = list->p->current_backend->dentry;

	start = 0;
	while (!strcmp (dentry->exec[start], "root-manager") ||
	       !strcmp (dentry->exec[start], "console-helper")) start++;

	/* Search $(prefix)/sbin if this is an XST */
	if (ConfigArchiver_Archive__get_isGlobal (list->p->archive, &ev)) {
		char *path, *path1;

		path = g_getenv ("PATH");

		if (!strstr (path, FRONTEND_DIR)) {
			path1 = g_strconcat (FRONTEND_DIR, ":", path, NULL);
			setenv ("PATH", path1, TRUE);
			g_free (path1);
		}
	}

	command = gnome_is_program_in_path (dentry->exec[start]);

	if (command == NULL) {
		gnome_warning_dialog (_("Could not launch configuration tool"));
		return;
	}

	args = g_strjoinv (" ", dentry->exec + start + 1);

	active_location = ConfigArchiver_Archive__get_currentLocation (list->p->archive, &ev);

	if (active_location == CORBA_OBJECT_NIL || BONOBO_EX (&ev)) {
		g_critical ("Could not get current location");
		return;
	}

	backend_id = get_backend_id (dentry, list->p->archive, &ev);

	res = ConfigArchiver_Location_doesBackendChange (list->p->current_location,
							 active_location, backend_id, &ev);

	if (res) {
		id = ConfigArchiver_Location__get_id (list->p->current_location, &ev);
		command_str = g_strconcat (command, " --location=\"", id,
					   "\" ", args, NULL);
		CORBA_free (id);
	} else {
		command_str = g_strconcat (command, " ", args, NULL);
	}

	bonobo_object_release_unref (active_location, NULL);

	if (system (command_str) == -1) {
		gchar *warning_string;
		GtkWidget *dialog;

		warning_string = g_strdup_printf (_("Could not launch configuration tool: %s"),
						  g_strerror (errno));
		dialog = gnome_warning_dialog (warning_string);
		gnome_dialog_run_and_close (GNOME_DIALOG (dialog));
		g_free (warning_string);
	}

	g_free (backend_id);
	g_free (command);
	g_free (args);
	g_free (command_str);

	CORBA_exception_free (&ev);
}

void
gui_backend_list_select_current (GuiBackendList *list)
{
	CORBA_Environment ev;

	g_return_if_fail (list != NULL);
	g_return_if_fail (IS_GUI_BACKEND_LIST (list));

	CORBA_exception_init (&ev);
	select_backend (list, list->p->current_backend, &ev);
	CORBA_exception_free (&ev);
}

void
gui_backend_list_unselect_current (GuiBackendList *list)
{
	CORBA_Environment ev;

	g_return_if_fail (list != NULL);
	g_return_if_fail (IS_GUI_BACKEND_LIST (list));

	CORBA_exception_init (&ev);
	unselect_backend (list, list->p->current_backend, &ev);
	CORBA_exception_free (&ev);
}



static void
append_entry (GuiBackendList *gui_list, gchar *backend_id, GnomeDesktopEntry *entry) 
{
	BackendNote *note;
	ConfigArchiver_ContainmentType res;
	CORBA_Environment ev;

	CORBA_exception_init (&ev);

	res = ConfigArchiver_Location_contains (gui_list->p->current_location, backend_id, &ev);

	note = g_new0 (BackendNote, 1);
	note->backend_id = backend_id;
	note->label = entry->name;
	note->selected = (res != ConfigArchiver_CONTAIN_NONE);
	note->dentry = entry;

	gui_list->p->backends = g_list_prepend (gui_list->p->backends, note);

	append_entry_to_widget (gui_list, note);

	CORBA_exception_free (&ev);
}

static void
append_entry_to_widget (GuiBackendList *gui_list, BackendNote *note) 
{
	gchar *titles[2];

	titles[0] = NULL;
	titles[1] = note->label;

	note->clist_row = 
		gtk_clist_append (GTK_CLIST (gui_list->p->widget), titles);
	gtk_clist_set_row_data (GTK_CLIST (gui_list->p->widget),
				note->clist_row, note);

	if (note->selected) {
		gtk_clist_set_pixmap
			(GTK_CLIST (gui_list->p->widget),
			 note->clist_row, 0, checked_pixmap, checked_mask);
	} else {
		gtk_clist_set_pixmap
			(GTK_CLIST (gui_list->p->widget),
			 note->clist_row, 0, unchecked_pixmap, unchecked_mask);
	}
}

static void
populate_from_dir (GuiBackendList *gui_list, ConfigArchiver_BackendList list, gchar *path, CORBA_Environment *ev) 
{
	struct dirent dir_entry, *result;
	DIR *dir;
	GnomeDesktopEntry *entry;
	gchar *command;
	gchar *file_path;

	dir = opendir (path);

	if (dir == NULL) {
		g_warning ("Cannot open backend directory: %s",
			   g_strerror (errno));
		return;
	}

	while (1) {
		if (readdir_r (dir, &dir_entry, &result) != 0) {
			g_warning ("Could not read directory: %s",
				   g_strerror (errno));
		}

		if (result == NULL) break;

		file_path = g_concat_dir_and_file (path, result->d_name);

		if (g_file_test (file_path, G_FILE_TEST_ISFILE) &&
		    strstr (result->d_name, ".desktop"))
		{
			entry = gnome_desktop_entry_load (file_path);

			if (entry == NULL) {
				g_free (file_path);
				continue;
			}

			command = get_backend_id (entry, gui_list->p->archive, ev);

			if (ConfigArchiver_BackendList_contains (list, command, ev))
				append_entry (gui_list, command, entry);
			else
				g_free (command);
		}
		else if (g_file_test (file_path, G_FILE_TEST_ISDIR) &&
			 strcmp (result->d_name, ".") &&
			 strcmp (result->d_name, ".."))
		{
			populate_from_dir (gui_list, list, file_path, ev);
		}

		g_free (file_path);
	}

	closedir (dir);
}

static void
populate (GuiBackendList *gui_list, CORBA_Environment *ev)
{
	ConfigArchiver_BackendList list;

	list = ConfigArchiver_Archive__get_backendList (gui_list->p->archive, ev);
	populate_from_dir (gui_list, list, DESKTOP_DATA_DIR, ev);
	bonobo_object_release_unref (list, NULL);
}

static gchar *
get_backend_id (GnomeDesktopEntry *dentry, ConfigArchiver_Archive archive, CORBA_Environment *ev) 
{
	gchar *tmp, *tmp1, *command;

	if (!strcmp (dentry->exec[0], "root-manager") ||
	    !strcmp (dentry->exec[0], "console-helper"))
	{
		if (strstr (dentry->exec[1], "-admin")) {
			tmp = g_strdup (dentry->exec[1]);
			tmp1 = strstr (dentry->exec[1], "-admin");
			*tmp1 = '\0';
			command = g_strconcat (tmp, "-conf");
			g_free (tmp);
		} else {
			command = g_strdup (dentry->exec[1]);
		}
	} else {
		command = g_strdup (dentry->exec[0]);
	}

	/* If we are dealing with an XST, replace -admin with
	 * -conf to get the backend name */
	if (ConfigArchiver_Archive__get_isGlobal (archive, ev)) {
		char *tmp;

		tmp = strstr (command, "-admin");
		if (tmp != NULL)
			strcpy (tmp, "-conf");
	}

	return command;
}

static void
select_row_cb (GtkCList *clist, gint row, gint column,
	       GdkEvent *event, GuiBackendList *list)
{
	BackendNote *note;
	CORBA_Environment ev;

	CORBA_exception_init (&ev);

	note = gtk_clist_get_row_data (clist, row);

	if (column == 0) {
		if (note->selected) {
			g_assert (ConfigArchiver_Location_contains
				  (list->p->current_location,
				   note->backend_id, &ev) != ConfigArchiver_CONTAIN_NONE);

			unselect_backend (list, note, &ev);
		} else {
			g_assert (ConfigArchiver_Location_contains
				  (list->p->current_location,
				   note->backend_id, &ev) == ConfigArchiver_CONTAIN_NONE);

			select_backend (list, note, &ev);
		}
	}

	list->p->current_backend = note;

	CORBA_exception_free (&ev);
}

static void
recheck_backends (GuiBackendList *list, CORBA_Environment *ev)
{
	GList *node;
	BackendNote *note;

	for (node = list->p->backends; node != NULL; node = node->next) {
		note = node->data;
		if (ConfigArchiver_Location_contains (list->p->current_location, note->backend_id, ev) != ConfigArchiver_CONTAIN_NONE) {
			note->selected = TRUE;
			gtk_clist_set_pixmap (GTK_CLIST (list->p->widget),
					      note->clist_row, 0,
					      checked_pixmap, checked_mask);
		} else {
			note->selected = FALSE;
			gtk_clist_set_pixmap (GTK_CLIST (list->p->widget),
					      note->clist_row, 0,
					      unchecked_pixmap, unchecked_mask);
		}
	}
}

static void
select_backend (GuiBackendList *list, BackendNote *note, CORBA_Environment *ev)
{
	ConfigArchiver_Location parent;

	parent = ConfigArchiver_Location__get_parent (list->p->current_location, ev);

	if (BONOBO_EX (ev) || parent == CORBA_OBJECT_NIL)
		return;

	bonobo_object_release_unref (parent, NULL);

	ConfigArchiver_Location_addBackend (list->p->current_location,
					    note->backend_id,
					    ConfigArchiver_CONTAIN_FULL, ev);
	gtk_clist_set_pixmap (GTK_CLIST (list->p->widget), note->clist_row, 0,
			      checked_pixmap, checked_mask);
	note->selected = TRUE;
}

static void
unselect_backend (GuiBackendList *list, BackendNote *note, CORBA_Environment *ev)
{
	ConfigArchiver_Location parent;

	parent = ConfigArchiver_Location__get_parent (list->p->current_location, ev);

	if (BONOBO_EX (ev) || parent == CORBA_OBJECT_NIL)
		return;

	bonobo_object_release_unref (parent, NULL);

	ConfigArchiver_Location_removeBackend (list->p->current_location, note->backend_id, ev);
	gtk_clist_set_pixmap (GTK_CLIST (list->p->widget), note->clist_row, 0,
			      unchecked_pixmap, unchecked_mask);
	note->selected = FALSE;
}

static void
backend_note_destroy (BackendNote *note)
{
	gnome_desktop_entry_free (note->dentry);
	g_free (note->backend_id);
	g_free (note);
}
