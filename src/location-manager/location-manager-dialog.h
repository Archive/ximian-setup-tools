/* -*- mode: c; style: linux -*- */

/* location-manager-dialog.h
 * Copyright (C) 2000 Ximian, Inc.
 *
 * Written by Bradford Hovinen <hovinen@helixcode.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */

#ifndef __LOCATION_MANAGER_DIALOG_H
#define __LOCATION_MANAGER_DIALOG_H

#include <gnome.h>

BEGIN_GNOME_DECLS

#define LOCATION_MANAGER_DIALOG(obj)          GTK_CHECK_CAST (obj, location_manager_dialog_get_type (), LocationManagerDialog)
#define LOCATION_MANAGER_DIALOG_CLASS(klass)  GTK_CHECK_CLASS_CAST (klass, location_manager_dialog_get_type (), LocationManagerDialogClass)
#define IS_LOCATION_MANAGER_DIALOG(obj)       GTK_CHECK_TYPE (obj, location_manager_dialog_get_type ())

typedef struct _LocationManagerDialog LocationManagerDialog;
typedef struct _LocationManagerDialogClass LocationManagerDialogClass;
typedef struct _LocationManagerDialogPrivate LocationManagerDialogPrivate;

struct _LocationManagerDialog 
{
	GnomeApp parent;

	LocationManagerDialogPrivate *p;
};

struct _LocationManagerDialogClass 
{
	GnomeAppClass gnome_app_class;
};

guint location_manager_dialog_get_type         (void);

GtkWidget *location_manager_dialog_new         (gboolean is_global);

END_GNOME_DECLS

#endif /* __LOCATION_MANAGER_DIALOG_H */
