/* -*- mode: c; style: linux; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */

/* location-list.c
 * Copyright (C) 2000-2001 Ximian, Inc.
 *
 * Written by Bradford Hovinen <hovinen@ximian.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include <bonobo.h>
#include "location-list.h"

enum {
	ARG_0,
	ARG_ARCHIVE,
	ARG_LOCATION_TREE,
	ARG_LOCATION_MENU
};

enum {
	LOCATION_CHANGED_SIGNAL,
	LAST_SIGNAL
};

struct _LocationListPrivate 
{
	CORBA_char                *selected_location_id;
	ConfigArchiver_Location    selected_location;

	ConfigArchiver_Archive     archive;

	GtkCTree                  *location_tree;
	GtkMenu                   *location_menu;
	GtkOptionMenu             *location_option_menu;
};

static GtkObjectClass *parent_class;

static gint location_list_signals[LAST_SIGNAL] = { 0 };

static void location_list_init        (LocationList *location_list);
static void location_list_class_init  (LocationListClass *class);

static void location_list_set_arg     (GtkObject *object, 
				       GtkArg *arg, 
				       guint arg_id);
static void location_list_get_arg     (GtkObject *object, 
				       GtkArg *arg, 
				       guint arg_id);
static void location_list_destroy     (GtkObject *object);
static void location_list_finalize    (GtkObject *object);

static void tree_select_row_cb        (GtkCTree *tree,
				       GList *node, gint column,
				       LocationList *list);
static void menu_item_select_cb       (GtkMenuItem *item,
				       LocationList *list);

static void populate_tree_iter        (ConfigArchiver_Archive archive,
				       ConfigArchiver_Location location,
				       GtkCTree *tree,
				       GtkCTreeNode *node,
				       CORBA_Environment *ev);
static void populate_tree             (LocationList *list,
				       CORBA_Environment *ev);
static void depopulate_tree_iter      (LocationList *list,
				       GtkCTreeNode *node);
static void depopulate_tree           (LocationList *list);

static void populate_menu_iter        (ConfigArchiver_Archive archive,
				       ConfigArchiver_Location location,
				       LocationList *list,
				       CORBA_Environment *ev,
				       guint *location_num,
				       guint *selected_location_num);
static void populate_menu             (LocationList *list,
				       CORBA_Environment *ev);
static void depopulate_menu           (LocationList *list);

guint
location_list_get_type (void)
{
	static guint location_list_type = 0;

	if (!location_list_type) {
		GtkTypeInfo location_list_info = {
			"LocationList",
			sizeof (LocationList),
			sizeof (LocationListClass),
			(GtkClassInitFunc) location_list_class_init,
			(GtkObjectInitFunc) location_list_init,
			(GtkArgSetFunc) NULL,
			(GtkArgGetFunc) NULL
		};

		location_list_type = 
			gtk_type_unique (gtk_object_get_type (), 
					 &location_list_info);
	}

	return location_list_type;
}

static void
location_list_init (LocationList *location_list)
{
	location_list->p = g_new0 (LocationListPrivate, 1);
}

static void
location_list_class_init (LocationListClass *class) 
{
	GtkObjectClass *object_class;

	gtk_object_add_arg_type ("LocationList::archive",
				 GTK_TYPE_POINTER,
				 GTK_ARG_READWRITE,
				 ARG_ARCHIVE);

	gtk_object_add_arg_type ("LocationList::location-tree",
				 GTK_TYPE_POINTER,
				 GTK_ARG_READWRITE | GTK_ARG_CONSTRUCT_ONLY,
				 ARG_LOCATION_TREE);

	gtk_object_add_arg_type ("LocationList::location-menu",
				 GTK_TYPE_POINTER,
				 GTK_ARG_READWRITE | GTK_ARG_CONSTRUCT_ONLY,
				 ARG_LOCATION_MENU);

	object_class = GTK_OBJECT_CLASS (class);
	object_class->destroy = location_list_destroy;
	object_class->finalize = location_list_finalize;
	object_class->set_arg = location_list_set_arg;
	object_class->get_arg = location_list_get_arg;

	location_list_signals[LOCATION_CHANGED_SIGNAL] =
		gtk_signal_new ("location-changed", GTK_RUN_FIRST,
				object_class->type,
				GTK_SIGNAL_OFFSET (LocationListClass,
						   location_changed),
				gtk_marshal_NONE__POINTER,
				GTK_TYPE_NONE, 1, GTK_TYPE_POINTER);

	gtk_object_class_add_signals (object_class, location_list_signals,
				      LAST_SIGNAL);

	parent_class = GTK_OBJECT_CLASS
		(gtk_type_class (gtk_object_get_type ()));
}

static void
location_list_set_arg (GtkObject *object, GtkArg *arg, guint arg_id) 
{
	LocationList *location_list;
	CORBA_Environment ev;

	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_LOCATION_LIST (object));

	CORBA_exception_init (&ev);

	location_list = LOCATION_LIST (object);

	switch (arg_id) {
	case ARG_ARCHIVE:
		if (GTK_VALUE_POINTER (*arg) == CORBA_OBJECT_NIL) return;

		location_list->p->archive =
			bonobo_object_dup_ref (GTK_VALUE_POINTER (*arg), &ev);

		if (location_list->p->selected_location_id != NULL)
			CORBA_free (location_list->p->selected_location_id);

		location_list->p->selected_location =
			ConfigArchiver_Archive__get_currentLocation (location_list->p->archive, &ev);
		location_list->p->selected_location_id =
			ConfigArchiver_Location__get_id (location_list->p->selected_location, &ev);

		if (location_list->p->location_tree != NULL)
			populate_tree (location_list, &ev);

		if (location_list->p->location_option_menu != NULL)
			populate_menu (location_list, &ev);

		break;

	case ARG_LOCATION_TREE:
		g_return_if_fail (GTK_VALUE_POINTER (*arg) == NULL ||
				  GTK_IS_CTREE (GTK_VALUE_POINTER (*arg)));

		location_list->p->location_tree = GTK_VALUE_POINTER (*arg);

		if (location_list->p->location_tree != NULL) {
			gtk_signal_connect (GTK_OBJECT
					    (location_list->p->location_tree),
					    "tree-select-row",
					    GTK_SIGNAL_FUNC
					    (tree_select_row_cb),
					    location_list);

			if (location_list->p->archive != CORBA_OBJECT_NIL)
				populate_tree (location_list, &ev);
		}

		break;

	case ARG_LOCATION_MENU:
		g_return_if_fail (GTK_VALUE_POINTER (*arg) == NULL ||
				  GTK_IS_OPTION_MENU
				  (GTK_VALUE_POINTER (*arg)));

		location_list->p->location_option_menu =
			GTK_VALUE_POINTER (*arg);

		if (location_list->p->location_option_menu != NULL &&
		    location_list->p->archive != CORBA_OBJECT_NIL)
			populate_menu (location_list, &ev);

		break;

	default:
		g_warning ("Bad argument set");
		break;
	}

	CORBA_exception_free (&ev);
}

static void
location_list_get_arg (GtkObject *object, GtkArg *arg, guint arg_id) 
{
	LocationList *location_list;

	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_LOCATION_LIST (object));

	location_list = LOCATION_LIST (object);

	switch (arg_id) {
	case ARG_ARCHIVE:
		GTK_VALUE_POINTER (*arg) = location_list->p->archive;
		break;

	case ARG_LOCATION_TREE:
		GTK_VALUE_POINTER (*arg) = location_list->p->location_tree;
		break;

	case ARG_LOCATION_MENU:
		GTK_VALUE_POINTER (*arg) = 
			location_list->p->location_option_menu;
		break;

	default:
		g_warning ("Bad argument get");
		break;
	}
}

static void
location_list_destroy (GtkObject *object) 
{
	LocationList *location_list;

	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_LOCATION_LIST (object));

	location_list = LOCATION_LIST (object);

	if (location_list->p->archive != CORBA_OBJECT_NIL)
		bonobo_object_release_unref (location_list->p->archive, NULL);

	if (location_list->p->selected_location_id != NULL)
		CORBA_free (location_list->p->selected_location_id);

	if (location_list->p->location_option_menu != NULL)
		depopulate_menu (location_list);

	if (location_list->p->location_tree != NULL)
		depopulate_tree (location_list);

	if (location_list->p->selected_location != CORBA_OBJECT_NIL)
		bonobo_object_release_unref (location_list->p->selected_location, NULL);

	GTK_OBJECT_CLASS (parent_class)->destroy (object);
}

static void
location_list_finalize (GtkObject *object) 
{
	LocationList *location_list;

	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_LOCATION_LIST (object));

	location_list = LOCATION_LIST (object);

	g_free (location_list->p);

	GTK_OBJECT_CLASS (parent_class)->finalize (object);
}

GtkObject *
location_list_new (ConfigArchiver_Archive archive, GtkWidget *widget) 
{
	GtkOptionMenu *menu = NULL;
	GtkCTree *ctree = NULL;

	if (GTK_IS_OPTION_MENU (widget)) menu = GTK_OPTION_MENU (widget);
	else if (GTK_IS_CTREE (widget)) ctree = GTK_CTREE (widget);

	return gtk_object_new (location_list_get_type (),
			       "archive", archive,
			       "location-tree", ctree,
			       "location-menu", menu,
			       NULL);
}

gchar *
location_list_get_selected_location_id (LocationList *list)
{
	g_return_val_if_fail (list != NULL, NULL);
	g_return_val_if_fail (IS_LOCATION_LIST (list), NULL);

	return list->p->selected_location_id;
}

ConfigArchiver_Location
location_list_get_selected_location (LocationList *list)
{
	g_return_val_if_fail (list != NULL, NULL);
	g_return_val_if_fail (IS_LOCATION_LIST (list), NULL);

	return list->p->selected_location;
}

void
location_list_select (LocationList *list, ConfigArchiver_Location location) 
{
	GList *node;
	GtkObject *current = NULL;
	CORBA_char *locid, *current_id;
	CORBA_Environment ev;

	g_return_if_fail (list != NULL);
	g_return_if_fail (IS_LOCATION_LIST (list));

	CORBA_exception_init (&ev);

	list->p->selected_location = bonobo_object_dup_ref (location, &ev);

	if (list->p->location_menu != NULL) {
		locid = ConfigArchiver_Location__get_id (location, &ev);
		node = GTK_MENU_SHELL (list->p->location_menu)->children;

		while (node != NULL) {
			current = node->data;
			g_assert (GTK_IS_MENU_ITEM (current));
			
			current_id = ConfigArchiver_Location__get_id
				(gtk_object_get_data (current, "location"), &ev);

			if (!strcmp (current_id, locid)) {
				CORBA_free (current_id);
				break;
			} else {
				CORBA_free (current_id);
				current = NULL;
			}

			node = node->next;
		}

		if (current != NULL)
			gtk_menu_item_activate (GTK_MENU_ITEM (current));

		CORBA_free (locid);
	}

	CORBA_exception_free (&ev);
}

void
location_list_reread (LocationList *list)
{
	CORBA_Environment ev;

	g_return_if_fail (list != NULL);
	g_return_if_fail (IS_LOCATION_LIST (list));

	CORBA_exception_init (&ev);

	if (list->p->location_tree != NULL) {
		depopulate_tree (list);
		gtk_clist_freeze (GTK_CLIST (list));
		gtk_clist_clear (GTK_CLIST (list));
		populate_tree (list, &ev);
		gtk_clist_thaw (GTK_CLIST (list));
	}

	if (list->p->location_option_menu != NULL) {
		depopulate_menu (list);
		populate_menu (list, &ev);
	}

	CORBA_exception_free (&ev);
}

static void
tree_select_row_cb (GtkCTree *tree, GList *node, gint column,
		    LocationList *list) 
{
	GtkCTreeRow *row;
	CORBA_Environment ev;

	g_return_if_fail (list != NULL);
	g_return_if_fail (IS_LOCATION_LIST (list));

	CORBA_exception_init (&ev);

	if (list->p->selected_location != CORBA_OBJECT_NIL)
		bonobo_object_release_unref (list->p->selected_location, NULL);

	row = GTK_CTREE_ROW (node);
	list->p->selected_location = bonobo_object_dup_ref (row->row.data, &ev);
	list->p->selected_location_id =
		GTK_CELL_PIXTEXT (row->row.cell[0])->text;

	gtk_signal_emit (GTK_OBJECT (list),
			 location_list_signals[LOCATION_CHANGED_SIGNAL],
			 row->row.data);

	CORBA_exception_free (&ev);
}

static void
menu_item_select_cb (GtkMenuItem *item, LocationList *list) 
{
	ConfigArchiver_Location location;
	CORBA_Environment ev;

	g_return_if_fail (list != NULL);
	g_return_if_fail (IS_LOCATION_LIST (list));

	CORBA_exception_init (&ev);

	location = gtk_object_get_data (GTK_OBJECT (item), "location");

	if (list->p->selected_location_id != NULL)
		CORBA_free (list->p->selected_location_id);

	if (list->p->selected_location != CORBA_OBJECT_NIL)
		bonobo_object_release_unref (list->p->selected_location, NULL);

	list->p->selected_location = bonobo_object_dup_ref (location, &ev);
	list->p->selected_location_id = ConfigArchiver_Location__get_id (location, &ev);

	gtk_signal_emit (GTK_OBJECT (list),
			 location_list_signals[LOCATION_CHANGED_SIGNAL],
			 location);

	CORBA_exception_free (&ev);
}

static void
populate_tree_iter (ConfigArchiver_Archive archive,
		    ConfigArchiver_Location location,
		    GtkCTree *tree,
		    GtkCTreeNode *node,
		    CORBA_Environment *ev)
{
	CORBA_char *label;
	GtkCTreeNode *new_node;
	ConfigArchiver_LocationSeq *children;
	ConfigArchiver_Location child_loc;
	guint i;

	children = ConfigArchiver_Archive_getChildLocations (archive, location, ev);

	for (i = 0; i < children->_length; i++) {
		child_loc = CORBA_Object_duplicate (children->_buffer[i], ev);
		label = ConfigArchiver_Location__get_label (child_loc, ev);

		new_node = gtk_ctree_insert_node (tree, node, NULL,
						  &label, GNOME_PAD_SMALL, NULL,
						  NULL, NULL, NULL, FALSE, TRUE);
		gtk_ctree_node_set_row_data (tree, new_node, child_loc);

		populate_tree_iter (archive, child_loc, tree, new_node, ev);
	}

	CORBA_free (children);
}

static void
populate_tree (LocationList *list, CORBA_Environment *ev) 
{
	populate_tree_iter (list->p->archive, CORBA_OBJECT_NIL,
			    list->p->location_tree, NULL, ev);
}

static void
depopulate_tree_iter (LocationList *list, GtkCTreeNode *node) 
{
	GtkCTreeRow *row;

	for (; node != NULL; node = (GtkCTreeNode *) node->list.next) {
		row = node->list.data;
		bonobo_object_release_unref (row->row.data, NULL);
	}
}

static void
depopulate_tree (LocationList *list) 
{
	depopulate_tree_iter (list, (GtkCTreeNode *) list->p->location_tree->clist.row_list);
}

static void
populate_menu_iter (ConfigArchiver_Archive archive,
		    ConfigArchiver_Location location,
		    LocationList *list,
		    CORBA_Environment *ev,
		    guint *location_num,
		    guint *selected_location_num) 
{
	GtkWidget *item;
	ConfigArchiver_LocationSeq *children;
	ConfigArchiver_Location child_loc;
	CORBA_char *label, *locid;
	guint i;

	children = ConfigArchiver_Archive_getChildLocations (archive, location, ev);

	for (i = 0; i < children->_length; i++) {
		child_loc = CORBA_Object_duplicate (children->_buffer[i], ev);
		label = ConfigArchiver_Location__get_label (child_loc, ev);
		item = gtk_menu_item_new_with_label (label);

		gtk_object_set_data (GTK_OBJECT (item), "location", child_loc);
		gtk_signal_connect (GTK_OBJECT (item), "activate",
				    GTK_SIGNAL_FUNC (menu_item_select_cb),
				    list);
		gtk_widget_show (item);

		gtk_menu_append (list->p->location_menu, item);

		locid = ConfigArchiver_Location__get_id (child_loc, ev);

		if (!strcmp (locid, list->p->selected_location_id))
			*selected_location_num = *location_num;
		else
			(*location_num)++;

		CORBA_free (locid);

		populate_menu_iter (archive, child_loc, list, ev,
				    location_num, selected_location_num);
	}

	CORBA_free (children);
}

static void
populate_menu (LocationList *list, CORBA_Environment *ev) 
{
	guint selected_location_num = 0, location_num = 0;

	list->p->location_menu = GTK_MENU (gtk_menu_new ());

	populate_menu_iter (list->p->archive, CORBA_OBJECT_NIL, list, ev,
			    &location_num, &selected_location_num);

	gtk_option_menu_set_menu (list->p->location_option_menu,
				  GTK_WIDGET (list->p->location_menu));
	gtk_option_menu_set_history (list->p->location_option_menu,
				     selected_location_num);
}

static void
depopulate_menu (LocationList *list) 
{
	GtkMenu *menu;
	GList *node;
	GtkObject *item;
	ConfigArchiver_Location location;

	menu = GTK_MENU (gtk_option_menu_get_menu (list->p->location_option_menu));

	for (node = GTK_MENU_SHELL (menu)->children; node != NULL; node = node->next) {
		item = node->data;
		location = gtk_object_get_data (item, "location");
		bonobo_object_release_unref (location, NULL);
	}
}
