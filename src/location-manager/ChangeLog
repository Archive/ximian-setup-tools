2002-02-11  Seth Nickell  <snickell@stanford.edu>

	reviewed by: <delete if not using a buddy>

	* location-manager.desktop.in.in:

2002-02-10  Chema Celorio  <chema@celorio.com>

	* Port to the GNOME 2.0 platform

2002-02-08  Chema Celorio  <chema@celorio.com>

	* configure.in: 0.12.0 here we go
2001-12-11  Israel Escalante <israel@ximian.com>

	* Release 0.10.0.

2001-12-12  Israel Escalante  <israel@ximian.com>

	* Makefile.am: use ../CommonMakefile and update to latest version

2001-08-31  Hans Petter Jansson  <hpj@ximian.com>

	* location-manager-dialog.c: Remove include that broke build.

	* location-list.c: Remove include that broke build.

	* gui-backend-list.c: Remove include that broke build.

	* location-dialog.c: Kill some whitespace.

2001-08-27  Bradford Hovinen  <hovinen@ximian.com>

	* location-dialog.c (ok_cb): release_unref the existing location
	if it was found

	* location-manager-dialog.c (delete_ok_cb): 
	(new_location_ok_cb): Remove calls to location_list_clear
	(delete_ok_cb): Treat edit list differently than select list
	(delete_ok_cb): Set current location on GUI backend list
	(delete_ok_cb): release_unref new_current

	* location-list.c (location_list_clear): Remove

2001-08-23  Bradford Hovinen  <hovinen@ximian.com>

	* location-list.c (location_list_clear): Release the selected
	location too

	* location-manager-dialog.c (delete_ok_cb): Call clear on each
	list before rereading
	(new_location_ok_cb): Ditto
	(delete_ok_cb): Free the exception structure

	* location-list.c (depopulate_tree_iter): Don't recurse
	(location_list_clear): Implement
	(location_list_reread): Remove calls to depopulate_*

	* location-dialog.c (location_dialog_destroy): Implement

	* location-list.c (location_list_reread): Depopulate tree/menu
	when reread requested

	* location-dialog.c (ok_cb): Check for exception when determining
	whether a location already exists
	(ok_cb): Close the dialog when we are done

2001-08-21  Bradford Hovinen  <hovinen@ximian.com>

	* location-manager-dialog.c (location_manager_dialog_destroy):
	Remove debugging messages

	* location-list.c (location_list_destroy): Remove debugging messages

	* gui-backend-list.c (gui_backend_list_destroy): Remove debugging
	messages

	* location-dialog.c (ok_cb): Remember to unref the location after
	it is created

	* location-list.c (populate_tree_iter):
	(populate_menu_iter): CORBA_Object_duplicate the child locations
	(depopulate_tree):
	(depopulate_tree_iter): Implement
	(tree_select_row_cb):
	(menu_item_select_cb): Unref the old current location
	(populate_menu_iter): Compare location IDs for determining
	selected location number

	* location-manager-dialog.c (location_manager_dialog_set_arg):
	Make sure to release the archive

	* location-list.c (location_list_destroy): Implement
	(location_list_finalize): Rip out most of the code and put into
	destroy

	* gui-backend-list.c (gui_backend_list_destroy): Implement
	(gui_backend_list_finalize): Rip out most of the code and put into
	destroy

2001-08-20  Bradford Hovinen  <hovinen@ximian.com>

	* all: Port to new CORBA-based interface

2001-08-01  Bradford Hovinen  <hovinen@ximian.com>

	* location-list.c (location_list_set_arg): strdup selected
	location id; free old one if it is present
	(menu_item_select_cb): Ditto
	(location_list_select): Declare locid in this scope as a const
	gchar *
	(location_list_select): Ditto
	(location_list_finalize): Free selected_location_id if not already
	done

	* location-dialog.c (ok_cb): Cast the GtkWidget correctly

	* gui-backend-list.c (gui_backend_list_launch_current): Remove
	unused FILE *output

	* main.c: Include xst.h

	* location-dialog.c (location_dialog_set_arg): Use updated macro
	name

	* location-manager-dialog.c (location_manager_dialog_init): Use
	updated macro name

	* Makefile.am: Get in sync with time, et al.

	* main.c (main): Default to global mode; we aren't supporting
	per-user mode now

	* location-manager-dialog.c: Remove xst_fool_the_linker; it is in
	libsetuptool

	* main.c (main): Use xst_init

	* gui-backend-list.c (gui_backend_list_launch_current): Don't dump
	rollback data to the backend when launching
	(gui_backend_list_launch_current): Use more informative warning
	dialog

2001-07-24  Zbigniew Chyla  <cyba@gnome.pl>

	* location-dialog.c (location_dialog_set_arg): Marked strings for
	translation.

2001-07-16  Kai Lahmann  <kl@linuxfaqs.de>

	* Makefile.am: .destop moved a bit

2001-07-12  Bradford Hovinen  <hovinen@ximian.com>

	* gui-backend-list.c (gui_backend_list_launch_current): Use
	backend_id when determining whether backend changes

2001-07-05  Bradford Hovinen  <hovinen@ximian.com>

	* location-manager-dialog.c: Removed #include
	"config-manager-dialog.h"

	* Makefile.am (-DGNOMELOCALEDIR): 
	(location_manager_capplet_LDADD): Use $(XST_TOOL_{CFLAGS|LIBS})

	* *.[ch]: Updated #include locations; removed calls to DEBUG_MSG;
	removed #include "util.h"

2001-06-22  Bradford Hovinen  <hovinen@ximian.com>

	* location-manager.desktop: Make a real desktop entry file

	* location-manager-dialog.c (location_manager_dialog_set_arg): Add
	a check to inform the user if the location manager was unable to
	form an initial configuration snapshot

2001-06-21  Bradford Hovinen  <hovinen@ximian.com>

	* gui-backend-list.c (gui_backend_list_launch_current): Use
	location_does_backend_change when deciding whether to pass
	--location on the command line

2001-06-19  Bradford Hovinen  <hovinen@ximian.com>

	* gui-backend-list.c (gui_backend_list_launch_current): Don't pass
	--location if we are editing in our own location
	(get_backend_id): Move code to convert frontend name to backend
	name into get_backend_id from populate_from_dir

2001-06-18  Bradford Hovinen  <hovinen@ximian.com>

	* Makefile.am (INCLUDES): Use gnome/apps/Settings rather than
	control-center for searching for .desktop files

	* gui-backend-list.c (populate_from_dir): Added debugging code

2001-05-30  Chema Celorio  <chema@celorio.com>

	* Makefile.am (-DGNOMELOCALEDIR): use XIMIAN_ARCHIVER_LIBS
	and XIMIAN_ARCHIVER_INCLUDEDIR

2001-05-24  Arturo Espinosa Aldama  <arturo@ximian.com>

	* 0.5 RELEASE

2001-05-24  Chema Celorio  <chema@celorio.com>

	* Makefile.am (Applicationsdir): added checked/unchecked.xpm
	so that distcheck will show me a little love

2001-05-09  Arturo Espinosa Aldama  <arturo@ximian.com>

	* 0.4 RELEASE

2001-05-03  Bradford Hovinen  <hovinen@ximian.com>

	* location-manager-dialog.c (help_cb): Implement
	(location_manager_dialog_init): Connect help signal

2001-04-22  Bradford Hovinen  <hovinen@ximian.com>

	* location-list.[ch]: Change to inherit GtkObject

	* location-manager-dialog.c (location_manager_dialog_destroy):
	Implement. Close and destroy all aggregated objects.
	(close_cb): Call gtk_object_destroy

2001-04-21  Bradford Hovinen  <hovinen@ximian.com>

	* location-manager-dialog.c (delete_location_cb): Don't let the
	user delete the default location
	(delete_ok_cb): Add some sanity checks to make sure the current
	location is set to something new if we delete it

	* location-list.c (location_list_select): Implement. Selectes the
	given location in the location list

	* location-manager-dialog.c (delete_ok_cb): Use correct semantics
	for callback

2001-04-14  Bradford Hovinen  <hovinen@ximian.com>

	* gui-backend-list.c (gui_backend_list_launch_current): Rewrite to
	use popen

2001-04-13  Bradford Hovinen  <hovinen@ximian.com>

	* gui-backend-list.c (gui_backend_list_launch_current): Send
	rollback data to subprocess
	(gui_backend_list_launch_current): Enable support for --location

2001-04-11  Bradford Hovinen  <hovinen@ximian.com>

	* gui-backend-list.c: Search XST_FRONTEND_LOCATION if we are in a
	global state, to make sure XSTs are found
	(populate_from_dir): If dealing with an XST, replace "-admin" with
	"-conf" to get the backend

	* Makefile.am (INCLUDES): Added XST_FRONTEND_LOCATION

2001-03-08  Bradford Hovinen  <hovinen@ximian.com>

	* gui-backend-list.c (get_backend_id): Implement. Given the name
	of a front end, find the name of its associated backend
	(populate_from_dir): Use output from get_backend_id
	(backend_note_destroy): Free backend_id, since it's not just an
	alias in the dentry any more

