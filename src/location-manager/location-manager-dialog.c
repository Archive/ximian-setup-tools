/* -*- mode: c; style: linux; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */

/* location-manager-dialog.c
 * Copyright (C) 2000 Ximian, Inc.
 *
 * Written by Bradford Hovinen <hovinen@helixcode.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include <bonobo.h>
#include <gnome.h>

#include <glade/glade.h>
#include <config-archiver/archiver-client.h>

#include "location-manager-dialog.h"
#include "location-list.h"
#include "location-dialog.h"
#include "gui-backend-list.h"
#include "xst-ui.h"

#include <gdk-pixbuf/gdk-pixbuf.h>
#include <gdk-pixbuf/gnome-canvas-pixbuf.h>

#define WID(str) glade_xml_get_widget (dialog->p->dialog_data, str)

enum {
	ARG_0,
	ARG_IS_GLOBAL
};

struct _LocationManagerDialogPrivate 
{
	GladeXML                *dialog_data;
	ConfigArchiver_Archive   archive;
	LocationList            *select_list;
	LocationList            *edit_list;
	GuiBackendList          *backend_list;

	gchar                   *selected_backend;
};

static GnomeAppClass *parent_class;



static void new_location_cb                     (GtkMenuItem *item,
						 LocationManagerDialog *dialog);
static void copy_location_cb                    (GtkMenuItem *item,
						 LocationManagerDialog *dialog);
static void rename_location_cb                  (GtkMenuItem *item,
						 LocationManagerDialog *dialog);
static void delete_location_cb                  (GtkMenuItem *item,
						 LocationManagerDialog *dialog);
static void apply_cb                            (GtkMenuItem *item,
						 LocationManagerDialog *dialog);
static void close_cb                            (GtkMenuItem *item,
						 LocationManagerDialog *dialog);

static void edit_backend_cb                     (GtkMenuItem *item,
						 LocationManagerDialog *dialog);
static void select_backend_cb                   (GtkMenuItem *item,
						 LocationManagerDialog *dialog);
static void deselect_backend_cb                 (GtkMenuItem *item,
						 LocationManagerDialog *dialog);

static void help_cb                             (GtkMenuItem *item,
						 LocationManagerDialog *dialog);
static void about_cb                            (GtkMenuItem *item,
						 LocationManagerDialog *dialog);

static void new_location_ok_cb                  (LocationDialog *widget,
						 LocationManagerDialog *dialog);
static void delete_ok_cb                        (gint reply,
						 LocationManagerDialog *dialog);



static GnomeUIInfo location_menu[] = {
	GNOMEUIINFO_MENU_NEW_ITEM (N_("_New..."),
				   N_("Create a new location"),
				   new_location_cb, NULL),
	GNOMEUIINFO_ITEM_NONE (N_("_Copy..."),
	       N_("Create a new location identical to the selected one"),
			       copy_location_cb),
	GNOMEUIINFO_ITEM_NONE (N_("_Rename..."),
			       N_("Rename the selected location"),
			       rename_location_cb),
	GNOMEUIINFO_ITEM_NONE (N_("_Delete..."),
			       N_("Delete the selected location"),
			       delete_location_cb),
	GNOMEUIINFO_SEPARATOR,
	GNOMEUIINFO_ITEM_NONE (N_("_Apply..."),
			       N_("Apply the changes"), apply_cb),
	GNOMEUIINFO_MENU_CLOSE_ITEM (close_cb, NULL),
	GNOMEUIINFO_END
};

static GnomeUIInfo settings_menu[] = {
	GNOMEUIINFO_ITEM_NONE (N_("_Edit..."),
			       N_("Edit settings for the selected backend"),
			       edit_backend_cb),
	GNOMEUIINFO_ITEM_NONE (N_("_Select"),
		       N_("Select the selected backend for this location"),
			       select_backend_cb),
	GNOMEUIINFO_ITEM_NONE (N_("_Deselect"),
		       N_("Deselect the selected backend for this location"),
			       deselect_backend_cb),
	GNOMEUIINFO_END
};

static GnomeUIInfo help_menu[] = {
	GNOMEUIINFO_MENU_ABOUT_ITEM (about_cb, NULL),
	GNOMEUIINFO_END
};

static GnomeUIInfo menubar[] = {
	GNOMEUIINFO_SUBTREE (N_("_Location"), location_menu),
	GNOMEUIINFO_SUBTREE (N_("_Settings"), settings_menu),
	GNOMEUIINFO_SUBTREE (N_("_Help"), help_menu),
	GNOMEUIINFO_END
};



static void location_manager_dialog_init        (LocationManagerDialog *dialog);
static void location_manager_dialog_class_init  (LocationManagerDialogClass *class);

static void location_manager_dialog_set_arg     (GtkObject *object, 
						 GtkArg *arg, 
						 guint arg_id);
static void location_manager_dialog_get_arg     (GtkObject *object, 
						 GtkArg *arg, 
						 guint arg_id);

static void location_manager_dialog_finalize    (GtkObject *object);
static void location_manager_dialog_destroy     (GtkObject *object);

static void edit_location_changed_cb            (LocationList *list,
						 ConfigArchiver_Location location,
						 LocationManagerDialog *dialog);



guint
location_manager_dialog_get_type (void)
{
	static guint location_manager_dialog_type = 0;

	if (!location_manager_dialog_type) {
		GtkTypeInfo location_manager_dialog_info = {
			"LocationManagerDialog",
			sizeof (LocationManagerDialog),
			sizeof (LocationManagerDialogClass),
			(GtkClassInitFunc) location_manager_dialog_class_init,
			(GtkObjectInitFunc) location_manager_dialog_init,
			(GtkArgSetFunc) NULL,
			(GtkArgGetFunc) NULL
		};

		location_manager_dialog_type = 
			gtk_type_unique (gnome_app_get_type (), 
					 &location_manager_dialog_info);
	}

	return location_manager_dialog_type;
}

static void
location_manager_dialog_init (LocationManagerDialog *dialog)
{
	gnome_app_construct (GNOME_APP (dialog),
			     "location-manager",
			     _("Location Manager"));

	dialog->p = g_new0 (LocationManagerDialogPrivate, 1);
	dialog->p->dialog_data =
		glade_xml_new (INTERFACES_DIR "/location-manager.glade",
			       "location_manager");

	gtk_signal_connect (GTK_OBJECT (WID ("settings_button")),
			    "clicked", edit_backend_cb, dialog);
	gtk_signal_connect (GTK_OBJECT (WID ("help_button")),
			    "clicked", help_cb, dialog);

	gnome_app_set_contents (GNOME_APP (dialog), WID ("location_manager"));
	gnome_app_create_menus_with_data (GNOME_APP (dialog), menubar, dialog);

	gtk_signal_connect (GTK_OBJECT (dialog), "destroy",
			    close_cb, dialog);
	gtk_window_set_policy (GTK_WINDOW (dialog), TRUE, TRUE, TRUE);
}

static void
location_manager_dialog_class_init (LocationManagerDialogClass *class) 
{
	GtkObjectClass *object_class;

	gtk_object_add_arg_type ("LocationManagerDialog::is-global",
				 GTK_TYPE_INT,
				 GTK_ARG_READWRITE | GTK_ARG_CONSTRUCT_ONLY,
				 ARG_IS_GLOBAL);

	object_class = GTK_OBJECT_CLASS (class);
	object_class->finalize = location_manager_dialog_finalize;
	object_class->destroy = location_manager_dialog_destroy;
	object_class->set_arg = location_manager_dialog_set_arg;
	object_class->get_arg = location_manager_dialog_get_arg;

	parent_class = GNOME_APP_CLASS
		(gtk_type_class (gnome_app_get_type ()));
}

static void
location_manager_dialog_set_arg (GtkObject *object, GtkArg *arg, guint arg_id) 
{
	LocationManagerDialog *dialog;
	ConfigArchiver_Location current;
	CORBA_Environment ev;

	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_LOCATION_MANAGER_DIALOG (object));

	CORBA_exception_init (&ev);

	dialog = LOCATION_MANAGER_DIALOG (object);

	switch (arg_id) {
	case ARG_IS_GLOBAL:
		if (GTK_VALUE_INT (*arg))
			dialog->p->archive =
				bonobo_get_object ("archive:global-archive", "IDL:ConfigArchiver/Archive:1.0", &ev);
		else
			dialog->p->archive =
				bonobo_get_object ("archive:user-archive", "IDL:ConfigArchiver/Archive:1.0", &ev);

		if (dialog->p->archive == CORBA_OBJECT_NIL) {
			g_critical ("Could not resolve archive moniker");
			break;
		}

		current = ConfigArchiver_Archive__get_currentLocation (dialog->p->archive, &ev);

		if (current == CORBA_OBJECT_NIL) {
			GtkWidget *warn_dialog;
			gchar *msg;

			if (GTK_VALUE_INT (*arg))
				msg = _("Could not retrieve a configuration\n" \
					"snapshot. This probably means you do\n" \
					"not have the Ximian Setup Tools\n" \
					"installed properly.");
			else
				msg = _("Could not retrieve a configuration\n" \
					"snapshot. This probably means you are\n" \
					"running an old version of the GNOME\n" \
					"control center. Please upgrade to\n" \
					"at least version 1.3.2 or CVS HEAD.");

			warn_dialog = gnome_error_dialog (msg);
			gnome_dialog_run_and_close (GNOME_DIALOG (warn_dialog));
			bonobo_object_release_unref (dialog->p->archive, NULL);
			exit (-1);
		}

		dialog->p->select_list =
			LOCATION_LIST
			(location_list_new
			 (dialog->p->archive, WID ("select_list")));

		dialog->p->edit_list =
			LOCATION_LIST
			(location_list_new
			 (dialog->p->archive, WID ("edit_list")));

		gtk_signal_connect (GTK_OBJECT (dialog->p->edit_list),
				    "location-changed",
				    GTK_SIGNAL_FUNC (edit_location_changed_cb),
				    dialog);

		dialog->p->backend_list =
			GUI_BACKEND_LIST 
			(gui_backend_list_new (dialog->p->archive, 
					       current,
					       WID ("backend_list")));

		bonobo_object_release_unref (current, NULL);
		break;

	default:
		g_warning ("Bad argument set");
		break;
	}

	CORBA_exception_free (&ev);
}

static void
location_manager_dialog_get_arg (GtkObject *object, GtkArg *arg, guint arg_id) 
{
	LocationManagerDialog *dialog;
	CORBA_Environment ev;

	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_LOCATION_MANAGER_DIALOG (object));

	CORBA_exception_init (&ev);

	dialog = LOCATION_MANAGER_DIALOG (object);

	switch (arg_id) {
	case ARG_IS_GLOBAL:
		GTK_VALUE_INT (*arg) = ConfigArchiver_Archive__get_isGlobal (dialog->p->archive, &ev);
		break;

	default:
		g_warning ("Bad argument get");
		break;
	}

	CORBA_exception_free (&ev);
}

static void
location_manager_dialog_finalize (GtkObject *object) 
{
	LocationManagerDialog *location_manager_dialog;

	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_LOCATION_MANAGER_DIALOG (object));

	location_manager_dialog = LOCATION_MANAGER_DIALOG (object);

	g_free (location_manager_dialog->p);

	GTK_OBJECT_CLASS (parent_class)->finalize (object);
}

static void
location_manager_dialog_destroy (GtkObject *object) 
{
	LocationManagerDialog *dialog;

	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_LOCATION_MANAGER_DIALOG (object));

	dialog = LOCATION_MANAGER_DIALOG (object);

	gtk_object_destroy (GTK_OBJECT (dialog->p->select_list));
	gtk_object_destroy (GTK_OBJECT (dialog->p->edit_list));
	gtk_object_destroy (GTK_OBJECT (dialog->p->backend_list));
	gtk_object_destroy (GTK_OBJECT (dialog->p->dialog_data));

	bonobo_object_release_unref (dialog->p->archive, NULL);

	GTK_OBJECT_CLASS (parent_class)->destroy (object);
}

GtkWidget *
location_manager_dialog_new (gboolean is_global) 
{
	return gtk_widget_new (location_manager_dialog_get_type (),
			       "is-global", is_global,
			       NULL);
}



static void
new_location_cb (GtkMenuItem *item, LocationManagerDialog *dialog)
{
	GtkWidget *new_dialog;

	new_dialog = location_dialog_new (NEW_DIALOG, dialog->p->archive, NULL);

	gtk_signal_connect (GTK_OBJECT (new_dialog), "ok-clicked",
			    GTK_SIGNAL_FUNC (new_location_ok_cb), dialog);
	gtk_widget_show (new_dialog);
}

static void
copy_location_cb (GtkMenuItem *item, LocationManagerDialog *dialog)
{
	GtkWidget *copy_dialog;

	copy_dialog = location_dialog_new
		(COPY_DIALOG, dialog->p->archive,
		 location_list_get_selected_location (dialog->p->edit_list));

	gtk_signal_connect (GTK_OBJECT (copy_dialog), "ok-clicked",
			    GTK_SIGNAL_FUNC (new_location_ok_cb), dialog);
	gtk_widget_show (copy_dialog);
}

static void
rename_location_cb (GtkMenuItem *item, LocationManagerDialog *dialog)
{
	GtkWidget *rename_dialog;

	rename_dialog = location_dialog_new
		(RENAME_DIALOG, dialog->p->archive,
		 location_list_get_selected_location (dialog->p->edit_list));

	gtk_signal_connect (GTK_OBJECT (rename_dialog), "ok-clicked",
			    GTK_SIGNAL_FUNC (new_location_ok_cb), dialog);
	gtk_widget_show (rename_dialog);
}

static void
delete_location_cb (GtkMenuItem *item, LocationManagerDialog *dialog)
{
	GtkWidget *confirm;
	gchar *message;
	gchar *location_id;

	location_id = location_list_get_selected_location_id
		(dialog->p->edit_list);

	if (!strcmp (location_id, "default")) {
		gnome_error_dialog
			(_("You cannot delete the default location"));
		return;
	}

	message = g_strdup_printf
		(_("Are you sure you wish to destroy the location %s?"),
		 location_id);

	confirm = gnome_question_dialog_modal
		(message, (GnomeReplyCallback) delete_ok_cb, dialog);
	gtk_widget_show (confirm);
}

static void
apply_cb (GtkMenuItem *item, LocationManagerDialog *dialog)
{
	CORBA_Environment ev;

	CORBA_exception_init (&ev);

	ConfigArchiver_Archive__set_currentLocation
		(dialog->p->archive,
		 location_list_get_selected_location (dialog->p->select_list), &ev);

	CORBA_exception_free (&ev);
}

static void
close_cb (GtkMenuItem *item, LocationManagerDialog *dialog)
{
	gtk_object_destroy (GTK_OBJECT (dialog));
	gtk_main_quit ();
}

static void
edit_backend_cb (GtkMenuItem *item, LocationManagerDialog *dialog)
{
	gui_backend_list_launch_current (dialog->p->backend_list);
}

static void
select_backend_cb (GtkMenuItem *item, LocationManagerDialog *dialog)
{
	gui_backend_list_select_current (dialog->p->backend_list);
}

static void
deselect_backend_cb (GtkMenuItem *item, LocationManagerDialog *dialog)
{
	gui_backend_list_unselect_current (dialog->p->backend_list);
}

static void
help_cb (GtkMenuItem *item, LocationManagerDialog *dialog) 
{
	GnomeHelpMenuEntry help_entry = {
		"location-manager-capplet", "index.html"
	};

	gnome_help_display (NULL, &help_entry);
}

static void
about_cb (GtkMenuItem *item, LocationManagerDialog *dialog)
{
	static GtkWidget *about;
	static const gchar *authors[] = {
		"Bradford Hovinen",
		NULL
	};

	if (about == NULL) {
		about = gnome_about_new
			(_("Location Manager"), VERSION,
			 "(C) 2001 Ximian, Inc.",
			 authors,
			 _("Ximian Setup Tools Location Manager"),
			 NULL);
	}

	gtk_widget_show (about);
}

static void
new_location_ok_cb (LocationDialog *widget, LocationManagerDialog *dialog)
{
	location_list_reread (dialog->p->select_list);
	location_list_reread (dialog->p->edit_list);
}

static void
delete_ok_cb (gint reply, LocationManagerDialog *dialog)
{
	ConfigArchiver_Location location, current, new_current, sel;
	CORBA_Environment ev;

	CORBA_exception_init (&ev);

	if (reply == 0) {
		location = location_list_get_selected_location (dialog->p->edit_list);
		current = ConfigArchiver_Archive__get_currentLocation (dialog->p->archive, &ev);
		sel = location_list_get_selected_location (dialog->p->edit_list);

		if (location == current || location == sel) {
			new_current = ConfigArchiver_Location__get_parent (location, &ev);

			if (new_current == CORBA_OBJECT_NIL)
				new_current = ConfigArchiver_Archive_getLocation
					(dialog->p->archive, "default", &ev);

			g_assert (new_current != CORBA_OBJECT_NIL);
			g_assert (new_current != location);

			if (location == current) {
				ConfigArchiver_Archive__set_currentLocation
					(dialog->p->archive, new_current, &ev);
				location_list_select (dialog->p->select_list, new_current);
			} 

			if (location == sel) {
				location_list_select (dialog->p->edit_list, new_current);
				gtk_object_set (GTK_OBJECT (dialog->p->backend_list),
						"current-location", new_current, NULL);
			}

			bonobo_object_release_unref (new_current, NULL);
		}

		bonobo_object_release_unref (current, NULL);

		ConfigArchiver_Location_delete (location, &ev);
		location_list_reread (dialog->p->select_list);
		location_list_reread (dialog->p->edit_list);
	}

	CORBA_exception_free (&ev);
}

static void
edit_location_changed_cb (LocationList *list, ConfigArchiver_Location location,
			  LocationManagerDialog *dialog) 
{
	gtk_object_set (GTK_OBJECT (dialog->p->backend_list),
			"current-location", location, NULL);
}
