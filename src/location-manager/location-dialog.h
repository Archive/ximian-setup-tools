/* -*- mode: c; style: linux -*- */

/* location-dialog.h
 * Copyright (C) 2000 Ximian, Inc.
 *
 * Written by Bradford Hovinen <hovinen@helixcode.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */

#ifndef __LOCATION_DIALOG_H
#define __LOCATION_DIALOG_H

#include <gnome.h>

#include <config-archiver/archiver-client.h>

#include "location-list.h"

BEGIN_GNOME_DECLS

#define LOCATION_DIALOG(obj)          GTK_CHECK_CAST (obj, location_dialog_get_type (), LocationDialog)
#define LOCATION_DIALOG_CLASS(klass)  GTK_CHECK_CLASS_CAST (klass, location_dialog_get_type (), LocationDialogClass)
#define IS_LOCATION_DIALOG(obj)       GTK_CHECK_TYPE (obj, location_dialog_get_type ())

typedef struct _LocationDialog LocationDialog;
typedef struct _LocationDialogClass LocationDialogClass;
typedef struct _LocationDialogPrivate LocationDialogPrivate;

typedef enum _LocationDialogType LocationDialogType;

struct _LocationDialog 
{
	GnomeDialog parent;

	LocationDialogPrivate *p;
};

struct _LocationDialogClass 
{
	GnomeDialogClass gnome_dialog_class;

	void (*ok_clicked) (LocationDialog *);
};

enum _LocationDialogType {
	NEW_DIALOG, COPY_DIALOG, RENAME_DIALOG
};

guint location_dialog_get_type         (void);

GtkWidget *location_dialog_new         (LocationDialogType type,
					ConfigArchiver_Archive archive,
					ConfigArchiver_Location location);

END_GNOME_DECLS

#endif /* __LOCATION_DIALOG_H */
