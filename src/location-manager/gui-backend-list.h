/* -*- mode: c; style: linux -*- */

/* gui-backend-list.h
 * Copyright (C) 2000 Ximian, Inc.
 *
 * Written by Bradford Hovinen <hovinen@helixcode.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */

#ifndef __GUI_BACKEND_LIST_H
#define __GUI_BACKEND_LIST_H

#include <gnome.h>

#include <config-archiver/archiver-client.h>

BEGIN_GNOME_DECLS

#define GUI_BACKEND_LIST(obj)          GTK_CHECK_CAST (obj, gui_backend_list_get_type (), GuiBackendList)
#define GUI_BACKEND_LIST_CLASS(klass)  GTK_CHECK_CLASS_CAST (klass, gui_backend_list_get_type (), GuiBackendListClass)
#define IS_GUI_BACKEND_LIST(obj)       GTK_CHECK_TYPE (obj, gui_backend_list_get_type ())

typedef struct _GuiBackendList GuiBackendList;
typedef struct _GuiBackendListClass GuiBackendListClass;
typedef struct _GuiBackendListPrivate GuiBackendListPrivate;

struct _GuiBackendList 
{
	GtkObject parent;

	GuiBackendListPrivate *p;
};

struct _GuiBackendListClass 
{
	GtkObjectClass gtk_object_class;
};

guint gui_backend_list_get_type         (void);

GtkObject *gui_backend_list_new         (ConfigArchiver_Archive archive,
					 ConfigArchiver_Location current,
					 GtkWidget *widget);

void gui_backend_list_launch_current    (GuiBackendList *list);
void gui_backend_list_select_current    (GuiBackendList *list);
void gui_backend_list_unselect_current  (GuiBackendList *list);

END_GNOME_DECLS

#endif /* __GUI_BACKEND_LIST_H */
