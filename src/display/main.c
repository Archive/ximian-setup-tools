/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/* main.c: this file is part of x-admin, a ximian-setup-tool frontend 
 * for user administration.
 * 
 * Copyright (C) 2000-2001 Ximian, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Tambet Ingo <tambet@ximian.com>.
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <gnome.h>
#include <glade/glade.h>

#include <time.h>
#include <stdlib.h>

#include "xst.h"
#include "xst-report-hook.h"
#include "transfer.h"
#include "callbacks.h"

#include "vidtune.h"

XstTool *tool;

static XstDialogSignal signals[] = {
	{ "move_up",         "clicked",    G_CALLBACK (move_up_clicked) },
	{ "move_down",       "clicked",    G_CALLBACK (move_down_clicked) },
	{ "move_right",      "clicked",    G_CALLBACK (move_right_clicked) },
	{ "move_left",       "clicked",    G_CALLBACK (move_left_clicked) },
	{ "move_shorter",    "clicked",    G_CALLBACK (move_shorter_clicked) },
	{ "move_narrower",   "clicked",    G_CALLBACK (move_narrower_clicked) },
	{ "move_wider",      "clicked",    G_CALLBACK (move_wider_clicked) },
	{ "move_taller",     "clicked",    G_CALLBACK (move_taller_clicked) },
	{ "restore",         "clicked",    G_CALLBACK (restore_clicked) },
	{ "appereance_test", "clicked",    G_CALLBACK (appereance_test_clicked) },
	{ "probe",           "clicked",    G_CALLBACK (probe_clicked) },

	{ NULL }
};

static const XstWidgetPolicy policies[] = {
	/* Name                     Basic                        Advanced               Root   User */
	{ "move_up",            XST_WIDGET_MODE_SENSITIVE,   XST_WIDGET_MODE_SENSITIVE, TRUE,  TRUE },
	{ "move_down",          XST_WIDGET_MODE_SENSITIVE,   XST_WIDGET_MODE_SENSITIVE, TRUE,  TRUE },
	{ "move_right",         XST_WIDGET_MODE_SENSITIVE,   XST_WIDGET_MODE_SENSITIVE, TRUE,  TRUE },
	{ "move_left",          XST_WIDGET_MODE_SENSITIVE,   XST_WIDGET_MODE_SENSITIVE, TRUE,  TRUE },
	{ "probe",              XST_WIDGET_MODE_HIDDEN,      XST_WIDGET_MODE_SENSITIVE, TRUE,  TRUE },
	{ "appereance_test",    XST_WIDGET_MODE_SENSITIVE,   XST_WIDGET_MODE_SENSITIVE, TRUE,  TRUE },
	{ NULL }
};

static XstReportHookEntry report_hooks[] = {
	{ "x_probe_needed", probe_need_cb,   XST_REPORT_HOOK_LOAD, TRUE,  NULL },
	{ "x_probe_error",  probe_error_cb,  XST_REPORT_HOOK_LOAD, TRUE,  NULL },
	{ NULL, NULL, -1, FALSE, NULL }
};

static void
connect_signals (void)
{
	xst_dialog_connect_signals (tool->main_dialog, signals);

	g_signal_connect (G_OBJECT (tool->main_dialog), "apply",
			  G_CALLBACK (dialog_applied), NULL);

	xst_tool_set_close_func (tool, close_cb, NULL);
}

static void
set_monitor_info (void)
{
	GtkWidget *w;
	gchar *buf;

	w = xst_dialog_get_widget (tool->main_dialog, "monitor_vendor");
	buf = video_monitor_get_vendor ();
	gtk_label_set_text (GTK_LABEL (w), buf);
	g_free (buf);

	w = xst_dialog_get_widget (tool->main_dialog, "monitor_model");
	buf = video_monitor_get_model ();
	gtk_label_set_text (GTK_LABEL (w), buf);
	g_free (buf);
}

int
main (int argc, char *argv[])
{
	xst_init ("display-admin", argc, argv, NULL);
	tool = xst_tool_new ();
	xst_tool_construct (tool, "display", _("X Display Configuration"));

	xst_tool_set_xml_funcs  (tool, transfer_xml_to_gui, transfer_gui_to_xml, NULL);
	xst_tool_add_report_hooks (tool, report_hooks);

	connect_signals ();

	xst_dialog_enable_complexity (tool->main_dialog);
	xst_dialog_set_widget_policies (tool->main_dialog, policies);
	set_monitor_info (); /* Testing purposes only */
	
	xst_tool_main (tool, FALSE);

	return 0;
}
