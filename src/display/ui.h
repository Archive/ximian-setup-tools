/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/* 
 * Copyright (C) 2000-2001 Ximian, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Tambet Ingo <tambet@ximian.com>.
 */

#ifndef __UI_H
#define __UI_H

#include "xst.h"

void        ui_depth_add        (XstTool *tool, const gchar *depth);
void        ui_depth_select     (XstTool *tool, const gchar *depth);
gchar      *ui_depth_selected   (XstTool *tool);
void        ui_depth_clear      (XstTool *tool);

void        ui_res_add          (XstTool *tool, const gchar *res);
void        ui_res_select       (XstTool *tool, const gchar *res);
gchar      *ui_res_selected     (XstTool *tool);
void        ui_res_clear        (XstTool *tool);

#endif /* UI_H */

