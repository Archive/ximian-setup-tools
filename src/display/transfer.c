/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/* transfer.c: this file is part of users-admin, a ximian-setup-tool frontend 
 * for user administration.
 * 
 * Copyright (C) 2000-2001 Ximian, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Tambet Ingo <tambet@ximian.com>.
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <ctype.h>
#include <gnome.h>

#include "xst.h"
#include "transfer.h"
#include "ui.h"
#include "callbacks.h"
#include "vidtune.h"

GHashTable *display_data;

static gboolean set_active_display (xmlNodePtr root, const gchar *depth);

static xmlNodePtr
get_active_serverlayout (xmlNodePtr root)
{
	xmlNodePtr node;
	
	g_return_val_if_fail (root != NULL, NULL);

	node = xst_xml_element_find_first (root, "ServerLayout");
	if (node == NULL) {
		g_warning ("Can't find default ServerLayout node.");
		return NULL;
	}
	
	return node;
}

static xmlNodePtr
get_active_screen (xmlNodePtr root)
{
	xmlNodePtr node, server_layout;
	gchar *screen_name, *buf;

	g_return_val_if_fail (root != NULL, NULL);

	server_layout = get_active_serverlayout (root);
	if (server_layout == NULL)
		return NULL;

	screen_name = NULL;
	node = xst_xml_element_find_first (server_layout, "Screen");
	while (node) {
		buf = xst_xml_element_get_attribute (node, "number");
		if (buf && !strcmp (buf, "0")) {
			g_free (buf);
			screen_name = xst_xml_element_get_attribute (node, "name");
			break;
		}

		if (buf) g_free (buf);
		node = xst_xml_element_find_first (node, "Screen");
	}

	if (screen_name == NULL) {
		g_warning ("Can't find default Screen name.");
		return NULL;
	}

	node = xst_xml_element_find_first (root, "Screen");
	while (node) {
		buf = xst_xml_get_child_content (node, "Identifier");
		if (buf == NULL)
			g_warning ("Screen node without 'Identifier'.");
		else {
			if ((strcmp (screen_name, buf)) == 0) {
				g_free (buf);
				break;
			}
			g_free (buf);
		}

		node = xst_xml_element_find_next (node, "Screen");
	}

	if (node == NULL)
		g_warning ("Can't find default Screen '%s'.", screen_name);

	g_free (screen_name);	
	return node;
}

static xmlNodePtr
get_active_monitor (xmlNodePtr root)
{
	xmlNodePtr screen, node;
	gchar *monitor_name, *buf;

	g_return_val_if_fail (root != NULL, NULL);

	screen = get_active_screen (root);
	if (screen == NULL)
		return NULL;

	monitor_name = xst_xml_get_child_content (screen, "Monitor");
	if (monitor_name == NULL) {
		g_warning ("Can't find active monitor.");
		return NULL;
	}

	node = xst_xml_element_find_first (root, "Monitor");
	while (node) {
		buf = xst_xml_get_child_content (node, "Identifier");
		if (buf == NULL)
			g_warning ("Monitor node without 'Identifier'.");
		else {
			if ((strcmp (monitor_name, buf)) == 0) {
				g_free (buf);
				break;
			}
			g_free (buf);
		}
		node = xst_xml_element_find_next (node, "Monitor");
	}

	if (node == NULL)
		g_warning ("Can't find default Monitor '%s'.", monitor_name);

	g_free (monitor_name);	
	return node;	
}

static xmlNodePtr
get_active_device (xmlNodePtr root)
{
	xmlNodePtr screen, node;
	gchar *device_name, *buf;

	g_return_val_if_fail (root != NULL, NULL);

	screen = get_active_screen (root);
	if (screen == NULL)
		return NULL;

	device_name = xst_xml_get_child_content (screen, "Device");
	if (device_name == NULL) {
		g_warning ("Can't find active device.");
		return NULL;
	}

	node = xst_xml_element_find_first (root, "Device");
	while (node) {
		buf = xst_xml_get_child_content (node, "Identifier");
		if (buf == NULL)
			g_warning ("Device node without 'Identifier'.");
		else {
			if ((strcmp (device_name, buf)) == 0) {
				g_free (buf);
				break;
			}
			g_free (buf);
		}
		node = xst_xml_element_find_next (node, "Device");
	}

	if (node == NULL)
		g_warning ("Can't find default Device '%s'.", device_name);

	g_free (device_name);
	return node;
}

static gchar *
get_active_colordepth (xmlNodePtr root)
{
	xmlNodePtr screen;
	gchar *depth;
	
	g_return_val_if_fail (root != NULL, NULL);

	screen = get_active_screen (root);
	if (screen == NULL)
		return NULL;

	depth = xst_xml_get_child_content (screen, "DefaultDepth");
	if (depth == NULL)
		/* TODO: get it from running X */
		g_warning ("Can't find default color depth.");

	return depth;
}

static gboolean
set_active_colordepth (xmlNodePtr root, const gchar *depth)
{
	xmlNodePtr screen;
	
	g_return_val_if_fail (root != NULL, FALSE);
	g_return_val_if_fail (depth != NULL, FALSE);

	screen = get_active_screen (root);
	if (screen == NULL)
		return FALSE;

	xst_xml_set_child_content (screen, "DefaultDepth", depth);
	set_active_display (root, depth);

	return TRUE;
}

static xmlNodePtr
get_active_display (xmlNodePtr root)
{
	xmlNodePtr screen, node;
	gchar *default_depth;

	g_return_val_if_fail (root != NULL, NULL);

	screen = get_active_screen (root);
	if (screen == NULL)
		return NULL;

	default_depth = get_active_colordepth (root);
	if (default_depth == NULL)
		return NULL;

	node = xst_xml_element_find_first (screen, "Display");
	while (node) {
		gchar *buf;

		buf = xst_xml_get_child_content (node, "Depth");
		if (buf == NULL)
			g_warning ("Display node without identifier Depth.");
		else {
			if ((strcmp (default_depth, buf)) == 0) {
				g_free (buf);
				break;
			}
			g_free (buf);
		}
		node = xst_xml_element_find_next (node, "Display");
	}

	if (node == NULL)
		g_warning ("Can't find default Display (depth '%s').", default_depth);

	g_free (default_depth);
	
	return node;
}

static gboolean
set_active_display (xmlNodePtr root, const gchar *depth)
{
	xmlNodePtr screen, node;

	g_return_val_if_fail (root != NULL, FALSE);
	g_return_val_if_fail (depth != NULL, FALSE);
	
	screen = get_active_screen (root);
	if (screen == NULL)
		return FALSE;

	node = xst_xml_element_find_first (screen, "Display");
	while (node) {
		gchar *buf;

		buf = xst_xml_get_child_content (node, "Depth");
		if (buf == NULL)
			g_warning ("Display node without identifier Depth.");
		else {
			if ((strcmp (depth, buf)) == 0) {
				g_free (buf);
				break;
			}
			g_free (buf);
		}
		node = xst_xml_element_find_next (node, "Display");
	}

	if (node == NULL) {
		node = xst_xml_element_add (screen, "Display");
		xst_xml_set_child_content (node, "Depth", depth);
	}
	return TRUE;
}

static gchar *
get_active_resolution (xmlNodePtr root)
{
	xmlNodePtr display, node;
	gchar *buf;
	
	g_return_val_if_fail (root != NULL, NULL);

	display = get_active_display (root);
	if (display == NULL)
		return NULL;

	/* TODO: Get from running X */
	node = xst_xml_element_find_first (display, "Modes");
	if (node == NULL) {
		g_warning ("Can't find current resolution (missing 'Modes' node).");
		return NULL;
	}

	buf = xst_xml_element_get_attribute (node, "arg1");
	if (buf == NULL)
		g_warning ("Can't find current resoultion (node 'Modes' is empty).");

	return buf;
}

static gboolean
set_active_resolution (xmlNodePtr root, const gchar *res)
{
	xmlNodePtr  display, node;
	gchar      *resolution, *buf, *s;
	GSList     *list, *tmp_list;
	gint        i;

	g_return_val_if_fail (root != NULL, FALSE);
	g_return_val_if_fail (res != NULL, FALSE);

	display = get_active_display (root);
	if (display == NULL)
		return FALSE;

	node = xst_xml_element_find_first (display, "Modes");
	if (node == NULL) {
		node = xst_xml_element_add (display, "Modes");
		xst_xml_element_set_attribute (node, "arg1", res);
		return TRUE;
	}

	resolution = g_strdup (res);
	list = NULL;
	
	list = g_slist_append (list, resolution);
	i = 1;
	while (TRUE) {
		s = g_strdup_printf ("arg%d", i++);
		buf = xst_xml_element_get_attribute (node, s);
		if (buf && (strcmp (buf, res) != 0))
			list = g_slist_append (list, buf);
		g_free (s);
		if (buf == NULL)
			break;
	}

	tmp_list = list;
	i = 1;
	while (tmp_list) {
		s = g_strdup_printf ("arg%d", i++);
		buf = tmp_list->data;
		tmp_list = tmp_list->next;

		xst_xml_element_set_attribute (node, s, buf);
		g_free (buf);
		g_free (s);
	}

	g_slist_free (list);

	return TRUE;
}

typedef struct {
	gchar *res;
	guint  refresh;
} XstDisplayModeKey;

static gint
mode_key_equal (gconstpointer a, gconstpointer b)
{
	const XstDisplayModeKey *key1 = a;
	const XstDisplayModeKey *key2 = b;

	if (key1->refresh != key2->refresh)
		return FALSE;

	if ((strcmp (key1->res, key2->res)) != 0)
		return FALSE;
	
	return TRUE;
}

static XstDisplayModeKey *
display_mode_key_new (xmlNodePtr node)
{
	XstDisplayModeKey *key;
	xmlNodePtr         n;
	gchar             *res, *x, *y;
	guint              refresh;

	g_return_val_if_fail (node != NULL, NULL);

	/* hdisp */
	n = xst_xml_element_find_first (node, "hdisp");
	if (n == NULL) {
		g_warning ("display_mode_key_new: Can't find <hdisp>.");
		return NULL;
	}
	x = xst_xml_element_get_content (n);

	/* vdisp */
	n = xst_xml_element_find_first (node, "vdisp");
	if (n == NULL) {
		g_warning ("display_mode_key_new: Can't find <vdisp>.");
		return NULL;
	}
	y = xst_xml_element_get_content (n);

	res = g_strdup_printf ("%sx%s", x, y);
	g_free (x);
	g_free (y);

	refresh = 0; /* TODO: calculate refresh */
	
	key = g_new0 (XstDisplayModeKey, 1);
	key->res = res;
	key->refresh = refresh;

	return key;
}

static GHashTable *
get_probed_modelines (xmlNodePtr root)
{
	GHashTable *hash;
	xmlNodePtr  node;

	g_return_val_if_fail (root != NULL, NULL);

	node = xst_xml_element_find_first (root, "ModeLine");
	if (node == NULL)
		return NULL;

	hash = g_hash_table_new (g_direct_hash, mode_key_equal);

	for (node = xst_xml_element_find_first (node, "Mode");
	     node != NULL;
	     node = xst_xml_element_find_next (node, "Mode")) {
		XstDisplayModeKey *key;

		key = display_mode_key_new (node);
		if (key)
			g_hash_table_insert (hash, key, node);
	}

	return hash;
}

static gboolean
display_data_res_free (gpointer key, gpointer val, gpointer data)
{
	g_free (key);
	g_free (val);
	return TRUE;
}

static gboolean
display_data_free (gpointer key, gpointer val, gpointer data)
{
	GHashTable *res = val;

	g_hash_table_foreach_remove (res, display_data_res_free, NULL);
	g_hash_table_destroy (res);
	g_free (key);

	return TRUE;
}

void
probe_x (XstTool *tool, gboolean cached)
{
	xmlDocPtr   xml;
	xmlNodePtr  root, depth_node;
	gchar       arg[2];

	g_return_if_fail (tool != NULL);
	g_return_if_fail (XST_IS_TOOL (tool));

	if (display_data == NULL)
		display_data = g_hash_table_new (g_str_hash, g_str_equal);
	else
		g_hash_table_foreach_remove (display_data, display_data_free, NULL);

	if (cached)
		strcpy (arg, "0");
	else
		strcpy (arg, "1");

	xml = xst_tool_run_get_directive (tool, _("Probing X server."), "probe", arg, NULL);
	if (xml == NULL)
		return;

	root = xst_xml_doc_get_root (xml);
	if (root == NULL)
		return;

	for (depth_node = xst_xml_element_find_first (root, "Depth");
	     depth_node != NULL;
	     depth_node = xst_xml_element_find_next (depth_node, "Depth")) {
		gchar *depth = xst_xml_element_get_attribute (depth_node, "value");
		if (depth != NULL) {
			GHashTable *res_table;

			res_table = get_probed_modelines (depth_node);
			if (res_table != NULL)
				g_hash_table_insert (display_data, depth, res_table);
			else
				g_free (depth);
		}
	}
}

static void
depth_add (gpointer key, gpointer value, gpointer data)
{
	XstTool *tool;
	gchar   *depth;

	if (data && XST_IS_TOOL (data))
		tool = XST_TOOL (data);
	else
		return;

	depth = (gchar *)key;
	ui_depth_add (tool, depth);
}

static void
resolution_add (gpointer key, gpointer value, gpointer data)
{
	XstTool           *tool;
	XstDisplayModeKey *modekey;

	if (data && XST_IS_TOOL (data))
		tool = XST_TOOL (data);
	else
		return;

	modekey = (XstDisplayModeKey *)key;
	ui_res_add (tool, modekey->res);
}

void
populate_resolutions (XstTool *tool, const gchar *depth)
{
	GHashTable *hash;

	g_return_if_fail (tool != NULL);
	g_return_if_fail (XST_IS_TOOL (tool));
	g_return_if_fail (depth != NULL);

	ui_res_clear (tool);
	hash = (gpointer) g_hash_table_lookup (display_data, (gconstpointer)depth);
	if (hash)
		g_hash_table_foreach (hash, resolution_add, (gpointer) tool);
}

static void
depth_gui_to_xml (XstTool *tool, xmlNodePtr root)
{
	gchar *depth;

	g_return_if_fail (tool != NULL);
	g_return_if_fail (XST_IS_TOOL (tool));
	g_return_if_fail (root != NULL);

	depth = ui_depth_selected (tool);
	if (depth)
		set_active_colordepth (root, depth);
}

static void
res_gui_to_xml (XstTool *tool, xmlNodePtr root)
{
	gchar *res;

	g_return_if_fail (tool != NULL);
	g_return_if_fail (XST_IS_TOOL (tool));
	g_return_if_fail (root != NULL);

	res = ui_res_selected (tool);
	if (res)
		set_active_resolution (root, res);
}

void
transfer_xml_to_gui (XstTool *tool, gpointer data)
{
	xmlNodePtr root = xst_xml_doc_get_root (tool->config);

	if (root) {
		gchar *buf;

		if (display_data == NULL)
			probe_x (tool, TRUE);

		ui_depth_clear (tool);
		g_hash_table_foreach (display_data, depth_add, (gpointer) tool);
		buf = get_active_colordepth (root);
		if (buf != NULL) {
			ui_depth_select (tool, buf);
			populate_resolutions (tool, buf);
			g_free (buf);
		}

		buf = video_resolution_get_current ();
		/* buf = get_active_resolution (root); */
		if (buf != NULL) {
			ui_res_select (tool, buf);
			g_free (buf);
		}
	} else {
		g_warning ("transfer_xml_to_gui: Can't find root xml node.");
	}
}

void
transfer_gui_to_xml (XstTool *tool, gpointer data)
{
	xmlNodePtr root = xst_xml_doc_get_root (tool->config);

	if (root) {
		depth_gui_to_xml (tool, root);
		res_gui_to_xml   (tool, root);
	} else {
		g_warning ("transfer_gui_to_xml: Can't find root xml node.");
	}
}
