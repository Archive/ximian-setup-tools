/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/* callbacks.h: this file is part of users-admin, a ximian-setup-tool frontend 
 * for user administration.
 * 
 * Copyright (C) 2000-2001 Ximian, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Tambet Ingo <tambet@ximian.com>.
 */

#ifndef __CALLBACKS_H
#define __CALLBACKS_H

#include <gnome.h>
#include <gnome-xml/tree.h>

void     move_up_clicked         (GtkButton *button, gpointer data);
void     move_down_clicked       (GtkButton *button, gpointer data);
void     move_right_clicked      (GtkButton *button, gpointer data);
void     move_left_clicked       (GtkButton *button, gpointer data);
void     move_shorter_clicked    (GtkButton *button, gpointer data);
void     move_narrower_clicked   (GtkButton *button, gpointer data);
void     move_wider_clicked      (GtkButton *button, gpointer data);
void     move_taller_clicked     (GtkButton *button, gpointer data);

void     resolution_changed      (GtkEditable *editable, gpointer data);
void     restore_clicked         (GtkButton *button, gpointer data);

void     dialog_applied          (XstDialog *xd, gpointer data);
void     close_cb                (XstTool *tool, gpointer data);

void     appereance_test_clicked (GtkButton *button, gpointer data);
gboolean probe_need_cb           (XstTool *tool, XstReportLine *rline, gpointer data);
void     probe_clicked           (GtkButton *button, gpointer data);
gboolean probe_error_cb          (XstTool *tool, XstReportLine *rline, gpointer data);

#endif /* CALLBACKS_H */

