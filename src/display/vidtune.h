/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/* callbacks.h: this file is part of users-admin, a ximian-setup-tool frontend 
 * for user administration.
 * 
 * Copyright (C) 2000-2001 Ximian, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Tambet Ingo <tambet@ximian.com>.
 */

#ifndef __VIDTUNE_H
#define __VIDTUNE_H

typedef enum {
	DIR_NONE,

	DIR_LEFT,
	DIR_RIGHT,
	DIR_UP,
	DIR_DOWN,
	DIR_WIDER,
	DIR_NARROWER,
	DIR_SHORTER,
	DIR_TALLER
} XstVideoDirection;

gchar    *video_monitor_get_vendor       (void);
gchar    *video_monitor_get_model        (void);

gboolean  video_screen_move              (XstVideoDirection dir);
GList    *video_screen_resolution_list   (void);
gchar    *video_resolution_get_current   (void);
gboolean  video_resolution_set           (const gchar *reso);
void      video_screen_restore           (void);

#endif /* VIDTUNE_H */
