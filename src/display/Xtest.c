#ifdef HAVE_CONFIG_H
#	include <config.h>
#endif

#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <signal.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <libintl.h>
#include <sys/wait.h>
#include <popt.h>
#include <locale.h>

#include <gtk/gtk.h>
#include <gdk/gdkx.h>
#include <gdk-pixbuf/gdk-pixbuf.h>

static gboolean works;

#define DESKTOP_BG_COLOR  0x4A55AC
#define TEMP_FILE         "/tmp/xst_testX"

static void
destroy(GtkWidget *widget, gpointer data)
{
	gtk_main_quit ();
}

struct TimeoutData {
	GtkWidget *label;
	GtkWidget *window;
	gint time;
	gint timeout_id;
};

static void
yes_callback (GtkWidget *widget, gpointer *data)
{
	struct TimeoutData *timeout_data = (struct TimeoutData *) data;

	works = 1;
	gtk_widget_destroy (timeout_data->window);
	gtk_timeout_remove (timeout_data->timeout_id);
	g_free (timeout_data);
	gtk_main_quit ();
}

static void
no_callback (GtkWidget *widget, gpointer *data)
{
	struct TimeoutData *timeout_data = (struct TimeoutData *) data;

	gtk_widget_destroy (timeout_data->window);
	gtk_timeout_remove (timeout_data->timeout_id);
	g_free (timeout_data);
	gtk_main_quit ();
}

static gint
timeout_callback (gpointer data)
{
	gchar buf[256];

	struct TimeoutData *timeout_data = (struct TimeoutData *) data;

	timeout_data->time--;

	snprintf (buf, sizeof (buf), ("Can you see this message?\n\n"
							"Automatic timeout in: %d seconds"),
			timeout_data->time);
	gtk_label_set_text (GTK_LABEL (timeout_data->label), buf);

	if (timeout_data->time == 0) {
		gtk_widget_destroy (timeout_data->window);
		gtk_timeout_remove (timeout_data->timeout_id);
		g_free (timeout_data);
		gtk_main_quit ();
		return FALSE;
	}
	return TRUE;
}

static GtkWidget *
accel_button (GtkAccelGroup *group, const char *text)
{
	GtkWidget *button, *label;
	gchar key;
    
	label = gtk_label_new (text);
	key = gtk_label_parse_uline (GTK_LABEL(label), text);
	button = gtk_button_new ();
	gtk_container_add (GTK_CONTAINER (button), label);
	gtk_widget_add_accelerator (button, "clicked", group, key, 0, 0);

	return button;
}

typedef struct {
        gchar *text;
	gint bpp;
} ColorNames;

static ColorNames color_names[] = {
	{ "16",           4 },
	{ "256",          8 },
	{ "32 thousand", 15 },
	{ "65 thousand", 16 },
	{ "16 million",  24 },
	{ NULL, -1},
};

static void
create_test_window (void)
{
	GtkWidget *window, *label, *button, *vbox, *hbox, *frame;
	GtkAccelGroup *accelgroup;
	gchar *buf, *colors = NULL;
	gint bpp, i;
	struct TimeoutData *timeout_data;

	timeout_data = g_malloc (sizeof (struct TimeoutData));

	window = gtk_window_new (GTK_WINDOW_POPUP);

	vbox = gtk_vbox_new (FALSE, 0);
	frame = gtk_frame_new (NULL);
	gtk_frame_set_shadow_type (GTK_FRAME (frame), GTK_SHADOW_OUT);
	gtk_container_border_width (GTK_CONTAINER (vbox), 25);

	gtk_container_add (GTK_CONTAINER (frame), vbox);
	gtk_container_add (GTK_CONTAINER (window), frame);

	bpp = DefaultDepthOfScreen (DefaultScreenOfDisplay (GDK_DISPLAY ()));

	for (i = 0;  color_names[i].text; i++) {
		if (color_names[i].bpp == bpp) {
			colors = color_names[i].text;
			break;
		}
	}
        
	if (color_names[i].text == NULL) {
		g_warning ("Invalid colordepth '%d'.", bpp);
		return;
	}
	
	buf = g_strdup_printf ("Testing graphic mode %dx%d pixels with %s colors.\n",
                               gdk_screen_width (),
                               gdk_screen_height (),
                               colors);
	
	label = gtk_label_new (buf);
	gtk_box_pack_start (GTK_BOX (vbox), label, TRUE, TRUE, 10);
	g_free (buf);
        
	buf = g_strdup_printf ("Can you see this message?\n\n"
                               "Automatic timeout in: %d seconds", 6);
	
	label = gtk_label_new (buf);
	gtk_box_pack_start (GTK_BOX (vbox), label, TRUE, TRUE, 10);
	g_free (buf);
        
	timeout_data->time = 6;
	timeout_data->label = label;
	timeout_data->window = window;
	
	timeout_data->timeout_id =
		gtk_timeout_add (1000, timeout_callback, timeout_data);

	hbox = gtk_hbox_new (TRUE, 10);
	button = gtk_button_new_with_mnemonic ("_Yes");
	g_signal_connect (G_OBJECT (button), "clicked",
				   G_CALLBACK (yes_callback),
				   timeout_data);
	gtk_box_pack_start (GTK_BOX (hbox), button, TRUE, TRUE, 0);

	gtk_window_set_focus (GTK_WINDOW (window), button);

	button = gtk_button_new_with_mnemonic ("_No");
	g_signal_connect (G_OBJECT (button), "clicked",
				   G_CALLBACK (no_callback),
				   timeout_data);
	gtk_box_pack_start (GTK_BOX (hbox), button, TRUE, TRUE, 0);
        
	gtk_box_pack_start (GTK_BOX (vbox), hbox, TRUE, TRUE, 0);
        
	g_signal_connect (G_OBJECT (window), "delete_event",
				   G_CALLBACK (destroy), window);
        
	g_signal_connect (G_OBJECT (window), "destroy",
				   G_CALLBACK (destroy), NULL);
        
        
	gtk_window_set_position (GTK_WINDOW (window),
                                 GTK_WIN_POS_CENTER);
	
	gtk_widget_show_all (window);
        
	/* default is the X cursor.  We would like an arrow pointer. */
	gdk_window_set_cursor (window->window, gdk_cursor_new (GDK_LEFT_PTR));
}

static GdkPixbuf *
open_sample_image (gchar *file)
{
	GdkPixbuf *retval = NULL;
	gchar *path;
	
	path = g_strdup_printf ("%s/%s", PIXMAPS_DIR, file);
	retval = gdk_pixbuf_new_from_file (path, NULL);
	g_free (path);
	
	return retval;
}

static void
set_background_bg (GdkGC *gc, gint width, gint height)
{
	GdkPixbuf *background;
	GdkPixbuf *back_rendered;
	gfloat tmp_x, tmp_y;
        
	background = open_sample_image ("background.jpeg");    
        
	if (background) {
		tmp_x = (gfloat)width  / (gfloat)gdk_pixbuf_get_width (background);
		tmp_y = (gfloat)height / (gfloat)gdk_pixbuf_get_height (background);
		back_rendered = gdk_pixbuf_new (GDK_COLORSPACE_RGB, FALSE,
                                                8, width, height);
                
		gdk_pixbuf_scale (background, back_rendered,
                                  0, 0, width, height, 0, 0,
                                  tmp_x, tmp_y, GDK_INTERP_TILES);
		gdk_pixbuf_render_to_drawable (back_rendered, GDK_ROOT_PARENT (), gc,
                                               0, 0, 0, 0, width, height,
                                               GDK_RGB_DITHER_MAX, 0, 0);
		gdk_pixbuf_unref (background);
		gdk_pixbuf_unref (back_rendered);
	}
}

static gint
set_background_top (GdkGC *gc, gint width, gint height)
{
	GdkPixbuf *left;
	GdkPixbuf *right;
	GdkPixbuf *mid;
	GdkPixbuf *mid_rendered;
	GdkPixmap *bg_pixmap;
	gint       rt_width, rt_height;
	gint       lt_width, lt_height;
	gint       mid_width, mid_height;
        
	bg_pixmap = GDK_ROOT_PARENT ();
        
	left  = open_sample_image ("panel_top_left.png");
	right = open_sample_image ("panel_top_right.png");
	mid   = open_sample_image ("panel_top_mid.png");
        
	if (left && right && mid) {
		lt_width  = gdk_pixbuf_get_width (left);
		lt_height = gdk_pixbuf_get_height (left);
		gdk_pixbuf_render_to_drawable (left, bg_pixmap, gc, 
								 0, 0, 
								 0, 0, 
								 lt_width, lt_height,
								 GDK_RGB_DITHER_MAX, 0, 0);

		rt_width  = gdk_pixbuf_get_width (right);
		rt_height = gdk_pixbuf_get_height (right);
		gdk_pixbuf_render_to_drawable (right, bg_pixmap, gc, 
								 0, 0,
								 width - rt_width,
								 0,
								 rt_width, rt_height,
								 GDK_RGB_DITHER_MAX, 0, 0);
		/* assume all the same height */
		mid_width  = width - rt_width - lt_width;
		mid_height = rt_height;
		mid_rendered = gdk_pixbuf_new (GDK_COLORSPACE_RGB,
								 TRUE, 8, mid_width, mid_height);
		
		gdk_pixbuf_scale (mid, mid_rendered,
					   0, 0, mid_width, mid_height, 0, 0,
					   (double)mid_width, 1.0, GDK_INTERP_TILES);

		gdk_pixbuf_render_to_drawable (mid_rendered, bg_pixmap, gc, 
								 0, 0, 
								 lt_width, 0,
								 mid_width, mid_height,
								 GDK_RGB_DITHER_MAX, 0, 0);				

		gdk_pixbuf_unref (right);
		gdk_pixbuf_unref (mid);
		gdk_pixbuf_unref (mid_rendered);
		gdk_pixbuf_unref (left);

		return rt_height;
	}

	return -1;
}

static void
set_background_bottom (GdkGC *gc, gint width, gint height)
{
	GdkPixbuf *left;
	GdkPixbuf *right;
	GdkPixbuf *mid;
	GdkPixbuf *mid_rendered;
	GdkPixmap *bg_pixmap;
	gint       rt_width, rt_height;
	gint       lt_width, lt_height;
	gint       mid_width, mid_height;

	bg_pixmap = GDK_ROOT_PARENT ();

	left  = open_sample_image ("panel_bottom_left.png");
	right = open_sample_image ("panel_bottom_right.png");
	mid   = open_sample_image ("panel_bottom_mid.png");

	if (left && right && mid) {
		lt_width  = gdk_pixbuf_get_width (left);
		lt_height = gdk_pixbuf_get_height (left);
		gdk_pixbuf_render_to_drawable (left, bg_pixmap, gc, 
								 0, 0, 
								 0, height - lt_height, 
								 lt_width, lt_height,
								 GDK_RGB_DITHER_MAX, 0, 0);

		rt_width  = gdk_pixbuf_get_width (right);
		rt_height = gdk_pixbuf_get_height (right);
		gdk_pixbuf_render_to_drawable (right, bg_pixmap, gc, 
								 0, 0, 
								 width - rt_width,
								 height - rt_height,
								 rt_width, rt_height,
								 GDK_RGB_DITHER_MAX, 0, 0);
		/* assume all the same height */
		mid_width  = width - rt_width - lt_width;
		mid_height = rt_height;
		mid_rendered = gdk_pixbuf_new (GDK_COLORSPACE_RGB,
								 TRUE, 8, mid_width, mid_height);
		
		gdk_pixbuf_scale (mid, mid_rendered,
					   0, 0, mid_width, mid_height, 0, 0,
					   (double)mid_width, 1.0, GDK_INTERP_TILES);
		
		gdk_pixbuf_render_to_drawable (mid_rendered, bg_pixmap, gc, 
								 0, 0, 
								 lt_width, height - mid_height,
								 mid_width, mid_height,
								 GDK_RGB_DITHER_MAX, 0, 0);				

		/* free everything */
		gdk_pixbuf_unref (right);
		gdk_pixbuf_unref (mid);
		gdk_pixbuf_unref (mid_rendered);
		gdk_pixbuf_unref (left);
	}
}

static void
set_background_icons (GdkPixmap *bg, gint height_top)
{
	GdkPixbuf *icons;

	icons = open_sample_image ("desktop_icons.png");
	if (icons) {
		gdk_pixbuf_render_to_drawable_alpha (icons, bg,
									  0, 0, 0, height_top,
									  gdk_pixbuf_get_width (icons),
									  gdk_pixbuf_get_height (icons),
									  GDK_PIXBUF_ALPHA_BILEVEL,
									  100,
									  GDK_RGB_DITHER_MAX, 0, 0);
		gdk_pixbuf_unref (icons);
	}
}

static void
set_background (void)
{
	gint       output_width, output_height;
	gint       top_height;
	GdkPixmap *bg_pixmap;
	GdkGC     *gc;

	gdk_rgb_init ();

	output_width = gdk_screen_width ();
	output_height = gdk_screen_height ();

	bg_pixmap = GDK_ROOT_PARENT ();
	gc = gdk_gc_new (bg_pixmap);

	set_background_bg (gc, output_width, output_height);
	top_height = set_background_top (gc, output_width, output_height);
	set_background_bottom (gc, output_width, output_height);
	set_background_icons (bg_pixmap, top_height);
}

static gboolean
Xtest (int argc, char *argv[], const char *config)
{
	gint childpid, fd;
	gint status=0;
	gint testpid;   

	g_return_val_if_fail (config != NULL, FALSE);
	
	if (!(childpid = fork ())) {
		if ((fd = open ("/dev/null", O_WRONLY)) > 0);
		dup2 (fd, 2);
		execl ("/usr/X11R6/bin/X", "X", "-xf86config",
			  config, ":9", NULL);
	}
	setenv ("DISPLAY", ":9", 1);
	
	sleep (3);

	testpid = waitpid (childpid, &status, WNOHANG);
 	if (testpid > 0 && WIFEXITED (status)) 
		return FALSE;

	gtk_set_locale ();
	gtk_init (&argc, &argv);
    
	set_background ();
	create_test_window ();

	gtk_main ();

	if (!works) {
		kill (childpid, SIGTERM);
		return FALSE;
	}
	
	kill (childpid, SIGTERM);
	
	return TRUE;
}

static void
save_status (gboolean status)
{
	FILE *f;
	gchar *fname = TEMP_FILE;

	/* This is hack. Will be removed when
	   reporting between frontends and
	   backends is working. */
	
	f = fopen (fname, "w");
	if (f == NULL) {
		g_warning ("Can't open file '%s' for writing.", fname);
		return;
	}

	putc (status, f);

	fclose (f);
}

int
main (int argc, char **argv)
{
	poptContext optCon;
	gchar *config = NULL;
	gint retcode;
	struct poptOption cmdOptions[] = {
		{ "config", 'c', POPT_ARG_STRING, &config, 0},
		POPT_AUTOHELP
		{0, 0, 0, 0, 0}
	};

	optCon = poptGetContext (NULL, argc, (gpointer)argv, cmdOptions, 0);

	if ((retcode = poptGetNextOpt(optCon)) < -1) {
		fprintf (stderr, "%s: %s\n",
			    poptBadOption (optCon, POPT_BADOPTION_NOALIAS),
			    poptStrerror (retcode));
		save_status (FALSE);
		_exit (EXIT_FAILURE);
	}
	
	if (poptGetArg(optCon)) {
		g_warning ("Unexpected argument. Only --config is valid.");
		save_status (FALSE);
		_exit (EXIT_FAILURE);
	}
	
	poptFreeContext (optCon);

	if (config == NULL) {
		g_warning ("Specify config file with --config=/path.");
		save_status (FALSE);
		_exit (EXIT_FAILURE);
	}
	
	if ((Xtest (argc, argv, config)) == TRUE) {
		save_status (TRUE);
		_exit (EXIT_SUCCESS);
	}
	
	save_status (FALSE);	
	_exit (EXIT_FAILURE);
}
