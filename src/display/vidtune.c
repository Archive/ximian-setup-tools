/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/* main.c: this file is part of x-admin, a ximian-setup-tool frontend 
 * for user administration.
 * 
 * Copyright (C) 2000-2001 Ximian, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Tambet Ingo <tambet@ximian.com>.
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <X11/Xlib.h>
#include <X11/extensions/xf86vmode.h>
#include <stdlib.h>

#include "xst.h"
#include "vidtune.h"

/* Copied from X header which isn't installed with X
 * xc/programs/Xserver/hw/xfree86/common/xf86str.h */

/* These are possible return values for xf86CheckMode() and ValidMode() */
enum {
	MODE_OK     = 0,    /* Mode OK */
	MODE_HSYNC,         /* hsync out of range */
	MODE_VSYNC,         /* vsync out of range */
	MODE_H_ILLEGAL,     /* mode has illegal horizontal timings */
	MODE_V_ILLEGAL,     /* mode has illegal horizontal timings */
	MODE_BAD_WIDTH,     /* requires an unsupported linepitch */
	MODE_NOMODE,        /* no mode with a maching name */
	MODE_NO_INTERLACE,  /* interlaced mode not supported */
	MODE_NO_DBLESCAN,   /* doublescan mode not supported */
	MODE_NO_VSCAN,      /* multiscan mode not supported */
	MODE_MEM,           /* insufficient video memory */
	MODE_VIRTUAL_X,     /* mode width too large for specified virtual size */
	MODE_VIRTUAL_Y,     /* mode height too large for specified virtual size */
	MODE_MEM_VIRT,      /* insufficient video memory given virtual size */
	MODE_NOCLOCK,       /* no fixed clock available */
	MODE_CLOCK_HIGH,    /* clock required is too high */
	MODE_CLOCK_LOW,     /* clock required is too low */  
	MODE_CLOCK_RANGE,   /* clock/mode isn't in a ClockRange */
	MODE_BAD_HVALUE,    /* horizontal timing was out of range */
	MODE_BAD_VVALUE,    /* vertical timing was out of range */
	MODE_BAD_VSCAN,     /* VScan value out of range */ 
	MODE_HSYNC_NARROW,  /* horizontal sync too narrow */
	MODE_HSYNC_WIDE,    /* horizontal sync too wide */
	MODE_HBLANK_NARROW, /* horizontal blanking too narrow */
	MODE_HBLANK_WIDE,   /* horizontal blanking too wide */
	MODE_VSYNC_NARROW,  /* vertical sync too narrow */  
	MODE_VSYNC_WIDE,    /* vertical sync too wide */   
	MODE_VBLANK_NARROW, /* vertical blanking too narrow */
	MODE_VBLANK_WIDE,   /* vertical blanking too wide */
	MODE_PANEL,         /* exceeds panel dimensions */
	MODE_INTERLACE_WIDTH, /* width too large for interlaced mode */
	MODE_ONE_WIDTH,    /* only one width is supported */
	MODE_ONE_HEIGHT,   /* only one height is supported */
	MODE_ONE_SIZE,     /* only one resolution is supported */
	MODE_BAD = -2,      /* unspecified reason */
	MODE_ERROR  = -1    /* error condition */
};

static XF86VidModeModeInfo *initial_modeinfo = NULL;

#if 0
static void
video_debug_print_modeinfo (XF86VidModeModeInfo *info)
{
	g_print ("Modeline:\n"
		 "dotclock: %d\n"
		 "hdisplay: %d hstart: %d hend: %d htotal: %d hskew: %d\n"
		 "vdisplay: %d vstart: %d vend: %d vtotal: %d\n",
		 info->dotclock, info->hdisplay, info->hsyncstart, info->hsyncend,
		 info->htotal, info->hskew, info->vdisplay, info->vsyncstart,
		 info->vsyncend, info->vtotal);
}
#endif

/* Function to get XF86VidModeModeInfo from XF86VidModeModeLine and dotclock */
static XF86VidModeModeInfo *
line_to_info (XF86VidModeModeLine *modeline, guint dot)
{
	XF86VidModeModeInfo *info;

	info = g_new0 (XF86VidModeModeInfo, 1);

	info->dotclock = dot;
	info->hdisplay = modeline->hdisplay;
	info->hsyncstart = modeline->hsyncstart;
	info->hsyncend = modeline->hsyncend;
	info->htotal = modeline->htotal;
#ifdef HAVE_MODELINE_HSKEW
	info->hskew = modeline->hskew; /* TODO: exists only on verion > 2 */
#endif
	info->vdisplay = modeline->vdisplay;
	info->vsyncstart = modeline->vsyncstart;
	info->vsyncend = modeline->vsyncend;
	info->vtotal = modeline->vtotal;
	info->flags = modeline->flags;
	info->privsize = modeline->privsize;
	
	return info;
}

/* Function to get XF86VidModeModeLine from XF86VidModeModeInfo */
static XF86VidModeModeLine *
info_to_line (XF86VidModeModeInfo *info)
{
	XF86VidModeModeLine *modeline;

	modeline = g_new0 (XF86VidModeModeLine, 1);

	modeline->hdisplay = info->hdisplay;
	modeline->hsyncstart = info->hsyncstart;
	modeline->hsyncend = info->hsyncend;
	modeline->htotal = info->htotal;
#ifdef HAVE_MODELINE_HSKEW
	modeline->hskew = info->hskew; /* TODO: exists only on verion >= 2 */
#endif
	modeline->vdisplay = info->vdisplay;
	modeline->vsyncstart = info->vsyncstart;
	modeline->vsyncend = info->vsyncend;
	modeline->vtotal = info->vtotal;
	modeline->flags = info->flags;
	modeline->privsize = info->privsize;
	
	return modeline;
}

static void
video_close (Display *dpy)
{
	XF86VidModeLockModeSwitch (dpy, XDefaultScreen (dpy), FALSE);
	XFlush (dpy);
	XCloseDisplay (dpy);
}

static Display *
video_init (const gchar *display_name)
{
	Display *dpy;
	gint event, error;
	gint major, minor;
	gchar *display;

	if (!display_name)
		display = g_strdup ("");
	else
		display = g_strdup (display_name);
	
	if (!(dpy = XOpenDisplay (display))) {
		g_warning ("video_init: Can't open display \"%s\".", display);
		g_free (display);
		return NULL;
	}
	g_free (display);

	if (!XF86VidModeQueryVersion (dpy, &major, &minor)) {
		g_warning ("video_init: Can't query XF86VidMode version.");
		video_close (dpy);
		return NULL;
	}

	if (major < 2) {
		g_warning ("video_init: Xserver is running an old"
			   "XFree86-VidModeExtension version (%d.%d).\n",
			   major, minor);
		
		g_warning ("Minimum required version is 2.0\n");
	}
	
	if (!XF86VidModeQueryExtension (dpy, &event, &error)) {
		g_warning ("video_init: Can't query XF86VidMode extension.");
		video_close (dpy);
		return NULL;
	}

	if (!XF86VidModeLockModeSwitch(dpy, XDefaultScreen (dpy), TRUE)) {
		g_warning ("video_init: Failed to disable mode-switch hot-keys.");
		video_close (dpy);
		return NULL;
	}

	if (initial_modeinfo == NULL) {
		XF86VidModeModeLine *modeline;
		gint screen = XDefaultScreen (dpy);
		gint dots;

		modeline = g_new (XF86VidModeModeLine, 1);
		XF86VidModeGetModeLine (dpy, screen, &dots, modeline);
		initial_modeinfo = line_to_info (modeline, dots);
		g_free (modeline);
	}
	
	return dpy;
}

/* Monitor functions */

static void
video_monitor_destroy (XF86VidModeMonitor monitor)
{
/*	if (monitor.vendor) XFree (monitor.vendor);
	if (monitor.model)  XFree (monitor.model);

	if (monitor.hsync)  XFree (monitor.hsync);
	if (monitor.vsync)  XFree (monitor.vsync);
*/
}

static XF86VidModeMonitor
video_monitor_get (Display *display, gint screen)
{
	XF86VidModeMonitor monitor;

	XF86VidModeGetMonitor (display, screen, &monitor);

	return monitor;
}

gchar *
video_monitor_get_vendor (void)
{
	gchar *vendor;
	XF86VidModeMonitor monitor;
	Display *dpy;
	gint screen;

	if (!(dpy = video_init (NULL)))
		return NULL;
	
	screen = XDefaultScreen (dpy);

	monitor = video_monitor_get (dpy, screen);
	video_close (dpy);
	
	vendor = g_strdup (monitor.vendor);
	video_monitor_destroy (monitor);

	return vendor;
}

gchar *
video_monitor_get_model (void)
{
	gchar *model;
	XF86VidModeMonitor monitor;
	Display *dpy;
	gint screen;

	if (!(dpy = video_init (NULL)))
		return NULL;
	
	screen = XDefaultScreen (dpy);
	
	monitor = video_monitor_get (dpy, screen);
	video_close (dpy);
	
	model = g_strdup (monitor.model);
	video_monitor_destroy (monitor);

	return model;
}

/* Video Mode functions */

static XF86VidModeModeInfo *
video_modeline_get_current (Display *display, gint screen)
{
	XF86VidModeModeLine *modeline;
	XF86VidModeModeInfo *modeinfo;
	gint dot_clock;
	
	g_return_val_if_fail (display != NULL, NULL);

	modeline = g_new0 (XF86VidModeModeLine, 1);
	
	if (!XF86VidModeGetModeLine (display, screen,
				     &dot_clock, modeline)) {
		g_warning ("video_get_current_modeline: Can't get current.");
		return NULL;
	}

	modeinfo = line_to_info (modeline, dot_clock);
	g_free (modeline);
	
	return modeinfo;
}

static GList *
video_modeline_get_modelines (Display *display, gint screen)
{
	GList *list = NULL;
	XF86VidModeModeInfo **modesinfo;
	gint count, i;
	gboolean ret;
	
	ret = XF86VidModeGetAllModeLines (display, screen, &count, &modesinfo);

	for (i = 0; i < count; i++)
		list = g_list_append (list, modesinfo[i]);
	
	return list;
}

static gboolean
video_modeline_diff (XF86VidModeModeInfo *modeinfo1, XF86VidModeModeInfo *modeinfo2)
{
	/* We only care those fields: */
	if (modeinfo1->dotclock == modeinfo2->dotclock &&
	    modeinfo1->hdisplay == modeinfo2->hdisplay &&
	    modeinfo1->vdisplay == modeinfo2->vdisplay)
		return FALSE;

	return TRUE;
}

static gboolean
video_modeline_modify (Display *display, gint screen, XF86VidModeModeInfo *modeinfo)
{
	XF86VidModeModeLine *modeline;
	XF86VidModeModeInfo *current;
	Status status;
	gboolean retval = FALSE;
	
	g_return_val_if_fail (display != NULL, FALSE);
	g_return_val_if_fail (modeinfo != NULL, FALSE);

        current = video_modeline_get_current (display, screen);
	if (video_modeline_diff (modeinfo, current)) {
		/* not current resolution */
		g_print ("video_modeline_modify: not changing.\n");
		g_free (current);
		return FALSE;
	}	
	g_free (current);	
	status = XF86VidModeValidateModeLine (display, screen, modeinfo);	

	switch (status) {
	case MODE_OK:		
		modeline = info_to_line (modeinfo);
		retval = XF86VidModeModModeLine (display, screen, modeline);
		if (retval) XFlush (display);
		break;
	default:
		/* TODO: give better reason. */
		g_warning ("video_modeline_modify: Can't set current modeline, status %d.",
			   status);
		break;
	}

	return retval;
}

static gboolean
video_modeline_set (Display *display, gint screen, XF86VidModeModeInfo *info)
{
	XF86VidModeModeInfo *tmpinfo;
	GList *list, *l, *next;	
	gboolean retval = FALSE;

	g_return_val_if_fail (display != NULL, FALSE);
	g_return_val_if_fail (info != NULL, FALSE);

	list = video_modeline_get_modelines (display, screen);
	for (l = list; l; l = next) {
		tmpinfo = l->data;
		next = l->next;

		if (!retval && !video_modeline_diff (info, tmpinfo)) {
			retval = XF86VidModeSwitchToMode (display, screen, tmpinfo);
			if (retval) XFlush (display);
		}
		
		list = g_list_remove_link (list, l);
		g_list_free_1 (l);
	}
	
	return retval;
}

#define MOVE_STEP 4

static void
video_modeline_move (XF86VidModeModeInfo *modeinfo, XstVideoDirection dir)
{
	g_return_if_fail (modeinfo != NULL);

	switch (dir) {
	case DIR_LEFT:
		modeinfo->hsyncstart += MOVE_STEP;
		modeinfo->hsyncend   += MOVE_STEP;
		break;
	case DIR_RIGHT:
		modeinfo->hsyncstart -= MOVE_STEP;
		modeinfo->hsyncend   -= MOVE_STEP;
		break;
	case DIR_UP:
		modeinfo->vsyncstart += MOVE_STEP;
		modeinfo->vsyncend   += MOVE_STEP;
		break;
	case DIR_DOWN:
		modeinfo->vsyncstart -= MOVE_STEP;
		modeinfo->vsyncend   -= MOVE_STEP;
		break;
	case DIR_WIDER:
		modeinfo->htotal     -= MOVE_STEP;
		break;
	case DIR_NARROWER:
		modeinfo->htotal     += MOVE_STEP;
		break;
	case DIR_SHORTER:
		modeinfo->htotal     += MOVE_STEP;
		break;
	case DIR_TALLER:
		modeinfo->htotal     -= MOVE_STEP;
		break;
	default:
		g_warning ("video_modeline_move: unknown direction %d.", dir);
		break;
	}
}

gboolean
video_screen_move (XstVideoDirection dir)
{
	gboolean ret;
	Display *display;
	gint screen;
	XF86VidModeModeInfo *modeinfo;

	if (!(display = video_init (NULL)))
		return FALSE;
	
	screen = XDefaultScreen (display);

	modeinfo = video_modeline_get_current (display, screen);
	if (!modeinfo) {
		video_close (display);
		return FALSE;
	}

	video_modeline_move (modeinfo, dir);

	ret = video_modeline_modify (display, screen, modeinfo);
	g_free (modeinfo);
	video_close (display);
	return ret;
}

GList *
video_screen_resolution_list (void)
{
	GList *list, *l, *next;
	GList *string_list = NULL;
	gchar *res;
	Display *display;	
	gint screen;

	if (!(display = video_init (NULL)))
		return FALSE;
	
	screen = XDefaultScreen (display);

	list = video_modeline_get_modelines (display, screen);
	video_close (display);

	for (l = list; l; l = next) {
		XF86VidModeModeInfo *info = l->data;

		next = l->next;

		list = g_list_remove_link (list, l);
		g_list_free_1 (l);
		res = g_strdup_printf ("%dx%d", info->hdisplay, info->vdisplay);
		/* XFree (info); */
		
		string_list = g_list_append (string_list, res);
	}
	
	return string_list;
}

gchar *
video_resolution_get_current (void)
{
	gchar               *res;
	Display             *display;
	gint                 screen;
	XF86VidModeModeInfo *info;

	if (!(display = video_init (NULL)))
		return NULL;

	screen = XDefaultScreen (display);
	if (!(info = video_modeline_get_current (display, screen)))
		return NULL;

	res = g_strdup_printf ("%dx%d", info->hdisplay, info->vdisplay);
	g_free (info);
	return res;
}

gboolean
video_resolution_set (const gchar *reso)
{
	Display *display;
	XF86VidModeModeInfo *info;
	GList *list, *l, *next;
	gboolean retval = FALSE;
	gint screen;
	gchar *buf;
	gchar **array;
	gint x, y;

	g_return_val_if_fail (reso != NULL, FALSE);

	buf = g_strdup (reso);
	g_strdown (buf);
	array = g_strsplit (buf, "x", 2);
	g_free (buf);

	/* TODO: add checks */
	x = atoi (array[0]);
	y = atoi (array[1]);
	
	g_strfreev (array);
	
	if (!(display = video_init (NULL)))
		return FALSE;
	
	screen = XDefaultScreen (display);

	list = video_modeline_get_modelines (display, screen);
	for (l = list; l; l = next) {
		info = l->data;
		next = l->next;

		if (info->hdisplay == x && info->vdisplay == y)
			retval = video_modeline_set (display, screen, info);
		
		list = g_list_remove_link (list, l);
		g_list_free_1 (l);
		/* XFree (info); */
	}	

	video_close (display);
	return retval;
}

void
video_screen_restore (void)
{
	if (initial_modeinfo) {
		Display *display;
		gint screen;

		if (!(display = video_init (NULL)))
			return;
	
		screen = XDefaultScreen (display);

		g_print ("Restoring...\n");
		video_modeline_set (display, screen, initial_modeinfo);
		video_modeline_modify (display, screen, initial_modeinfo);

		video_close (display);
	}
}
