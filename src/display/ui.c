/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/* 
 * Copyright (C) 2000-2001 Ximian, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Tambet Ingo <tambet@ximian.com>.
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <gnome.h>

#include "xst.h"
#include "ui.h"
#include "transfer.h"

/* Common helpers */

static gint
clist_add_row_unique (GtkCList *list, const gchar *text)
{
	gchar *item[2];
	gchar *buf;
	gint   row;

	for (row = 0; gtk_clist_get_text (list, row, 0, &buf); row++) {
		if (buf && (strcmp (buf, text)) == 0)
			return -1;
	}
	
	item[0] = (gchar *)text;
	item[1] = NULL;	
	row = gtk_clist_append (list, item);

	return row;
}

/* Colordepth widget */
/* GtkTreeView */

#define UI_WIDGET_DEPTH "colordepth_list"

enum {
	DEPTH_COL_DEPTH,
	DEPTH_COL_DATA,

	DEPTH_COL_LAST
};

typedef struct {
        gchar *text;
        gint id;
} XstDisplayListItem;

static XstDisplayListItem colordepth_items[] = {
	{ N_("256 colors"),          8 },
	{ N_("32 thousand colors"), 15 },
	{ N_("65 thousand colors"), 16 },
	{ N_("16 million colors"),  24 },
	{ NULL, -1},
};

static void
ui_signal_block (GtkWidget *treeview, gboolean block)
{
	GtkTreeSelection *select;
	guint             id;

	select = gtk_tree_view_get_selection (GTK_TREE_VIEW (treeview));
	id = GPOINTER_TO_UINT (g_object_get_data (G_OBJECT (select), "signal-select-row"));

	if (block)
		g_signal_handler_block (G_OBJECT (select), id);
	else
		g_signal_handler_unblock (G_OBJECT (select), id);
}

static const gchar *
depth_to_label (const gchar *depth)
{
	gint val, i;
	
	g_return_val_if_fail (depth != NULL, NULL);
	
	val = atoi (depth);
	for (i = 0; colordepth_items[i].id != -1; i++) {
		if (colordepth_items[i].id == val)
			return colordepth_items[i].text;
	}

	g_warning ("depth_to_label: Unknown depth %s.", depth);
	return NULL;
}

static gchar *
depth_from_label (const gchar *label)
{
	gint i;
	
	g_return_val_if_fail (label != NULL, NULL);
	
	for (i = 0; colordepth_items[i].id != -1; i++) {
		if ((strcmp (colordepth_items[i].text, label)) == 0)
			return g_strdup_printf ("%d", colordepth_items[i].id);
	}

	g_warning ("depth_from_label: Unknown label %s.", label);
	return NULL;
}

static void
depth_selection_changed (GtkCList *list, gint row, gint col, GdkEventButton *event, gpointer data)
{
	gchar   *depth;
	gchar   *res;
	XstTool *tool = XST_TOOL (data);

	xst_dialog_modify (tool->main_dialog);

	res = ui_res_selected (tool);
	depth = gtk_clist_get_row_data (GTK_CLIST (list), row);
	if (depth == NULL)
		g_warning ("depth_selection_changed: Selected row has no data.");
	else {
		populate_resolutions (tool, depth);
		if (res)
			ui_res_select (tool, res);
	}
}

static gint
depth_cmp (GtkTreeModel *model, GtkTreeIter *a, GtkTreeIter *b, gpointer data)
{
	gchar       *text1, *text2;
	gint         n1, n2;

	gtk_tree_model_get (model, a, DEPTH_COL_DATA, &text1, -1);
	gtk_tree_model_get (model, b, DEPTH_COL_DATA, &text2, -1);

	if (!text2)
		return (text1 != NULL);

	if (!text1)
		return -1;

	n1 = atoi (text1);
	n2 = atoi (text2);

	if (n1 == n2)
		return 0;

	if (n1 > n2)
		return 1;

	return -1;
}

static gboolean
ui_depth_get_iter (GtkTreeView *list, const gchar *depth, GtkTreeIter *iter)
{
	GtkTreeModel    *model;
	gchar           *buf;
	gboolean         valid;

	g_return_val_if_fail (depth != NULL, FALSE);

	model = gtk_tree_view_get_model (list);

	valid = gtk_tree_model_get_iter_first (model, iter);
	while (valid) {
		gtk_tree_model_get (model, iter, DEPTH_COL_DATA, &buf, -1);

		if (!strcmp (depth, buf)) {
			g_free (buf);
			return TRUE;
		}

		g_free (buf);
		valid = gtk_tree_model_iter_next (model, iter);
	}

	return FALSE;
}

static GtkTreeModel *
ui_depth_get_model (XstTool *tool, GtkTreeView *list)
{
	static GtkTreeModel *model;
	GtkTreeSelection    *select;
	GtkCellRenderer     *cell;
	GtkTreeViewColumn   *col;
	gint                 id;

	if (!model) {
		model = GTK_TREE_MODEL (gtk_list_store_new (DEPTH_COL_LAST, G_TYPE_STRING, G_TYPE_STRING));

		gtk_tree_view_set_model (list, model);
		select = gtk_tree_view_get_selection (list);
		id = g_signal_connect (G_OBJECT (select), "changed",
				       G_CALLBACK (xst_dialog_modify_cb), tool->main_dialog);

		g_object_set_data (G_OBJECT (select), "signal-select-row", GUINT_TO_POINTER (id));

		cell = gtk_cell_renderer_text_new ();
		col = gtk_tree_view_column_new_with_attributes (_("Depth"), cell,
								"text", DEPTH_COL_DEPTH, NULL);
		gtk_tree_view_column_set_sort_column_id (col, DEPTH_COL_DEPTH);
		gtk_tree_view_column_set_sort_order (col, GTK_SORT_ASCENDING);
		gtk_tree_view_append_column (list, col);

		gtk_tree_sortable_set_sort_func (GTK_TREE_SORTABLE (model), DEPTH_COL_DEPTH,
						 depth_cmp, NULL, NULL);
		gtk_tree_sortable_set_sort_column_id (GTK_TREE_SORTABLE (model),
						      DEPTH_COL_DEPTH, GTK_SORT_ASCENDING);
	}

	return model;
}

void
ui_depth_add (XstTool *tool, const gchar *depth)
{
	GtkWidget    *list;
	GtkTreeModel *model;
	GtkTreeIter   iter;
	const gchar  *label;

	g_return_if_fail (tool != NULL);
	g_return_if_fail (XST_IS_TOOL (tool));
	g_return_if_fail (depth != NULL);

	label = depth_to_label (depth);
	if (label == NULL)
		return;

	list = xst_dialog_get_widget (tool->main_dialog, UI_WIDGET_DEPTH);
	if (list == NULL)
		return;

	model = ui_depth_get_model (tool, GTK_TREE_VIEW (list));

	if (!ui_depth_get_iter (GTK_TREE_VIEW (list), depth, &iter)) {
		ui_signal_block (list, TRUE);

		gtk_list_store_append (GTK_LIST_STORE (model), &iter);
		gtk_list_store_set (GTK_LIST_STORE (model), &iter,
				    DEPTH_COL_DEPTH, label,
				    DEPTH_COL_DATA, depth,
				    -1);

		ui_signal_block (list, FALSE);
	}
}

void
ui_depth_select (XstTool *tool, const gchar *depth)
{
	GtkWidget        *list;
	GtkTreeSelection *select;
	GtkTreeIter       iter;

	g_return_if_fail (tool != NULL);
	g_return_if_fail (XST_IS_TOOL (tool));
	g_return_if_fail (depth != NULL);

	list = xst_dialog_get_widget (tool->main_dialog, UI_WIDGET_DEPTH);
	if (list == NULL)
		return;

	select = gtk_tree_view_get_selection (GTK_TREE_VIEW (list));
	if (ui_depth_get_iter (GTK_TREE_VIEW (list), depth, &iter)) {
		ui_signal_block (list, TRUE);
		gtk_tree_selection_select_iter (select, &iter);
		ui_signal_block (list, FALSE);
	}
}

gchar *
ui_depth_selected (XstTool *tool)
{
	GtkWidget        *list;
	GtkTreeSelection *select;
	GtkTreeModel     *model;
	GtkTreeIter       iter;
	gchar            *buf = NULL;

	g_return_val_if_fail (tool != NULL, NULL);
	g_return_val_if_fail (XST_IS_TOOL (tool), NULL);

	list = xst_dialog_get_widget (tool->main_dialog, UI_WIDGET_DEPTH);
	if (list == NULL)
		return NULL;

	select = gtk_tree_view_get_selection (GTK_TREE_VIEW (list));
	if (gtk_tree_selection_get_selected (select, &model, &iter))
		gtk_tree_model_get (model, &iter, DEPTH_COL_DATA, &buf, -1);

	return buf;
}

void
ui_depth_clear (XstTool *tool)
{
	GtkWidget    *list;
	GtkTreeModel *model;

	g_return_if_fail (tool != NULL);
	g_return_if_fail (XST_IS_TOOL (tool));

	list = xst_dialog_get_widget (tool->main_dialog, UI_WIDGET_DEPTH);
	model = ui_depth_get_model (tool, GTK_TREE_VIEW (list));
	if (model)
		gtk_list_store_clear (GTK_LIST_STORE (model));
}

/* Resolution widget */
/* GtkTreeView */

#define UI_WIDGET_RESOLUTION "resolution_list"

enum {
	RES_COL_RES,
	RES_COL_LAST
};

static gint
res_cmp (GtkTreeModel *model, GtkTreeIter *a, GtkTreeIter *b, gpointer data)
{
	gchar *text1, *text2;
	gint   x1, x2;
	gint   y1, y2;

	gtk_tree_model_get (model, a, RES_COL_RES, &text1, -1);
	gtk_tree_model_get (model, b, RES_COL_RES, &text2, -1);

	if (!text2)
		return (text1 != NULL);

	if (!text1)
		return -1;

	sscanf (text1, "%dx%d", &x1, &y1);
	sscanf (text2, "%dx%d", &x2, &y2);
	
	if (x1 > x2)
		return 1;
	if (x1 < x2)
		return -1;

	if (y1 > y2)
		return 1;
	if (y1 < y2)
		return -1;

	return 0;
}

static gboolean
ui_res_get_iter (GtkTreeView *list, const gchar *res, GtkTreeIter *iter)
{
	GtkTreeModel    *model;
	gchar           *buf;
	gboolean         valid;

	g_return_val_if_fail (res != NULL, FALSE);

	model = gtk_tree_view_get_model (list);

	valid = gtk_tree_model_get_iter_first (model, iter);
	while (valid) {
		gtk_tree_model_get (model, iter, RES_COL_RES, &buf, -1);

		if (!strcmp (res, buf)) {
			g_free (buf);
			return TRUE;
		}

		g_free (buf);
		valid = gtk_tree_model_iter_next (model, iter);
	}

	return FALSE;
}

static GtkTreeModel *
ui_res_get_model (XstTool *tool, GtkTreeView *list)
{
	static GtkTreeModel *model;
	GtkTreeSelection    *select;
	GtkCellRenderer     *cell;
	GtkTreeViewColumn   *col;
	gint                 id;

	if (!model) {
		model = GTK_TREE_MODEL (gtk_list_store_new (RES_COL_LAST, G_TYPE_STRING));

		gtk_tree_view_set_model (list, model);
		select = gtk_tree_view_get_selection (list);
		id = g_signal_connect (G_OBJECT (select), "changed",
				       G_CALLBACK (xst_dialog_modify_cb), tool->main_dialog);

		g_object_set_data (G_OBJECT (select), "signal-select-row", GUINT_TO_POINTER (id));

		cell = gtk_cell_renderer_text_new ();
		col = gtk_tree_view_column_new_with_attributes (_("Resolution"), cell,
								"text", RES_COL_RES, NULL);
		gtk_tree_view_column_set_sort_column_id (col, RES_COL_RES);
		gtk_tree_view_column_set_sort_order (col, GTK_SORT_ASCENDING);
		gtk_tree_view_append_column (list, col);

		gtk_tree_sortable_set_sort_func (GTK_TREE_SORTABLE (model), RES_COL_RES,
						 res_cmp, NULL, NULL);
		gtk_tree_sortable_set_sort_column_id (GTK_TREE_SORTABLE (model),
						      RES_COL_RES, GTK_SORT_ASCENDING);
	}

	return model;
}

void
ui_res_add (XstTool *tool, const gchar *res)
{
	XstDialog    *xd;
	GtkWidget    *list;
	GtkTreeModel *model;
	GtkTreeIter   iter;

	g_return_if_fail (tool != NULL);
	g_return_if_fail (XST_IS_TOOL (tool));
	g_return_if_fail (res != NULL);

	xd = tool->main_dialog;
	list = xst_dialog_get_widget (xd, UI_WIDGET_RESOLUTION);
	if (list == NULL)
		return;

	model = ui_res_get_model (tool, GTK_TREE_VIEW (list));

	if (!ui_res_get_iter (GTK_TREE_VIEW (list), res, &iter)) {
		ui_signal_block (list, TRUE);

		gtk_list_store_append (GTK_LIST_STORE (model), &iter);
		gtk_list_store_set (GTK_LIST_STORE (model), &iter,
				    RES_COL_RES, res, -1);

		ui_signal_block (list, FALSE);
	}
}

void
ui_res_select (XstTool *tool, const gchar *res)
{
	GtkWidget        *list;
	GtkTreeSelection *select;
	GtkTreeIter       iter;

	g_return_if_fail (tool != NULL);
	g_return_if_fail (XST_IS_TOOL (tool));
	g_return_if_fail (res != NULL);

	list = xst_dialog_get_widget (tool->main_dialog, UI_WIDGET_RESOLUTION);
	if (list == NULL)
		return;

	select = gtk_tree_view_get_selection (GTK_TREE_VIEW (list));
	if (ui_res_get_iter (GTK_TREE_VIEW (list), res, &iter)) {
		ui_signal_block (list, TRUE);
		gtk_tree_selection_select_iter (select, &iter);
		ui_signal_block (list, FALSE);
	}
}

gchar *
ui_res_selected (XstTool *tool)
{
	GtkWidget        *list;
	GtkTreeSelection *select;
	GtkTreeModel     *model;
	GtkTreeIter       iter;
	gchar            *buf = NULL;

	g_return_val_if_fail (tool != NULL, NULL);
	g_return_val_if_fail (XST_IS_TOOL (tool), NULL);

	list = xst_dialog_get_widget (tool->main_dialog, UI_WIDGET_RESOLUTION);
	if (list == NULL)
		return NULL;

	select = gtk_tree_view_get_selection (GTK_TREE_VIEW (list));
	if (gtk_tree_selection_get_selected (select, &model, &iter))
		gtk_tree_model_get (model, &iter, RES_COL_RES, &buf, -1);

	return buf;
}

void
ui_res_clear (XstTool *tool)
{
	GtkWidget    *list;
	GtkTreeModel *model;

	g_return_if_fail (tool != NULL);
	g_return_if_fail (XST_IS_TOOL (tool));

	list = xst_dialog_get_widget (tool->main_dialog, UI_WIDGET_RESOLUTION);
	model = ui_res_get_model (tool, GTK_TREE_VIEW (list));
	if (model)
		gtk_list_store_clear (GTK_LIST_STORE (model));
}
