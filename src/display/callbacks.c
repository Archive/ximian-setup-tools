/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/* callbacks.c: this file is part of users-admin, a ximian-setup-tool frontend 
 * for user administration.
 * 
 * Copyright (C) 2000-2001 Ximian, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Tambet Ingo <tambet@ximian.com>.
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <ctype.h>
#include <gnome.h>

#include "xst.h"
#include "callbacks.h"
#include "transfer.h"
#include "vidtune.h"

XstTool *tool;

GtkWidget *
xst_error_dialog_new (char *message, GtkWindow *parent)
{
        GtkWidget *dialog, *label, *image, *hbox;

        dialog = gtk_dialog_new_with_buttons ("An error has occured",
                                              parent,
                                              GTK_DIALOG_DESTROY_WITH_PARENT,
                                              GTK_STOCK_OK, GTK_RESPONSE_OK,
                                              NULL);
        label = gtk_label_new (message);
        image = gtk_image_new_from_stock (GTK_STOCK_DIALOG_ERROR, GTK_ICON_SIZE_DIALOG);

        hbox = gtk_hbox_new (FALSE, 5);
        gtk_box_pack_start (GTK_BOX (hbox), image, FALSE, FALSE, 5);
        gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 5);

        gtk_window_set_default_size (GTK_WINDOW (dialog), 300, 150);
        
        gtk_container_add (GTK_CONTAINER (GTK_DIALOG(dialog)->vbox), hbox);
        gtk_widget_show_all (dialog);

        return dialog;
}

GtkWidget *
xst_ok_dialog_new (char *message, GtkWindow *parent)
{
        GtkWidget *dialog, *label, *image, *hbox;

        dialog = gtk_dialog_new_with_buttons ("Information",
                                              parent,
                                              GTK_DIALOG_DESTROY_WITH_PARENT,
                                              GTK_STOCK_OK, GTK_RESPONSE_OK,
                                              NULL);
        label = gtk_label_new (message);
        image = gtk_image_new_from_stock (GTK_STOCK_DIALOG_INFO, GTK_ICON_SIZE_DIALOG);

        hbox = gtk_hbox_new (FALSE, 5);
        gtk_box_pack_start (GTK_BOX (hbox), image, FALSE, FALSE, 5);
        gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 5);

        gtk_window_set_default_size (GTK_WINDOW (dialog), 300, 150);
        
        gtk_container_add (GTK_CONTAINER (GTK_DIALOG(dialog)->vbox), hbox);
        gtk_widget_show_all (dialog);

        return dialog;
}

void
move_up_clicked (GtkButton *button, gpointer data)
{
	video_screen_move (DIR_UP);
}

void
move_down_clicked (GtkButton *button, gpointer data)
{
	video_screen_move (DIR_DOWN);
	g_warning ("move down");
}

void
move_right_clicked (GtkButton *button, gpointer data)
{
	video_screen_move (DIR_RIGHT);
	g_warning ("move right");
}

void
move_left_clicked (GtkButton *button, gpointer data)
{
	video_screen_move (DIR_LEFT);	
}

void
move_shorter_clicked (GtkButton *button, gpointer data)
{
	video_screen_move (DIR_SHORTER);
}

void
move_narrower_clicked (GtkButton *button, gpointer data)
{
	video_screen_move (DIR_NARROWER);
}

void
move_wider_clicked (GtkButton *button, gpointer data)
{
	video_screen_move (DIR_WIDER);
}

void
move_taller_clicked (GtkButton *button, gpointer data)
{
	video_screen_move (DIR_TALLER);
}

void
resolution_changed (GtkEditable *editable, gpointer data)
{
	gchar *buf;

	buf = gtk_editable_get_chars (editable, 0, -1);
	video_resolution_set (buf);
	g_free (buf);
}

void
restore_clicked (GtkButton *button, gpointer data)
{
	video_screen_restore ();
}

static gboolean applied = FALSE;

void
dialog_applied (XstDialog *xd, gpointer data)
{
	applied = TRUE;
}

void
close_cb (XstTool *tool, gpointer data)
{
	if (applied)
		xst_ui_logout_dialog (NULL);
}

#define TEMP_FILE "/tmp/xst_testX"

void
appereance_test_clicked (GtkButton *button, gpointer data)
{
	FILE *f;
	gchar *fname = TEMP_FILE;
	gint status;
	GtkWidget *d;
	xmlDocPtr backup_xml;	
	
	/* get current xml */
	backup_xml = xmlCopyDoc (tool->config, 1);

	/* Save selected one */
	transfer_gui_to_xml (tool, NULL);

	/* run test */
	xst_tool_run_set_directive (tool, tool->config, N_("Testing current settings."), "test", NULL);

	/* Restore xml (it's not 'apply'!) */
	xmlFreeDoc (tool->config);
	tool->config = backup_xml;

	/* read test results */
	/* From file while reporting is not working */
	f = fopen (fname, "r");
	if (f != NULL) {
		status = getc (f);
		fclose (f);

		if (status == 1) 
			return;
	} else {
		g_warning ("Can't open file '%s' for reading.", fname);
	}

	/* something went wrong, restore gui, give information */	
	transfer_xml_to_gui (tool, NULL);

	d = xst_ok_dialog_new (N_("Restoring settings..."), GTK_WINDOW (tool->main_dialog));

	if (gtk_dialog_run (GTK_DIALOG (d)) == GTK_RESPONSE_OK)
	    gtk_widget_destroy (d);
	
	return;
}

static gint
probe_idle_cb (gpointer data)
{
	XstTool *tool = data;

	probe_x (tool, FALSE);
	transfer_xml_to_gui (tool, NULL);
	return FALSE;
}

gboolean
probe_need_cb (XstTool *tool, XstReportLine *rline, gpointer data)
{
	GtkWidget *d;

        d = xst_ok_dialog_new (_("Going to probe X server. The screen will flicker\n"
					   "several times.\nPlease be patient."), NULL);
	
	if (gtk_dialog_run (GTK_DIALOG (d)) == GTK_RESPONSE_OK)
	    gtk_widget_destroy (d);

	gtk_idle_add (probe_idle_cb, (gpointer) tool);

	return TRUE;
}

void
probe_clicked (GtkButton *button, gpointer data)
{
	XstDialog *d = XST_DIALOG (data);

	gtk_idle_add (probe_idle_cb, (gpointer) d->tool);
}

gboolean
probe_error_cb (XstTool *tool, XstReportLine *rline, gpointer data)
{
	GtkWidget *d;

	d = xst_error_dialog_new (_("Unable to probe your X server.\n"
				    "Please report it through\n"
				    "http://bugzilla.ximian.com\n"
				    "Thank you!"), NULL);

	if (gtk_dialog_run (GTK_DIALOG (d)) == GTK_RESPONSE_OK)
	    gtk_widget_destroy (d);

	return TRUE;
}
