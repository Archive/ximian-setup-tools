/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */

/* This file is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GNU Emacs; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Authors: Tambet Ingo <tambet@ximian.com>
 */

#ifndef _XST_FONT_TOOL_H_
#define _XST_FONT_TOOL_H_

#include "xst.h"
#include <libgnomeprint/gnome-font.h>

// BEGIN_XST_DECLS

#define XST_TYPE_FONT_TOOL	        (xst_font_tool_get_type ())
#define XST_FONT_TOOL(obj)	        (GTK_CHECK_CAST ((obj), XST_TYPE_FONT_TOOL, XstFontTool))
#define XST_FONT_TOOL_CLASS(klass)    (GTK_CHECK_CLASS_CAST ((klass), XST_TYPE_FONT_TOOL, XstFontToolClass))
#define XST_IS_FONT_TOOL(obj)	        (GTK_CHECK_TYPE ((obj), XST_TYPE_FONT_TOOL))
#define XST_IS_FONT_TOOL_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), XST_TYPE_FONT_TOOL))

typedef struct _XstFontTool	      XstFontTool;
typedef struct _XstFontToolClass	 XstFontToolClass;


/*****************************************************************************
 * XstFontTool functions.
 *****************************************************************************/

GtkType	xst_font_tool_get_type (void);

XstTool   *xst_font_tool_new (void);
void       xst_font_tool_construct (XstFontTool *fonttool);
void       xst_font_tool_updated (XstFontTool *fonttool);


// END_GNOME_DECLS

#endif /* _XST_FONT_TOOL_H */
