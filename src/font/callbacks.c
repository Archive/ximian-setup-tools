/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */

/* This file is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GNU Emacs; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Authors: Tambet Ingo <tambet@ximian.com>
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include "xst.h"
#include "callbacks.h"
#include "transfer.h"
#include "xst-font-tool.h"

extern XstTool *tool;

static void
store_filename (GtkFileSelection *selector, gpointer data)
{
	gchar     *fname;
	xmlDocPtr  xml;
	xmlNodePtr root;
	GtkWidget *filesel = GTK_WIDGET (data);

	fname = gtk_file_selection_get_filename (GTK_FILE_SELECTION(filesel));

	xml = xst_tool_run_get_directive (tool, _("Searching fonts."), "test", fname, NULL);
	if (xml == NULL)
		return;

	root = xst_xml_doc_get_root (xml);
	if (root == NULL)
		return;

	read_xml (tool, root);
	xst_font_tool_updated (XST_FONT_TOOL (tool));
	xst_dialog_modify (tool->main_dialog);
}

static void
filename_get ()
{
	GtkWidget *filesel;

	filesel = gtk_file_selection_new (_("Select file or dir"));

	gtk_file_selection_hide_fileop_buttons (GTK_FILE_SELECTION (filesel));
	gtk_signal_connect (GTK_OBJECT (GTK_FILE_SELECTION(filesel)->ok_button),
			    "clicked", GTK_SIGNAL_FUNC (store_filename),
			    (gpointer) filesel);

	gtk_signal_connect_object (GTK_OBJECT (GTK_FILE_SELECTION(filesel)->ok_button),
				   "clicked", GTK_SIGNAL_FUNC (gtk_widget_destroy),
				   (gpointer) filesel);

	gtk_signal_connect_object (GTK_OBJECT (GTK_FILE_SELECTION(filesel)->cancel_button),
				   "clicked", GTK_SIGNAL_FUNC (gtk_widget_destroy),
				   (gpointer) filesel);

	gtk_widget_show (filesel);
}

static void
delete_clicked (GtkButton *button, gpointer data)
{
}

static void
add_clicked (GtkButton *button, gpointer data)
{
	filename_get ();
}

void
callbacks_connect (XstTool *tool)
{
	XstDialogSignal signals[] = {
		{ "font_delete",   "clicked", delete_clicked },
		{ "font_add",      "clicked", add_clicked },
		{ NULL }
	};

	xst_dialog_connect_signals (tool->main_dialog, signals);
}
