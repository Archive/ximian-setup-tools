/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/* 
 * Copyright (C) 2000-2001 Ximian, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Tambet Ingo <tambet@ximian.com>.
 */

/* Functions for transferring information between XML tree and UI */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include "xst.h"

#include "transfer.h"
#include "xst-font.h"
#include "xst-font.h"
#include "xst-font-tool.h"


extern XstTool *tool;

void
read_xml (XstTool *tool, xmlNodePtr root)
{
	xmlNodePtr node;
	guint      i;

	g_return_if_fail (tool != NULL);
	g_return_if_fail (root != NULL);

	node = xst_xml_element_find_first (root, "font");
	while (node) {
		XstFont *font;

		font = xst_font_new_from_xml (node);
		if (font) {
			i = xst_font_list_add (font);
//			xst_font_list_add (i);

			node = xst_xml_element_find_next (node, "font");
		}
	}
}

void
transfer_xml_to_gui (XstTool *tool, gpointer data)
{
	xmlNodePtr root;

	g_return_if_fail (tool != NULL);
	root = xst_xml_doc_get_root (tool->config);
	g_return_if_fail (root != NULL);

	read_xml (tool, root);
	xst_font_tool_construct (XST_FONT_TOOL (tool));

}

void
write_xml (XstTool *tool, gpointer data)
{
	g_return_if_fail (tool != NULL);

	xst_font_list_save ();
}
