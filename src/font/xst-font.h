/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */

/* This file is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GNU Emacs; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Authors: Tambet Ingo <tambet@ximian.com>
 */

#ifndef XST_FONT_H
#define XST_FONT_H

#include "xst.h"
#include <libgnomeprint/gnome-font-face.h>

typedef enum {
	XST_FILE_UNKNOWN,
	XST_FILE_PFB,
	XST_FILE_PFA,
	XST_FILE_AFM,
	XST_FILE_TTF
} XstFontFileType;

typedef struct {
	XstFontFileType  type;
	gchar           *path;
	off_t            size;
	time_t           mtime;
} XstFontFile;


typedef enum {
	XST_FONT_UNKNOWN,
	XST_FONT_TYPE1,
	XST_FONT_TRUE,
	XST_FONT_TYPE1ALIAS,
} XstFontType;

typedef struct {
	XstFontType  type;
	gchar       *name;
	gchar       *version;
	gchar       *familyname;
	gchar       *speciesname;
	gchar       *psname;
	gchar       *weight;
	gchar       *italicangle;
	GPtrArray   *files;
} XstFont;


XstFont *xst_font_new             (void);
XstFont *xst_font_new_from_xml    (xmlNodePtr node);
void     xst_font_to_xml          (xmlNodePtr root, XstFont *font);
void     xst_font_destroy         (XstFont *font);

GList   *xst_font_family_list            (void);
void     xst_font_family_list_free       (GList *families);
GList   *xst_font_family_style_list      (const gchar *family);
void     xst_font_family_style_list_free (GList *styles);


void     xst_font_type_set        (XstFont *font, const gchar *value);
void     xst_font_name_set        (XstFont *font, const gchar *value);
void     xst_font_version_set     (XstFont *font, const gchar *value);
void     xst_font_familyname_set  (XstFont *font, const gchar *value);
void     xst_font_speciesname_set (XstFont *font, const gchar *value);
void     xst_font_psname_set      (XstFont *font, const gchar *value);
void     xst_font_weight_set      (XstFont *font, const gchar *value);
void     xst_font_italicangle_set (XstFont *font, const gchar *value);
void     xst_font_add_file        (XstFont *font, XstFontFile *ffile);

const gchar *xst_font_type_get        (XstFont *font);
const gchar *xst_font_name_get        (XstFont *font);
const gchar *xst_font_version_get     (XstFont *font);
const gchar *xst_font_familyname_get  (XstFont *font);
const gchar *xst_font_speciesname_get (XstFont *font);
const gchar *xst_font_psname_get      (XstFont *font);
const gchar *xst_font_weight_get      (XstFont *font);
const gchar *xst_font_italicangle_get (XstFont *font);
GList       *xst_font_get_file        (XstFont *font);

GnomeFontFace *xst_font_family_get_face_by_stylename (const gchar *family, const gchar *style);

gint xst_font_list_size (void);
gint xst_font_list_add (XstFont *font);
gint xst_font_list_remove (XstFont *font);
XstFont *xst_font_list_nth (gint n);
void xst_font_list_save (void);

#endif /* XST_FONT_H */
