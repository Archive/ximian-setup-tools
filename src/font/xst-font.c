/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */

/* This file is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GNU Emacs; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Authors: Tambet Ingo <tambet@ximian.com>
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include "xst-font.h"
#include <libgnomeprint/gnome-font-face-private.h>

XstTool *tool;


static void xst_font_save_file (XstFont *font, xmlNodePtr node);

/* XstFontFile start */

static XstFontFile *
xst_font_file_new ()
{
	XstFontFile *file;

	file = g_new0 (XstFontFile, 1);
	g_return_val_if_fail (file != NULL, NULL);

	return file;
}

static void
xst_font_file_type_set (XstFontFile *ffile, gchar *value)
{
	g_return_if_fail (ffile != NULL);
	g_return_if_fail (value != NULL);

	if (strcmp (value, "pfb") == 0) {
		ffile->type = XST_FILE_PFB;
		return;
	}

	if (strcmp (value, "pfa") == 0) {
		ffile->type = XST_FILE_PFA;
		return;
	}

	if (strcmp (value, "afm") == 0) {
		ffile->type = XST_FILE_AFM;
		return;
	}

	if (strcmp (value, "ttf") == 0) {
		ffile->type = XST_FILE_TTF;
		return;
	}

	g_warning ("xst_font_file_set_type: Unknown font type: '%s'.\n", value);
	ffile->type = XST_FILE_UNKNOWN;
}

static const gchar *
xst_font_file_type_get (XstFontFile *ffile)
{
	g_return_val_if_fail (ffile != NULL, NULL);

	switch (ffile->type) {
	case XST_FILE_PFB: return "pfb"; break;
	case XST_FILE_PFA: return "pfa"; break;
	case XST_FILE_AFM: return "afm"; break;
	case XST_FILE_TTF: return "ttf"; break;
	case XST_FILE_UNKNOWN:
	default:
		g_warning ("xst_font_file_get_type: Unknown font type.\n");
	}
	
	return NULL;
}

static void
xst_font_file_path_set (XstFontFile *ffile, gchar *value)
{
	g_return_if_fail (ffile != NULL);
	g_return_if_fail (value != NULL);

	if (ffile->path) g_free (ffile->path);
	ffile->path = g_strdup (value);
}

static const gchar *
xst_font_file_path_get (XstFontFile *ffile)
{
	g_return_val_if_fail (ffile != NULL, NULL);

	return ffile->path;
}

static void
xst_font_file_size_set (XstFontFile *ffile, gchar *value)
{
	g_return_if_fail (ffile != NULL);
	g_return_if_fail (value != NULL);

	ffile->size = g_strtod (value, NULL);
}

static const gchar *
xst_font_file_size_get (XstFontFile *ffile)
{
	g_return_val_if_fail (ffile != NULL, NULL);

	return g_strdup_printf ("%l", ffile->size);
}

static void
xst_font_file_mtime_set (XstFontFile *ffile, gchar *value)
{
	g_return_if_fail (ffile != NULL);
	g_return_if_fail (value != NULL);

	ffile->mtime = g_strtod (value, NULL);
}

static const gchar *
xst_font_file_mtime_get (XstFontFile *ffile)
{
	g_return_val_if_fail (ffile != NULL, NULL);

	return g_strdup_printf ("%l", ffile->mtime);
}

static XstFontFile *
xst_font_file_new_from_xml (xmlNodePtr node)
{
	XstFontFile *file;
	gchar *buf;

	g_return_val_if_fail (node != NULL, NULL);

	file = xst_font_file_new ();
	if (file == NULL) {
		/* FIXME: add warning */
		return NULL;
	}

	/* Type */
	buf = xst_xml_element_get_attribute (node, "type");
	if (buf) {
		xst_font_file_type_set (file, buf);
		g_free (buf);
	}

	/* Path */
	buf = xst_xml_element_get_attribute (node, "path");
	if (buf) {
		xst_font_file_path_set (file, buf);
		g_free (buf);
	}

	/* File size */
	buf = xst_xml_element_get_attribute (node, "size");
	if (buf) {
		xst_font_file_size_set (file, buf);
		g_free (buf);
	}

	/* Modification time */
	buf = xst_xml_element_get_attribute (node, "mtime");
	if (buf) {
		xst_font_file_mtime_set (file, buf);
		g_free (buf);
	}

	return file;
}

static void
xst_font_file_to_xml (xmlNodePtr node, XstFontFile *ffile)
{
	xmlNodePtr  n;
	const gchar      *buf;

	g_return_if_fail (node != NULL);
	g_return_if_fail (ffile != NULL);

	n = xst_xml_element_add (node, "file");

	/* Type */
	buf = xst_font_file_type_get (ffile);
	if (buf)
		xst_xml_element_set_attribute (n, "type", g_strdup (buf));

	/* Path */
	buf = xst_font_file_path_get (ffile);
	if (buf)
		xst_xml_element_set_attribute (n, "path", g_strdup (buf));

	/* File size */
	buf = xst_font_file_size_get (ffile);
	if (buf)
		xst_xml_element_set_attribute (n, "size", buf);

	/* Modification time */
	buf = xst_font_file_mtime_get (ffile);
	if (buf)
		xst_xml_element_set_attribute (n, "mtime", buf);
}

static void
xst_font_file_destroy (XstFontFile *file)
{
	g_return_if_fail (file != NULL);

	if (file->path) g_free (file->path);

	g_free (file);
}


/* XstFontFile end */

/* XstFont start */

XstFont *
xst_font_new ()
{
	XstFont *font;

	font = g_new0 (XstFont, 1);
	g_return_val_if_fail (font != NULL, NULL);

	font->files = g_ptr_array_new ();
	return font;
}

XstFont *
xst_font_new_from_xml (xmlNodePtr node)
{
	XstFont    *font;
	xmlNodePtr  n;
	gchar      *buf;

	g_return_val_if_fail (node != NULL, NULL);

	font = xst_font_new ();
	if (font == NULL) {
		/* give warning */
		return NULL;
	}

	/* Type */
	buf = xst_xml_element_get_attribute (node, "format");
	if (buf) {
		xst_font_type_set (font, buf);
		g_free (buf);
	}

	/* Name */
	buf = xst_xml_element_get_attribute (node, "name");
	if (buf) {
		xst_font_name_set (font, buf);
		g_free (buf);
	}

	/* Version */
	buf = xst_xml_element_get_attribute (node, "version");
	if (buf) {
		xst_font_version_set (font, buf);
		g_free (buf);
	}

	/* Family name */
	buf = xst_xml_element_get_attribute (node, "familyname");
	if (buf) {
		xst_font_familyname_set (font, buf);
		g_free (buf);
	}

	/* Species name */
	buf = xst_xml_element_get_attribute (node, "speciesname");
	if (buf) {
		xst_font_speciesname_set (font, buf);
		g_free (buf);
	}

	/* Postscript name */
	buf = xst_xml_element_get_attribute (node, "psname");
	if (buf) {
		xst_font_psname_set (font, buf);
		g_free (buf);
	}

	/* Weight */
	buf = xst_xml_element_get_attribute (node, "weight");
	if (buf) {
		xst_font_weight_set (font, buf);
		g_free (buf);
	}

	/* Italic angle */
	buf = xst_xml_element_get_attribute (node, "italic angle");
	if (buf) {
		xst_font_italicangle_set (font, buf);
		g_free (buf);
	}

	/* Font files */
	for (n = xst_xml_element_find_first (node, "file");
		n != NULL;
		n = xst_xml_element_find_next (n, "file")) {

		XstFontFile *ffile;

		ffile = xst_font_file_new_from_xml (n);
		xst_font_add_file (font, ffile);
	}

	return font;
}

void
xst_font_to_xml (xmlNodePtr root, XstFont *font)
{
	xmlNodePtr  node;
	const gchar      *buf;

	g_return_if_fail (root != NULL);
	g_return_if_fail (font != NULL);

	node = xst_xml_element_add (root, "font");

	/* Type */
	buf = xst_font_type_get (font);
	if (buf)
		xst_xml_element_set_attribute (node, "format", g_strdup (buf));

	/* Name */
	buf = xst_font_name_get (font);	
	if (buf)
		xst_xml_element_set_attribute (node, "name", g_strdup (buf));

	/* Version */
	buf = xst_font_version_get (font);	
	if (buf)
		xst_xml_element_set_attribute (node, "version", g_strdup (buf));

	/* Family name */
	buf = xst_font_familyname_get (font);	
	if (buf)
		xst_xml_element_set_attribute (node, "familyname", g_strdup (buf));

	/* Species name */
	buf = xst_font_speciesname_get (font);	
	if (buf)
		xst_xml_element_set_attribute (node, "speciesname", g_strdup (buf));

	/* Postscript name */
	buf = xst_font_psname_get (font);	
	if (buf)
		xst_xml_element_set_attribute (node, "psname", g_strdup (buf));

	/* Weight */
	buf = xst_font_weight_get (font);	
	if (buf)
		xst_xml_element_set_attribute (node, "weight", g_strdup (buf));

	/* Italic angle */
	buf = xst_font_italicangle_get (font);	
	if (buf)
		xst_xml_element_set_attribute (node, "italicangle", g_strdup (buf));

	/* Font files */
	xst_font_save_file (font, node);
}

void
xst_font_type_set (XstFont *font, const gchar *value)
{
	g_return_if_fail (font != NULL);
	g_return_if_fail (value != NULL);

	if (strcmp (value, "truetype") == 0) {
		font->type = XST_FONT_TRUE;
		return;
	}

	if (strcmp (value, "type1") == 0) {
		font->type = XST_FONT_TYPE1;
		return;
	}

	if (strcmp (value, "type1alias") == 0) {
		font->type = XST_FONT_TYPE1ALIAS;
		return;
	}

	g_warning ("xst_font_type_set: Unknown font type '%s'.", value);
	font->type = XST_FONT_UNKNOWN;
}

const gchar *
xst_font_type_get (XstFont *font)
{
	g_return_val_if_fail (font != NULL, NULL);

	switch (font->type) {
	case XST_FONT_TRUE: return "truetype"; break;
	case XST_FONT_TYPE1: return "type1"; break;
	case XST_FONT_TYPE1ALIAS: return "type1alias"; break;
	default:
		g_warning ("xst_font_type_get: Unknown font type.");
	}

	return NULL;
}

void
xst_font_name_set (XstFont *font, const gchar *value)
{
	g_return_if_fail (font != NULL);
	g_return_if_fail (value != NULL);

	if (font->name) g_free (font->name);
	font->name = g_strdup (value);
}

const gchar *
xst_font_name_get (XstFont *font)
{
	g_return_val_if_fail (font != NULL, NULL);

	return font->name;
}

void
xst_font_version_set (XstFont *font, const gchar *value)
{
	g_return_if_fail (font != NULL);
	g_return_if_fail (value != NULL);

	if (font->version) g_free (font->version);
	font->version = g_strdup (value);
}

const gchar *
xst_font_version_get (XstFont *font)
{
	g_return_val_if_fail (font != NULL, NULL);

	return font->version;
}

void
xst_font_familyname_set (XstFont *font, const gchar *value)
{
	g_return_if_fail (font != NULL);
	g_return_if_fail (value != NULL);

	if (font->familyname) g_free (font->familyname);
	font->familyname = g_strdup (value);
}

const gchar *
xst_font_familyname_get (XstFont *font)
{
	g_return_val_if_fail (font != NULL, NULL);

	return font->familyname;
}

void
xst_font_speciesname_set (XstFont *font, const gchar *value)
{
	g_return_if_fail (font != NULL);
	g_return_if_fail (value != NULL);

	if (font->speciesname) g_free (font->speciesname);
	font->speciesname = g_strdup (value);
}

const gchar *
xst_font_speciesname_get (XstFont *font)
{
	g_return_val_if_fail (font != NULL, NULL);

	return font->speciesname;
}

void
xst_font_psname_set (XstFont *font, const gchar *value)
{
	g_return_if_fail (font != NULL);
	g_return_if_fail (value != NULL);

	if (font->psname) g_free (font->psname);
	font->psname = g_strdup (value);
}

const gchar *
xst_font_psname_get (XstFont *font)
{
	g_return_val_if_fail (font != NULL, NULL);

	return font->psname;
}

void
xst_font_weight_set (XstFont *font, const gchar *value)
{
	g_return_if_fail (font != NULL);
	g_return_if_fail (value != NULL);

	if (font->weight) g_free (font->weight);
	font->weight = g_strdup (value);
}

const gchar *
xst_font_weight_get (XstFont *font)
{
	g_return_val_if_fail (font != NULL, NULL);

	return font->weight;
}

void
xst_font_italicangle_set (XstFont *font, const gchar *value)
{
	g_return_if_fail (font != NULL);
	g_return_if_fail (value != NULL);

	if (font->italicangle) g_free (font->italicangle);
	font->italicangle = g_strdup (value);
}

const gchar *
xst_font_italicangle_get (XstFont *font)
{
	g_return_val_if_fail (font != NULL, NULL);

	return font->italicangle;
}

void
xst_font_add_file (XstFont *font, XstFontFile *ffile)
{
	g_ptr_array_add (font->files, (gpointer) ffile);
}

GList *
xst_font_get_file (XstFont *font)
{
	gint len, i;
	const gchar *type;
	XstFontFile *ffile;
	GList       *list = NULL;

	g_return_val_if_fail (font != NULL, NULL);

	len = font->files->len;
	for (i = 0; i < len; i++) {
		ffile = g_ptr_array_index (font->files, i);
		if (ffile) {
			type = xst_font_file_type_get (ffile);			
			if (!type)
				continue;

			if ((strcmp (type, "pfb") != 0) ||
			    (strcmp (type, "ttf") != 0))
				list = g_list_prepend (list,
						       (gpointer) xst_font_file_path_get (ffile));
			else
				list = g_list_append (list,
						      (gpointer) xst_font_file_path_get (ffile));
		}
	}

	return list;
}

static void
xst_font_save_file (XstFont *font, xmlNodePtr node)
{
	gint len, i;
	XstFontFile *ffile;

	g_return_if_fail (font != NULL);
	g_return_if_fail (node != NULL);

	len = font->files->len;
	for (i = 0; i < len; i++) {
		ffile = g_ptr_array_index (font->files, i);
		if (ffile)
			xst_font_file_to_xml (node, ffile);
	}
}

void
xst_font_destroy (XstFont *font)
{
	gint i;

	g_return_if_fail (font != NULL);

	if (font->name)        g_free (font->name);
	if (font->version)     g_free (font->version);
	if (font->familyname)  g_free (font->familyname);
	if (font->speciesname) g_free (font->speciesname);
	if (font->psname)      g_free (font->psname);
	if (font->weight)      g_free (font->weight);
	if (font->italicangle) g_free (font->italicangle);

	if (font->files) {
		for (i = 0; i < font->files->len; i++)
			xst_font_file_destroy (g_ptr_array_index (font->files, i));

		g_ptr_array_free (font->files, FALSE);
	}

	g_free (font);
}



static GPtrArray *font_list = NULL;


gint
xst_font_list_size (void)
{
	if (font_list == NULL)
		return -1;

	return font_list->len;
}

gint
xst_font_list_add (XstFont *font)
{
	g_return_val_if_fail (font != NULL, 0);

	if (font_list == NULL)
		font_list = g_ptr_array_new ();

	g_ptr_array_add (font_list, (gpointer) font);
	return xst_font_list_size ();
}

gint
xst_font_list_remove (XstFont *font)
{
	g_return_val_if_fail (font != NULL, 0);

	if (font_list == NULL)
		return -1;

	if (g_ptr_array_remove (font_list, (gpointer) font) == FALSE)
		return -1;

	return xst_font_list_size ();
}

XstFont *
xst_font_list_nth (gint n)
{
	if (font_list == NULL)
		return NULL;

	if (xst_font_list_size () < n) {
		g_warning ("xst_font_tool_font_nth: Trying to access non-existant value.");
		return NULL;
	}

	return g_ptr_array_index (font_list, n);
}

void
xst_font_list_save (void)
{
	gint        i, len;
	XstFont    *font;
	xmlNodePtr  root;

	if (font_list == NULL)
		return;

	xst_xml_doc_destroy (XST_TOOL (tool)->config);
	XST_TOOL (tool)->config = xst_xml_doc_create (XST_TOOL (tool)->name);
	root = xst_xml_doc_get_root (XST_TOOL (tool)->config);

	len = xst_font_list_size ();
	for (i = 0; i < len; i++) {
		font = g_ptr_array_index (font_list, i);
		if (font)
			xst_font_to_xml (root, font);
	}
}






static gboolean
my_glist_exists (GList *list, const gchar *name)
{
	GList    *tmp;
	gchar   *tmp_name;

	if (list == NULL || name == NULL)
		return FALSE;

	tmp = list;
	while (tmp) {
		tmp_name = tmp->data;
		tmp = g_list_next (tmp);

		if (tmp_name && (strcmp (name, tmp_name) == 0))
			return TRUE;
	}

	return FALSE;
}

GList *
xst_font_family_list (void)
{
	XstFont *font;
	gint    i, len;
	const gchar *name;
	GList   *list = NULL;

	len = xst_font_list_size ();
	for (i = 0; i < len; i++) {
		font = xst_font_list_nth (i);
		name = xst_font_familyname_get (font);
		if (font && ! my_glist_exists (list, name))
			list = g_list_append (list, (gpointer) name);
	}

	return list;
}

void
xst_font_family_list_free (GList *families)
{
	if (families)
		g_list_free (families);
}

GList *
xst_font_family_style_list (const gchar *family)
{
	XstFont *font;
	gint    i, len;
	const gchar *tmp;
	GList   *list = NULL;

	len = xst_font_list_size ();
	for (i = 0; i < len; i++) {
		font = xst_font_list_nth (i);
		if (font) {
			tmp = xst_font_familyname_get (font);
			if (tmp && (strcmp (tmp, family) == 0))
				list = g_list_append (list, (gpointer) xst_font_speciesname_get (font));
		}
	}

	return list;
}

void
xst_font_family_style_list_free (GList *styles)
{
	if (styles)
		g_list_free (styles);
}

GnomeFontFace *
xst_font_family_get_face_by_stylename (const gchar *family, const gchar *style)
{
	gint           i, len;
	XstFont       *font;
	const gchar   *tmp_fam, *tmp_sty;
	GnomeFontFace *face = NULL;

	len = xst_font_list_size ();
	for (i = 0; i < len; i++) {
		font = xst_font_list_nth (i);
		if (font) {
			tmp_fam = xst_font_familyname_get (font);
			tmp_sty = xst_font_speciesname_get (font);

			if (tmp_fam && tmp_sty &&
			    (strcmp (family, tmp_fam) == 0) &&
			    (strcmp (style,  tmp_sty) == 0)) {
				/* const gchar *name = xst_font_name_get (font);
				   face = gnome_font_face_new (name); */
				GList *list;
				const gchar *filename;

				list = xst_font_get_file (font);
				if (list == 0)
					break;

				list = g_list_first (list);
				filename = list->data;
				list = g_list_remove (list, (gpointer) filename);
				face = gnome_font_face_private_from_files (filename, 0, list);
				g_list_free (list);
				break;
			}
		}
	}

	return face;
}

/* XstFont end */
