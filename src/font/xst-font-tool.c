/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */

/* This file is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GNU Emacs; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Authors: Tambet Ingo <tambet@ximian.com>
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include "xst.h"
#include "xst-font-tool.h"

#include <stdlib.h>
#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>
#include <libgnomeprint/gnome-font-dialog.h>
#include <libgnomeui/gnome-stock.h>
#include <libgnomeui/gnome-canvas-rect-ellipse.h>


#include "xst-font.h"

#ifdef HAVE_STRING_H
#include <string.h>
#endif

struct _XstFontTool
{
	XstTool tool;
  
	GtkWidget *family;
	GtkWidget *style;
	GtkWidget *size;

	GtkWidget *previewcontainer;

	gchar         *selectedfamily;
	GnomeFontFace *selectedface;
	GnomeFont     *selectedfont;
	gdouble        selectedsize;

	GtkWidget     *preview;
};


struct _XstFontToolClass
{
	XstToolClass parent_class;

	void (* font_set) (XstFontTool *fonttool, GnomeFont *font);
};

enum {FONT_SET, LAST_SIGNAL};

/* These are what we use as the standard font sizes, for the size clist.
   Note that when using points we still show these integer point values but
   we work internally in decipoints (and decipoint values can be typed in). */
static gchar * font_sizes[] = {
  "8", "9", "10", "11", "12", "13", "14", "16", "18", "20", "22", "24", "26", "28",
  "32", "36", "40", "48", "56", "64", "72"
};


static void xst_font_tool_class_init (XstFontToolClass *klass);
static void xst_font_tool_init       (XstFontTool *fonttool);
static void xst_font_tool_destroy    (GtkObject *object);

static void xst_font_tool_select_family (GtkCList *clist, gint row, gint column, GdkEvent *event, gpointer data);
static void xst_font_tool_select_style (GtkCList *clist, gint row, gint column, GdkEvent *event, gpointer data);
static void xst_font_tool_select_size (GtkEditable *editable, gpointer data);
static void xst_font_tool_fill_families (XstFontTool *fonttool);
static void xst_font_tool_fill_styles (XstFontTool *fonttool);
static void update_preview (XstFontTool *fonttool, GnomeFont *font, gpointer data);

static XstToolClass *parent_class = NULL;
static guint fonttool_signals[LAST_SIGNAL] = {0};

GtkType
xst_font_tool_get_type ()
{
	static GtkType font_tool_type = 0;
	if (!font_tool_type) {
		static const GtkTypeInfo fonttool_type_info = {
			"XstFontTool",
			sizeof (XstFontTool),
			sizeof (XstFontToolClass),
			(GtkClassInitFunc) xst_font_tool_class_init,
			(GtkObjectInitFunc) xst_font_tool_init,
			NULL, NULL,
			(GtkClassInitFunc) NULL,
		};
		font_tool_type = gtk_type_unique (XST_TYPE_TOOL, &fonttool_type_info);
	}
	return font_tool_type;
}

static void
xst_font_tool_class_init (XstFontToolClass *klass)
{
	GtkObjectClass *object_class;
  
	object_class = (GtkObjectClass *) klass;
  
	parent_class = gtk_type_class (XST_TYPE_TOOL);

	fonttool_signals[FONT_SET] = gtk_signal_new ("font_set",
										GTK_RUN_LAST,
										object_class->type,
										GTK_SIGNAL_OFFSET (XstFontToolClass, font_set),
										gtk_marshal_NONE__POINTER,
										GTK_TYPE_NONE,
										1, GTK_TYPE_OBJECT);
	gtk_object_class_add_signals (object_class, fonttool_signals, LAST_SIGNAL);

	object_class->destroy = xst_font_tool_destroy;
}

static void
xst_font_tool_init (XstFontTool *fonttool)
{
	fonttool->family = NULL;
	fonttool->style = NULL;
	fonttool->size = NULL;

	fonttool->previewcontainer = NULL;

	fonttool->selectedfamily = NULL;
	fonttool->selectedface = NULL;
	fonttool->selectedfont = NULL;
	fonttool->selectedsize = 0;

	fonttool->preview = NULL;
}

static void
xst_font_tool_destroy (GtkObject *object)
{
	XstFontTool *fonttool;
  
	g_return_if_fail (object != NULL);
	g_return_if_fail (XST_IS_FONT_TOOL (object));
  
	fonttool = XST_FONT_TOOL (object);

	if (fonttool->selectedfont) gnome_font_unref (fonttool->selectedfont);
	if (fonttool->selectedface) gnome_font_face_unref (fonttool->selectedface);
	if (fonttool->selectedfamily) g_free (fonttool->selectedfamily);

  	if (GTK_OBJECT_CLASS (parent_class)->destroy)
		(* GTK_OBJECT_CLASS (parent_class)->destroy) (object);
}

void
xst_font_tool_construct (XstFontTool *fonttool)
{
	static GList * sizelist = NULL;
	GtkWidget *c;

	g_return_if_fail (fonttool != NULL);
	g_return_if_fail (XST_IS_FONT_TOOL (fonttool));

	/* Family frame */

	fonttool->family = xst_dialog_get_widget (XST_TOOL (fonttool)->main_dialog, "family");
	gtk_clist_set_auto_sort (GTK_CLIST (fonttool->family), TRUE);
	gtk_signal_connect (GTK_OBJECT (fonttool->family), "select_row",
			    GTK_SIGNAL_FUNC (xst_font_tool_select_family), fonttool);

	fonttool->selectedfamily = NULL;

	/* Stylebox */

	fonttool->style = xst_dialog_get_widget (XST_TOOL (fonttool)->main_dialog, "style");
	gtk_clist_set_auto_sort (GTK_CLIST (fonttool->style), TRUE);
	gtk_signal_connect (GTK_OBJECT (fonttool->style), "select_row",
			    GTK_SIGNAL_FUNC (xst_font_tool_select_style), fonttool);

	fonttool->selectedface = NULL;

	/* Sizebox */

	c = xst_dialog_get_widget (XST_TOOL (fonttool)->main_dialog, "size");
	fonttool->size = c;
	gtk_signal_connect (GTK_OBJECT (((GtkCombo *) c)->entry), "changed",
			    GTK_SIGNAL_FUNC (xst_font_tool_select_size), fonttool);

	if (!sizelist) {
		gint i;
		for (i = 0; i < (sizeof (font_sizes) / sizeof (font_sizes[0])); i++) {
			sizelist = g_list_prepend (sizelist, font_sizes[i]);
		}
		sizelist = g_list_reverse (sizelist);
	}

	gtk_combo_set_popdown_strings ((GtkCombo *) c, sizelist);

	gtk_entry_set_text ((GtkEntry *) ((GtkCombo *) c)->entry, "12");
	fonttool->selectedsize = 12.0;

	/* Preview */

	fonttool->previewcontainer = xst_dialog_get_widget (XST_TOOL (fonttool)->main_dialog, "preview_container");
	fonttool->preview = gnome_font_preview_new ();
	gtk_widget_show (fonttool->preview);
	gtk_container_add (GTK_CONTAINER (fonttool->previewcontainer), fonttool->preview);

	gtk_signal_connect (GTK_OBJECT (fonttool), "font_set",
			    GTK_SIGNAL_FUNC (update_preview), NULL);


	xst_font_tool_fill_families (fonttool);

	/* Select first font in family list */

	gtk_clist_select_row ((GtkCList *) fonttool->family, 0, 0);	
}

XstTool *
xst_font_tool_new()
{
	XstFontTool *fonttool;
  
	fonttool = gtk_type_new (XST_TYPE_FONT_TOOL);
  	
	return XST_TOOL (fonttool);
}

void
xst_font_tool_updated (XstFontTool *fonttool)
{
	g_return_if_fail (fonttool != NULL);
	g_return_if_fail (XST_IS_FONT_TOOL (fonttool));

	xst_font_tool_fill_families (fonttool);
}	

static void
xst_font_tool_select_family (GtkCList *clist, gint row, gint column, GdkEvent *event, gpointer data)
{
	XstFontTool     *fonttool;
	gchar           *familyname;

	fonttool = XST_FONT_TOOL (data);

	gtk_clist_get_text (clist, row, column, &familyname);

	if (fonttool->selectedfamily) {
		g_free (fonttool->selectedfamily);
	}

	if (familyname)
		fonttool->selectedfamily = g_strdup (familyname);
	else
		fonttool->selectedfamily = NULL;

	xst_font_tool_fill_styles (fonttool);
}

static void
xst_font_tool_select_style (GtkCList *clist, gint row, gint column, GdkEvent *event, gpointer data)
{
	XstFontTool *fonttool;
	gchar       *style;

	fonttool = XST_FONT_TOOL (data);

	if (!fonttool->selectedfamily) return;

	gtk_clist_get_text (clist, row, column, &style);

	if (fonttool->selectedface) gnome_font_face_unref (fonttool->selectedface);
	fonttool->selectedface = xst_font_family_get_face_by_stylename (fonttool->selectedfamily, style);
//	fonttool->selectedface = gnome_font_family_get_face_by_stylename (fonttool->selectedfamily, style);

	if (fonttool->selectedfont) gnome_font_unref (fonttool->selectedfont);
	fonttool->selectedfont = gnome_font_face_get_font_default (fonttool->selectedface, fonttool->selectedsize);

	gtk_signal_emit (GTK_OBJECT (fonttool), fonttool_signals[FONT_SET], fonttool->selectedfont);
}

static void
xst_font_tool_select_size (GtkEditable *editable, gpointer data)
{
	XstFontTool *fonttool;
	gchar       *sizestr;

	fonttool = XST_FONT_TOOL (data);

	if (!fonttool->selectedface) return;

	sizestr = gtk_editable_get_chars (GTK_EDITABLE (GTK_COMBO (fonttool->size)->entry), 0, -1);

	fonttool->selectedsize = MAX (atoi (sizestr), 1.0);

	g_free (sizestr);

	if (fonttool->selectedfont) gnome_font_unref (fonttool->selectedfont);
	fonttool->selectedfont = gnome_font_face_get_font_default (fonttool->selectedface, fonttool->selectedsize);

	gtk_signal_emit (GTK_OBJECT (fonttool), fonttool_signals[FONT_SET], fonttool->selectedfont);
}

static void
xst_font_tool_fill_families (XstFontTool *fonttool)
{
	GList * families, * l;

	families = xst_font_family_list ();
	g_return_if_fail (families != NULL);

	gtk_clist_freeze ((GtkCList *) fonttool->family);
	gtk_clist_clear ((GtkCList *) fonttool->family);

	for (l = families; l != NULL; l = l->next) {
		gtk_clist_append ((GtkCList *) fonttool->family, (gchar **) &l->data);
	}

	gtk_clist_thaw ((GtkCList *) fonttool->family);

	xst_font_family_list_free (families);
}

static void
xst_font_tool_fill_styles (XstFontTool *fonttool)
{
	GList * styles, * l;

	gtk_clist_freeze ((GtkCList *) fonttool->style);
	gtk_clist_clear ((GtkCList *) fonttool->style);

	if (fonttool->selectedfamily) {
		styles = xst_font_family_style_list (fonttool->selectedfamily);
		for (l = styles; l != NULL; l = l->next) {
			gtk_clist_append ((GtkCList *) fonttool->style, (gchar **) &l->data);
		}
		xst_font_family_style_list_free (styles);
	}

	gtk_clist_thaw ((GtkCList *) fonttool->style);

	/* Select first font in style list */

	gtk_clist_select_row ((GtkCList *) fonttool->style, 0, 0);
}

static void
update_preview (XstFontTool *fonttool, GnomeFont *font, gpointer data)
{
	gnome_font_preview_set_font ((GnomeFontPreview *) fonttool->preview, font);
}

