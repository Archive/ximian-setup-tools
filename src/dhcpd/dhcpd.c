/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2001 Ximian, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Chema Celorio <chema@celorio.com>
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <gnome.h>
#include <glade/glade.h>

#include "xst.h"
#include "xst-ip.h"
#include "dhcpd.h"
#include "dhcpd-subnet.h"
#include "dhcpd-range.h"
#include "dhcpd-host.h"

XST_TOOL_MAKE_TYPE(dhcpd, Dhcpd)

static void
xst_dhcpd_tool_type_init (XstDhcpdTool *tool)
{
}

static void
xst_dhcpd_tool_populate_advanced (XstDhcpdTool *tool)
{
	XstDhcpdRange *range = NULL;
	XstDhcpdSubnet *subnet = NULL;

	g_implement_me ();

	g_assert (tool->subnets);
	subnet = tool->subnets->data;
	g_assert (subnet->ranges);
	range = subnet->ranges->data;
	
	tool->selected_subnet = subnet;
	tool->selected_range   = range;
	g_print ("Set %x\n", GPOINTER_TO_INT (subnet));
}

#define my_clear(__a__) gtk_entry_set_text (GTK_ENTRY (tool->__a__), "");
#define my_set(__a__,__b__) gtk_entry_set_text (GTK_ENTRY (tool->__a__),\
                                      xst_ip_get_string (__b__));

static void
xst_dhcpd_tool_populate_basic (XstDhcpdTool *tool)
{
	XstDhcpdRange *range = NULL;
	XstDhcpdSubnet *subnet = NULL;

	g_assert (tool->subnets);
	subnet = tool->subnets->data;
	g_assert (subnet->ranges);
	range = subnet->ranges->data;
	
	tool->selected_subnet = subnet;
	tool->selected_range  = range;
	
	my_set (range_start, range->start);
	my_set (range_end,   range->end);
	my_set (subnet_mask, subnet->mask);
	gtk_entry_set_text (GTK_ENTRY (tool->domain), subnet->domain);
	my_set (dns1, subnet->dns1);
	my_set (dns2, subnet->dns2);
	my_set (router, subnet->router);
}
#undef my_set
#undef my_clear

void
xst_dhcpd_tool_populate (XstDhcpdTool *tool)
{
	XstTool *xst_tool = XST_TOOL (tool);
	
	if (xst_tool->main_dialog->complexity == XST_DIALOG_ADVANCED)
		xst_dhcpd_tool_populate_advanced (tool);
	else
		xst_dhcpd_tool_populate_basic (tool);
}

void
xst_dhcpd_tool_update_complexity_cb (GtkObject *main_dialog, XstTool *xst_tool)
{
	XstDhcpdTool *tool = XST_DHCPD_TOOL (xst_tool);

	if (!xst_dhcpd_tool_verify_mode (tool)) {
		GtkWidget *dialog;
		dialog = gnome_ok_dialog (_("You can't run this tool in simple mode because you have "
					    "either more than one range or more than one subnets defined\n"));
		gtk_window_set_title (GTK_WINDOW (dialog), _("Can't run in simple mode."));
		gnome_dialog_run_and_close (GNOME_DIALOG (dialog));
	}
}


/**
 * xst_dhcpd_tool_verify_mode:
 * @tool: 
 * 
 * 
 * 
 * Return Value: TRUE if mode was not changed, FALSE if it was
 **/
gboolean
xst_dhcpd_tool_verify_mode (XstDhcpdTool *tool)
{
	XstTool *xst_tool = XST_TOOL (tool);
	gboolean multiple_subnets = FALSE;
	gboolean multiple_ranges = FALSE;

	/* If we have more than one subnet, or if we have more than one
	 * ranges in the subnet we need to run in ADVANCED mode
	 */
	multiple_subnets = tool->subnets && (g_list_length (tool->subnets) > 1);
	if (multiple_subnets) {
		XstDhcpdSubnet * subnet = tool->subnets->data;
		multiple_ranges = subnet->ranges && (g_list_length (subnet->ranges) > 1);
	}

	if (!multiple_ranges && !multiple_subnets)
		return TRUE; 

	if (XST_DIALOG_ADVANCED == xst_tool->main_dialog->complexity)
		return TRUE;
	
	xst_dialog_set_complexity (xst_tool->main_dialog,
				   XST_DIALOG_ADVANCED);
	return FALSE;
}

XstTool *
xst_dhcpd_tool_new (void)
{
	XstDhcpdTool *tool;
	XstTool *xst_tool;
	
	xst_tool = XST_TOOL (gtk_type_new (XST_TYPE_DHCPD_TOOL));

	tool = XST_DHCPD_TOOL (xst_tool);
	tool->installed = FALSE;

	return xst_tool;
}

void
xst_dhcpd_tool_load_widgets (XstDhcpdTool *tool)
{
	XstTool *xst_tool = XST_TOOL (tool);
	XstDialog *dialog;

	dialog = xst_tool->main_dialog;
	/* Widgets */
	tool->range_start = xst_dialog_get_widget (dialog, "range_start_entry");
	tool->range_end   = xst_dialog_get_widget (dialog, "range_end_entry");
	tool->subnet_mask = xst_dialog_get_widget (dialog, "subnetmask_entry");
	tool->domain      = xst_dialog_get_widget (dialog, "domain_entry");
	tool->dns1        = xst_dialog_get_widget (dialog, "dns1_entry");
	tool->dns2        = xst_dialog_get_widget (dialog, "dns2_entry");
	tool->router      = xst_dialog_get_widget (dialog, "router_entry");

	xst_ip_entry_apply_filters (GTK_ENTRY (tool->range_start));
	xst_ip_entry_apply_filters (GTK_ENTRY (tool->range_end));
	xst_ip_entry_apply_filters (GTK_ENTRY (tool->subnet_mask));
	xst_ip_entry_apply_filters (GTK_ENTRY (tool->dns1));
	xst_ip_entry_apply_filters (GTK_ENTRY (tool->dns2));
	xst_ip_entry_apply_filters (GTK_ENTRY (tool->router));

	xst_dhcpd_hosts_editor_init (tool);
}

gboolean
xst_dhcpd_tool_verify_apply (XstDhcpdTool *tool)
{
	g_print ("Verify\n");
	
	return FALSE;
}
