/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2001 Ximian, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Chema Celorio <chema@ximian.com>
 *
 */

#ifndef __XST_DHCPD_SUBNET_H__
#define __XST_DHCPD_SUBNET_H__

#include "xst-xml.h"
#include "xst-ip.h"

typedef struct _XstDhcpdSubnet      XstDhcpdSubnet;
struct _XstDhcpdSubnet
{
	GList *ranges; /* Of type XstDhcpdRange */
	GList *hosts;  /* Of type XstDhcpdHost */
	
	XstIp *ip;     /* Ip of the subnet, for example 192.168.0.0 (192.168.0.x) */
	XstIp *mask;   /* Subnet mask, for example 255.255.255.0 */

	XstIp *dns1;
	XstIp *dns2;

	XstIp *router;

	gchar *domain; /* domain.org */
};

XstDhcpdSubnet * xst_dhcpd_subnet_new_empty (void);

XstDhcpdSubnet * xst_dhcpd_subnet_new_from_node (xmlNodePtr node);
void             xst_dhcpd_subnet_free_list (GList *list);
void             xst_dhcpd_subnet_write (XstDhcpdSubnet *subnet,
					 xmlNodePtr parent);

#endif /* __XST_DHCPD_SUBNET_H__ */
