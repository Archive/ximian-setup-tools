/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2001 Ximian, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Chema Celorio <chema@ximian.com>
 *
 */

#ifdef HAVE_CONFIG_H
#   include <config.h>
#endif

#include "xst-xml.h"
#include "xst-ip.h"
#include "dhcpd-subnet.h"
#include "dhcpd-host.h"

static XstDhcpdHost *
xst_dhcpd_host_new (void)
{
	XstDhcpdHost *host;

	host = g_new0 (XstDhcpdHost, 1);

	return host;
}

static void
xst_dhcpd_host_dump (XstDhcpdHost *host)
{
	return;
	g_print ("\nDumping host\n");
	g_print ("   name: %s\n", host->hostname);
	g_print ("   ip: %s\n",   xst_ip_get_string (host->ip));
	g_print ("   mac: %s\n",  host->mac);
	g_print ("   comment: %s\n",  host->comment);
}

XstDhcpdHost *
xst_dhcpd_host_new_from_node (xmlNodePtr parent)
{
	XstDhcpdHost *host;
	xmlNodePtr node;

	host = xst_dhcpd_host_new ();

	node = xst_xml_element_find_first (parent, "host_name");
	if (node)
		host->hostname = xst_xml_element_get_content (node);
	else
		host->hostname = g_strdup ("");
	node = xst_xml_element_find_first (parent, "mac_address");
	if (node)
		host->mac = xst_xml_element_get_content (node);
	else
		host->mac = g_strdup ("");
	node = xst_xml_element_find_first (parent, "comment");
	if (node)
		host->comment = xst_xml_element_get_content (node);
	else 
		host->comment = g_strdup ("");

	node = xst_xml_element_find_first (parent, "ip");
	if (node) {
		gchar *tmp = xst_xml_element_get_content (node);
		host->ip = xst_ip_new_from_string_and_type (tmp,
							    XST_IP_TYPE_V4);
	} else {
		host->ip = xst_ip_new ();
	}

	xst_dhcpd_host_dump (host);
	
	return host;
}

static void
xst_dhcpd_hosts_editor_load_host (XstDhcpdHostsEditor *editor,
				  XstDhcpdHost *host)
{
	gchar *hostname = "";
	gchar *ip = "";
	gchar *mac = "";
	gchar *comment = "";
	
	g_return_if_fail (editor);

	editor->selected_host = NULL;
	
	if (host) {
		hostname = host->hostname;
		ip       = (gchar *) xst_ip_get_string (host->ip);
		mac      = host->mac;
		comment  = host->comment;
	}
	
	gtk_entry_set_text (GTK_ENTRY (editor->name),    hostname ? : "");
	gtk_entry_set_text (GTK_ENTRY (editor->ip),      ip ? ip : "");
	gtk_entry_set_text (GTK_ENTRY (editor->mac),     mac ? mac : "");
	gtk_entry_set_text (GTK_ENTRY (editor->comment), comment ? comment : "");

	editor->selected_host = host;
}

static void
xst_dhcpd_hosts_editor_set_sensitivity (XstDhcpdHostsEditor *editor)
{
	gboolean add = TRUE;
	gboolean delete = TRUE;
	gint len = strlen (gtk_entry_get_text (GTK_ENTRY (editor->name))); 
	
	if (0 == len)
		add = FALSE;
	if (!editor->selected_host)
		delete = FALSE;

	gtk_widget_set_sensitive (editor->add, add);
	gtk_widget_set_sensitive (editor->delete, delete);
}


static void
xst_dhcpd_hosts_editor_select_row_cb (GtkWidget *list, gint row, gint col,
				      GdkEvent *event,
				      XstDhcpdHostsEditor *editor)
{
	XstDhcpdHost *host;
	GtkCList *clist = GTK_CLIST (list);

	g_return_if_fail (GTK_IS_CLIST (list));

	host = gtk_clist_get_row_data (clist, row);
	xst_dhcpd_hosts_editor_load_host (editor,
					  clist->selection ? host : NULL);
	xst_dhcpd_hosts_editor_set_sensitivity (editor);

}


static void
xst_dhcpd_hosts_editor_name_changed_cb (GtkWidget *widget,
					XstDhcpdHostsEditor *editor)
{
	XstDhcpdHost * host = editor->selected_host;
	gint row;

	xst_dhcpd_hosts_editor_set_sensitivity (editor);
	
	if (!host)
		return;
	
        g_free (host->hostname);
	host->hostname = g_strdup (gtk_entry_get_text (GTK_ENTRY (widget)));
	row = gtk_clist_find_row_from_data (GTK_CLIST (editor->list),
					    editor->selected_host);
	gtk_clist_set_text (GTK_CLIST (editor->list), row, 0, host->hostname);
}

static void
xst_dhcpd_hosts_editor_ip_changed_cb (GtkWidget *widget,
				      XstDhcpdHostsEditor *editor)
{
	XstDhcpdHost * host = editor->selected_host;
	const gchar *text;
	gint row;
	
	if (!host)
		return;

	xst_ip_free (host->ip);
	text = gtk_entry_get_text (GTK_ENTRY (widget));
	host->ip = xst_ip_new_from_string (text);
	row = gtk_clist_find_row_from_data (GTK_CLIST (editor->list),
					    editor->selected_host);
	gtk_clist_set_text (GTK_CLIST (editor->list), row, 1, text);
}

static void
xst_dhcpd_hosts_editor_mac_changed_cb (GtkWidget *widget,
				       XstDhcpdHostsEditor *editor)
{
	XstDhcpdHost * host = editor->selected_host;
	
	if (!host)
		return;
	
        g_free (host->mac);
	host->mac = g_strdup (gtk_entry_get_text (GTK_ENTRY (widget)));
}

static void
xst_dhcpd_hosts_editor_comment_changed_cb (GtkWidget *widget,
					   XstDhcpdHostsEditor *editor)
{
	XstDhcpdHost * host = editor->selected_host;
	
	if (!host)
		return;
	
        g_free (host->comment);
	host->comment = g_strdup (gtk_entry_get_text (GTK_ENTRY (widget)));
}

static XstDhcpdHost *
xst_dhcpd_host_copy (const XstDhcpdHost *host)
{
	XstDhcpdHost *new = xst_dhcpd_host_new ();

	new->hostname = g_strdup (host->hostname);
	new->mac      = g_strdup (host->mac);
	new->ip       = xst_ip_copy (host->ip);
	new->comment  = g_strdup (host->comment);

	return new;
}

static GList *
xst_dhcpd_hosts_list_copy (GList *list)
{
	GList *new = NULL;

	for (;list ; list = list->next) {
		XstDhcpdHost *host;
		host = xst_dhcpd_host_copy (list->data);
		new = g_list_prepend (new, host);
	}

	return g_list_reverse (new);
}

static void
xst_dhcpd_host_free (XstDhcpdHost *host)
{
	g_free (host->hostname);
	g_free (host->mac);
	g_free (host->comment);
	xst_ip_free (host->ip);

	host->hostname = NULL;
	host->mac      = NULL;
	host->comment  = NULL;
	host->ip       = NULL;

	g_free (host);		
}

static void
xst_dhcpd_hosts_list_free (GList *list)
{
	while (list) {
		xst_dhcpd_host_free (list->data);
		list->data = NULL;
		list = list->next;
	}
	g_list_free (list);
}

static void
xst_dhcpd_hosts_editor_load_list (XstDhcpdHostsEditor *editor, GList *list)
{
	XstDhcpdHost *host;
	GtkCList *clist;
	gchar * text [3];
	gint row;

	g_return_if_fail (editor != NULL);

	clist = GTK_CLIST (editor->list);
	
	text [2] = NULL;
	editor->hosts = list;
	gtk_clist_clear (clist);
	editor->selected_host = NULL;
	xst_dhcpd_hosts_editor_load_host (editor, NULL);
	
	while (list) {
		host = list->data;
		text [0] = host->hostname;
		text [1] = host->ip ? (gchar *) xst_ip_get_string (host->ip) : NULL;
		row = gtk_clist_append (clist, text);
		gtk_clist_set_row_data (clist, row, list->data);
		list = list->next;
	}

	xst_dhcpd_hosts_editor_set_sensitivity (editor);
}

void
xst_dhcpd_hosts_edit_cb (GtkWidget *widget, gpointer data)
{
	XstDhcpdHostsEditor *editor;
	XstDhcpdSubnet *subnet;
	XstDhcpdTool *tool;
	XstDialog *dialog = data;
	GList *hosts;
	gint ret;

	g_return_if_fail (XST_IS_DIALOG (dialog));

	tool = XST_DHCPD_TOOL (dialog->tool);
	editor = tool->hosts_editor;
	g_assert (editor);
	
	subnet = tool->selected_subnet;
	hosts = xst_dhcpd_hosts_list_copy (subnet->hosts);

	xst_dhcpd_hosts_editor_load_list (editor, hosts);
	ret = gnome_dialog_run_and_close (GNOME_DIALOG (editor->dialog));
	
	if (ret == 0) {
		xst_dhcpd_hosts_list_free (subnet->hosts);
		subnet->hosts = editor->hosts;
	} else {
		xst_dhcpd_hosts_list_free (editor->hosts);
	}

	xst_dhcpd_hosts_editor_load_list (editor, NULL);
	gtk_widget_grab_focus (editor->name);
}

static void
xst_dhcpd_hosts_editor_delete_cb (GtkWidget *button, XstDhcpdHostsEditor *editor)
{
	editor->hosts = g_list_remove (editor->hosts, editor->selected_host);
	xst_dhcpd_hosts_editor_load_host (editor, NULL);
	xst_dhcpd_hosts_editor_load_list (editor, editor->hosts);
	xst_dhcpd_hosts_editor_set_sensitivity (editor);
}

static void
xst_dhcpd_hosts_editor_add_cb (GtkWidget *button, XstDhcpdHostsEditor *editor)
{
	XstDhcpdHost *host = xst_dhcpd_host_new ();
	gboolean selected = editor->selected_host ? TRUE : FALSE;
	gint row;

	if (selected) {
		row = gtk_clist_find_row_from_data (GTK_CLIST (editor->list),
						    editor->selected_host);
		gtk_clist_unselect_row (GTK_CLIST (editor->list), row, -1);
		gtk_widget_grab_focus (editor->name);
		return;
	}
	
	host->hostname = g_strdup (gtk_entry_get_text (GTK_ENTRY (editor->name)));
	host->mac      = g_strdup (gtk_entry_get_text (GTK_ENTRY (editor->mac)));
	host->comment  = g_strdup (gtk_entry_get_text (GTK_ENTRY (editor->comment)));
	host->ip       = xst_ip_new_from_string (gtk_entry_get_text (GTK_ENTRY (editor->ip)));

	editor->hosts = g_list_prepend (editor->hosts, host);
	xst_dhcpd_hosts_editor_load_list (editor, editor->hosts);
	gtk_widget_grab_focus (editor->name);
	xst_dhcpd_hosts_editor_load_host (editor, NULL);
	xst_dhcpd_hosts_editor_set_sensitivity (editor);
}

#define my_connect(__a__,__b__,__c__) {\
	gtk_signal_connect \
	(GTK_OBJECT (editor->__a__), __b__,\
	xst_dhcpd_hosts_editor_ ## __c__ ## _cb, editor);}

void
xst_dhcpd_hosts_editor_init (XstDhcpdTool *tool)
{
	XstDhcpdHostsEditor *editor;
	XstDialog *dialog = XST_TOOL (tool)->main_dialog;

	editor = g_new0 (XstDhcpdHostsEditor, 1);
	editor->dialog  = xst_dialog_get_widget (dialog, "edit_hosts_dialog");
	editor->list    = xst_dialog_get_widget (dialog, "host_clist");
	editor->name    = xst_dialog_get_widget (dialog, "host_name_entry");
	editor->mac     = xst_dialog_get_widget (dialog, "host_mac_address_entry");
	editor->ip      = xst_dialog_get_widget (dialog, "host_ip_address_entry");
	editor->comment = xst_dialog_get_widget (dialog, "host_comment_entry");
	editor->add     = xst_dialog_get_widget (dialog, "host_add_button");
	editor->delete  = xst_dialog_get_widget (dialog, "host_delete_button");

	tool->hosts_editor = editor;
	
	gnome_dialog_close_hides (GNOME_DIALOG (editor->dialog), TRUE);

	my_connect (list,    "select_row",   select_row);
	my_connect (list,    "unselect_row", select_row);
	my_connect (add,     "clicked",      add);
	my_connect (delete,  "clicked",      delete);
	my_connect (name,    "changed",      name_changed);
	my_connect (mac,     "changed",      mac_changed);
	my_connect (comment, "changed",      comment_changed);
	my_connect (ip,      "changed",      ip_changed);

	xst_ip_entry_apply_filters (GTK_ENTRY (editor->ip));
}

void
xst_dhcpd_host_write (XstDhcpdHost *host, xmlNodePtr parent)
{
	xmlNodePtr node;

	xst_dhcpd_host_dump (host);
	
	node = xst_xml_element_add (parent, "host");

	if (host->hostname)
		xst_xml_element_add_with_content (node, "host_name",
						  host->hostname);
	if (xst_ip_get_string (host->ip))
		xst_xml_element_add_with_content (node, "ip",
						  xst_ip_get_string (host->ip));
	if (strlen (host->mac))
		xst_xml_element_add_with_content (node, "mac_address",
						  host->mac);
	if (strlen (host->comment))
		xst_xml_element_add_with_content (node, "comment",
						  host->comment);
}
