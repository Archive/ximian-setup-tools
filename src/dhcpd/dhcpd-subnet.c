/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2001 Ximian, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Chema Celorio <chema@ximian.com>
 *
 */

#ifdef HAVE_CONFIG_H
#   include <config.h>
#endif

#include <glib.h>
#include <string.h>
#include "dhcpd-range.h"
#include "dhcpd-subnet.h"
#include "dhcpd-host.h"

static XstDhcpdSubnet *
xst_dhcpd_subnet_new (void)
{
	XstDhcpdSubnet *subnet;

	subnet = g_new0 (XstDhcpdSubnet, 1);

	return subnet;
}

XstDhcpdSubnet *
xst_dhcpd_subnet_new_empty (void)
{
	XstDhcpdSubnet *subnet;
	XstDhcpdRange *range;

	subnet = xst_dhcpd_subnet_new ();
	range = xst_dhcpd_range_new_empty ();
	
	subnet->ranges = g_list_prepend (NULL, range);

	subnet->ip     = xst_ip_new ();
	subnet->mask   = xst_ip_new ();
	subnet->dns1   = xst_ip_new ();
	subnet->dns2   = xst_ip_new ();
	subnet->router = xst_ip_new ();
	subnet->domain = g_strdup ("");
	
	return subnet;
}


#define my_read(__b__,__a__) {\
	xmlNodePtr child;\
	XstIp *ip;\
	child = xst_xml_element_find_first (parent, __a__);\
	if (!child) { __b__ = xst_ip_new ();} else { \
	string = xst_xml_element_get_content (child);\
	ip = xst_ip_new_from_string_and_type (string, XST_IP_TYPE_V4);\
	g_free (string);\
	if (!ip) {\
		g_warning ("Could not read \"%s\" from subnet\n", __a__);\
	} else {\
		 __b__ = ip;}}\
}

XstDhcpdSubnet *
xst_dhcpd_subnet_new_from_node (xmlNodePtr parent)
{
	XstDhcpdSubnet *subnet = NULL;
	xmlNodePtr node;
	gchar *string;

	subnet = xst_dhcpd_subnet_new ();
	
	node = xst_xml_element_find_first (parent, "options");

	my_read (subnet->ip,   "ip");
	my_read (subnet->mask, "netmask");
	my_read (subnet->dns1, "dns1");
	my_read (subnet->dns2, "dns2");
	my_read (subnet->router, "routers");

	node = xst_xml_element_find_first (parent, "domain-name");
	if (node)
		subnet->domain = xst_xml_element_get_content (node);
	else
		subnet->domain = g_strdup ("");
	
	/* Read Ranges */
	node = parent->childs;
	while (node) {
		XstDhcpdRange *range;
		if ((node->name) && (strcmp (node->name, "range") == 0)) {
			range = xst_dhcpd_range_new_from_node (node);
			if (range)
				subnet->ranges = g_list_prepend (subnet->ranges,
								 range);
		}
		node = node->next;
	}
	subnet->ranges = g_list_reverse (subnet->ranges);
	if (!subnet->ranges)
		subnet->ranges = g_list_prepend (NULL, xst_dhcpd_range_new_empty ());

	/* Read Hosts */
	node = parent->childs;
	while (node) {
		XstDhcpdHost *host;
		if ((node->name) && (strcmp (node->name, "host") == 0)) {
			host = xst_dhcpd_host_new_from_node (node);
			if (host)
				subnet->hosts = g_list_prepend (subnet->hosts,
								host);
		}
		node = node->next;
	}
	subnet->hosts = g_list_reverse (subnet->hosts);
	
	return subnet;
}

static void
xst_dhcpd_subnet_free (XstDhcpdSubnet *subnet)
{
	g_return_if_fail (subnet != NULL);
	
	g_implement_me ();
}

void
xst_dhcpd_subnet_free_list (GList *list)
{
	GList *l;

	for (l = list; l != NULL; l = l->next) {
		xst_dhcpd_subnet_free (l->data);
	}

	g_list_free (list);
}

#define my_add(__a__,__b__) {\
        g_assert (subnet->__b__);\
        if ((0 != strlen (xst_ip_get_string (subnet->__b__)))) \
                xst_xml_element_add_with_content (node, __a__,\
                           xst_ip_get_string (subnet->__b__));}

void
xst_dhcpd_subnet_write (XstDhcpdSubnet *subnet, xmlNodePtr parent)
{
	xmlNodePtr node;
	GList *list;

	node = xst_xml_element_add (parent, "subnet");

	my_add ("ip", ip);
	my_add ("dns1", dns1);
	my_add ("dns2", dns2);
	my_add ("netmask", mask);
	my_add ("routers", router);

	xst_xml_element_add_with_content (node, "domain-name", subnet->domain);

	list = subnet->ranges;
	while (list) {
		xst_dhcpd_range_write (list->data, node);
		list = list->next;
	}
	list = subnet->hosts;
	while (list) {
		xst_dhcpd_host_write (list->data, node);
		list = list->next;
	}
}
