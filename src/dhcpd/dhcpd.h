/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2001 Ximian, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Chema Celorio <chema@celorio.com>
 *
 */

#ifndef __DHCPD_H__
#define __DHCPD_H__

#include <gnome.h>

#include "xst.h"
#include "dhcpd-subnet.h"
#include "dhcpd-range.h"


#define g_implement_me(m) g_print ("Imlpement me. File: %s Line: %d\n", __FILE__, __LINE__);
/* Our XstTool inherited class */

#define XST_TYPE_DHCPD_TOOL            (xst_dhcpd_tool_get_type ())
#define XST_DHCPD_TOOL(obj)            (GTK_CHECK_CAST ((obj), XST_TYPE_DHCPD_TOOL, XstDhcpdTool))
#define XST_DHCPD_TOOL_CLASS(class)    (GTK_CHECK_CLASS_CAST ((class), XST_TYPE_DHCPD_TOOL, XstDhcpdToolClass))
#define XST_IS_DHCPD_TOOL(obj)         (GTK_CHECK_TYPE ((obj), XST_TYPE_DHCPD_TOOL))
#define XST_IS_DHCPD_TOOL_CLASS(class) (GTK_CHECK_CLASS_TYPE ((class), XST_TYPE_DHCPD_TOOL))

typedef struct _XstDhcpdTool        XstDhcpdTool;
typedef struct _XstDhcpdToolClass   XstDhcpdToolClass;
typedef struct _XstDhcpdHostsEditor XstDhcpdHostsEditor;
typedef struct _XstDhcpdHost        XstDhcpdHost;

struct _XstDhcpdTool {
	XstTool         tool;
	
	/* Tool data */
	gboolean installed;
	gboolean active;
	GList *subnets; /* of type XstDhcpdSubnet */

	XstDhcpdHostsEditor *hosts_editor;

	/* Selected */
	XstDhcpdSubnet *selected_subnet;
	XstDhcpdRange  *selected_range;
	
	/* Widgets */
	GtkWidget *range_start;
	GtkWidget *range_end;
	GtkWidget *subnet_mask;
	GtkWidget *domain;
	GtkWidget *dns1;
	GtkWidget *dns2;
	GtkWidget *router;
};

struct _XstDhcpdToolClass {
	XstToolClass parent_class;
};

GtkType  xst_dhcpd_tool_get_type (void);
XstTool *xst_dhcpd_tool_new (void);

gboolean xst_dhcpd_tool_allow_simple_mode (XstDhcpdTool *tool);
gboolean xst_dhcpd_tool_verify_mode (XstDhcpdTool *tool);

void     xst_dhcpd_tool_update_complexity_cb (GtkObject *dialog, XstTool *tool);
gboolean xst_dhcpd_tool_verify_apply (XstDhcpdTool *tool);

void     xst_dhcpd_tool_populate (XstDhcpdTool *tool);
void     xst_dhcpd_tool_load_widgets (XstDhcpdTool *tool);

#endif /*  __DHCPD_H__  */
