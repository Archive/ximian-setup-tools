/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/* 
 * Copyright (C) 2001 Ximian, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Chema Celorio <chema@celorio.com>
 *
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <gnome.h>

#include "xst.h"
#include "callbacks.h"
#include "transfer.h"
#include "dhcpd.h"

void
on_enabled_button_toggled (GtkWidget *button, XstDialog *dialog)
{
	XstTool *xst_tool = dialog->tool;
	XstDhcpdTool *tool = XST_DHCPD_TOOL (xst_tool);
	GtkWidget *table;
	gboolean new_state;

	new_state = gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (button));
	table = xst_dialog_get_widget (xst_tool->main_dialog, "main_table");

	if (!tool->installed && new_state) {
		GtkWidget *widget;
		
		gtk_signal_handler_block_by_func (GTK_OBJECT (button),
						  on_enabled_button_toggled,
						  dialog);
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (button), FALSE);
		gtk_signal_handler_unblock_by_func (GTK_OBJECT (button),
						    on_enabled_button_toggled,
						    dialog);

		widget = gnome_ok_dialog (_("You don't have the DHCP server package installed.\n"
					    "You need to install it to enable the service."));
		gtk_window_set_title (GTK_WINDOW (widget), _("DHCPd not installed."));
		gnome_dialog_run_and_close (GNOME_DIALOG (widget));
		return;
	}
	
	gtk_widget_set_sensitive (table, new_state);
	tool->active = new_state;
	xst_dialog_modify (dialog);
}

void
on_domain_changed_cb (GtkWidget *entry, XstDialog *dialog)
{
	XstTool *xst_tool = dialog->tool;
	XstDhcpdTool *tool = XST_DHCPD_TOOL (xst_tool);
	XstDhcpdSubnet *subnet = tool->selected_subnet;

	g_assert (subnet);
	
	g_free (subnet->domain);
	subnet->domain = g_strdup (gtk_entry_get_text (GTK_ENTRY (entry)));
}

void
on_range_start_changed_cb  (GtkWidget *entry, XstDialog *dialog)
{
	XstTool *xst_tool = dialog->tool;
	XstDhcpdTool *tool = XST_DHCPD_TOOL (xst_tool);
	XstDhcpdRange *range  = tool->selected_range;

	g_assert (range);
	
	xst_ip_set_string (range->start, gtk_entry_get_text (GTK_ENTRY (entry)));
}

void
on_range_end_changed_cb (GtkWidget *entry, XstDialog *dialog)
{
	XstTool *xst_tool = dialog->tool;
	XstDhcpdTool *tool = XST_DHCPD_TOOL (xst_tool);
	XstDhcpdRange *range  = tool->selected_range;

	g_assert (range);
	
	xst_ip_set_string (range->end, gtk_entry_get_text (GTK_ENTRY (entry)));
}

void
on_subnetmask_changed_cb (GtkWidget *entry, XstDialog *dialog)
{
	XstTool *xst_tool = dialog->tool;
	XstDhcpdTool *tool = XST_DHCPD_TOOL (xst_tool);
	XstDhcpdSubnet *subnet  = tool->selected_subnet;

	g_assert (subnet);
	
	xst_ip_set_string (subnet->mask, gtk_entry_get_text (GTK_ENTRY (entry)));
}

void
on_dns1_changed_cb (GtkWidget *entry, XstDialog *dialog)
{
	XstTool *xst_tool = dialog->tool;
	XstDhcpdTool *tool = XST_DHCPD_TOOL (xst_tool);
	XstDhcpdSubnet *subnet  = tool->selected_subnet;

	g_assert (subnet);
	
	xst_ip_set_string (subnet->dns1, gtk_entry_get_text (GTK_ENTRY (entry)));
}

void
on_dns2_changed_cb (GtkWidget *entry, XstDialog *dialog)
{
	XstTool *xst_tool = dialog->tool;
	XstDhcpdTool *tool = XST_DHCPD_TOOL (xst_tool);
	XstDhcpdSubnet *subnet  = tool->selected_subnet;

	g_assert (subnet);
	
	xst_ip_set_string (subnet->dns2, gtk_entry_get_text (GTK_ENTRY (entry)));
}

void
on_router_changed_cb (GtkWidget *entry, XstDialog *dialog)
{
	XstTool *xst_tool = dialog->tool;
	XstDhcpdTool *tool = XST_DHCPD_TOOL (xst_tool);
	XstDhcpdSubnet *subnet  = tool->selected_subnet;

	g_assert (subnet);
	
	xst_ip_set_string (subnet->router, gtk_entry_get_text (GTK_ENTRY (entry)));
}
