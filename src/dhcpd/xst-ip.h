/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2001 Ximian, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Chema Celorio <chema@ximian.com>
 *
 */

#ifndef __XST_IP_H__
#define __XST_IP_H__

#include <gtk/gtk.h>

typedef struct _XstIp XstIp;

typedef enum {
	XST_IP_TYPE_V4,
	XST_IP_TYPE_V6,
	XST_IP_TYPE_UNKNOWN,
	XST_IP_TYPE_INVALID
} XstIpType;
	
XstIp *   xst_ip_new (void);
XstIp *   xst_ip_new_from_string (const gchar *string);
XstIp *   xst_ip_new_from_string_and_type (const gchar *string, XstIpType typeo);
XstIp *   xst_ip_copy (const XstIp* ip);

void      xst_ip_free (XstIp *ip);


gboolean  xst_ip_is_string_valid (const gchar *string);
gboolean  xst_ip_is_string_valid_with_type (const gchar *string, XstIpType type);
XstIpType xst_ip_type_from_string (const gchar *string);
gboolean  xst_ip_is_loopback (XstIp *ip);

const gchar * xst_ip_get_string (XstIp *ip);
gboolean      xst_ip_set_string (XstIp *ip, const gchar *string);

void      xst_ip_entry_apply_filters (GtkEntry *entry);

#endif /* __XST_IP__H__ */

