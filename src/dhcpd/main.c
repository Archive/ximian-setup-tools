/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2001 Ximian, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Chema Celorio <chema@ximian.com>
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <gnome.h>
#include <gnome-xml/tree.h>
#include <gnome-xml/parser.h>
#include <glade/glade.h>

#include "xst.h"

#include "transfer.h"
#include "callbacks.h"
#include "dhcpd.h"
#include "dhcpd-host.h"

XstDialogSignal signals[] = {
	{ "domain_entry",            "changed",         xst_dialog_modify_cb },
	{ "range_start_entry",       "changed",         xst_dialog_modify_cb },
	{ "range_end_entry",         "changed",         xst_dialog_modify_cb },
	{ "subnetmask_entry",        "changed",         xst_dialog_modify_cb },
	{ "dns1_entry",              "changed",         xst_dialog_modify_cb },
	{ "dns2_entry",              "changed",         xst_dialog_modify_cb },
	{ "router_entry",            "changed",         xst_dialog_modify_cb },

	{ "enabled_button",          "toggled",         on_enabled_button_toggled },
	{ "domain_entry",            "changed",         on_domain_changed_cb },
	{ "range_start_entry",       "changed",         on_range_start_changed_cb },
	{ "range_end_entry",         "changed",         on_range_end_changed_cb },
	{ "subnetmask_entry",        "changed",         on_subnetmask_changed_cb },
	{ "dns1_entry",              "changed",         on_dns1_changed_cb },
	{ "dns2_entry",              "changed",         on_dns2_changed_cb },
	{ "router_entry",            "changed",         on_router_changed_cb },

#if 0
	{ "host_name_entry",         "changed",         xst_dialog_modify_cb },
	{ "host_mac_address_entry",  "changed",         xst_dialog_modify_cb },
	{ "host_ip_address_entry",   "changed",         xst_dialog_modify_cb },
	{ "host_comment_entry",      "changed",         xst_dialog_modify_cb },
	{ "host_add_button",         "clicked",         xst_dialog_modify_cb },
	{ "host_delete_button",      "clicked",         xst_dialog_modify_cb },
#else	
	{ "edit_hosts_button",       "clicked",         xst_dialog_modify_cb },
#endif
	{ "edit_hosts_button",       "clicked",         xst_dhcpd_hosts_edit_cb },
	{ NULL }
};


static void
connect_signals (XstTool *tool)
{
	gtk_signal_connect (GTK_OBJECT (tool->main_dialog), "complexity_change",
			    GTK_SIGNAL_FUNC (xst_dhcpd_tool_update_complexity_cb),
			    tool);
	
	xst_dialog_connect_signals (tool->main_dialog, signals);
}

int
main (int argc, char *argv[])
{
	XstTool *tool;

	XstWidgetPolicy policies[] = {
		/* Name            Basic                        Advanced                   Root   User */
		{ "advanced_hbox", XST_WIDGET_MODE_HIDDEN,      XST_WIDGET_MODE_SENSITIVE, TRUE,  FALSE },
		{ NULL },
	};

	xst_init ("dhcpd-admin", argc, argv, NULL);

	tool = xst_dhcpd_tool_new ();
	xst_tool_construct (tool, "dhcpd", _("DHCP server configurator"));
	xst_tool_set_xml_funcs (tool, transfer_xml_to_gui, transfer_gui_to_xml, NULL);

	/* Complexity's not supported yet. */
	/* xst_dialog_enable_complexity (tool->main_dialog); */

	xst_dialog_set_widget_policies (tool->main_dialog, policies);
	xst_dhcpd_tool_load_widgets (XST_DHCPD_TOOL (tool));

	connect_signals (tool);

	xst_tool_main (tool, FALSE);
	
	return 0;
}

