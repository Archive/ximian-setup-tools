/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2001 Ximian, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Chema Celorio <chema@ximian.com>
 *
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <gnome.h>
#include <gnome-xml/tree.h>
#include <gnome-xml/parser.h>
#include <glade/glade.h>

#include "xst.h"
#include "transfer.h"
#include "callbacks.h"
#include "dhcpd.h"
#include "dhcpd-subnet.h"

static void
transfer_status_xml_to_gui (XstDhcpdTool *tool, xmlNodePtr node)
{
	XstTool *xst_tool = XST_TOOL (tool);
	GtkWidget *enabled;
	GtkWidget *table;
	gboolean active;

	tool->installed = xst_xml_element_get_boolean (node, "installed");

	active = xst_xml_element_get_boolean (node, "active");
	active = active && tool->installed;
	tool->active = active;
	
	enabled = xst_dialog_get_widget (xst_tool->main_dialog, "enabled_button");
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (enabled), active);
	
	table = xst_dialog_get_widget (xst_tool->main_dialog, "main_table");
	gtk_widget_set_sensitive (table, active);
}

static void
transfer_subnets_xml_to_gui (XstDhcpdTool *tool, xmlNodePtr root)
{
	XstDhcpdSubnet *subnet;
	xmlNodePtr node;
	GList *list = NULL;

	xst_dhcpd_subnet_free_list (tool->subnets);
	tool->subnets = NULL;

	node = root->childs;
	while (node) {
		if (node->name && (strcmp (node->name, "subnet") == 0)) {
			subnet = xst_dhcpd_subnet_new_from_node (node);
			if (subnet)
				list = g_list_prepend (list, subnet);
		}
		node = node->next;
	}
	list = g_list_reverse (list);
	/* Make sure we always have a subnet, even if it is empty */
	if (list == NULL)
		list = g_list_prepend (list, xst_dhcpd_subnet_new_empty ());
	tool->subnets = list;
	
	xst_dhcpd_tool_verify_mode (tool);
}

void
transfer_xml_to_gui (XstTool *xst_tool, gpointer data)
{
	XstDhcpdTool *tool = XST_DHCPD_TOOL (xst_tool);
	xmlNodePtr root;

	root = xst_xml_doc_get_root (xst_tool->config);

	transfer_status_xml_to_gui  (tool, root);
	transfer_subnets_xml_to_gui (tool, root);

	xst_dhcpd_tool_populate (tool);
}

/* Maybe we should just add a "create" flag to the function ?? */
static xmlNodePtr
xst_xml_element_find_first_or_create_it (xmlNodePtr node,
					 const gchar *name)
{
	xmlNodePtr ret;

	ret = xst_xml_element_find_first (node, name);
	if (!ret)
		ret = xst_xml_element_add (node, name);

	return ret;
}

static void
transfer_status_gui_to_xml (XstDhcpdTool *tool, xmlNodePtr root)
{
	xmlNodePtr node;

	node = xst_xml_element_find_first_or_create_it (root, "active");
	xst_xml_element_set_content (node, tool->active ? "1" : "0");
}

static void
transfer_subnets_gui_to_xml (XstDhcpdTool *tool, xmlNodePtr root)
{
	GList *list;
	
	xst_xml_element_destroy_children_by_name (root, "subnet");

	list = tool->subnets;
	while (list) {
		xst_dhcpd_subnet_write (list->data, root);
		list = list->next;
	}
}

void
transfer_gui_to_xml (XstTool *xst_tool, gpointer data)
{
	XstDhcpdTool *tool = XST_DHCPD_TOOL (xst_tool);
	xmlNodePtr root;

#if 0	/* This does not work, but requires fixification in xst-tool.c
	 * right now we can't stop the apply action with this. Chema
	 */
	if (!xst_dhcpd_tool_verify_apply (tool)) {
		gtk_signal_emit_stop_by_name (GTK_OBJECT (xst_tool->main_dialog),
					      "apply");
		gtk_signal_emit_stop_by_name (GTK_OBJECT (xst_tool),
					      "fill_xml");
		return;
	}
#endif	

	root = xst_xml_doc_get_root (xst_tool->config);

	transfer_status_gui_to_xml  (tool, root);
	transfer_subnets_gui_to_xml (tool, root);
	
}
