/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2001 Ximian, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Chema Celorio <chema@ximian.com>
 *
 */

#ifdef HAVE_CONFIG_H
#   include <config.h>
#endif

#include <glib.h>
#include "dhcpd-range.h"
#include "xst-ip.h"

static XstDhcpdRange *
xst_dhcpd_range_new (void)
{
	XstDhcpdRange *range;

	range = g_new0 (XstDhcpdRange, 1);

	return range;
}

XstDhcpdRange *
xst_dhcpd_range_new_empty (void)
{
	XstDhcpdRange *range;

	range = xst_dhcpd_range_new ();
	range->start = xst_ip_new ();
	range->end   = xst_ip_new ();

	return range;
}

XstDhcpdRange *
xst_dhcpd_range_new_from_node (xmlNodePtr parent)
{
	XstDhcpdRange *range;
	xmlNodePtr node;
	XstIpType type;
	gchar *start;
	gchar *end;
	XstIp *s;
	XstIp *e;
	gboolean b;

	type  = XST_IP_TYPE_V4; /* FIXME, hardcode for now */

	node  = xst_xml_element_find_first (parent, "start");
	start = xst_xml_element_get_content (node);
	s = xst_ip_new_from_string_and_type (start, type);
	
	node  = xst_xml_element_find_first (parent, "end");
	end   = xst_xml_element_get_content (node);
	e = xst_ip_new_from_string_and_type (end, type);

	b = xst_xml_element_get_boolean (parent, "dynamic-bootp");

	if (!e || !s) {
		g_warning ("Could not read range from *%s* and *%s*\n",
			   start, end);
		return NULL;
	}
	g_free (start);
	g_free (end);

	range = xst_dhcpd_range_new ();
	range->start = s;
	range->end   = e;
	range->bootp = b;
	
	return range;
}

void
xst_dhcpd_range_write (XstDhcpdRange *range, xmlNodePtr parent)
{
	xmlNodePtr node;

	node = xst_xml_element_add (parent, "range");

	xst_xml_element_add_with_content (node, "start",
					  xst_ip_get_string (range->start));
	xst_xml_element_add_with_content (node, "end",
					  xst_ip_get_string (range->end));
	xst_xml_element_add_with_content (node, "dynamic-bootp",
					  range->bootp ? "1" : "0");
}
