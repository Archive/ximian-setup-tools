/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2001 Ximian, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Chema Celorio <chema@ximian.com>
 *
 */

/*

--  IP V6 --
  
The preferred form of representing an IPv6 address is:
   FEDC:BA98:7654:3210:FEDC:BA98:7654:3210

Addresses will often have a number of zeros in them, such as:
   FF01:0:0:0:0:0:0:43

A special syntax is available for representing groups of zeros to allow the address to be compressed. The use of '::' indicates multiple
groups of 16-bits of zeros. For example, the address above may be represented as:
   FF01::43

Another form of address is available for use within a mixed IPv4 and IPv6 environment. An address such as:
   ::10.5.27.200
will send IPv6-in-IPv4 packets.

*/


#ifdef HAVE_CONFIG_H
#   include <config.h>
#endif

#include <glib.h>
#include <stdlib.h> /* For atoi */
#include <string.h> /* For strlen */
#include "xst-ip.h"

#define g_implement_me() g_print("implement me [%s]\n", __FUNCTION__);

struct _XstIp
{
	XstIpType type;
	gchar *ip;
};

XstIp *
xst_ip_new (void)
{
	XstIp *ip;

	ip = g_new0 (XstIp, 1);
	ip->type = XST_IP_TYPE_INVALID;
	ip->ip   = g_strdup ("");
		
	return ip;
}

/**
 * xst_ip_set_string:
 * @ip: 
 * @string: 
 * 
 * 
 * 
 * Return Value: TRUE on success
 **/
gboolean
xst_ip_set_string (XstIp *ip, const gchar *string)
{
	g_return_val_if_fail (ip, FALSE);
	g_return_val_if_fail (string, FALSE);

	g_free (ip->ip);
	ip->ip = g_strdup (string);

	return TRUE;
}

static XstIp *
xst_ip_new_from_string_v4 (const gchar *string)
{
	XstIp *ip;
#if 0	
	gchar **tokens;
	gint i, size = 0;
	
	g_return_val_if_fail (string != NULL, NULL);

	tokens = g_strsplit (string, ".", 5);
	i = 0;
	while (tokens [i++])
		size++;

	if (size != 4) {
		g_warning ("Error whle parsing IP address (v4) -%s- "
			   "size is not 4\n", string);
		return NULL;
	}
	
	for (i = 0; i < size; i++) {
		gint val;
		val = atoi (tokens [i]);
		if (val < 0 || val > 255) {
			g_warning ("Error whle parsing IP address (v4), it contained an invalid token -%s- ",
				   tokens [i]);
			return NULL;
		}
	}
#endif	
			
	ip = xst_ip_new ();
	xst_ip_set_string (ip, string);
	
	return ip;
}

static XstIp *
xst_ip_new_from_string_v6 (const gchar *string)
{
	g_return_val_if_fail (string != NULL, NULL);

	g_implement_me ();

	return NULL;
}

XstIp *
xst_ip_new_from_string_and_type (const gchar *string, XstIpType type)
{
	g_return_val_if_fail (string != NULL, NULL);
	g_return_val_if_fail (type != XST_IP_TYPE_INVALID, NULL);

	switch (type) {
	case XST_IP_TYPE_V4:
		return xst_ip_new_from_string_v4 (string);
	case XST_IP_TYPE_V6:
		return xst_ip_new_from_string_v6 (string);
	default:
		g_assert_not_reached ();
	}

	return NULL;
}

XstIpType
xst_ip_type_from_string (const gchar *string)
{
	g_return_val_if_fail (string != NULL, XST_IP_TYPE_INVALID);

	return XST_IP_TYPE_V4;
}
		
XstIp *
xst_ip_new_from_string (const gchar *string)
{
	XstIpType type;
	XstIp *ip = NULL;

	g_return_val_if_fail (string != NULL, NULL);

	type = xst_ip_type_from_string (string);
	if (type != XST_IP_TYPE_INVALID)
		ip = xst_ip_new_from_string_and_type (string, type);

	return ip;
}

XstIp *
xst_ip_copy (const XstIp *ip)
{
	XstIp *new;

	g_return_val_if_fail (ip, NULL);

	new = xst_ip_new ();
	new->type = ip->type;
	new->ip   = ip->ip;

	return new;
}

void
xst_ip_free (XstIp *ip)
{
	g_return_if_fail (ip);
	
	ip->type = XST_IP_TYPE_INVALID;
	g_free (ip);
}

static gboolean
xst_ip_is_string_valid_v4 (const gchar *string)
{
	g_return_val_if_fail (string, FALSE);
	
	g_implement_me ();

	return FALSE;
}

static gboolean
xst_ip_is_string_valid_v6 (const gchar *string)
{
	g_return_val_if_fail (string, FALSE);
	
	g_implement_me ();

	return FALSE;
}

gboolean
xst_ip_is_string_valid_with_type (const gchar *string, XstIpType type)
{
	g_return_val_if_fail (type != XST_IP_TYPE_INVALID, FALSE);
	
	switch (type) {
	case XST_IP_TYPE_V4:
		return xst_ip_is_string_valid_v4 (string);
	case XST_IP_TYPE_V6:
		return xst_ip_is_string_valid_v6 (string);
	default:
	}

	g_assert_not_reached ();

	return FALSE;
}

gboolean
xst_ip_is_string_valid (const gchar *string)
{
	gboolean valid = FALSE;
	XstIpType type;

	g_return_val_if_fail (string, FALSE);
	
	type = xst_ip_type_from_string (string);
	if (type != XST_IP_TYPE_INVALID)
		valid = xst_ip_is_string_valid_with_type (string, type);

	return valid;
}
		

const gchar *
xst_ip_get_string (XstIp *ip)
{
	g_return_val_if_fail (ip, NULL);
	
	return ip->ip;
}

/* This is broken */
static gboolean
is_ip_text_ok  (const gchar *text)
{
	XstIpType ver;
	gint i, dst, numdots;

	g_return_val_if_fail (text, FALSE);
	
	ver = XST_IP_TYPE_UNKNOWN;
	dst = numdots = 0;

	for (i = 0; text[i]; i++) {
		/* IPv6 cases */
		if (text[i] >= 'a' && text[i] <= 'f') {
			if (ver == XST_IP_TYPE_V4)
				return FALSE;
			dst ++;
			if (dst > 2)
				return FALSE;
			ver = XST_IP_TYPE_V6;
			continue;
		}

		if (text[i] == ':') {
			if (ver == XST_IP_TYPE_V4)
				return FALSE;
			dst = 0;
			ver = XST_IP_TYPE_V6;
			continue;
		}

		/* IPv4 cases */
		if (text[i] == '.') {
			if (ver == XST_IP_TYPE_V6)
				return FALSE;
			if (i == 0 || text[i - 1] == '.')
				return FALSE;
			numdots ++;
			if (numdots > 3)
				return FALSE;
			dst = 0;
			ver = XST_IP_TYPE_V4;
			continue;
		}

		if ((text[i] >= '0') && (text[i] <= '9')) {
			dst ++;
			if (dst == 3) {
				if (ver == XST_IP_TYPE_V6)
					return FALSE;
				ver = XST_IP_TYPE_V4;
				continue;
			}

			if (dst > 3)
				return FALSE;

			continue;
		}

		return FALSE;
	}

	return TRUE;
}

static gchar *
str_insert_text (const gchar *str, const gchar *text, gint length, gint pos)
{
	gchar *buff;
	gint i, len;

	len = strlen (str);
	
	g_assert (pos <= len);

	buff = g_new0 (char, len + length + 1);

	for (i = 0; i < pos; i++)
		buff[i] = str[i];
	for (i = 0; i < length; i++)
		buff[i + pos] = text[i];
	for (i = 0; i < len - pos; i++)
		buff[i + pos + length] = str[i + pos];
	buff[len + length] = 0;

	return buff;
}

static void
xst_ip_insert_text_cb (GtkEditable *editable, const gchar *text, gint length,
		       gint *pos, gpointer data)
{
	gboolean success;
	gchar *s = NULL;
		
	s = str_insert_text (gtk_editable_get_chars (editable, 0, -1), text, length, *pos);
	success = is_ip_text_ok (s);
	g_free (s);
	s = NULL;
		
	if (!success) {
		gdk_beep ();
	
		gtk_signal_emit_stop_by_name (GTK_OBJECT (editable), "insert_text");
		if (s) {
			gtk_signal_handler_block_by_func (GTK_OBJECT (editable),
							  xst_ip_insert_text_cb, data);
			gtk_editable_insert_text (editable, s, strlen (s), pos);
			gtk_signal_handler_unblock_by_func (GTK_OBJECT (editable),
							    xst_ip_insert_text_cb, data);
		}
	}


	return;
}
	
void
xst_ip_entry_apply_filters (GtkEntry *entry)
{
	gtk_signal_connect (GTK_OBJECT (entry), "insert_text",
			    xst_ip_insert_text_cb, NULL);
}
