/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/* main.c: this file is part of users-admin, a ximian-setup-tool frontend 
 * for user administration.
 * 
 * Copyright (C) 2000-2001 Ximian, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Carlos Garnacho Parro <garparr@teleline.es>,
 *          Tambet Ingo <tambet@ximian.com> and 
 *          Arturo Espinosa <arturo@ximian.com>.
 */


#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <gnome.h>
#include <gdk-pixbuf/gdk-pixbuf.h>
#include <glade/glade.h>

#include <time.h>
#include <stdlib.h>

#include "transfer.h"
#include "table.h"
#include "callbacks.h"
#include "search-bar/search-bar.h"
#include "xst.h"

XstTool *tool;

void quit_cb (XstTool *tool, gpointer data);

static XstDialogSignal signals[] = {
	{ "notebook",                    	"switch_page",   	G_CALLBACK (on_notebook_switch_page) },
	{ "showall",                     	"toggled",       	G_CALLBACK (on_showall_toggled) },
	
	/* User settings dialog callbacks */
	{ "user_settings_dialog",		"delete_event",		G_CALLBACK (on_user_settings_dialog_delete_event) },
	{ "user_settings_dialog",		"show",			G_CALLBACK (on_user_settings_dialog_show) },
	{ "user_settings_ok",			"clicked",		G_CALLBACK (on_user_settings_ok_clicked) },
	{ "user_settings_cancel",		"clicked",		G_CALLBACK (on_user_settings_dialog_delete_event) },
	{ "user_settings_add",			"clicked",		G_CALLBACK (on_add_remove_button_clicked) },
	{ "user_settings_remove",		"clicked",		G_CALLBACK (on_add_remove_button_clicked) },
	{ "user_passwd_manual",			"toggled",		G_CALLBACK (on_user_settings_passwd_toggled) },
	{ "user_passwd_random",			"toggled",		G_CALLBACK (on_user_settings_passwd_toggled) },
	{ "user_passwd_random_new",		"clicked",		G_CALLBACK (on_user_settings_passwd_random_new) },
	{ "user_passwd_entry1",			"changed",		G_CALLBACK (on_user_settings_passwd_changed) },
	{ "user_passwd_entry2",			"changed",		G_CALLBACK (on_user_settings_passwd_changed) },
//	{ "user_settings_profile_button",	"clicked",		G_CALLBACK (profile_table_run) },
	
	/* Group settings dialog callbacks */
	{ "group_settings_dialog",		"delete_event",  	G_CALLBACK (on_group_settings_dialog_delete_event) },
	{ "group_settings_dialog",		"show",          	G_CALLBACK (on_group_settings_dialog_show) },
	{ "group_settings_ok",			"clicked",       	G_CALLBACK (on_group_settings_ok_clicked) },
	{ "group_settings_cancel",		"clicked",       	G_CALLBACK (on_group_settings_dialog_delete_event) },
	{ "group_settings_add",			"clicked",       	G_CALLBACK (on_add_remove_button_clicked) },
	{ "group_settings_remove",		"clicked",       	G_CALLBACK (on_add_remove_button_clicked) },

	/* Main dialog callbacks, users tab */
	{ "user_new",				"clicked",		G_CALLBACK (on_user_new_clicked) },
	{ "user_settings",             		"clicked",       	G_CALLBACK (on_user_settings_clicked) },
	{ "user_delete",                	"clicked",       	G_CALLBACK (on_user_delete_clicked) },
//	{ "user_profiles",               	"clicked",       	G_CALLBACK (profile_table_run) },
	
	/* Main dialog callbacks, groups tab */
	{ "group_new",				"clicked",		G_CALLBACK (on_group_new_clicked) },
	{ "group_settings",			"clicked",		G_CALLBACK (on_group_settings_clicked) },
	{ "group_delete",			"clicked",       	G_CALLBACK (on_group_delete_clicked) },
	{ NULL }};

static const XstWidgetPolicy policies[] = {
	/* Name                     Basic                        Advanced                   Root   User */
	{ "users_table",            XST_WIDGET_MODE_SENSITIVE,   XST_WIDGET_MODE_SENSITIVE, TRUE,  TRUE  },
	{ "user_new",               XST_WIDGET_MODE_SENSITIVE,   XST_WIDGET_MODE_SENSITIVE, TRUE,  TRUE  },
	{ "user_settings_advanced", XST_WIDGET_MODE_SENSITIVE,   XST_WIDGET_MODE_SENSITIVE, TRUE,  TRUE  },
	{ "user_delete",            XST_WIDGET_MODE_SENSITIVE,   XST_WIDGET_MODE_SENSITIVE, TRUE,  FALSE },
	{ "user_settings",          XST_WIDGET_MODE_SENSITIVE,   XST_WIDGET_MODE_SENSITIVE, TRUE,  FALSE },
/* Hiding user_profiles for now, until next release when profiles UI is given a face-lift */
	{ "user_profiles",          XST_WIDGET_MODE_HIDDEN,      XST_WIDGET_MODE_HIDDEN, TRUE,  TRUE  },
	{ "user_profiles",          XST_WIDGET_MODE_HIDDEN,      XST_WIDGET_MODE_HIDDEN,    TRUE,  TRUE  },
	{ "groups_table",           XST_WIDGET_MODE_SENSITIVE,   XST_WIDGET_MODE_SENSITIVE, FALSE, TRUE  },
	{ "group_new",              XST_WIDGET_MODE_SENSITIVE,   XST_WIDGET_MODE_SENSITIVE, TRUE,  TRUE  },
	{ "group_delete",           XST_WIDGET_MODE_SENSITIVE,   XST_WIDGET_MODE_SENSITIVE, TRUE,  FALSE },
	{ "group_settings",         XST_WIDGET_MODE_SENSITIVE,   XST_WIDGET_MODE_SENSITIVE, TRUE,  FALSE },
	{ "group_settings_name_label", XST_WIDGET_MODE_SENSITIVE, XST_WIDGET_MODE_SENSITIVE, TRUE, TRUE  },
	{ "network_user_new",       XST_WIDGET_MODE_INSENSITIVE, XST_WIDGET_MODE_SENSITIVE, TRUE,  TRUE  },
	{ "network_group_new",      XST_WIDGET_MODE_INSENSITIVE, XST_WIDGET_MODE_SENSITIVE, TRUE,  TRUE  },
	{ "network_delete",         XST_WIDGET_MODE_SENSITIVE,   XST_WIDGET_MODE_SENSITIVE, TRUE,  FALSE },
	{ "network_settings",       XST_WIDGET_MODE_SENSITIVE,   XST_WIDGET_MODE_SENSITIVE, TRUE,  FALSE },
	{ "showall",                XST_WIDGET_MODE_HIDDEN,      XST_WIDGET_MODE_SENSITIVE, FALSE, TRUE  },
	{ NULL }
};

static SearchBarItem user_search_menu_items[] = {
	{ N_("Show All"), 0 },
	{ NULL, -1}
};

static void
user_menu_activated (SearchBar *sb, int id, gpointer user_data)
{
	switch (id)
	{
	case 0:
		user_query_string_set ("all");
		tables_update_content ();
		break;
	default:
		g_warning ("user_menu_activated: shouldn't be here.");
		break;
	}
}

enum {
	SB_USER_NAME,
	SB_USER_UID,
	SB_GROUP_NAME,
	SB_USER_GID,
};

static SearchBarItem user_search_option_items[] = {
	{ N_("User name contains"), SB_USER_NAME },
	{ N_("User ID is"), SB_USER_UID },
	{ N_("Group name contains"), SB_GROUP_NAME },
	{ N_("User GID is"), SB_USER_GID },
	{ NULL, -1 }
};

static void
user_query_changed (SearchBar *esb, gpointer user_data)
{
	gchar *search_word, *search_query;
	int search_type;

	gtk_object_get (GTK_OBJECT (esb),
			"text", &search_word,
			"option_choice", &search_type,
			NULL);

	if (search_word && strlen (search_word)) {
		switch (search_type) {
		case SB_USER_NAME:
			search_query = g_strdup_printf ("contains login %s",
							search_word);
			break;
		case SB_USER_UID:
			search_query = g_strdup_printf ("is uid %s",
							search_word);
			break;
		case SB_USER_GID:
			search_query = g_strdup_printf ("is gid %s",
							search_word);
			break;
		case SB_GROUP_NAME:
			search_query = g_strdup_printf ("contains group %s",
							search_word);
			break;
		default:
			search_query = g_strdup ("all");
			break;
		}
	}

	else
		search_query = g_strdup ("all");
	
	user_query_string_set (search_query);
	tables_update_content ();
	
	g_free (search_query);
	g_free (search_word);
}

static void
update_searchbar_complexity (XstDialogComplexity complexity)
{	
	SearchBar *sb = SEARCH_BAR (gtk_object_get_data (GTK_OBJECT (tool->main_dialog), 
	                               "SearchBar"));

	switch (complexity) {
	case XST_DIALOG_BASIC:
		gtk_widget_hide (GTK_WIDGET (sb));
		break;
	case XST_DIALOG_ADVANCED:
		gtk_widget_show (GTK_WIDGET (sb));
		break;
	default:
		g_warning ("update_searchbar_complexity: Unsupported complexity.");
		return;
	}
}

static void
update_notebook_complexity (XstDialogComplexity complexity)
{
	GtkWidget *notebook = xst_dialog_get_widget (tool->main_dialog, "notebook");

	switch (complexity) {
	case XST_DIALOG_BASIC:
		gtk_notebook_set_show_tabs (GTK_NOTEBOOK (notebook), FALSE);
		gtk_notebook_set_current_page (GTK_NOTEBOOK (notebook), 0);
		break;
	case XST_DIALOG_ADVANCED:
		gtk_notebook_set_show_tabs (GTK_NOTEBOOK (notebook), TRUE);
		break;
	default:
		g_warning ("update_notebook_complexity: Unsupported complexity.");
	}
}

static void
update_toggle_complexity (XstDialogComplexity complexity)
{
	GtkWidget *toggle = xst_dialog_get_widget (tool->main_dialog, "showall");
	
	switch (complexity) {
	case XST_DIALOG_BASIC:
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (toggle), FALSE);
		break;
	case XST_DIALOG_ADVANCED:
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (toggle), xst_conf_get_boolean (tool, "showall"));
		break;
	default:
		g_warning ("update_notebook_complexity: Unsupported complexity.");
	}
}

static void
update_complexity (void)
{
	XstDialogComplexity complexity = tool->main_dialog->complexity;

	update_notebook_complexity (complexity);
	update_toggle_complexity (complexity);
//	update_searchbar_complexity (complexity);	
	update_tables_complexity (complexity);
}

static void
connect_signals (void)
{
	g_signal_connect (G_OBJECT (tool->main_dialog), "complexity_change",
	                  G_CALLBACK (update_complexity),
	                  NULL);

	xst_dialog_connect_signals (tool->main_dialog, signals);
}

static void
create_searchbar (void)
{
	GtkWidget *table;
	SearchBar *search;

	table = xst_dialog_get_widget (tool->main_dialog, "user_parent");

	search = SEARCH_BAR (search_bar_new (user_search_menu_items, user_search_option_items));
	gtk_table_attach (GTK_TABLE (table), GTK_WIDGET (search), 0, 1, 0, 1,
			  GTK_FILL, GTK_FILL, 0, 0);

	gtk_signal_connect (GTK_OBJECT (search), "query_changed",
			    GTK_SIGNAL_FUNC (user_query_changed), 0);
	gtk_signal_connect (GTK_OBJECT (search), "menu_activated",
			    GTK_SIGNAL_FUNC (user_menu_activated), 0);
	gtk_object_set_data (GTK_OBJECT (tool->main_dialog), "SearchBar",
			     (gpointer) search);

	/* Show/hide */
	update_searchbar_complexity (tool->main_dialog->complexity);
}

static void
main_window_prepare (void)
{
	/* For random password generation. */
	srand (time (NULL));

	/* Create tables */
	construct_tables ();
	
//	create_searchbar ();

	/* General complexity update */
	update_complexity ();

	/* This sucks, but calculating the needed size for simple mode based on the
	 * hidden widgets plus the tabs size is going to be ugly. Chema
	 */
	gtk_window_set_default_size (GTK_WINDOW (tool->main_dialog), 550, 400);
}

int
main (int argc, char *argv[])
{
	xst_init ("users-admin", argc, argv, NULL);
	tool = xst_tool_new ();
	xst_tool_construct (tool, "users", _("Users and Groups"));

	xst_tool_set_xml_funcs  (tool, transfer_xml_to_gui, transfer_gui_to_xml, NULL);

	xst_dialog_enable_complexity (tool->main_dialog);
	xst_dialog_set_widget_policies (tool->main_dialog, policies);

	main_window_prepare ();
	connect_signals ();

	xst_tool_main (tool, FALSE);
	
	return 0;
}
