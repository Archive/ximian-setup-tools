2002-04-22  Kjartan Maraas  <kmaraas@gnome.org>

	* callbacks.c: Fix a missing space in a string.
	
2002-04-10  Tambet Ingo  <tambet@ximian.com>

	* hosts.c: Removed some old junk.

	* connection.c (connection_list_select_connection): Check return value, doh.

2002-04-08  Tambet Ingo  <tambet@ximian.com>

	* Port to the GNOME 2.0 platform, usable, with
	some little bugs.

2002-02-11  Seth Nickell  <snickell@stanford.edu>

	reviewed by: <delete if not using a buddy>

	* network.desktop.in.in:

2002-02-10  Chema Celorio  <chema@celorio.com>

	* Port to the GNOME 2.0 platform

2002-02-08  Chema Celorio  <chema@celorio.com>

	* configure.in: 0.12.0 here we go

2002-01-02  Arturo Espinosa Aldama  <arturo@ximian.com>

	* callbacks.c (on_connection_deactivate_clicked): Use
	async directives again.
	POLL_HACK activated again.
	Removed debug stuff.

2001-12-20  Arturo Espinosa Aldama  <arturo@ximian.com>

	* transfer.c (transfer_string_list_gui_to_xml): Skip
	(transfer_string_is_empty): Check if a string is not
	composed of alnums.
	(transfer_string_list_gui_to_xml): skip empty lines.
	This avoids empty entries in /etc/resolv.conf.

	* connection.c (connection_init_clist): Made the colum
	titles passive: they don't do anything.

2001-12-19  Arturo Espinosa Aldama  <arturo@ximian.com>

	* callbacks.c (callbacks_tool_not_found_hook): removed
	debug code.
	(on_connection_deactivate_clicked): Don't use async
	directive queuing.
	(on_connection_activate_clicked): same.

	* connection.c (connection_default_gw_set_manual): Check
	remote_address first before g_strdup'ing.
	(connection_default_gw_add): Receive the whole connection
	structure and do a smarter detection of lo ifaces.

2001-12-18  Arturo Espinosa Aldama  <arturo@ximian.com>

	* connection.c (connection_new_from_node): Don't derive
	gateway from global gateway: they mean different things.

	* transfer.c (transfer_interfaces_to_xml): save those
	interfaces in the hidden list too.
	(transfer_interfaces_to_gui): update the connection
	complexity, to hide the lo ifaces if necesary.

	* connection.c (connection_update_row_do): some wrappers
	for the lo connection hiding hack.
	(connection_add_to_list_do): same.
	(connection_update_complexity_advanced): complexity handlers.
	(connection_update_complexity_basic): same.
	(connection_update_complexity): same.
	(connection_actions_set_sensitive): moved from callbacks.c

	* callbacks.c (on_connection_list_select_row): unsensitize
	buttons only if in basic mode.
	(on_network_admin_show): call connection_init_gui from here.

	* connection.c (connection_dialog_set_visible_pages): Hide DHCP
	options if configuring a lo iface.

2001-12-18  Tambet Ingo  <tambet@ximian.com>

	* callbacks.c (callbacks_has_dialer): Implement. Before I got warning
	message about missing wvdial even though I didn't use PPP.
	(callbacks_check_dialer): Use it!
	(callbacks_check_dialer_hook): ditto.

2001-12-17  Arturo Espinosa Aldama  <arturo@ximian.com>

	* hosts.c (on_hosts_delete_clicked): Take the first field, not the
	second. This made the confirmation not to make sense.

2001-12-11  Israel Escalante <israel@ximian.com>

	* Release 0.10.0.

2001-12-11  Arturo Espinosa Aldama  <arturo@ximian.com>

	* main.c (main): add the new check_hostname hook.

	* callbacks.c (callbacks_check_hostname_hook): was using freed memory
	to set the old entrie's value. fixed.
	(callbacks_tool_not_found_hook): Warn the user if some important program
	ran by the backend was not found.

2001-10-17  Arturo Espinosa Aldama  <arturo@ximian.com>

	* callbacks.c (poll_connections_cb): Quick hack to make the
	clist change if an interface changed status by an external program.

2001-12-02  Chema Celorio  <chema@celorio.com>

	* move more stuff into CommonMakefile

	* Makefile.am: replace the @XST_TOOL_DESKTOP_IN_RULE@ with :
	include ../common/CommonMakefile. If this does not break anything
	i'll make the same for the rest of the tools.

2001-10-16  Arturo Espinosa Aldama  <arturo@ximian.com>

	* callbacks.c: Relocated POLL_HACK #ifdef to avoid warnings.

2001-10-06  Arturo Espinosa Aldama  <arturo@ximian.com>

	* callbacks.c (on_network_notebook_switch_page): If the connections
	page is selected, start polling timeouts, but only the first time.
	(poll_connections): The timeout function that queues the directive.
	(poll_connections_cb): The directive callback that processes the
	XML and animates the clist rows.
	Some other minor changes to make all this work.
	All this is disabled: uncomment the POLL_HACK #define.

2001-09-25  Arturo Espinosa Aldama  <arturo@ximian.com>

	* main.c: In general, removed the tool global variable, but had to leave
	it there because the callbacks depend to heavily on this.

	* connection.c (load_icon): Hack to make pixmap be centered on row too,
	required because of a bug in GtkCList.
	(connection_init_clist): Set the rowheight to double the size.
	(connection_add_to_list): Set the shift of every cell manually, to center
	the text and pixmaps.

2001-09-13  Arturo Espinosa Aldama  <arturo@ximian.com>

	* ppp-druid.c (ppp_druid_new): indentation.

	* connection.c (connection_configure): removed minor leak.

2001-09-05  Chema Celorio  <chema@celorio.com>

	* connection.c (connection_configure): set cxn->tmp_ip_config everytime
	the dialog is configured. Fixes a bug i found at LWE while demoing the tools
	where the sensitivity of the IP entry fields will not be set correctly from
	the connection type. Other code cleaning.

2001-08-15  Arturo Espinosa Aldama  <arturo@ximian.com>

	* callbacks.h: added #ifndef __CALLBACKS_H__ hack.

2001-08-09  Arturo Espinosa Aldama  <arturo@ximian.com>

	* connection.c (connection_configure): Call the check every
	time a ppp connection is going to be configured.

	* callbacks.c (callbacks_check_dialer): Pop dialog complaining
	wvdial is not installed if such is the case.

2001-07-25  Arturo Espinosa Aldama  <arturo@ximian.com>

	* connection.c (connection_dev_get_next): Deprecated.
	(connection_new_from_type): Figure a new device name. Just
	gets the next dev number (eth0, eth1, etc). TODO: get a
	list of detected (ifconfig -a) ifaces and take next from
	there.
	(connection_configure): Disabled wavelan settings tab
	until this stuff is supported.

	* callbacks.c (on_connection_add_clicked): irlan support.

Wed Jul 25 03:13:49 2001  Arturo Espinosa Aldama  <arturo@ximian.com>

	* connection.c (connection_addr_to_str): use %u, not %d.
	(hookup_callbacks): Also check netmask when leaving
	netmask entry.
	(connection_updatedns_supported): PLIP is not supported
	by pump.

2001-07-24  Arturo Espinosa Aldama  <arturo@ximian.com>

	* connection.c (connection_configure): Change pixmap on
	the General Tab for all connection types, now that we
	have the pixmaps.
	(connection_check_netmask_gui): Taken out from
	on_ip_address_focus_out for empty_ip to use.
	(connection_set_bcast_and_network): calculate this info
	from cxn->address and cxn->netmask, if possible.
	(connection_str_to_addr): helper.
	(connection_addr_to_str): same.
	(empty_ip): Do connection_check_netmask_gui befor getting info.
	(connection_addr_to_str): Removed unused variable.

2001-07-23  Arturo Espinosa Aldama  <arturo@ximian.com>

	* connection.c (on_ip_address_focus_out): minor style edit.
	(connection_type_is_lan): helper.
	Added IRLAN support all over the place.
	(connection_default_gw_check_manual): IRLAN is same as
	ETH and WVLAN.
	(connection_default_gw_set_manual): same.
	(connection_type_is_lan): same.
	(connection_new_from_type): same.
	(connection_validate): same.
	(connection_empty_gui): Made eth case explicit. Added irlan.
	(fill_ip): Renamed "blah" array (wtf?) to bootproto_labels.
	Waiting for IRLAN icon to be produced. #3059 marked NOTABUG.
	(connection_configure): Change the pixmap on the General tab
	depending on the connection type.

2001-07-19  Joakim Ziegler  <joakim@ximian.com>

  * connection.c: Some robustness fixes from Hans Petter.

  * connection.c (on_ip_address_focus_out): Added netmask auto-fillin on
  focus out from the ip address.

Sun Jul 15 03:32:43 2001  Arturo Espinosa Aldama  <arturo@ximian.com>

	* connection.c (connection_configure): If distro doesn't support
	mortal user control of interface activation, hide corresponding
	widget.
	(on_connection_modified): Removed debug warning.

	* callbacks.c (on_connection_deactivate_clicked): Save tool
	if it has changed.
	(on_connection_activate_clicked): same.

	* main.c (main): removed update_connections hook registration.

	* connection.c (connection_update_row_enabled): We're departing
	from the "To (Ina/A)ctivate" model: just use "(Ina/A)ctivate".

	* callbacks.c (callbacks_update_connections_hook): Use
	connection_update_clist_enabled_apply now.

	* connection.c (connection_update_clist_enabled_apply): Change all
	connections from "To (Ina/A)ctivate" to "(Ina/A)ctive).

Sat Jul 14 15:18:43 2001  Arturo Espinosa Aldama  <arturo@ximian.com>

	* connection.c (connection_new_from_node): Set new file struct member.

	* callbacks.c (on_connection_activate_clicked): Run enable_iface
	directive to perform operation.
	(on_connection_deactivate_clicked): same.

Fri Jul 13 19:10:59 2001  Arturo Espinosa Aldama  <arturo@ximian.com>

	* main.c (update_notebook_complexity): Return trivialy if
	hosts container has no page, because that could happen if
	the tool starts in advanced mode.

	* connection.c (connection_updatedns_supported): Tell if the
	connection can handle the updatedns tag.
	(connection_save_to_node): If it has support, then save the
	updatedns node.
	(on_connection_ok_clicked): Only connection_config_save
	if cxn->modified, making OK button not highlight Apply if no
	changes were made.

2001-07-12  Chema Celorio  <chema@celorio.com>

	* main.c (main): set the complexity before calling
	xst_tool_main

2001-07-11  Arturo Espinosa Aldama  <arturo@ximian.com>

	* transfer.c (transfer_gatewaydev_to_xml): Set gatewaydev
	AND gateway tags. gatewaydev only if supported by distro.

	* connection.c (connection_default_gw_init): Renamed from
	connection_default_gw_init. Setup "Auto" entry, which comes
	from the glade file too.
	(connection_default_gw_get_connection): Get the connection
	struct that has been chosen for gatewaydev.
	(connection_default_gw_check_manual): Check if this connection
	is ready to point to a default gw. Return first found problem.
	(connection_default_gw_fix): Fix the problem.
	(connection_default_gw_set_manual): Assuming the connection
	is ready, go ahead and set the user_data accordingly.
	(connection_default_gw_find_static): Try fo find an active
	ethernet or wavelan connection with a static gateway definded.
	(connection_default_gw_find_ppp): Try fo find an active PPP
	connection with the default_gw bit on.
	(connection_default_gw_find_dynamic): Try fo find an active
	ethernet or wavelan connection with dynamic configuration.
	(connection_default_gw_find_plip): Try fo find an active plip
	connection with a set remote address.
	(connection_default_gw_set_auto): Try all the above, in that
	order, and use that connection as the default gw device.
	(connection_find_by_dev): Find in connection_list a connection
	with given dev name.
	Changed FIXME #warnings to comments.

	* main.c: cosmetic stuff.

	* callbacks.c (callbacks_check_hostname_hook): cosmetic.
	(callbacks_disabled_gatewaydev_warn): Tell the user
	that the default gw is not activated, with option to
	immediately activate.
	(callbacks_check_manual_gatewaydev): The default gw
	has been manually set. Check that the device can have a
	default gateway, and if so, set it.
	(callbacks_check_gateway_hook): Hook for checking that
	the stuff from the default gw widget is ready.

2001-07-10  Arturo Espinosa Aldama  <arturo@ximian.com>

	* callbacks.c (callbacks_check_hostname_hook): memory
	leak.

	* main.c (connect_signals): Connect selection-done to
	set selected device.
	(update_notebook_complexity): Move from page only if
	current page is the one to remove.

	* transfer.c (transfer_gatewaydev_to_xml): Get selected
	value and put into corresponding tag.
	(transfer_xml_to_gatewaydev): converse.
	(transfer_misc_tool_to_xml): Just to keep with the naming.
	(transfer_misc_xml_to_tool): Do the gatewaydev stuff too.
	(transfer_gui_to_xml): Do the misc stuff too.

	* connection.c (connection_update_complexity): Removed:
	using XstWidgetMode for this.

2001-07-09  Arturo Espinosa Aldama  <arturo@ximian.com>

	* connection.c (connection_update_complexity):
	Show/hide default gw option menu.
	(connection_default_gw_exists): Check if a dev
	is already in defgw omenu.
	(connection_default_gw_add): Add a dev to the omenu.
	(connection_default_gw_remove): Remove.

	* main.c (update_notebook_complexity): Remove and
	append Hosts tab depending on complexity.
	(main): activate complexity support.
	(update_complexity): callback.

2001-06-18    <chema@celorio.com>

	* connection.c: s/networking/network/g .png

2001-05-31  Tambet Ingo  <tambet@ximian.com>

	* connection.c: Removed (my_entry_set_text).
	Replaced calls with xst_ui_entry_set_text.

2001-05-29  Arturo Espinosa Aldama  <arturo@ximian.com>

	* connection.c (connection_validate): Check if
	the information for a connection is consistent.
	Display error message if not.
	(strempty): helper.
	(connection_config_save): Validate connection
	info befor accepting changes.
	(connection_empty_gui): Sepparated from
	connection_config_save.
	(connection_config_save): Also copy ip_config
	struct members, fixing bug 3115.

2001-05-28  Arturo Espinosa Aldama  <arturo@ximian.com>

	* connection.c (hookup_callbacks): Do IP checks on
	the pertinent connection entries too.

	* callbacks.c (filter_editable): Special case check
	for EF_ALLOW_IP
	(str_insert_text): inserts one string into another.
	(is_ip_text_ok): Routine that checks if a string
	is not an invalid partial IP.

2001-05-29  Chema Celorio  <chema@celorio.com>

	* Makefile.am (network_admin_LDADD): use XST_LIBS
	(network_admin_LDADD): clean

2001-05-26  Arturo Espinosa Aldama  <arturo@ximian.com>

	* connection.c (connection_clist_cmp): tyring to strcmp
	null strings? Refrain.
	(my_strcmp): Part of the same fix to bug 3055.

2001-05-25  Arturo Espinosa Aldama  <arturo@ximian.com>

	* connection.h: removed peerdns from connection struct.

2001-05-24  Arturo Espinosa Aldama  <arturo@ximian.com>

	* ppp-druid.c (ppp_druid_save): peerdns has been deprecated.
	Using update_dns.

2001-05-24  Arturo Espinosa Aldama  <arturo@ximian.com>

	* 0.5 RELEASE

2001-05-24  Arturo Espinosa Aldama  <arturo@ximian.com>

	* connection.[hc], callbacks.c: fixed a couple of trivial
	warnings and made Unknown interfaces appear with the
	default icon and device name in connections list.

2001-05-23  Arturo Espinosa Aldama  <arturo@ximian.com>

	* transfer.c (transfer_misc_xml_to_tool): Added smartdhcpcd
	userdata, although it is not required now.

	* main.c: More dns_dhcp removing.

	* connection.c: Lots of changes to make the update_dns
	connection checkbox work and be hidden in unsupporting distros.

	* callbacks.c (on_network_admin_show): removing dns_dhcp code.
	(on_network_notebook_switch_page): Focus the domain entry when
	witching now.
	(on_dns_dhcp_toggled): removed.

2001-05-24  Chema Celorio  <chema@celorio.com>

	* callbacks.c : added g_return_if_fail checks.

	* connection.c (connection_update_row): XST_CONNECTION_UNKNOWN is -1,
	and we where using it as an array offset of mini_pm[cxt->type]. Fixed.
	
	- added a bunch of g_return_if_fail.

2001-05-23  Arturo Espinosa Aldama  <arturo@ximian.com>

	* hosts.c (on_hosts_alias_changed): changed warning to simple
	if/return: mortal users are allowed to view the information.

2001-05-22  Carlos Perell� Mar�n <carlos@gnome-db.org>

	* connection.c: Marked xst_iface_desc[i].description to be translated
	as Arturo has suggested me.

2001-05-21  Chema Celorio  <chema@celorio.com>

	* callbacks.c: minor cosmetic fix

2001-05-18  Hans Petter Jansson  <hpj@ximian.com>

	* *.pam: Modified PAM configuration so that invoking as root works.

2001-05-17  Hans Petter Jansson  <hpj@ximian.com>

	* *.pam: Modified PAM configuration so that console-helper will ask
	  for root auth.

2001-05-14  Arturo Espinosa Aldama  <arturo@ximian.com>

	* connection.c (connection_xml_wvsection_has_name): Segmentation
	fault bug fixed by G�rard Milmeister <gemi@bluewin.ch>.

2001-05-13  Chema Celorio  <chema@celorio.com>

	* ppp-druid.c (ppp_druid_check_last): don't include newlines in
	strings, use \n instead

2001-05-13  Chema Celorio  <chema@celorio.com>

	* ppp-druid.c: s/..\/common/globa.h/xst.h/g

2001-05-12  Chema Celorio  <chema@celorio.com>

	* transfer.c (xst_xml_element_get_boolean): remove from here and put
	in src/common/xst-xml

2001-05-10  Chema Celorio  <chema@celorio.com>

	* main.c (main): update to the new initialization method

2001-05-09  Arturo Espinosa Aldama  <arturo@ximian.com>

	* 0.4 RELEASE

2001-05-08  Arturo Espinosa Aldama  <arturo@ximian.com>

	* transfer.c (xst_xml_element_get_boolean): utility.
	(transfer_misc_xml_to_tool): Also get the dialinstalled
	flag from the XML.

	* main.c (main): add check_dialer apply hook.

	* callbacks.c (on_network_admin_show): hiding dns_dhcp
	until it actually does sth.
	(callbacks_check_dialer_hook): Warn the user if wvdial
	is not installed.

	* transfer.c (transfer_string_list_gui_to_xml): fixed
	core dump caused by editables not ending in \n.

2001-04-26  Chema Celorio  <chema@celorio.com>

	* Makefile.am: add $(srcdir) to security.in

2001-04-20  Arturo Espinosa Aldama  <arturo@ximian.com>

	* main.c (main): Moved init_editable_filters inisde else
	clause: it's not required for the ppp druid.

	* connection.c (hookup_callbacks): put some filters to the
	entries in the connection dialog too.

	* callbacks.c (is_char_ok): Allow _ and - if EF_ALLOW_TEXT.
	(init_editable_filters): Moved code from main() to here.

2001-04-19  Arturo Espinosa Aldama  <arturo@ximian.com>

	* main.c (main): spaces are not allowed for the alias text area.

	* hosts.c (xst_hosts_update_sensitivity): change button is no longer used.
	(on_hosts_alias_changed): record updating is done now as we type.

	* connection.c (connection_update_row): Doesn't call connection_update_enabled
	  anymore. Calling this by hand where needed in this file.

	* callbacks.c (on_dns_dhcp_toggled): changes sensitivity for the whole
	  dns_table.

	* transfer.c (transfer_misc_xml_to_tool): add an object data that flags if
	  the tool has passed the configuration phase, so the callback knows what's
	  the conext.

	* main.c: samba_use widget doesn't automatically call modify_cb: it depends
	  on samba being actually installed.

	* connection.c (connection_clist_cmp): comparisong function that sorts by
	  dev, then by description.
	(connection_init_clist): Configure the connections clist.
	(connection_init_gui): call init_clist.
	(connection_add_to_list): deleted spurious code.

	* callbacks.c (on_connection_add_clicked): use new_from_type.
	(on_samba_use_toggled): Check if we are in configuration fase. If so and
	  samba is not installed and we are trying to activate smb, show message.
	  Don't call modify_cb if !smb_installed.


2001-04-17  Arturo Espinosa Aldama  <arturo@ximian.com>

	* transfer.c (transfer_misc_xml_to_tool): set flags and variables that don't
	  have a related GUI element, such as smb_installed.
	  In general, passed the tool var to all functions and deleted global tool
	  pointer.

	* callbacks.c (on_samba_use_toggled): if samba is not installed, don't allow
	  windows networking activation, and send according message to user.

	* connection.c (empty_general, fill_general, hookup_callbacks): removed code
	regarding the old "enable" check, which caused warnings.

2001-04-14  Chema Celorio  <chema@celorio.com>

	* connection.c (connection_dev_get_next): add a g_return_if_fail to
	avoid a crash. Add a #warning so that arturo fixes the real problem.

2001-04-03  Arturo Espinosa Aldama  <arturo@ximian.com>

	* main.c (main): add update_connections hook.

	* connection.c (connection_set_row_pixtext): Sepparated this code
	from connection_update_row_enabled to call from new hook.

	* callbacks.c (callbacks_update_connections_hook): A hook to change
	the connections labels when applying.

2001-03-30  Tambet Ingo  <tambet@ximian.com>

	* Makefile.am: Add gconf cflags.

2001-03-28  Arturo Espinosa Aldama  <arturo@ximian.com>

	* transfer.c (transfer_string_entry_table): Added smbuse entry.

2001-03-23  Tambet Ingo  <tambet@ximian.com>

	* connection.c (connection_xml_get_str): Removed. We already had that
	  function in common/xst-xml.c Replaced in callers too.

2001-03-22  Arturo Espinosa Aldama  <arturo@ximian.com>

	* callbacks.c (on_connection_delete_clicked): If the connection has
	  no name, don't try to display it when confirming.
	* connection.c: PLIP connections don't require the netmask param
	  anymore.
	* connectioin.[hc]: When creating an interface, the configure
	  dialog comes up. If Cancel is hit, delete the interface.

2001-03-16  Hans Petter Jansson  <hpj@ximian.com>

	* main.c: Added widget policy table.
	  (main): Added call to xst_dialog_set_widget_policies ().

	* callbacks.c (on_network_admin_show): Removed manual widget policing
	  based on access.
	  (connection_actions_set_sensitive): Use
	  xst_dialog_widget_set_user_sensitive ().
	  (on_dns_dhcp_toggled): Ditto.
	  (on_samba_use_toggled): Ditto.
	  (on_wins_use_toggled): Ditto.
	  (callbacks_check_hostname_hook): Fixed spelling and broke up string
	  for nicer source formatting.

2001-03-12  Arturo Espinosa Aldama  <arturo@ximian.com>

	* connection.c: PLIP support. Don't care if nobody cares.
	* hosts.c: deleted all debug messages.
	* ppp-druid.c: deleted all debug messages.
	* transfer.c: deleted all debug messages.
	
2001-03-12  Arturo Espinosa Aldama  <arturo@ximian.com>

	* connection.c (connection_update_row_enabled): Function that
	  sets the "active" column, and if it is to enable, disables all
	  other connection configurations that use the same device.
	  (connection_update_row): Use the new function.

	* callbacks.c (on_connection_activate_clicked): Use the new function.
	(on_connection_deactivate_clicked): Use the new function.

2001-03-05  Arturo Espinosa Aldama  <arturo@ximian.com>

	* main.c (main): Add callbacks_check_hostname_hook to the list of
	apply hooks.

	* callbacks.c (callbacks_check_hostname_hook): Check if hostname
	changed, and if so, warn the user with the option to revert the
	host change.

2001-03-01  Arturo Espinosa Aldama  <arturo@ximian.com>

	* main.c: added activation callbacks to table.

	* connection.c (connection_update_row): Made this function public.

	* callbacks.c (on_connection_deactivate_clicked): activation callback.
	(on_connection_activate_clicked): activation callback.

	* connection.c (load_icon): scale to 16x16, not 24x24. Now the list
	looks much better.
	* moved to new unified xst_iface_desc table, instead of a bunch of
	little tables.

2001-03-01  Chema Celorio  <chema@celorio.com>

	* connection.c (load_icon): constify
	move all the table of connection types outside. This is not compiling
	but Arturo is going to finish it.

2001-03-01  Arturo Espinosa Aldama  <arturo@ximian.com>

	* callbacks.c (on_connection_delete_clicked): Better confirm message.

	* connection.c (connection_free): free allocated strings and destroy
	XML node.

2001-02-28  Hans Petter Jansson  <hpj@ximian.com>

	* Makefile.am: Disable control-center entry.

2001-02-28  Arturo Espinosa Aldama  <arturo@ximian.com>

	* connection.c (connection_wvsection_name_generate): forgot ++i. it hung.

2001-02-27  Arturo Espinosa Aldama  <arturo@ximian.com>

	* transfer.c (transfer_string_entry_table): add wins_use.
	(transfer_string_entry_xml_to_gui): fix support for toggle buttons.
	(transfer_string_entry_gui_to_xml): fix support for toggle buttons.

	* main.c: signals for wins_use.

	* hosts.c (xst_hosts_update_sensitivity): don't let loopback
	statichost entry be modified.
	(on_hosts_add_clicked): highlight newly added entry.

	* connection.c (connection_new_from_node): take global gateway
	param if one for the connection is not present.
	(fill_ppp_adv): set wins_use state.

	* callbacks.c (on_connection_delete_clicked): Activate "Apply".
	(on_wins_use_toggled): callback for new check button.
	

2001-02-27  Chema Celorio  <chema@celorio.com>

	* Makefile.am (EXTRA_DIST): distcheck fixes

2001-02-27  Arturo Espinosa Aldama  <arturo@ximian.com>

	* ppp-druid.c (my_get_widget): support new widget name prefix.
	  (ppp_druid_get_serial_port): to get the default serial port:
	  most people have only 1 modem.
	  (ppp_druid_save): get all required info into the connection
	  structure and signal the tool to apply.
	  (ppp_druid_gui_to_xml): tool callback to apply the ppp druid
	  info into the XML.

	* main.c (watch_it_now_watch_it): not pertinent anymore.
	  all widgets connected to delete_text are now connected to changed.
	  (main): popt options moved here.
	  Due to glade file merge, start-up code is now better shared.
	  

	* connection.c (connection_wvsection_name_generate): replacement for
	  devkey_generate. This one creates a less arbitrary name.
	  (connection_dev_get_next): Try to find the next device of the requested
	  type. Useful for ppp, but will probably have to be more intelligent for
	  other interface types.
	  (connection_get_serial_port_from_node): Get the modem device for a
	  wvsection. Very useful for section "Defaults".
	  (connection_new_from_type): don't call add_connection_to_list here.
	  (connection_new_from_type_add): do it here. For code reusability. Also,
	  pass the root node in these two, to use the system discovery tags.

	* callbacks.c (on_connection_list_select_row): don't set the actions sensitive
	  if a loopback interface is selected.
	
2001-02-26  Chema Celorio  <chema@celorio.com>

	* hosts.c: more fixes for the hosts lists

2001-02-26  Arturo Espinosa Aldama  <arturo@ximian.com>

	* transfer.c (transfer_string_clist2_gui_to_xml_item): Take data from correct rows.

2001-02-24  Arturo Espinosa Aldama  <arturo@ximian.com>

	* connection.[hc]: peerdns support.
	
2001-02-24  Arturo Espinosa Aldama  <arturo@ximian.com>

	* connection.c (connection_xml_wvsection_search): specify type you are looking for.
	(connection_xml_wvsection_get_inherits_node): for the code's sake. Also, improves
	wvdial support, as this function looks for the Defaults section if the section
	doesn't inherit anything.
	(connection_xml_wvsection_node_get_str): Finally, a compact function that gets it
	right too.

2001-02-23  Arturo Espinosa Aldama  <arturo@ximian.com>

	* connection.h: struc Connection: use gchar, gint and gfloat types. file field.

	* connection.c (connection_devkey_generate): name change from wvsection_generate.
	(connection_get_ppp_from_node):	for code readability. Don't get if interface is
	not CONNECTION_PPP type.
	(connection_new_from_node): handle the file tag.
	(connection_new_from_type): the default autoboot value varies on the type.
	

2001-02-23  Chema Celorio  <chema@celorio.com>

	* main.c: update some signals

	* hosts.c: and into here. Rewrite it is now smarter when the user
	types an ip that is on the list. The button sensitivity now makes
	a bit more sense than before.

	* callbacks.c: remove all the statichosts from here

2001-02-22  Chema Celorio  <chema@celorio.com>

	* callbacks.c (on_statichost_add_clicked): remove the enable/disbale
	thingie.

	* transfer.c (transfer_string_clist2_xml_to_gui): remove the enable/disable
	thingie

	* callbacks.c (on_statichost_update_clicked): remove the enable thingie
	(on_statichost_add_clicked): ditto
	(on_statichost_changed): ditto

2001-02-22  Arturo Espinosa Aldama  <arturo@ximian.com>

	* connection.c (connection_xml_wvsection_search): return the node of a wvsection, by name.
	* connection.c (connection_xml_wvsection_save_str_to_node): and the boolean counterpart.
	* connection.c (connection_wvsection_generate): generate a random wvsection name.
	* connection.c: a bunch of widget renamings, for consistent naming, and PPP parameter saving.

2001-02-22  Chema Celorio  <chema@celorio.com>

	* transfer.c (transfer_string_clist2_xml_to_gui): plug a mem-leak
	: remove g_prints
	(transfer_string_clist2_xml_to_gui): another one

2001-02-22  Chema Celorio  <chema@celorio.com>

	* add xst_ prefix to the xml_ functions

2001-02-22  Chema Celorio  <chema@celorio.com>

	* connection.c: fix cvs conflicts

2001-02-22  Arturo Espinosa Aldama  <arturo@ximian.com>

	* callbacks.c (scrolled_window_scroll_bottom): helper.
	  Changed the connection_type* widget names.
	* callbacks.c (on_connection_add_clicked): When adding a connection, 
	  pop up its config dialog immediatly, and scroll clist to the bottom, 
	  to see the newly created element.
	* connection.c: Changed all status_button stuff to status_enabled.
	  wvsection XML reading functions for ppp parameters.
	* connection.c (connection_new_from_node): Fill all the ppp params.
	* connection.c (fill_ppp): implemented.
	* connection.c (fill_ppp_adv): implemented.
	* connection.c (connection_configure): kill ip page when configuring for
	  a ppp iface.
	* connection.h (Connection): new ppp members in structure.

2001-02-21  Chema Celorio  <chema@celorio.com>

	* transfer.c (transfer_gui_to_xml): remove #if 0 that I left
	here.

2001-02-21  Chema Celorio  <chema@celorio.com>

	* connection.c (connection_description_from_type): return a const
	string. Avoids a crash.
	(connection_new_from_node): g_strdup the string that goes
	into cxt->name.

2001-02-21  Arturo Espinosa  <arturo@ximian.com>

	* connection.c: changed some callback names and set stuff up
	  for the new OK/Cancel dialog form.
	
2001-02-20  Arturo Espinosa  <arturo@ximian.com>

	* connection.c: added static directive to all functions that
	  were not used elsewhere. Renamed some callbacks to follow
	  glade naming.
	  (my_get_widget): issues warning when widget could not be found.
	  (my_entry_set_text): if text is NULL, use empty string.
	  (connection_xml_get_str): saves from repetitive work.
	  (connection_xml_get_boolean): same.
	  (connection_xml_save_str_to_node): creates/destroys node if necessary.
	  (connection_xml_save_boolean_to_node): same, just interpreting as a boolean.
	  (connection_config_type_from_str): take XML string value, give a type.
	  (connection_config_type_to_str): take connection type, give XML string value.
	  (connection_init_icons): saner name for init_icons.
	  Commented out add_connections_to_list.
	  (connection_description_from_type): gives a default description to interfaces, by type.
	  Handle PLIP and LO connection types.
	  (connection_save_to_node): Write dev, name, enabled, user, auto, address, netmask, broadcast,
	  network, gateway and bootproto features.
	  (connection_new_from_node): same, but for reading. Save xml node used for subsequent saving.
	* connection.h:
	  (Connection): handle new features, and renamed others to match tag names.
	    Member to keep xml node.
	  Added "extern" directive to public functions.
	  PLIP and LO connection types.
	* main.c: call connection_init_icons instead.
	* transfer.c: uncomment interface saving call.
	
2001-02-19  Chema Celorio  <chema@celorio.com>

	* transfer.c (transfer_string_clist2_gui_to_xml): split into
	(transfer_string_clist2_gui_to_xml_item): and clean 

2001-02-19  Arturo Espinosa  <arturo@ximian.com>

	* transfer.c: don't add "enabled" attribute.
	  Fixed bug that repeated ip tag inside statichost tags.
	* connection.c: (connection_new_from_node): set enabled bool.
	  method tag is now named bootproto. comply.

2001-02-17  Chema Celorio  <chema@celorio.com>

	* main.c (options): add options to network. 
	(main): only run the internet druid if "--internet" is specified on
	the command line

2001-02-17  Arturo Espinosa  <arturo@ximian.com>

	* ppp-druid.c: support for password confirmation, a more general
	  entry activation handler and command argument parsing: now you
	  use --internet_druid to come up with the druid.
	  
2001-02-16  Chema Celorio  <chema@celorio.com>

	* network.desktop.in: change the name to Newtworking
	from Networking 2

2001-02-16  Arturo Espinosa  <arturo@ximian.com>

	* ppp-druid.c: the GUI is almost finished. Added some warning
	  labels that come up when the user is trying to put invalid
	  data. The code has been tested an bug fixed.
	
2001-02-15  Arturo Espinosa  <arturo@ximian.com>

	* ppp-druid.[hc]: Initial experimental code for druid.
	  Still lacking a couple of functions, but almost complete.
	  No XML transferring yet.
	
2001-02-12  Arturo Espinosa  <arturo@ximian.com>

	* ppp-druid.[hc]: We'll start coding in these files the
	internet connection druid.
	* Makefile.am: take these new files as sources too.

2001-02-09  Chema Celorio  <chema@celorio.com>

	* transfer.c (transfer_string_list_gui_to_xml): fix a bug
	when we multiple entries where beeing merged.

2001-02-06  jacob berkman  <jacob@ximian.com>

	* main.c: add signals list

2001-02-03  Christian Meyer  <chrisime@gnome.org>

	* network.desktop.in: Added German strings.
	 
2001-02-02  jacob berkman  <jacob@ximian.com>

	* *.c: port to new api

2001-01-29  Arturo Espinosa  <arturo@ximian.com>

	* connection.c: connection_new_from_dev_name: take the connection
	type from the device name, displaying the right icon for the loaded
	configuration.

2001-01-29  jacob berkman  <jacob@ximian.com>

	* callbacks.c: use more helpful messages

2001-01-18  jacob berkman  <jacob@ximian.com>

	* connection.c (update_status): fixup activate stuff

	* transfer.c: read the interfaces from the backend

	* connection.c (connection_configure): fix up the help button, but
	disable it for now since there is no help for this dialog

2001-01-16  jacob berkman  <jacob@ximian.com>

	* connection.c: fix sensitivity on ip page based on config type.
	Also some other stuff.
	
2001-01-15  Chema Celorio  <chema@celorio.com>

	*  Start ChangeLog

