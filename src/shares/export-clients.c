/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/* Copyright (C) 2000-2001 Ximian, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Hans Petter Jansson <hpj@ximian.com>.
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <gnome.h>
#include "xst.h"

#include "tables.h"
#include "xml.h"
#include "validate.h"
#include "export-clients.h"

extern XstTool *tool;

#define string_is_blank(s) (!s || !strlen (s))

/* Structure holding refs to widgets in the export settings dialog, for easy access. */

typedef struct
{
	GtkEntry        *pattern;
	GtkToggleButton *write;
}
ExportClientsWidgets;

static void
export_clients_get_widgets (ExportClientsWidgets *w)
{
	w->pattern = GTK_ENTRY (xst_dialog_get_widget (tool->main_dialog, "export_client_pattern"));
	w->write   = GTK_TOGGLE_BUTTON (xst_dialog_get_widget (tool->main_dialog, "export_client_write"));
}

void
export_clients_clear_entries (void)
{
	ExportClientsWidgets w;

	export_clients_get_widgets (&w);
	xst_ui_entry_set_text        (w.pattern, NULL);
	gtk_toggle_button_set_active (w.write, FALSE);

	export_clients_apply_policies ();
}

void
export_clients_clear (void)
{
	export_clients_table_clear   ();
	export_clients_clear_entries ();
}

void
export_clients_from_client (ExportClient *client)
{
	ExportClientsWidgets w;

	export_clients_get_widgets (&w);

	xst_ui_entry_set_text        (w.pattern, client->pattern);
	gtk_toggle_button_set_active (w.write,   client->write);
}

void
export_clients_from_current_client (void)
{
	export_clients_from_client (export_clients_table_get_selected ());
}

void
export_clients_to_client (ExportClient *client)
{
	ExportClientsWidgets w;

	g_return_if_fail (client != NULL);

	export_clients_get_widgets (&w);

	if (client->pattern)
		g_free (client->pattern);

	client->pattern = g_strdup (gtk_entry_get_text           (w.pattern));
	client->write   =           gtk_toggle_button_get_active (w.write);
}

void
export_clients_to_current_client (void)
{
	ExportClient *client;

	client = export_clients_table_get_selected ();
	g_return_if_fail (client != NULL);

	export_clients_to_client (client);
}

void
export_clients_apply_policies (void)
{
	ExportClientsWidgets w;
	const gchar *s;

	export_clients_get_widgets (&w);
	s = gtk_entry_get_text (w.pattern);

	if (string_is_blank (s))
	{
		xst_dialog_widget_set_user_mode (tool->main_dialog, "export_clients_add_button", XST_WIDGET_MODE_INSENSITIVE);
		xst_dialog_widget_set_user_mode (tool->main_dialog, "export_clients_update_button", XST_WIDGET_MODE_INSENSITIVE);
	}
	else
	{
		xst_dialog_widget_set_user_mode (tool->main_dialog, "export_clients_add_button", XST_WIDGET_MODE_SENSITIVE);
		if (export_clients_table_get_selected ())
			xst_dialog_widget_set_user_mode (tool->main_dialog, "export_clients_update_button",
							 XST_WIDGET_MODE_SENSITIVE);
	}

	if (!export_clients_table_get_selected ())
		xst_dialog_widget_set_user_mode (tool->main_dialog, "export_clients_delete_button",
						 XST_WIDGET_MODE_INSENSITIVE);
}

void
export_clients_run (void)
{
	GtkWidget *clients_dialog;
	gint ret;

	clients_dialog = xst_dialog_get_widget (tool->main_dialog, "export_clients");
	export_clients_apply_policies ();
	ret = gtk_dialog_run (GTK_DIALOG (clients_dialog));
	gtk_widget_destroy (clients_dialog);
}

static gboolean
validate_client (ExportClient *client)
{
	g_return_val_if_fail (client != NULL, FALSE);

	if (!validate_export_client_pattern (client->pattern))
		return FALSE;

	return TRUE;
}

void
export_client_add_cb (void)
{
	ExportClient *client;

	client = g_new0 (ExportClient, 1);
	export_clients_to_client (client);
	if (!validate_client (client))
	{
		g_free (client);
		return;
	}

	export_clients_table_insert (client);
	export_clients_clear_entries ();
}

void
export_client_update_cb (void)
{
	export_clients_to_current_client ();
	export_clients_table_update_selected ();
}

void
export_client_delete_cb (void)
{
	export_clients_table_delete_selected ();
	export_clients_clear_entries ();
}

void
export_client_changed_cb (void)
{
	export_clients_apply_policies ();
}
