/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */

/* This file is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GNU Emacs; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Authors: Hans Petter Jansson <hpj@ximian.com> */

#include "xst-shares-tool.h"

XST_TOOL_MAKE_TYPE(shares,Shares)

void
xst_shares_tool_type_init (XstSharesTool *tool)
{
	tool->failed_mounts   = NULL;
	tool->failed_unmounts = NULL;
}

XstTool *
xst_shares_tool_new (void)
{
	return XST_TOOL (gtk_type_new (XST_TYPE_SHARES_TOOL));
}
