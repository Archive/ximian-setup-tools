/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */

/* This file is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GNU Emacs; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA. */

#ifndef XST_MOUNT_H
#define XST_MOUNT_H

#include <glib.h>

typedef struct _XstMount XstMount;

struct _XstMount
{
	gchar *device;
	gchar *point;
};

XstMount    *xst_mount_new  (const gchar *device, const gchar *point);
void         xst_mount_free (XstMount *mount);

const gchar *xst_mount_get_device (XstMount *mount);
const gchar *xst_mount_get_point  (XstMount *mount);

#endif
