/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/* Copyright (C) 2000-2001 Ximian, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Hans Petter Jansson <hpj@ximian.com>.
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <gnome.h>
#include "xst.h"

#include "tables.h"
#include "xml.h"
#include "validate.h"
#include "import-settings.h"

extern XstTool *tool;

#define string_is_blank(s) (!s || !strlen (s))

/* Structure holding information that is encompassed by the import settings dialog. */

typedef struct
{
	xmlNodePtr          node;

	ImportSettingsType  type;
	gchar              *host;
	gchar              *path;
	gchar              *point;
	gchar              *user;
	gchar              *password;
	gboolean            mounted;
	gboolean            do_not_remount;
}
ImportSettingsConfiguration;

/* Structure holding refs to widgets in the import settings dialog, for easy access. */

typedef struct
{
	GtkEntry        *host;
	GtkEntry        *path;
	GtkEntry        *point;
	GtkEntry        *user;
	GtkEntry        *password;
	GtkToggleButton *mounted;
	GtkToggleButton *do_not_remount;
}
ImportSettingsWidgets;

/* Policies for import settings widgets when the dialog is in SMB mode. */

static const XstWidgetUserPolicy smb_policies[] = {
	{ "import_settings_user",           XST_WIDGET_MODE_SENSITIVE },
	{ "import_settings_user_label",     XST_WIDGET_MODE_SENSITIVE },
	{ "import_settings_password",       XST_WIDGET_MODE_SENSITIVE },
	{ "import_settings_password_label", XST_WIDGET_MODE_SENSITIVE },
	{ NULL }
};

/* Policies for import settings widgets when the dialog is in NFS mode. */

static const XstWidgetUserPolicy nfs_policies[] = {
	{ "import_settings_user",           XST_WIDGET_MODE_HIDDEN },
	{ "import_settings_user_label",     XST_WIDGET_MODE_HIDDEN },
	{ "import_settings_password",       XST_WIDGET_MODE_HIDDEN },
	{ "import_settings_password_label", XST_WIDGET_MODE_HIDDEN },
	{ NULL }
};

struct {
	ImportSettingsType  type;
	gchar              *label;
} import_menu_items[] = {
	{ IMPORT_SETTINGS_TYPE_SMB,     N_("Samba") },
	{ IMPORT_SETTINGS_TYPE_NFS,     N_("NFS") },
	{ IMPORT_SETTINGS_TYPE_UNKNOWN, NULL }
};

/**
 * import_settings_set_type: Change appearance of the import settings dialog.
 * @type: IMPORT_SETTINGS_TYPE_SMB (SMB) or IMPORT_SETTINGS_TYPE_NFS (NFS).
 *
 * Applies the widget policies in smb_policies or nfs_policies.
 **/

void
import_settings_set_type (ImportSettingsType type)
{
	GtkOptionMenu *option_menu;

	option_menu = GTK_OPTION_MENU (xst_dialog_get_widget (tool->main_dialog, "import_settings_type"));

	switch (type)
	{
	case IMPORT_SETTINGS_TYPE_SMB:
		xst_dialog_set_widget_user_modes (tool->main_dialog, smb_policies);
		break;
	case IMPORT_SETTINGS_TYPE_NFS:
		xst_dialog_set_widget_user_modes (tool->main_dialog, nfs_policies);
		break;
	default:
		g_warning ("Tried to set invalid ImportSettingsType.");
		return;
		break;
	}

	gtk_option_menu_set_history (option_menu, type);
}

/**
 * import_settings_get_type: Get current appearance of the import settings dialog.
 *
 * Returns: IMPORT_SETTINGS_TYPE_SMB (SMB) or IMPORT_SETTINGS_TYPE_NFS (NFS).
 **/

ImportSettingsType
import_settings_get_type ()
{
	GtkOptionMenu      *option_menu;
	GtkWidget          *menu;
	GtkWidget          *item;
	ImportSettingsType  type;

	option_menu = GTK_OPTION_MENU (xst_dialog_get_widget (tool->main_dialog, "import_settings_type"));
	menu = gtk_option_menu_get_menu (option_menu);
	item = gtk_menu_get_active (GTK_MENU (menu));
	type = GPOINTER_TO_UINT (g_object_get_data (G_OBJECT (item), "type"));

	return type;
}

/**
 * import_settings_select_type_cb: Set dialog appearance based on user input.
 *
 * Gets called when user selects a share type from the dialog's option menu.
 * Updates widget appearance based on selection done.
 **/

void
import_settings_select_type_cb (GtkOptionMenu *option_menu, gpointer user_data)
{
	ImportSettingsType  type;
	GtkWidget          *menu;
	GtkWidget          *item;

	menu = gtk_option_menu_get_menu (option_menu);
	item = gtk_menu_get_active (GTK_MENU (menu));
	type = GPOINTER_TO_UINT (g_object_get_data (G_OBJECT (item), "type"));

	import_settings_set_type (type);
}

static void
import_settings_get_widgets (ImportSettingsWidgets *w)
{
	w->host     = GTK_ENTRY (GTK_COMBO (
		xst_dialog_get_widget (tool->main_dialog, "import_settings_host"))->entry);
	w->path     = GTK_ENTRY (xst_dialog_get_widget (tool->main_dialog, "import_settings_path"));
	w->point    = GTK_ENTRY (gnome_file_entry_gtk_entry (GNOME_FILE_ENTRY (
		xst_dialog_get_widget (tool->main_dialog, "import_settings_point"))));
	w->user     = GTK_ENTRY (xst_dialog_get_widget (tool->main_dialog, "import_settings_user"));
	w->password = GTK_ENTRY (xst_dialog_get_widget (tool->main_dialog, "import_settings_password"));
	w->mounted  = GTK_TOGGLE_BUTTON (
		xst_dialog_get_widget (tool->main_dialog, "import_settings_mounted"));
	w->do_not_remount = GTK_TOGGLE_BUTTON (
		xst_dialog_get_widget (tool->main_dialog, "import_settings_no_remount"));
}

static void
import_settings_set_configuration (ImportSettingsConfiguration *c)
{
	ImportSettingsWidgets w;

	import_settings_get_widgets (&w);

	xst_ui_entry_set_text        (w.host,           c->host);
	xst_ui_entry_set_text        (w.path,           c->path);
	xst_ui_entry_set_text        (w.point,          c->point);
	xst_ui_entry_set_text        (w.user,           c->user);
        xst_ui_entry_set_text        (w.password,       c->password);

	gtk_toggle_button_set_active (w.mounted,        c->mounted);
	gtk_toggle_button_set_active (w.do_not_remount, c->do_not_remount);

	import_settings_set_type     (c->type);
}

static void
import_settings_get_configuration (ImportSettingsConfiguration *c)
{
	ImportSettingsWidgets w;

	import_settings_get_widgets (&w);

	c->host           = (gchar *)gtk_entry_get_text  (w.host);
	c->path           = (gchar *)gtk_entry_get_text  (w.path);
	c->point          = (gchar *)gtk_entry_get_text  (w.point);
	c->user           = (gchar *)gtk_entry_get_text  (w.user);
	c->password       = (gchar *)gtk_entry_get_text  (w.password);

	c->mounted        = gtk_toggle_button_get_active (w.mounted);
	c->do_not_remount = gtk_toggle_button_get_active (w.do_not_remount);

	c->type           = import_settings_get_type ();

	c->node           = import_table_get_selected_node ();
}

/**
 * import_settings_validate: Validate data currently in the import settings dialog.
 *
 * If any of the data is invalid, shows a modal error dialog detailing the nature of
 * the problem.
 *
 * Returns: TRUE if valid, FALSE if invalid.
 **/
static gboolean
import_settings_validate (void)
{
	ImportSettingsConfiguration c;

	import_settings_get_configuration (&c);

	return  validate_import_type        (c.type) &&
		validate_import_host_name   (c.host) &&
		validate_import_remote_path (c.path) &&
		validate_import_local_path  (c.point);
}

void
import_settings_clear (void)
{
	ImportSettingsConfiguration config;

	memset (&config, 0, sizeof (config));
	config.type = IMPORT_SETTINGS_TYPE_SMB;
	import_settings_set_configuration (&config);
}

void
import_settings_from_node (xmlNodePtr root)
{
	gchar *type = NULL;
	gboolean listed = FALSE;
	ImportSettingsConfiguration config;

	memset (&config, 0, sizeof (config));

	if (root)
	{
		type             = xst_xml_element_get_attribute (root, "type");
		config.host      = xst_xml_get_child_content     (root, "host");
		config.path      = xst_xml_get_child_content     (root, "path");
		config.point     = xst_xml_get_child_content     (root, "point");
		config.user      = xst_xml_get_child_content     (root, "user");
		config.password  = xst_xml_get_child_content     (root, "password");
		config.mounted   = xst_xml_element_get_state     (root, "mounted");
		listed           = xst_xml_element_get_state     (root, "listed");
	}

	if (!type || !g_strcasecmp (type, "smb"))
		config.type = IMPORT_SETTINGS_TYPE_SMB;
	else
		config.type = IMPORT_SETTINGS_TYPE_NFS;

	config.do_not_remount = !(config.mounted && listed);
	config.node = root;

	import_settings_set_configuration (&config);

	if (type)            g_free (type);
	if (config.host)     g_free (config.host);
	if (config.path)     g_free (config.path);
	if (config.point)    g_free (config.point);
	if (config.user)     g_free (config.user);
	if (config.password) g_free (config.password);
}

void
import_settings_from_current_node (void)
{
	ImportSettingsConfiguration config;

	import_settings_get_configuration (&config);
	import_settings_from_node (config.node);
}

void
import_settings_to_node (xmlNodePtr root)
{
	ImportSettingsConfiguration config;
	gchar *host, *path, *point;

	import_settings_get_configuration (&config);

	g_return_if_fail (root != NULL);

	host  = config.host  ? xst_util_nice_hostname_dup (config.host)  : NULL;
	path  = config.path  ? xst_util_nice_path_dup     (config.path)  : NULL;
	point = config.point ? xst_util_nice_path_dup     (config.point) : NULL;

	xst_xml_element_destroy_children_by_name (root, "host");
	xst_xml_element_destroy_children_by_name (root, "path");
	xst_xml_element_destroy_children_by_name (root, "point");
	xst_xml_element_destroy_children_by_name (root, "user");
	xst_xml_element_destroy_children_by_name (root, "password");

	if (host)            xst_xml_set_child_content (root, "host",     host);
	if (path)            xst_xml_set_child_content (root, "path",     path);
	if (point)           xst_xml_set_child_content (root, "point",    point);
	if (config.user)     xst_xml_set_child_content (root, "user",     config.user);
	if (config.password) xst_xml_set_child_content (root, "password", config.password);

	if (config.type == IMPORT_SETTINGS_TYPE_SMB)
		xst_xml_element_set_attribute (root, "type", "smb");
	else
		xst_xml_element_set_attribute (root, "type", "nfs");

	xst_xml_element_set_state (root, "mounted", config.mounted);
	xst_xml_element_set_state (root, "listed",  !config.do_not_remount);

	if (host)  g_free (host);
	if (path)  g_free (path);
	if (point) g_free (point);
}

void
import_settings_to_current_node (void)
{
	ImportSettingsConfiguration config;

	import_settings_get_configuration (&config);
	import_settings_to_node (config.node);
}

static void
import_settings_type_prepare (void)
{
	GtkWidget *option_menu;
	GtkWidget *old_menu, *new_menu;
	GtkWidget *menu_item;
	gint       i;
	static gboolean built;

	if (!built) {
		option_menu = xst_dialog_get_widget (tool->main_dialog, "import_settings_type");

		new_menu = gtk_menu_new ();
		for (i = 0; import_menu_items[i].type != IMPORT_SETTINGS_TYPE_UNKNOWN; i++) {
			menu_item = gtk_menu_item_new_with_label (import_menu_items[i].label);
			g_object_set_data (G_OBJECT (menu_item), "type",
					   GUINT_TO_POINTER (import_menu_items[i].type));

			gtk_menu_shell_append (GTK_MENU_SHELL (new_menu), menu_item);
		}

		old_menu = gtk_option_menu_get_menu (GTK_OPTION_MENU (option_menu));
		gtk_object_destroy (GTK_OBJECT (old_menu));
		gtk_option_menu_set_menu (GTK_OPTION_MENU (option_menu), new_menu);

		gtk_widget_show_all (GTK_WIDGET (option_menu));

		built = TRUE;
	}
}

void
import_settings_run (void)
{
	GtkWidget *settings_dialog;
	gint ret;

	settings_dialog = xst_dialog_get_widget (tool->main_dialog, "import_settings");
	import_settings_type_prepare ();
	import_settings_from_current_node ();

	do
	{
		ret = gtk_dialog_run (GTK_DIALOG (settings_dialog));

		if (ret != GTK_RESPONSE_OK)
		{
			gtk_widget_hide (settings_dialog);

			/* Need to re-read the XML config here, to preserve the state
			 * of the dialog. */

			import_settings_from_current_node ();
			return;
		}
	}
	while (!import_settings_validate ());

	import_settings_to_current_node ();
	import_table_update_selected ();
	gtk_widget_hide (settings_dialog);
}

void
import_settings_add_run (void)
{
	ImportSettingsConfiguration config;
	xmlNodePtr prev_import_node, import_node;
	GtkWidget *settings_dialog;
	gint ret;

	import_settings_get_configuration (&config);
	prev_import_node = config.node;

	import_node = shares_xml_new_import_node ();
	import_settings_from_node (import_node);

	settings_dialog = xst_dialog_get_widget (tool->main_dialog, "import_settings");
	import_settings_type_prepare ();

	do
	{
		ret = gtk_dialog_run (GTK_DIALOG (settings_dialog));

		if (ret != GTK_RESPONSE_OK)
		{
			import_settings_from_node (prev_import_node);
			xst_xml_element_destroy (import_node);
			gtk_widget_hide (settings_dialog);
			return;
		}
	}
	while (!import_settings_validate ());

	import_settings_to_node (import_node);
	import_table_insert_node (import_node);
	gtk_widget_hide (settings_dialog);
}
