/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/* Copyright (C) 2000-2001 Ximian, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Hans Petter Jansson <hpj@ximian.com>.
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <errno.h>
#include <gnome.h>

#include "xst.h"
#include "validate.h"
#include "xml.h"

extern XstTool *tool;

#define string_is_blank(s) (!s || !strlen (s))

typedef enum
{
	XST_PATH_NONEXISTENT,
	XST_PATH_INACCESSIBLE,
	XST_PATH_IS_FILE,
	XST_PATH_IS_LINK,
	XST_PATH_IS_NODE,
	XST_PATH_IS_DIR
}
XstPathStatus;

static void
show_error (gchar *message)
{
	GtkWidget *dialog;

	dialog = gtk_message_dialog_new (GTK_WINDOW (tool->main_dialog),
					 GTK_DIALOG_MODAL,
					 GTK_MESSAGE_ERROR,
					 GTK_BUTTONS_OK,
					 message);

	gtk_dialog_run (GTK_DIALOG (dialog));
	gtk_widget_destroy (dialog);
}

static gboolean
show_error_if_blank (gchar *string, gchar *message)
{
	if (string_is_blank (string))
	{
		show_error (message);
		return FALSE;
	}

	return TRUE;
}

static gboolean
show_boolean_choice (gchar *message)
{
	GtkWidget *dialog;
	gint ret;

	dialog = gtk_message_dialog_new (GTK_WINDOW (tool->main_dialog),
					 GTK_DIALOG_MODAL,
					 GTK_MESSAGE_QUESTION,
					 GTK_BUTTONS_OK_CANCEL,
					 message);

	ret = gtk_dialog_run (GTK_DIALOG (dialog));
	gtk_widget_destroy (dialog);

	if (ret != GTK_RESPONSE_OK)
		return FALSE;

	return TRUE;
}

static XstPathStatus
check_path (gchar *path)
{
	XstPathStatus ret;
	struct stat sbuf;

	if (stat (path, &sbuf) < 0)  /* lstat */
	{
		switch (errno)
		{
		case EACCES:
		case ENAMETOOLONG:
			ret = XST_PATH_INACCESSIBLE;
			break;

		default:
			ret = XST_PATH_NONEXISTENT;
			break;
		}
	}
	else
	{
		if      (S_ISREG (sbuf.st_mode)) ret = XST_PATH_IS_FILE;
/*		else if (S_ISLNK (sbuf.st_mode)) ret = XST_PATH_IS_LINK; */
		else if (S_ISDIR (sbuf.st_mode)) ret = XST_PATH_IS_DIR;
		else                             ret = XST_PATH_IS_NODE;
	}

	return ret;
}

static XstPathStatus
check_or_create_path (gchar *path)
{
	XstPathStatus pstat;

	pstat = check_path (path);

	if (pstat == XST_PATH_NONEXISTENT)
	{
		mkdir (path, S_IRWXU | S_IRWXG | S_IRWXO);
		pstat = XST_PATH_IS_DIR;
	}

	return pstat;
}

static XstPathStatus
create_path_elements (gchar *path)
{
	XstPathStatus pstat;
	gchar *p0, *pelem;

	for (p0 = strchr (path + 1, '/'); p0; p0 = strchr (p0 + 1, '/'))
	{
		pelem = g_strndup (path, p0 - path);
		pstat = check_or_create_path (pelem);
		g_free (pelem);

		if (pstat != XST_PATH_IS_DIR)
			return pstat;
	}

	return check_or_create_path (path);
}

static gboolean
create_path_interactive (gchar *path)
{
	XstPathStatus pstat;

	pstat = create_path_elements (path);

	if (pstat == XST_PATH_IS_DIR)
		return TRUE;

	if (pstat == XST_PATH_IS_FILE)
		show_error (_("Failed to create the path you requested:\n"
			      "A path element is a regular file, not a directory."));
	else if (pstat == XST_PATH_IS_NODE)
		show_error (_("Failed to create the path you requested:\n"
			      "A path element is a special filesystem node, not a directory."));
	else if (pstat == XST_PATH_INACCESSIBLE)
		show_error (_("Failed to create the path you requested:\n"
			      "You don't have access to that local location."));
	else
		show_error (_("Failed to create the path you requested."));

	return FALSE;
}

gboolean
validate_export_name (gpointer name)
{
	return show_error_if_blank (name, _("You need to specify a name for the export."));
}

gboolean
validate_export_local_path (gpointer path)
{
	XstPathStatus pstat;
	gboolean bool;

	if (!show_error_if_blank (path, _("You need to specify a path, i.e. the directory\n"
					  "on your computer you would like to share.")))
		return FALSE;

	pstat = check_path (path);

	if (pstat == XST_PATH_INACCESSIBLE)
	{
		show_error (_("You do not have access to the path you want to share.\n"
			      "Please select another one."));
		return FALSE;
	}

	if (pstat == XST_PATH_NONEXISTENT)
	{
		bool = show_boolean_choice (_("The path you want to share does not exist.\n"
					      "Would you like to create it?"));

		if (bool)
			return create_path_interactive (path);

		return FALSE;
	}

	if (pstat == XST_PATH_IS_FILE)
	{
		show_error (_("The path you want to share points to a specific file.\n"
			      "You can only share directories, not files."));

		return FALSE;
	}

	if (pstat != XST_PATH_IS_DIR)
	{
		show_error (_("The path you want to share is not a directory,\n"
			      "and you can only share directories."));

		return FALSE;
	}

	return TRUE;
}

gboolean
validate_import_type (ImportSettingsType type)
{
	gboolean retval = TRUE;

	switch (type)
	{
	case IMPORT_SETTINGS_TYPE_SMB:
		if (!shares_xml_smbmount ())
		{
			show_error (_("You don't have smbclient package installed,\n"
				      "Can't mount smb shares."));
			retval = FALSE;
		}
		break;
	case IMPORT_SETTINGS_TYPE_NFS:
/*		if (!shares_xml_smbmount ())
		{
			show_error (_("You don't have NFS package installed,\n"
				      "Can't mount NFS shares."));
			retval = FALSE;
			} */
		break;
	default:
		g_warning ("validate_import_type: Unkonwn type.");
		retval = FALSE;
		break;
	}

	return retval;
}

gboolean
validate_export_type (ExportSettingsType type)
{
	gboolean retval = TRUE;

	switch (type)
	{
	case EXPORT_SETTINGS_TYPE_SMB:
		if (!shares_xml_smbserver ())
		{
			show_error (_("You don't have samba package installed,\n"
				      "Can't export smb shares."));
			retval = FALSE;
		}
		break;
	case EXPORT_SETTINGS_TYPE_NFS:
/*		if (!shares_xml_smbserver ())
		{
			show_error (_("You don't have NFS package installed,\n"
				      "Can't export NFS shares."));
			retval = FALSE;
			} */
		break;
	default:
		g_warning ("validate_export_type: Unkonwn type.");
		retval = FALSE;
		break;
	}

	return retval;
}

gboolean
validate_import_host_name (gpointer name)
{
	return show_error_if_blank (name, _("You need to name the host providing the import."));
}

gboolean
validate_import_remote_path (gpointer path)
{
	return show_error_if_blank (path, _("You need to specify a path, i.e. where on the host\n"
					    "the share is imported from."));
}

gboolean
validate_import_local_path (gpointer path)
{
	XstPathStatus pstat;
	gboolean bool;

	if (!show_error_if_blank (path, _("You need to specify the local point, i.e. a directory,\n"
					  "where the import will be mounted.")))
		return FALSE;

	pstat = check_path (path);

	if (pstat == XST_PATH_INACCESSIBLE)
	{
		show_error (_("You do not have access to the local point, i.e. directory\n"
			      "where the import would be mounted.\n"
			      "Please select another one."));
		return FALSE;
	}

	if (pstat == XST_PATH_NONEXISTENT)
	{
		bool = show_boolean_choice (_("The local mount point does not exist.\n"
					      "Would you like to create it?"));

		if (bool)
			return create_path_interactive (path);

		return FALSE;
	}

	if (pstat == XST_PATH_IS_FILE)
	{
		show_error (_("The mount point you specified is not a directory.\n"
			      "You can only import to directories, not files."));

		return FALSE;
	}

	if (pstat != XST_PATH_IS_DIR)
	{
		show_error (_("The mount point you specified is not a directory,\n"
			      "and you can only import to directories."));

		return FALSE;
	}

	return TRUE;
}

gboolean
validate_export_client_pattern (gpointer pattern)
{
	return show_error_if_blank (pattern, _("You need to specify a client pattern, e.g.\n"
					       "\"*.myschool.edu\" or \"192.168.0.1/24\"."));
}
