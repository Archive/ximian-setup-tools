/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/* Copyright (C) 2000-2001 Ximian, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Hans Petter Jansson <hpj@ximian.com>
 */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

#include <gnome.h>
#include "xst.h"
#include <gal/e-table/e-table-scrolled.h>
#include <gal/e-table/e-table-memory.h>
#include <gal/e-table/e-table-memory-callbacks.h>
#include <gal/e-table/e-cell-text.h>
#include <gal/e-paned/e-hpaned.h>
#include <gdk-pixbuf/gdk-pixbuf.h>
#include "xst-shares-tool.h"
#include "xst-mount.h"
#include "tables.h"
#include "xml.h"
#include "validate.h"
#include "import-settings.h"
#include "export-settings.h"
#include "export-clients.h"

#define IMPORTS_COL_LAST        3
#define EXPORTS_COL_LAST        3
#define EXPORT_CLIENTS_COL_LAST 3

#define IMPORTS_TABLE_SPEC "imports.etspec"
#define EXPORTS_TABLE_SPEC "exports.etspec"
#define EXPORT_CLIENTS_TABLE_SPEC "export_clients.etspec"

typedef gboolean  (XstValidatorFunc)(gpointer val);
typedef gchar    *(XstFilterFunc)   (gchar *val);
typedef gboolean  (XstChangedFunc)  (gpointer val);
typedef gpointer  (XstValueFunc)    (xmlNodePtr node);

typedef enum
{
	XST_TABLE_COL_STRING,
	XST_TABLE_COL_STRING_BLANK,
	XST_TABLE_COL_STATE
}
XstTableColType;

typedef struct
{
	gchar            *xml_tag;

	XstTableColType   type;
	XstFilterFunc    *filter;
	XstValidatorFunc *validator;
	XstChangedFunc   *changed;
	XstValueFunc     *value;
}
XstTableColDef;

static gboolean mount_state_changed_from_table (gpointer unmount);
static void     set_mount_button_label_neg     (gpointer unmount);
static void     mount_button_from_import_node  (xmlNodePtr node);
static gboolean export_set_writable            (gpointer writable);
static gpointer export_get_writable            (xmlNodePtr node);

extern XstTool *tool;

GtkWidget *imports_table_scrolled;
GtkWidget *exports_table_scrolled;
GtkWidget *export_clients_table_scrolled;

static const gchar *imports_basic_state =
"<ETableState>"
"  <column source='0'/>"
"  <column source='1'/>"
"  <column source='2'/>"
"  <column source='3'/>"
"  <grouping>"
"    <leaf column='0' ascending='true'/>"
"  </grouping>"
"</ETableState>";

static const gchar *exports_basic_state =
"<ETableState>"
"  <column source='0'/>"
"  <column source='1'/>"
"  <column source='2'/>"
"  <grouping>"
"    <leaf column='0' ascending='true'/>"
"  </grouping>"
"</ETableState>";

static const gchar *export_clients_basic_state =
"<ETableState>"
"  <column source='0'/>"
"  <column source='1'/>"
"  <grouping>"
"    <leaf column='0' ascending='true'/>"
"  </grouping>"
"</ETableState>";

XstTableColDef imports_table_col_def [] =
{
	{ "host",    XST_TABLE_COL_STRING, xst_util_nice_hostname_dup, validate_import_host_name,   NULL, NULL },
	{ "path",    XST_TABLE_COL_STRING, xst_util_nice_path_dup,     validate_import_remote_path, NULL, NULL },
	{ "point",   XST_TABLE_COL_STRING, xst_util_nice_path_dup,     validate_import_local_path,  NULL, NULL },
	{ "mounted", XST_TABLE_COL_STATE,  NULL,                       NULL,                        mount_state_changed_from_table, NULL },
	{ NULL }
};

XstTableColDef exports_table_col_def [] =
{
	{ "name",     XST_TABLE_COL_STRING,       NULL,                   validate_export_name,       NULL, NULL },
	{ "path",     XST_TABLE_COL_STRING,       xst_util_nice_path_dup, validate_export_local_path, NULL, NULL },
/*	{ "clients",  XST_TABLE_COL_STRING_BLANK, NULL,                   NULL,                       NULL, NULL }, */
	{ "write",    XST_TABLE_COL_STATE,        NULL,                   NULL,                       export_set_writable, export_get_writable },
	{ NULL }
};

XstWidgetUserPolicy import_selected_policies[] =
{
	{ "imports_settings_button", XST_WIDGET_MODE_SENSITIVE },
	{ "imports_delete_button",   XST_WIDGET_MODE_SENSITIVE },
	{ "imports_mount_button",    XST_WIDGET_MODE_SENSITIVE },
	{ NULL }
};

XstWidgetUserPolicy import_unselected_policies[] =
{
	{ "imports_settings_button", XST_WIDGET_MODE_INSENSITIVE },
	{ "imports_delete_button",   XST_WIDGET_MODE_INSENSITIVE },
	{ "imports_mount_button",    XST_WIDGET_MODE_INSENSITIVE },
	{ NULL }
};

XstWidgetUserPolicy export_selected_policies[] =
{
	{ "exports_settings_button", XST_WIDGET_MODE_SENSITIVE },
	{ "exports_delete_button",   XST_WIDGET_MODE_SENSITIVE },
	{ NULL }
};

XstWidgetUserPolicy export_unselected_policies[] =
{
	{ "exports_settings_button", XST_WIDGET_MODE_INSENSITIVE },
	{ "exports_delete_button",   XST_WIDGET_MODE_INSENSITIVE },
	{ NULL }
};

XstWidgetUserPolicy export_clients_selected_policies[] =
{
	{ "export_clients_update_button", XST_WIDGET_MODE_SENSITIVE },
	{ "export_clients_delete_button", XST_WIDGET_MODE_SENSITIVE },
	{ NULL }
};

XstWidgetUserPolicy export_clients_unselected_policies[] =
{
	{ "export_clients_update_button", XST_WIDGET_MODE_INSENSITIVE },
	{ "export_clients_delete_button", XST_WIDGET_MODE_INSENSITIVE },
	{ NULL }
};

static gchar *blank_string = "";

static gchar *
print_mount_list (GList *list)
{
	gchar *text = NULL;

	for ( ; list; list = g_list_next (list)) {
		XstMount *mount = list->data;
		gchar *s0, *s1;

		s0 = g_strdup_printf (_("%s on %s\n"), mount->device, mount->point);
		if (text) {
			s1 = g_strconcat (text, s0, NULL);
			g_free (text);
			text = s1;
		} else {
			text = g_strdup (s0);
		}
		g_free (s0);
	}

	return text;
}

static void
set_mount_list_mounted (XstTool *tool, GList *list, gboolean state)
{
	xst_dialog_freeze (tool->main_dialog);

	for ( ; list; list = g_list_next (list)) {
		XstMount *mount = list->data;
		xmlNodePtr node;

		node = shares_xml_find_import_node_by_point (mount->point);
		if (!node) {
			g_warning ("Could not match mount point: %s", mount->point);
			continue;
		}

		xst_xml_element_set_state (node, "mounted", state);
	}

	xst_dialog_thaw (tool->main_dialog);
}

static void
free_mount_list (GList *list)
{
	for ( ; list; list = g_list_next (list)) {
		xst_mount_free (list->data);
	}

	g_list_free (list);
}

void
check_mount_change (XstSharesTool *tool)
{
	gchar *message = NULL, *s0, *s1;
	GtkWidget *dialog;
	xmlNodePtr node;

	xst_dialog_freeze (XST_TOOL (tool)->main_dialog);

	if (tool->failed_mounts) {
		gchar *head_text = g_list_next (tool->failed_mounts) ?
			_("Could not mount the following shares -\n"
			  "they could be temporarily unavailable, or you\n"
			  "might have specified bad parameters:\n\n") :
			_("Could not mount the following share -\n"
			  "it could be temporarily unavailable, or you\n"
			  "might have specified bad parameters:\n\n");

		s0 = print_mount_list (tool->failed_mounts);
		message = g_strconcat (head_text, s0, NULL);
		g_free (s0);

		set_mount_list_mounted (XST_TOOL (tool), tool->failed_mounts, FALSE);
		free_mount_list (tool->failed_mounts);
		tool->failed_mounts = NULL;
	}

	if (tool->failed_unmounts) {
		if (message) {
			gchar *head_text = g_list_next (tool->failed_unmounts) ?
				_("\nAlso, could not unmount the following shares -\n"
				  "they might be in use by programs:\n\n") :
				_("\nAlso, could not unmount the following share -\n"
				  "it might be in use by programs:\n\n");

			s0 = g_strconcat (message, head_text, NULL);
			g_free (message);
			message = s0;
		} else {
			gchar *head_text = g_list_next (tool->failed_unmounts) ?
				_("Could not unmount the following shares -\n"
				  "they might be in use by programs:\n\n") :
				_("Could not unmount the following share -\n"
				  "it might be in use by programs:\n\n");

			message = g_strdup (head_text);
		}

		s0 = print_mount_list (tool->failed_unmounts);
		s1 = g_strconcat (message, s0, NULL);
		g_free (message);
		g_free (s0);
		message = s1;

		set_mount_list_mounted (XST_TOOL (tool), tool->failed_unmounts, TRUE);
		free_mount_list (tool->failed_unmounts);
		tool->failed_unmounts = NULL;
	}

	node = import_table_get_selected_node ();
	if (node)
		mount_button_from_import_node (node);

	if (message) {
		dialog = gtk_message_dialog_new (NULL,
						 GTK_DIALOG_MODAL,
						 GTK_MESSAGE_ERROR,
						 GTK_BUTTONS_OK,
						 message);

		g_free (message);
		gtk_dialog_run (GTK_DIALOG (dialog));
		gtk_widget_destroy (dialog);
	}

	xst_dialog_thaw (XST_TOOL (tool)->main_dialog);
	xst_dialog_set_modified (XST_TOOL (tool)->main_dialog, FALSE);
}

static void
apply_mount_change (XstSharesTool *tool)
{
	xst_tool_save (XST_TOOL (tool));
	check_mount_change (tool);
}

static void
set_value_at_with_col_def (ETableModel *etm, int col, int row, const void *val, void *data)
{
	XstTableColDef *def_table;
	XstTableColDef *def;
	xmlNodePtr node;
	void *value;

	g_return_if_fail (xst_tool_get_access (tool));

	def_table = data;
	def = &def_table [col];
	value = (void *) val;

	if (!def_table)
		g_error ("No column definition for this table.");

	if (!def)
		g_error ("No column definition for this table/column.");

	node = e_table_memory_get_data (E_TABLE_MEMORY (etm), row);
	if (!node)
		return;

	if (def->filter)
		value = def->filter (value);

	if (def->validator && !def->validator (value))
		return;

	if (def->type == XST_TABLE_COL_STRING)
	{
		xst_xml_set_child_content (node, def->xml_tag, g_strdup (value));

		if (def->filter)
			free (value);
	}
	else if (def->type == XST_TABLE_COL_STRING_BLANK)
	{
		if (def->filter)
			free (value);
	}
	else if (def->type == XST_TABLE_COL_STATE)
	{
		xst_xml_element_set_state (node, def->xml_tag, (gboolean) value);
	}

	if (def->changed)
	{
		if (def->changed (value))
			xst_dialog_modify (tool->main_dialog);
	}
	else
		xst_dialog_modify (tool->main_dialog);
}

static gpointer
value_at_with_col_def (ETableModel *etm, int col, int row, void *data)
{
	XstTableColDef *def_table;
	XstTableColDef *def;
	xmlNodePtr node;
	gpointer ret;

	def_table = data;
	def = &def_table [col];

	if (!def_table)
		g_error ("No column definition for this table.");

	if (!def)
		g_error ("No column definition for this table/column.");

	node = e_table_memory_get_data (E_TABLE_MEMORY (etm), row);

	if (def->value)
	{
		ret = def->value (node);
	}
	else if (def->type == XST_TABLE_COL_STRING)
	{
		if (!node)
			return blank_string;

		node = xst_xml_element_find_first (node, def->xml_tag);
		if (!node)
			return blank_string;

		ret = (gpointer) xst_xml_element_peek_content (node);
	}
	else if (def->type == XST_TABLE_COL_STRING_BLANK)
	{
		ret = blank_string;
	}
	else if (def->type == XST_TABLE_COL_STATE)
	{
		ret = (void *) xst_xml_element_get_state (node, def->xml_tag);
	}
	else
		ret = blank_string;

	return ret;
}

static int
imports_col_count (ETableModel *etm, void *data)
{
        return IMPORTS_COL_LAST;
}

static int
exports_col_count (ETableModel *etm, void *data)
{
        return EXPORTS_COL_LAST;
}

static int
export_clients_col_count (ETableModel *etm, void *data)
{
        return EXPORT_CLIENTS_COL_LAST;
}

static void *
duplicate_value (ETableModel *etm, int col, const void *value, void *data)
{
        return g_strdup (value);
}

static void
free_value (ETableModel *etm, int col, void *value, void *data)
{
	g_free (value);
}

static void *
initialize_value (ETableModel *etm, int col, void *data)
{
        return g_strdup ("");
}

static gboolean
value_is_empty (ETableModel *etm, int col, const void *value, void *data)
{
        return !(value && *(char *) value);
}

static char *
value_to_string (ETableModel *etm, int col, const void *value, void *data)
{
        return g_strdup (value);
}

static void
set_value_at (ETableModel *etm, int col, int row, const void *val, void *data)
{
}

static gboolean
is_editable (ETableModel *etm, int col, int row, void *model_data)
{
	return (xst_tool_get_access (tool) &&
		(xst_dialog_get_complexity (tool->main_dialog) == XST_DIALOG_ADVANCED));
}

static void
export_clients_set_value_at (ETableModel *etm, int col, int row, const void *val, void *data)
{
	ExportClient *client;

	g_return_if_fail (xst_tool_get_access (tool));

	client = e_table_memory_get_data (E_TABLE_MEMORY (etm), row);
	if (!client)
		return;

	if (col > 1)
		g_error ("Bad column passed to exports_clients_set_value_at().");

	if (col == 0)
	{
		if (!validate_export_client_pattern ((gpointer) val))
			return;

		if (client->pattern)
			g_free (client->pattern);

		client->pattern = g_strdup (val);
	}
	else
		client->write = (gboolean) val;

	export_clients_from_current_client ();
}

static void *
export_clients_value_at (ETableModel *etm, int col, int row, void *model_data)
{
	ExportClient *client;
	void *ret;

	client = e_table_memory_get_data (E_TABLE_MEMORY (etm), row);
	if (!client)
		return blank_string;

	if (col > 1)
		g_error ("Bad column passed to export_clients_value_at().");

	switch (col)
	{
	case 0:
		ret = client->pattern;
		break;
	case 1:
		ret = (void *) client->write;
		break;
		
	default:
		ret = NULL;
	}

	return ret;
}

static void
imports_table_cursor_change_cb (ETable *et, gint row, gpointer user_data)
{
	ETableModel *model;
	xmlNodePtr node;

	/* Prepare import settings dialog */

	model = et->model;
	node = e_table_memory_get_data (E_TABLE_MEMORY (model), row);
	if (!node)
		g_error ("Row has no model.");

	import_settings_from_node (node);
	mount_button_from_import_node (node);

	/* Activate list action buttons */

	xst_dialog_set_widget_user_modes (tool->main_dialog, import_selected_policies);
}

static void
exports_table_cursor_change_cb (ETable *et, gint row, gpointer user_data)
{
	ETableModel *model;
	xmlNodePtr node;

	/* Prepare export settings dialog */

	model = et->model;
	node = e_table_memory_get_data (E_TABLE_MEMORY (model), row);
	if (!node)
		g_error ("Row has no model.");

	export_settings_from_node (node);

	/* Activate list action buttons */

	xst_dialog_set_widget_user_modes (tool->main_dialog, export_selected_policies);
}

static void
export_clients_table_cursor_change_cb (ETable *et, gint row, gpointer user_data)
{
	ETableModel *model;
	ExportClient *client;

	/* Update export clients dialog */

	model = et->model;
	client = e_table_memory_get_data (E_TABLE_MEMORY (model), row);
	if (!client)
		g_error ("Row has no model.");

	export_clients_from_client (client);

	/* Activate list action buttons */

	xst_dialog_set_widget_user_modes (tool->main_dialog, export_clients_selected_policies);
}

static ETableExtras *
create_extras (void)
{
	ETableExtras *extras;
/*	ECell *ec; */

	extras = e_table_extras_new ();

/*	e_table_extras_add_compare (extras, "id_compare", id_compare); */

	/* Sometimes we want to center cols, sometimes we don't... */
/*	ec = e_cell_text_new (NULL, GTK_JUSTIFY_CENTER);
	gtk_object_set (GTK_OBJECT (ec), "color_column", COL_USER_COLOR, NULL);
	e_table_extras_add_cell (extras, "my_cell", ec);
*/	
	return extras;
}

static ETableScrolled *
create_generic_table (ETableMemoryCalbacksColumnCountFn col_count_fn,
		      ETableMemoryCalbacksValueAtFn value_at_fn,
		      ETableMemoryCalbacksSetValueAtFn set_value_at_fn,
		      const gchar *spec, const gchar *state, XstTableColDef *col_def)
{
	ETableScrolled *table_scrolled;
	ETableModel *model;
	ETableExtras *extras;

	extras = create_extras ();

	model = e_table_memory_callbacks_new (col_count_fn,
					      value_at_fn,
					      set_value_at_fn ? set_value_at_fn : set_value_at,
					      is_editable,
					      duplicate_value,
					      free_value,
					      initialize_value,
					      value_is_empty,
					      value_to_string,
					      col_def);

#if 0
	root_path = e_table_memory_node_insert (E_TABLE_MEMORY (model), NULL, 0, g_strdup (""));
	e_tree_model_root_node_set_visible (model, FALSE);
#endif
	table_scrolled = E_TABLE_SCROLLED (e_table_scrolled_new (E_TABLE_MODEL (model), extras, spec, state));

	return (table_scrolled);
}

static ETableScrolled *
create_imports_table (void)
{
	ETableScrolled *imports_table;
	ETable *table;
	gchar *spec;

	spec = xst_conf_get_string (tool, "ispec");
	if (!spec) {
		spec = xst_ui_load_etspec (tool->etspecs_common_path, IMPORTS_TABLE_SPEC);
		if (!spec)
			g_error ("create_imports_table: Couldn't make table.");
		xst_conf_set_string (tool, "ispec", spec);
	}
	
	imports_table = create_generic_table (imports_col_count, value_at_with_col_def,
					      set_value_at_with_col_def, spec, imports_basic_state,
					      imports_table_col_def);

	table = e_table_scrolled_get_table (E_TABLE_SCROLLED (imports_table));

	g_signal_connect (G_OBJECT (table), "cursor_change",
			  G_CALLBACK (imports_table_cursor_change_cb), NULL);
#if 0
	g_signal_connect (G_OBJECT (table), "double_click",
			  G_CALLBACK (on_settings_clicked), NULL);
#endif

	return imports_table;
}

static ETableScrolled *
create_exports_table (void)
{
	ETableScrolled *exports_table;
	ETable *table;
	gchar* spec;

	spec = xst_conf_get_string (tool, "espec");
	if (!spec) {
		spec = xst_ui_load_etspec (tool->etspecs_common_path, EXPORTS_TABLE_SPEC);
		if (!spec)
			g_error ("create_exports_table: Couldn't make table.");
		xst_conf_set_string (tool, "espec", spec);
	}
	exports_table = create_generic_table (exports_col_count, value_at_with_col_def,
					      set_value_at_with_col_def, spec, exports_basic_state,
					      exports_table_col_def);

	table = e_table_scrolled_get_table (E_TABLE_SCROLLED (exports_table));

	g_signal_connect (G_OBJECT (table), "cursor_change",
			  G_CALLBACK (exports_table_cursor_change_cb), NULL);
#if 0
	g_signal_connect (G_OBJECT (table), "double_click",
			  G_CALLBACK (on_settings_clicked), NULL);
#endif

	return exports_table;
}

static ETableScrolled *
create_export_clients_table (void)
{
	ETableScrolled *export_clients_table;
	ETable *table;
	gchar* spec;

	spec = xst_conf_get_string (tool, "ecspec");
	if (!spec) {
		spec = xst_ui_load_etspec (tool->etspecs_common_path, EXPORT_CLIENTS_TABLE_SPEC);
		if (!spec)
			g_error ("create_export_clients_table: Couldn't make table.");
		xst_conf_set_string (tool, "ecspec", spec);
	}

	export_clients_table = create_generic_table (export_clients_col_count, export_clients_value_at,
						     export_clients_set_value_at, spec,
						     export_clients_basic_state, NULL);

	table = e_table_scrolled_get_table (E_TABLE_SCROLLED (export_clients_table));

	g_signal_connect (G_OBJECT (table), "cursor_change",
			  G_CALLBACK (export_clients_table_cursor_change_cb), NULL);
#if 0
	g_signal_connect (G_OBJECT (table), "double_click",
			  G_CALLBACK (on_settings_clicked), NULL);
#endif

	return export_clients_table;
}

void
create_tables (void)
{
	GtkWidget *container;
        GtkWidget *table_scrolled;

	/* Imports table */

	table_scrolled = GTK_WIDGET (create_imports_table ());

	container = xst_dialog_get_widget (tool->main_dialog, "imports_holder");
	gtk_box_pack_end (GTK_BOX (container), table_scrolled, TRUE, TRUE, 0);
	gtk_widget_show_all (table_scrolled);

	imports_table_scrolled = table_scrolled;

	/* Exports table */

	table_scrolled = GTK_WIDGET (create_exports_table ());

	container = xst_dialog_get_widget (tool->main_dialog, "exports_holder");
	gtk_box_pack_end (GTK_BOX (container), table_scrolled, TRUE, TRUE, 0);
	gtk_widget_show_all (table_scrolled);

	exports_table_scrolled = table_scrolled;

	/* Export clients table */

	table_scrolled = GTK_WIDGET (create_export_clients_table ());

	container = xst_dialog_get_widget (tool->main_dialog, "export_clients_holder");
	gtk_box_pack_end (GTK_BOX (container), table_scrolled, TRUE, TRUE, 0);
	gtk_widget_show_all (table_scrolled);

	export_clients_table_scrolled = table_scrolled;
}

static void
populate_table (ETableModel *model, xmlNodePtr root_node, gchar *type)
{
	xmlNodePtr node;

	g_return_if_fail (model != NULL);

	if (!root_node)
		return;

	e_table_memory_freeze (E_TABLE_MEMORY (model));

	for (node = xst_xml_element_find_first (root_node, type); node;
	     node = xst_xml_element_find_next (node, type))
	{
		e_table_memory_insert (E_TABLE_MEMORY (model), -1, node);
	}

	e_table_memory_thaw (E_TABLE_MEMORY (model));
}

void
populate_tables (void)
{
	ETableModel *model;
	ETable *table [3];
	xmlNodePtr node [3];
	gchar *type [3] = { "import", "export", NULL };
	gint i;

	g_assert (imports_table_scrolled);
	g_assert (exports_table_scrolled);

	printf ("Populating tables.\n");

	table [0] = e_table_scrolled_get_table (E_TABLE_SCROLLED (imports_table_scrolled));
	table [1] = e_table_scrolled_get_table (E_TABLE_SCROLLED (exports_table_scrolled));
	table [2] = NULL;

	node [0] = shares_xml_get_imports_node ();
	node [1] = shares_xml_get_exports_node ();
	node [2] = NULL;

	for (i = 0; table [i]; i++)
	{
		model = (table [i]->model);
		populate_table (model, node [i], type [i]);
	}
}

/**
 * table_get_rows_as_gslist: Gets all row data from table, returning it in a GSList.
 **/
GSList *
table_get_rows_as_gslist (ETableMemory *model)
{
	GSList *gslist = NULL;
	gint rows;
	gint i;

	rows = e_table_model_row_count (E_TABLE_MODEL (model));

	for (i = 0; i < rows; i++)
	{
		gslist = g_slist_append (gslist, e_table_memory_get_data (E_TABLE_MEMORY (model), i));
	}

	return gslist;
}

/**
 * table_set_rows_from_gslist: Takes a GSList of row data, to replace current contents of table.
 **/
void
table_set_rows_from_gslist (ETableMemory *model, GSList *gslist)
{
	gint row;

	e_table_memory_clear (model);

	for (row = 0; gslist; gslist = gslist->next, row++)
	{
		e_table_memory_insert (model, row, gslist->data);
	}
}

static void
table_insert_row (ETable *table, void *data)
{
	ETableModel *model;

	model = table->model;

	/* e_table_memory_freeze (E_TABLE_MEMORY (model)); */
	e_table_memory_insert (E_TABLE_MEMORY (model), -1, data);
	e_table_set_cursor_row (table, e_table_model_row_count (model) - 1);
	/* e_table_memory_thaw (E_TABLE_MEMORY (model)); */
}

/* --- */

static void
set_mount_button_label (gboolean mount)
{
	GtkWidget *button;

	button = xst_dialog_get_widget (tool->main_dialog, "imports_mount_button");
	g_return_if_fail (button != NULL);

	gtk_widget_set (button, "label", mount ? _("Mount") : _("Unmount"), NULL);
}

static void
set_mount_button_label_neg (gpointer unmount)
{
	set_mount_button_label (!((gboolean) unmount));
}

static gboolean
mount_state_changed_from_table (gpointer unmount)
{
	set_mount_button_label_neg (unmount);

	/* Save changes */
	apply_mount_change (XST_SHARES_TOOL (tool));
	return FALSE;
}

static void
mount_button_from_import_node (xmlNodePtr node)
{
	gboolean state;
	
	state = xst_xml_element_get_state (node, "mounted");
	set_mount_button_label (!state);
}

void
import_table_update_selected (void)
{
	ETable *table;
	xmlNodePtr node;

	table = e_table_scrolled_get_table (E_TABLE_SCROLLED (imports_table_scrolled));
	e_table_model_row_changed (table->model, e_table_get_cursor_row (table));

	node = import_table_get_selected_node ();
	g_assert (node);

	mount_button_from_import_node (node);

	xst_dialog_modify (tool->main_dialog);
}


xmlNodePtr
import_table_get_selected_node (void)
{
	ETable *table;
	ETableModel *model;
	gint row;

	table = e_table_scrolled_get_table (E_TABLE_SCROLLED (imports_table_scrolled));
	model = table->model;
	row = e_table_get_cursor_row (table);
	if (row < 0)
		return NULL;

	return e_table_memory_get_data (E_TABLE_MEMORY (model), row);
}

void
import_table_insert_node (xmlNodePtr node)
{
	ETable *table;

	table = e_table_scrolled_get_table (E_TABLE_SCROLLED (imports_table_scrolled));
	table_insert_row (table, node);

	xst_dialog_set_widget_user_modes (tool->main_dialog, import_unselected_policies);
	xst_dialog_modify (tool->main_dialog);
}

void
import_table_delete_selected_node (void)
{
	ETable *table;
	ETableModel *model;
	xmlNodePtr node;

	table = e_table_scrolled_get_table (E_TABLE_SCROLLED (imports_table_scrolled));
	model = table->model;

	/* Deactivate list action buttons */

	xst_dialog_set_widget_user_modes (tool->main_dialog, import_unselected_policies);

	/* Remove from XML and table */

	e_table_memory_freeze (E_TABLE_MEMORY (model));

	node = import_table_get_selected_node ();
	g_assert (node);
	xst_xml_element_destroy (node);

	e_table_memory_remove (E_TABLE_MEMORY (model), e_table_get_cursor_row (table));
	e_table_memory_thaw (E_TABLE_MEMORY (model));

	xst_dialog_modify (tool->main_dialog);
}

void
import_table_mount_selected_node_toggle_cb (void)
{
	ETable *table;
	ETableModel *model;
	xmlNodePtr node;
	gboolean state;

	xst_dialog_freeze (tool->main_dialog);

	table = e_table_scrolled_get_table (E_TABLE_SCROLLED (imports_table_scrolled));
	model = table->model;

	node = import_table_get_selected_node ();
	g_assert (node);

	state = xst_xml_element_get_state (node, "mounted");
	xst_xml_element_set_state (node, "mounted", !state);

	import_table_update_selected ();

	/* Save changes */
	apply_mount_change (XST_SHARES_TOOL (tool));

	xst_dialog_thaw (tool->main_dialog);
}

/* --- */

static gboolean
export_set_writable (gpointer writable)
{
	xmlNodePtr node;

	node = export_table_get_selected_node ();
	shares_xml_export_set_general_write (node, (gboolean) writable);
	return TRUE;
}

static gpointer
export_get_writable (xmlNodePtr node)
{
	return (gpointer) shares_xml_export_get_general_write (node);
}

void
export_table_update_selected (void)
{
	ETable *table;

	table = e_table_scrolled_get_table (E_TABLE_SCROLLED (exports_table_scrolled));
	e_table_model_row_changed (table->model, e_table_get_cursor_row (table));

	xst_dialog_modify (tool->main_dialog);
}


xmlNodePtr
export_table_get_selected_node (void)
{
	ETable *table;
	ETableModel *model;
	gint row;

	table = e_table_scrolled_get_table (E_TABLE_SCROLLED (exports_table_scrolled));
	model = table->model;
	row = e_table_get_cursor_row (table);
	if (row < 0)
		return NULL;

	return e_table_memory_get_data (E_TABLE_MEMORY (model), row);
}

void
export_table_insert_node (xmlNodePtr node)
{
	ETable *table;

	table = e_table_scrolled_get_table (E_TABLE_SCROLLED (exports_table_scrolled));
	table_insert_row (table, node);

	xst_dialog_set_widget_user_modes (tool->main_dialog, export_unselected_policies);
	xst_dialog_modify (tool->main_dialog);
}

void
export_table_delete_selected_node (void)
{
	ETable *table;
	ETableModel *model;
	xmlNodePtr node;

	table = e_table_scrolled_get_table (E_TABLE_SCROLLED (exports_table_scrolled));
	model = table->model;

	/* Deactivate list action buttons */

	xst_dialog_set_widget_user_modes (tool->main_dialog, export_unselected_policies);

	/* Remove from XML and table */

	e_table_memory_freeze (E_TABLE_MEMORY (model));

	node = export_table_get_selected_node ();
	g_assert (node);
	xst_xml_element_destroy (node);

	e_table_memory_remove (E_TABLE_MEMORY (model), e_table_get_cursor_row (table));
	e_table_memory_thaw (E_TABLE_MEMORY (model));

	xst_dialog_modify (tool->main_dialog);
}

/* --- */

void
export_clients_table_update_selected (void)
{
	ETable *table;

	table = e_table_scrolled_get_table (E_TABLE_SCROLLED (export_clients_table_scrolled));
	e_table_model_row_changed (table->model, e_table_get_cursor_row (table));
}

ExportClient *
export_clients_table_get_selected (void)
{
	ETable *table;
	ETableModel *model;
	gint row;

	table = e_table_scrolled_get_table (E_TABLE_SCROLLED (export_clients_table_scrolled));
	model = table->model;
	row = e_table_get_cursor_row (table);
	if (row < 0)
		return NULL;

	return e_table_memory_get_data (E_TABLE_MEMORY (model), row);
}

void
export_clients_table_insert (ExportClient *client)
{
	ETable *table;

	table = e_table_scrolled_get_table (E_TABLE_SCROLLED (export_clients_table_scrolled));
	table_insert_row (table, client);

	xst_dialog_set_widget_user_modes (tool->main_dialog, export_clients_unselected_policies);
}

void
export_clients_table_delete_selected (void)
{
	ETable *table;
	ETableModel *model;
	ExportClient *client;

	table = e_table_scrolled_get_table (E_TABLE_SCROLLED (export_clients_table_scrolled));
	model = table->model;

	/* Deactivate list action buttons */

	xst_dialog_set_widget_user_modes (tool->main_dialog, export_clients_unselected_policies);

	/* Remove from XML and table */

	e_table_memory_freeze (E_TABLE_MEMORY (model));

	client = export_clients_table_get_selected ();
	g_assert (client);
	g_free (client->pattern);
	g_free (client);

	e_table_memory_remove (E_TABLE_MEMORY (model), e_table_get_cursor_row (table));
	e_table_memory_thaw (E_TABLE_MEMORY (model));
}

void
export_clients_table_clear (void)
{
	ETable *table;

	table = e_table_scrolled_get_table (E_TABLE_SCROLLED (export_clients_table_scrolled));
	e_table_memory_clear (E_TABLE_MEMORY (table->model));
}

void
export_clients_table_to_export_node (xmlNodePtr node)
{
	ETable *table;
	ETableModel *model;
	GSList *rowdata, *slist;

	shares_xml_remove_allow_nodes (node);

	table = e_table_scrolled_get_table (E_TABLE_SCROLLED (export_clients_table_scrolled));
	model = table->model;
	rowdata = table_get_rows_as_gslist (E_TABLE_MEMORY (model));

	for (slist = rowdata ; slist; slist = g_slist_next (slist))
	{
		ExportClient *client;

		client = slist->data;
		shares_xml_new_allow_node (node, client->pattern, client->write);
	}

	if (rowdata)
		g_slist_free (rowdata);
}

void
export_clients_table_from_export_node (xmlNodePtr export_node)
{
	ETable *table;
	ETableModel *model;
	GSList *rowdata = NULL;
	xmlNodePtr node;

	table = e_table_scrolled_get_table (E_TABLE_SCROLLED (export_clients_table_scrolled));
	model = table->model;

	export_clients_table_clear ();

	for (node = xst_xml_element_find_first (export_node, "allow"); node;
	     node = xst_xml_element_find_next (node, "allow"))
	{
		ExportClient *client;
		gchar *pattern;

		client = g_new0 (ExportClient, 1);
		pattern = xst_xml_get_child_content (node, "pattern");
		if (pattern && strcmp (pattern, "0.0.0.0/0"))
		{
			client->pattern = pattern;
			client->write = xst_xml_element_get_state (node, "write");
			rowdata = g_slist_append (rowdata, client);
		}
		else if (pattern)
			g_free (pattern);
	}

	if (rowdata)
	{
		table_set_rows_from_gslist (E_TABLE_MEMORY (model), rowdata);
		g_slist_free (rowdata);
	}
}

gboolean
import_mount_failed (XstSharesTool *tool, XstReportLine *rline, gpointer data)
{
	XstMount *mount;

	mount = xst_mount_new (rline->argv [0], rline->argv [1]);
	tool->failed_mounts = g_list_append (tool->failed_mounts, mount);

	return TRUE;
}

gboolean
import_unmount_failed (XstSharesTool *tool, XstReportLine *rline, gpointer data)
{
	XstMount *mount;

	mount = xst_mount_new (rline->argv [0], rline->argv [1]);
	tool->failed_unmounts = g_list_append (tool->failed_unmounts, mount);

	return TRUE;
}
