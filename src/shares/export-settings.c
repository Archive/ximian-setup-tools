/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/* Copyright (C) 2000-2001 Ximian, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Hans Petter Jansson <hpj@ximian.com>.
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <gnome.h>
#include "xst.h"

#include "tables.h"
#include "xml.h"
#include "validate.h"
#include "export-settings.h"

extern XstTool *tool;

#define string_is_blank(s) (!s || !strlen (s))

/* Structure holding information that is encompassed by the export settings dialog. */

typedef struct
{
	xmlNodePtr          node;

	ExportSettingsType  type;
	gchar              *name;
	gchar              *path;
	gchar              *comment;

	gboolean            browseable;
	gboolean            writable;
	gboolean            public;

	gboolean            restrict;

	gboolean            enabled;
}
ExportSettingsConfiguration;

/* Structure holding refs to widgets in the export settings dialog, for easy access. */

typedef struct
{
	GtkEntry        *name;
	GtkEntry        *path;
	GtkEntry        *comment;

	GtkToggleButton *browseable;
	GtkToggleButton *writable;
	GtkToggleButton *public;

	GtkToggleButton *restrict;

	GtkToggleButton *enabled;
}
ExportSettingsWidgets;

/* Policies for export settings widgets when the dialog is in SMB mode. */

static const XstWidgetUserPolicy smb_policies[] = {
	{ "export_settings_ac_samba",       XST_WIDGET_MODE_SENSITIVE },
	{ "export_settings_ac_nfs",         XST_WIDGET_MODE_HIDDEN    },
	{ NULL }
};

/* Policies for export settings widgets when the dialog is in NFS mode. */

static const XstWidgetUserPolicy nfs_policies[] = {
	{ "export_settings_ac_samba",       XST_WIDGET_MODE_HIDDEN    },
	{ "export_settings_ac_nfs",         XST_WIDGET_MODE_SENSITIVE },
	{ NULL }
};

struct {
	ExportSettingsType  type;
	gchar              *label;
} export_menu_items[] = {
	{ EXPORT_SETTINGS_TYPE_SMB,     N_("Samba") },
	{ EXPORT_SETTINGS_TYPE_NFS,     N_("NFS") },
	{ EXPORT_SETTINGS_TYPE_UNKNOWN, NULL }
};

/**
 * export_settings_set_type: Change appearance of the export settings dialog.
 * @type: EXPORT_SETTINGS_TYPE_SMB (SMB) or EXPORT_SETTINGS_TYPE_NFS (NFS).
 *
 * Applies the widget policies in smb_policies or nfs_policies.
 **/

void
export_settings_set_type (ExportSettingsType type)
{
	GtkOptionMenu *option_menu;

	option_menu = GTK_OPTION_MENU (xst_dialog_get_widget (tool->main_dialog, "export_settings_type"));

	switch (type)
	{
	case EXPORT_SETTINGS_TYPE_SMB:
		xst_dialog_set_widget_user_modes (tool->main_dialog, smb_policies);
		break;
	case EXPORT_SETTINGS_TYPE_NFS:
		xst_dialog_set_widget_user_modes (tool->main_dialog, nfs_policies);
		break;
	default:
		g_warning ("Tried to set invalid ExportSettingsType.");
		return;
		break;
	}

	gtk_option_menu_set_history (option_menu,type);
}

/**
 * export_settings_get_type: Get current appearance of the export settings dialog.
 *
 * Returns: EXPORT_SETTINGS_TYPE_SMB (SMB) or EXPORT_SETTINGS_TYPE_NFS (NFS).
 **/

ExportSettingsType
export_settings_get_type ()
{
	GtkOptionMenu      *option_menu;
	GtkWidget          *menu;
	GtkWidget          *item;
	ExportSettingsType  type;

	option_menu = GTK_OPTION_MENU (xst_dialog_get_widget (tool->main_dialog, "export_settings_type"));
	menu = gtk_option_menu_get_menu (option_menu);
	item = gtk_menu_get_active (GTK_MENU (menu));
	type = GPOINTER_TO_UINT (g_object_get_data (G_OBJECT (item), "type"));

	return type;
}

/**
 * export_settings_select_type_cb: Set dialog appearance based on user input.
 *
 * Gets called when user selects a share type from the dialog's option menu.
 * Updates widget appearance based on selection done.
 **/

void
export_settings_select_type_cb (GtkOptionMenu *option_menu, gpointer user_data)
{
	ExportSettingsType  type;
	GtkWidget          *menu;
	GtkWidget          *item;

	menu = gtk_option_menu_get_menu (option_menu);
	item = gtk_menu_get_active (GTK_MENU (menu));
	type = GPOINTER_TO_UINT (g_object_get_data (G_OBJECT (item), "type"));

	export_settings_set_type (type);
}

void
export_settings_restrict_cb (GtkWidget *toggle, gpointer user_data)
{
	gboolean restrict;

	restrict = gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (toggle));
	if (restrict)
		xst_dialog_widget_set_user_mode (tool->main_dialog, "export_settings_clients_button",
						 XST_WIDGET_MODE_SENSITIVE);
	else
		xst_dialog_widget_set_user_mode (tool->main_dialog, "export_settings_clients_button",
						 XST_WIDGET_MODE_INSENSITIVE);
}

static void
export_settings_get_widgets (ExportSettingsWidgets *w)
{
	w->name       = GTK_ENTRY (xst_dialog_get_widget (tool->main_dialog, "export_settings_name"));
	w->path       = GTK_ENTRY (gnome_file_entry_gtk_entry (GNOME_FILE_ENTRY (
		xst_dialog_get_widget (tool->main_dialog, "export_settings_path"))));
	w->comment    = GTK_ENTRY (xst_dialog_get_widget (tool->main_dialog, "export_settings_comment"));
	w->browseable = GTK_TOGGLE_BUTTON (
		xst_dialog_get_widget (tool->main_dialog, "export_settings_browseable"));
	w->writable   = GTK_TOGGLE_BUTTON (
		xst_dialog_get_widget (tool->main_dialog, "export_settings_writable"));
	w->public     = GTK_TOGGLE_BUTTON (
		xst_dialog_get_widget (tool->main_dialog, "export_settings_public"));
	w->restrict   = GTK_TOGGLE_BUTTON (
		xst_dialog_get_widget (tool->main_dialog, "export_settings_restrict_toggle"));
	w->enabled    = GTK_TOGGLE_BUTTON (
		xst_dialog_get_widget (tool->main_dialog, "export_settings_enabled"));
}

static void
export_settings_set_configuration (ExportSettingsConfiguration *c)
{
	ExportSettingsWidgets w;

	export_settings_get_widgets (&w);

	xst_ui_entry_set_text        (w.name,           c->name);
	xst_ui_entry_set_text        (w.path,           c->path);
	xst_ui_entry_set_text        (w.comment,        c->comment);

	gtk_toggle_button_set_active (w.browseable,     c->browseable);
	gtk_toggle_button_set_active (w.writable,       c->writable);
	gtk_toggle_button_set_active (w.public,         c->public);
	gtk_toggle_button_set_active (w.restrict,       c->restrict);
	gtk_toggle_button_set_active (w.enabled,        c->enabled);

	export_settings_set_type     (c->type);
}

static void
export_settings_get_configuration (ExportSettingsConfiguration *c)
{
	ExportSettingsWidgets w;

	export_settings_get_widgets (&w);

	c->name           = (gchar *)gtk_entry_get_text  (w.name);
	c->path           = (gchar *)gtk_entry_get_text  (w.path);
	c->comment        = (gchar *)gtk_entry_get_text  (w.comment);

	c->browseable     = gtk_toggle_button_get_active (w.browseable);
	c->writable       = gtk_toggle_button_get_active (w.writable);
	c->public         = gtk_toggle_button_get_active (w.public);
	c->restrict       = gtk_toggle_button_get_active (w.restrict);
	c->enabled        = gtk_toggle_button_get_active (w.enabled);

	c->type           = export_settings_get_type ();

	c->node           = export_table_get_selected_node ();
}

/**
 * export_settings_validate: Validate data currently in the export settings dialog.
 *
 * If any of the data is invalid, shows a modal error dialog detailing the nature of
 * the problem.
 *
 * Returns: TRUE if valid, FALSE if invalid.
 **/
static gboolean
export_settings_validate (void)
{
	ExportSettingsConfiguration c;

	export_settings_get_configuration (&c);

	return validate_export_type       (c.type) &&
	       validate_export_name       (c.name) &&
	       validate_export_local_path (c.path);
}

void
export_settings_clear (void)
{
	ExportSettingsConfiguration config;

	memset (&config, 0, sizeof (config));
	config.type = EXPORT_SETTINGS_TYPE_SMB;
	export_settings_set_configuration (&config);
}

void
export_settings_from_node (xmlNodePtr root)
{
	gchar *type = NULL;
	ExportSettingsConfiguration config;

	memset (&config, 0, sizeof (config));

	if (root)
	{
		type              = xst_xml_element_get_attribute (root, "type");
		config.name       = xst_xml_get_child_content     (root, "name");
		config.path       = xst_xml_get_child_content     (root, "path");
		config.comment    = xst_xml_get_child_content     (root, "comment");

		config.browseable = xst_xml_element_get_state     (root, "browse");
		config.writable   = xst_xml_element_get_state     (root, "write");
		config.public     = xst_xml_element_get_state     (root, "public");
		config.enabled    = xst_xml_element_get_state     (root, "enabled");
	}

	if (!type || !g_strcasecmp (type, "smb"))
	{
		config.type = EXPORT_SETTINGS_TYPE_SMB;
		export_clients_table_clear ();
		config.restrict = FALSE;
	}
	else
	{
		config.type = EXPORT_SETTINGS_TYPE_NFS;
		export_clients_table_from_export_node (root);
		config.restrict = shares_xml_export_get_restricted (root);
	}

	config.node = root;

	export_settings_set_configuration (&config);

	if (type)            g_free (type);
	if (config.name)     g_free (config.name);
	if (config.path)     g_free (config.path);
	if (config.comment)  g_free (config.comment);
}

void
export_settings_from_current_node (void)
{
	ExportSettingsConfiguration config;

	export_settings_get_configuration (&config);
	export_settings_from_node (config.node);
}

void
export_settings_to_node (xmlNodePtr root)
{
	ExportSettingsConfiguration config;
	gchar *path;

	export_settings_get_configuration (&config);

	g_return_if_fail (root != NULL);

	path = config.path ? xst_util_nice_path_dup (config.path) : NULL;

	xst_xml_element_destroy_children_by_name (root, "name");
	xst_xml_element_destroy_children_by_name (root, "path");
	xst_xml_element_destroy_children_by_name (root, "comment");

	if (config.name)    xst_xml_set_child_content (root, "name",    config.name);
	if (path)           xst_xml_set_child_content (root, "path",    path);
	if (config.comment) xst_xml_set_child_content (root, "comment", config.comment);

	if (config.type == EXPORT_SETTINGS_TYPE_SMB)
	{
		xst_xml_element_set_attribute (root, "type",       "smb");
		xst_xml_element_set_state     (root, "browse",     config.browseable);
		xst_xml_element_set_state     (root, "write",      config.writable);
		xst_xml_element_set_state     (root, "public",     config.public);
		shares_xml_remove_allow_nodes (root);
	}
	else
	{
		xst_xml_element_set_attribute (root, "type", "nfs");
		if (config.restrict)
			export_clients_table_to_export_node (root);
		else
			shares_xml_remove_allow_nodes (root);

		shares_xml_export_set_restricted (root, config.restrict);
	}

	xst_xml_element_set_state (root, "enabled", TRUE);

	if (path) g_free (path);
}

void
export_settings_to_current_node (void)
{
	ExportSettingsConfiguration config;

	export_settings_get_configuration (&config);
	export_settings_to_node (config.node);
}

static void
export_settings_type_prepare (void)
{
	GtkWidget *option_menu;
	GtkWidget *old_menu, *new_menu;
	GtkWidget *menu_item;
	gint       i;
	static gboolean built;

	if (!built) {
		option_menu = xst_dialog_get_widget (tool->main_dialog, "export_settings_type");

		new_menu = gtk_menu_new ();
		for (i = 0; export_menu_items[i].type != EXPORT_SETTINGS_TYPE_UNKNOWN; i++) {
			menu_item = gtk_menu_item_new_with_label (export_menu_items[i].label);
			g_object_set_data (G_OBJECT (menu_item), "type",
					   GUINT_TO_POINTER (export_menu_items[i].type));

			gtk_menu_shell_append (GTK_MENU_SHELL (new_menu), menu_item);
		}

		old_menu = gtk_option_menu_get_menu (GTK_OPTION_MENU (option_menu));
		gtk_object_destroy (GTK_OBJECT (old_menu));
		gtk_option_menu_set_menu (GTK_OPTION_MENU (option_menu), new_menu);

		gtk_widget_show_all (GTK_WIDGET (option_menu));

		built = TRUE;
	}
}

void
export_settings_run (void)
{
	GtkWidget *settings_dialog;
	gint ret;

	settings_dialog = xst_dialog_get_widget (tool->main_dialog, "export_settings");
	export_settings_type_prepare ();
	export_settings_from_current_node ();

	do
	{
		ret = gtk_dialog_run (GTK_DIALOG (settings_dialog));

		if (ret != GTK_RESPONSE_OK)
		{
			gtk_widget_hide (settings_dialog);

			/* Need to re-read the XML config here, to preserve the state
			 * of the dialog. */

			export_settings_from_current_node ();
			return;
		}
	}
	while (!export_settings_validate ());

	export_settings_to_current_node ();
	export_table_update_selected ();
	gtk_widget_hide (settings_dialog);
}

void
export_settings_add_run (void)
{
	ExportSettingsConfiguration config;
	xmlNodePtr prev_export_node, export_node;
	GtkWidget *settings_dialog;
	gint ret;

	export_settings_get_configuration (&config);
	prev_export_node = config.node;

	export_node = shares_xml_new_export_node ();
	export_settings_from_node (export_node);

	settings_dialog = xst_dialog_get_widget (tool->main_dialog, "export_settings");
	export_settings_type_prepare ();

	do
	{
		ret = gtk_dialog_run (GTK_DIALOG (settings_dialog));

		if (ret != GTK_RESPONSE_OK)
		{
			export_settings_from_node (prev_export_node);
			xst_xml_element_destroy (export_node);
			gtk_widget_hide (settings_dialog);
			return;
		}
	}
	while (!export_settings_validate ());

	export_settings_to_node (export_node);
	export_table_insert_node (export_node);
	gtk_widget_hide (settings_dialog);
}
