/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/* main.c: Shares administrator main file, part of the Ximian Setup Tools.
 * 
 * Copyright (C) 2000-2001 Ximian, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Hans Petter Jansson <hpj@ximian.com>
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <gnome.h>
#include <gdk-pixbuf/gdk-pixbuf.h>
#include <glade/glade.h>

#include <time.h>
#include <stdlib.h>

#include "xst.h"
#include "xst-shares-tool.h"
#include "tables.h"
#include "import-settings.h"
#include "export-settings.h"
#include "export-clients.h"

#define HIDDEN      XST_WIDGET_MODE_HIDDEN
#define INSENSITIVE XST_WIDGET_MODE_INSENSITIVE
#define SENSITIVE   XST_WIDGET_MODE_SENSITIVE

XstTool *tool;

static XstDialogSignal signals[] = {
	/* Imports tab */
	{ "imports_settings_button",         "clicked", import_settings_run },
	{ "imports_add_button",              "clicked", import_settings_add_run },
	{ "imports_delete_button",           "clicked", import_table_delete_selected_node },
	{ "imports_mount_button",            "clicked", import_table_mount_selected_node_toggle_cb },
	{ "import_settings_type",            "changed", G_CALLBACK (import_settings_select_type_cb) },

	/* Exports tab */
	{ "exports_settings_button",         "clicked", export_settings_run },
	{ "exports_add_button",              "clicked", export_settings_add_run },
	{ "exports_delete_button",           "clicked", export_table_delete_selected_node },
	{ "export_settings_type",            "changed", G_CALLBACK (export_settings_select_type_cb) },

	/* Export dialog */
	{ "export_settings_restrict_toggle", "toggled", G_CALLBACK (export_settings_restrict_cb) },
	{ "export_settings_clients_button",  "clicked", export_clients_run },

	/* Export clients dialog */
	{ "export_clients_add_button",       "clicked", export_client_add_cb },
	{ "export_clients_update_button",    "clicked", export_client_update_cb },
	{ "export_clients_delete_button",    "clicked", export_client_delete_cb },
	{ "export_client_pattern",           "changed", export_client_changed_cb },

	{ NULL }
};

static const XstWidgetPolicy policies[] = {
	/* Name                             Basic        Advanced     Root   User */
	{ "imports_add_button",             SENSITIVE,   SENSITIVE,   TRUE,  TRUE  },
	{ "imports_delete_button",          SENSITIVE,   SENSITIVE,   TRUE,  FALSE },
	{ "imports_settings_button",        SENSITIVE,   SENSITIVE,   FALSE, FALSE },
	{ "imports_mount_button",           SENSITIVE,   SENSITIVE,   TRUE,  FALSE },
	{ "imports_scan_button",            HIDDEN,      HIDDEN,      FALSE, FALSE },

	{ "import_settings_user",           SENSITIVE,   SENSITIVE,   FALSE, TRUE  },
	{ "import_settings_user_label",     SENSITIVE,   SENSITIVE,   FALSE, TRUE  },
	{ "import_settings_password",       SENSITIVE,   SENSITIVE,   FALSE, TRUE  },
	{ "import_settings_password_label", SENSITIVE,   SENSITIVE,   FALSE, TRUE  },

	{ "exports_add_button",             SENSITIVE,   SENSITIVE,   TRUE,  TRUE  },
	{ "exports_delete_button",          SENSITIVE,   SENSITIVE,   TRUE,  FALSE },
	{ "exports_settings_button",        SENSITIVE,   SENSITIVE,   FALSE, FALSE },
	{ "exports_enable_button",          HIDDEN,      SENSITIVE,   TRUE,  FALSE },

	{ "export_settings_enabled",        HIDDEN,      HIDDEN,      FALSE, FALSE },
	{ "export_settings_clients_button", SENSITIVE,   SENSITIVE,   FALSE, TRUE  },
	{ "export_settings_ac_samba",       SENSITIVE,   SENSITIVE,   FALSE, TRUE  },
	{ "export_settings_ac_nfs",         SENSITIVE,   SENSITIVE,   FALSE, FALSE },

	{ "export_clients_add_button",      SENSITIVE,   SENSITIVE,   TRUE,  TRUE  },
	{ "export_clients_update_button",   SENSITIVE,   SENSITIVE,   FALSE, FALSE },
	{ "export_clients_delete_button",   SENSITIVE,   SENSITIVE,   TRUE,  FALSE },
	{ NULL }
};

XstReportHookEntry report_hooks[] = {
	{ "filesys_mount_failed",          /* Report minor we're binding. */
	  (XstReportHookFunc *) import_mount_failed,             /* hook function */
	  XST_REPORT_HOOK_SAVE,            /* When to check for this hook. */
	  TRUE, NULL },                    /* Repeat?, user data. */

	{ "filesys_unmount_failed",        /* Report minor we're binding. */
	  (XstReportHookFunc *) import_unmount_failed,           /* hook function */
	  XST_REPORT_HOOK_SAVE,            /* When to check for this hook. */
	  TRUE, NULL },                    /* Repeat?, user data. */

	{ NULL, NULL, -1, FALSE, NULL }    /* Terminating record */
};

static void
apply_clicked_cb (XstDialog *dialog)
{
	check_mount_change (XST_SHARES_TOOL (dialog->tool));
}

static void
connect_signals (void)
{
	g_signal_connect (G_OBJECT (tool->main_dialog), "apply",
			  G_CALLBACK (apply_clicked_cb), tool->main_dialog);

	xst_dialog_connect_signals (tool->main_dialog, signals);
}

static void
transfer_xml_to_gui (XstTool *tool, gpointer data)
{
	populate_tables ();
}

int
main (int argc, char *argv[])
{
	xst_init ("shares-admin", argc, argv, NULL);
	tool = xst_shares_tool_new ();
	xst_tool_construct (tool, "shares", _("New Shares Admin"));

	xst_tool_add_report_hooks (tool, report_hooks);
	xst_tool_set_xml_funcs (tool, transfer_xml_to_gui, NULL, NULL);

	create_tables ();
	connect_signals ();
	xst_dialog_enable_complexity (tool->main_dialog);
	xst_dialog_set_widget_policies (tool->main_dialog, policies);

	xst_tool_main (tool, FALSE);

	return 0;
}
