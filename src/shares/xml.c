/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/* Copyright (C) 2000-2001 Ximian, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Hans Petter Jansson <hpj@ximian.com>
 */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

#include <gnome.h>
#include "xst.h"
#include "xml.h"

extern XstTool *tool;


xmlNodePtr
shares_xml_get_imports_node (void)
{
	xmlNodePtr node;

	node = xst_xml_element_find_first (xst_xml_doc_get_root (tool->config), "imports");

	if (!node) {
		node = xst_xml_element_add (xst_xml_doc_get_root (tool->config), "imports");
	}

	return node;
}

xmlNodePtr
shares_xml_get_exports_node (void)
{
	xmlNodePtr node;

	node = xst_xml_element_find_first (xst_xml_doc_get_root (tool->config), "exports");

	if (!node) {
		node = xst_xml_element_add (xst_xml_doc_get_root (tool->config), "exports");
	}

	return node;
}

xmlNodePtr
shares_xml_new_import_node (void)
{
	xmlNodePtr imports_node, import_node;

	imports_node = shares_xml_get_imports_node ();
	g_assert (imports_node);

	import_node = xst_xml_element_add (imports_node, "import");
	return import_node;
}

xmlNodePtr
shares_xml_new_export_node (void)
{
	xmlNodePtr exports_node, export_node;

	exports_node = shares_xml_get_exports_node ();
	g_assert (exports_node);

	export_node = xst_xml_element_add (exports_node, "export");
	return export_node;
}

xmlNodePtr
shares_xml_new_allow_node (xmlNodePtr export_node, gchar *clients, gboolean write)
{
	xmlNodePtr allow_node, pattern_node;

	allow_node = xst_xml_element_add (export_node, "allow");
	pattern_node = xst_xml_element_add (allow_node, "pattern");
	xst_xml_element_set_content (pattern_node, clients);
	xst_xml_element_set_state (allow_node, "write", write);

	return allow_node;
}

xmlNodePtr
shares_xml_find_import_node_by_point (const gchar *point)
{
	xmlNodePtr imports_node, import_node;

	imports_node = shares_xml_get_imports_node ();

	for (import_node = xst_xml_element_find_first (imports_node, "import");
	     import_node;
	     import_node = xst_xml_element_find_next (import_node, "import")) {
		xmlNodePtr point_node;
		const xmlChar *point_name;

		point_node = xst_xml_element_find_first (import_node, "point");
		if (!point_node)
			continue;

		point_name = xst_xml_element_peek_content (point_node);
		if (!strcmp ((char *) point_name, point))
			break;
	}

	return import_node;
}

void
shares_xml_remove_allow_nodes (xmlNodePtr export_node)
{
	xst_xml_element_destroy_children_by_name (export_node, "allow");
}

static xmlNodePtr
find_nfs_world_allow (xmlNodePtr export_node)
{
	xmlNodePtr node, ret = NULL;
	gchar *pattern;

	node = xst_xml_element_find_first (export_node, "allow");
	if (node)
	{
		pattern = xst_xml_get_child_content (node, "pattern");
		if (pattern)
		{
			if (!strcmp (pattern, "0.0.0.0/0"))
				ret = node;
			g_free (pattern);
		}
	}

	return ret;
}

void
shares_xml_export_set_restricted (xmlNodePtr export_node, gboolean restricted)
{
	xmlNodePtr node;
	gchar *pattern;

	for (node = xst_xml_element_find_first (export_node, "allow"); node;
	     node = xst_xml_element_find_next (node, "allow"))
	{
		pattern = xst_xml_get_child_content (node, "pattern");
		if (!strcmp (pattern, "0.0.0.0/0"))
		{
			xst_xml_element_destroy (node);
			g_free (pattern);
			break;
		}
		g_free (pattern);
	}

	if (!restricted)
		shares_xml_new_allow_node (export_node, "0.0.0.0/0", FALSE);
}

gboolean
shares_xml_export_get_restricted (xmlNodePtr export_node)
{
	xmlNodePtr node;

	node = find_nfs_world_allow (export_node);
	if (node)
		return FALSE;

	return TRUE;
}

void
shares_xml_export_set_general_write (xmlNodePtr export_node, gboolean general_write)
{
	xmlNodePtr node;

	/* This is sort of a hack. The tag doesn't do anything for NFS exports, but
	 * lets the ETable detect its setting */

	xst_xml_element_set_state (export_node, "write", general_write);

	for (node = xst_xml_element_find_first (export_node, "allow"); node;
	     node = xst_xml_element_find_next (node, "allow"))
	{
		gchar *pattern;
		
		pattern = xst_xml_get_child_content (node, "pattern");
		if (pattern && !strcmp (pattern, "0.0.0.0/0"))
		{
			xst_xml_element_set_state (node, "write", general_write);
			break;
		}
		else if (pattern)
			     g_free (pattern);
	}
}

gboolean
shares_xml_export_get_general_write (xmlNodePtr export_node)
{
	xmlNodePtr node;
	gchar *type;
	gboolean ret;

	type = xst_xml_element_get_attribute (export_node, "type");

	if (!strcasecmp (type, "nfs"))
	{
		node = find_nfs_world_allow (export_node);
		if (!node)
			ret = FALSE;
		else
			ret = xst_xml_element_get_state (node, "write");
	}
	else
		ret = xst_xml_element_get_state (export_node, "write");

	g_free (type);
	return ret;
}

gboolean
shares_xml_smbserver (void)
{
	xmlNodePtr node;

	node = xst_xml_element_find_first (xst_xml_doc_get_root (tool->config), "smbserver");

	if (node)
	{
		const gchar *buf = xst_xml_element_peek_content (node);
		if (strcmp (buf, "1") == 0)
			return TRUE;
	}

	return FALSE;
}

gboolean
shares_xml_smbmount (void)
{
	xmlNodePtr node;

	node = xst_xml_element_find_first (xst_xml_doc_get_root (tool->config), "smbmount");

	if (node && xst_xml_element_peek_content (node))
		return TRUE;

	return FALSE;
}

