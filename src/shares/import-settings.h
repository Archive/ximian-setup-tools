/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/* Copyright (C) 2000-2001 Ximian, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Hans Petter Jansson <hpj@ximian.com>.
 */

#ifndef __IMPORT_SETTINGS_H
#define __IMPORT_SETTINGS_H

typedef enum
{
	IMPORT_SETTINGS_TYPE_SMB,
	IMPORT_SETTINGS_TYPE_NFS,
	IMPORT_SETTINGS_TYPE_UNKNOWN
}
ImportSettingsType;

ImportSettingsType import_settings_get_type          (void);
void               import_settings_set_type          (ImportSettingsType type);

void               import_settings_select_type_cb    (GtkOptionMenu *option_menu, gpointer user_data);

void               import_settings_clear             (void);
void               import_settings_from_node         (xmlNodePtr root);
void               import_settings_from_current_node (void);
void               import_settings_to_node           (xmlNodePtr root);
void               import_settings_to_current_node   (void);

void               import_settings_run               (void);
void               import_settings_add_run           (void);

#endif /* __IMPORT_SETTINGS_H */
