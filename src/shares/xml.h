/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/* Copyright (C) 2000-2001 Ximian, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Hans Petter Jansson <hpj@ximian.com>
 */

xmlNodePtr shares_xml_get_imports_node      (void);
xmlNodePtr shares_xml_get_exports_node      (void);

xmlNodePtr shares_xml_new_import_node       (void);
xmlNodePtr shares_xml_new_export_node       (void);

xmlNodePtr shares_xml_new_allow_node        (xmlNodePtr export_node, gchar *clients, gboolean write);
xmlNodePtr shares_xml_find_import_node_by_point (const gchar *point);
void       shares_xml_remove_allow_nodes    (xmlNodePtr export_node);

void       shares_xml_export_set_restricted (xmlNodePtr export_node, gboolean restricted);
gboolean   shares_xml_export_get_restricted (xmlNodePtr export_node);

void       shares_xml_export_set_general_write (xmlNodePtr export_node, gboolean general_write);
gboolean   shares_xml_export_get_general_write (xmlNodePtr export_node);

gboolean   shares_xml_smbserver             (void);
gboolean   shares_xml_smbmount              (void);
