/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/* Copyright (C) 2000-2001 Ximian, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Hans Petter Jansson <hpj@ximian.com>
 */

#include <glib.h>
#include <gal/e-table/e-table-memory.h>
#include "xst-shares-tool.h"

/* Structure holding information for an export client */

typedef struct
{
	gchar *pattern;
	gboolean write;
}
ExportClient;

void          create_tables                         (void);
void          populate_tables                       (void);

GSList       *table_get_rows_as_gslist              (ETableMemory *model);
void          table_set_rows_from_gslist            (ETableMemory *model, GSList *gslist);

void          import_table_insert_node              (xmlNodePtr node);
xmlNodePtr    import_table_get_selected_node        (void);
void          import_table_update_selected          (void);
void          import_table_delete_selected_node     (void);
void          import_table_mount_selected_node_toggle_cb (void);

gboolean      import_mount_failed                   (XstSharesTool *tool, XstReportLine *rline, gpointer data);
gboolean      import_unmount_failed                 (XstSharesTool *tool, XstReportLine *rline, gpointer data);

void          export_table_insert_node              (xmlNodePtr node);
xmlNodePtr    export_table_get_selected_node        (void);
void          export_table_update_selected          (void);
void          export_table_delete_selected_node     (void);

void          export_clients_table_insert           (ExportClient *client);
ExportClient *export_clients_table_get_selected     (void);
void          export_clients_table_update_selected  (void);
void          export_clients_table_delete_selected  (void);
void          export_clients_table_clear            (void);
void          export_clients_table_to_export_node   (xmlNodePtr node);
void          export_clients_table_from_export_node (xmlNodePtr export_node);

void          check_mount_change                    (XstSharesTool *tool);
