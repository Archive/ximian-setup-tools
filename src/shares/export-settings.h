/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/* Copyright (C) 2000-2001 Ximian, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Hans Petter Jansson <hpj@ximian.com>.
 */

#ifndef __EXPORT_SETTINGS_H
#define __EXPORT_SETTINGS_H

typedef enum
{
	EXPORT_SETTINGS_TYPE_SMB,
	EXPORT_SETTINGS_TYPE_NFS,
	EXPORT_SETTINGS_TYPE_UNKNOWN
}
ExportSettingsType;

ExportSettingsType export_settings_get_type          (void);
void               export_settings_set_type          (ExportSettingsType type);

void               export_settings_select_type_cb    (GtkOptionMenu *option_menu, gpointer user_data);
void               export_settings_restrict_cb       (GtkWidget *toggle, gpointer user_data);

void               export_settings_clear             (void);
void               export_settings_from_node         (xmlNodePtr root);
void               export_settings_from_current_node (void);
void               export_settings_to_node           (xmlNodePtr root);
void               export_settings_to_current_node   (void);

void               export_settings_run               (void);
void               export_settings_add_run           (void);

#endif /* __EXPORT_SETTINGS_H */
