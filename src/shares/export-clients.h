/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/* Copyright (C) 2000-2001 Ximian, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Hans Petter Jansson <hpj@ximian.com>.
 */

void               export_clients_clear_entries       (void);
void               export_clients_clear               (void);
void               export_clients_from_client         (ExportClient *client);
void               export_clients_from_current_client (void);
void               export_clients_to_client           (ExportClient *client);
void               export_clients_to_current_client   (void);
void               export_clients_apply_policies      (void);

void               export_clients_run                 (void);

void               export_client_add_cb               (void);
void               export_client_update_cb            (void);
void               export_client_delete_cb            (void);
void               export_client_changed_cb           (void);
