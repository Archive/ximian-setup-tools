/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */

/* This file is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GNU Emacs; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Authors: Hans Petter Jansson <hpj@ximian.com> */

#ifndef XST_SHARES_TOOL_H
#define XST_SHARES_TOOL_H

#include "xst.h"

#define XST_TYPE_SHARES_TOOL            (xst_shares_tool_get_type ())
#define XST_SHARES_TOOL(obj)            (GTK_CHECK_CAST ((obj), XST_TYPE_SHARES_TOOL, XstSharesTool))
#define XST_SHARES_TOOL_CLASS(class)    (GTK_CHECK_CLASS_CAST ((class), XST_TYPE_SHARES_TOOL, XstSharesToolClass))
#define XST_IS_SHARES_TOOL(obj)         (GTK_CHECK_TYPE ((obj), XST_TYPE_SHARES_TOOL))
#define XST_IS_SHARES_TOOL_CLASS(class) (GTK_CHECK_CLASS_TYPE ((class), XST_TYPE_SHARES_TOOL))

typedef struct _XstSharesTool      XstSharesTool;
typedef struct _XstSharesToolClass XstSharesToolClass;

struct _XstSharesTool {
	XstTool tool;

	GList *failed_mounts;
	GList *failed_unmounts;
};

struct _XstSharesToolClass {
	XstToolClass parent_class;
};

GtkType  xst_shares_tool_get_type (void);
XstTool *xst_shares_tool_new      (void);

#endif
