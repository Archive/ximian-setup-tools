/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/* Copyright (C) 2000-2001 Ximian, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Hans Petter Jansson <hpj@ximian.com>.
 */

#include "import-settings.h"
#include "export-settings.h"

gboolean validate_import_type           (ImportSettingsType type);
gboolean validate_export_type           (ExportSettingsType type);
gboolean validate_export_name           (gpointer name);
gboolean validate_export_local_path     (gpointer path);
gboolean validate_import_host_name      (gpointer name);
gboolean validate_import_remote_path    (gpointer path);
gboolean validate_import_local_path     (gpointer path);
gboolean validate_export_client_pattern (gpointer pattern);
