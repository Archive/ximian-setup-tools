/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/* settings.h: this file is part of users-admin, a ximian-setup-tool frontend 
 * for user administration.
 * 
 * Copyright (C) 2000-2001 Ximian, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Tambet Ingo <tambet@ximian.com>
 */

#ifndef __SETTINGS_GUI_H
#define __SETTINGS_GUI_H

#include <gnome.h>
#include <gnome-xml/tree.h>

#include "settings.h"

typedef struct {
	GtkWidget       *top;
	PrinterSettings *settings;
	GladeXML        *xml;

	/* Connection frame */
	GtkEntry        *conn_name;
	GtkEntry        *conn_host;
	GtkEntry        *conn_queue;
	GtkEntry        *conn_workgroup;
	GtkEntry        *conn_user;
	GtkEntry        *conn_password;
	GtkEntry        *conn_port;
	GtkOptionMenu   *conn_device;

	/* Driver frame */
	GtkCList        *driver_list;
	GtkOptionMenu   *driver_resolution;
	GtkOptionMenu   *driver_papersize;
	GtkOptionMenu   *driver_color;

	/* Options frame */
	GtkToggleButton *option_eof;
	GtkToggleButton *option_lf;
	GtkToggleButton *option_fasttext;
	
} PrinterSettingsGui;

PrinterSettingsGui *printer_settings_gui_new     (PrinterSettings *settings, GtkWidget *parent);
void                printer_settings_gui_setup   (PrinterSettingsGui *gui, GtkWidget *top);
gboolean            printer_settings_gui_save    (PrinterSettingsGui *gui);
void                printer_settings_gui_destroy (PrinterSettingsGui *gui);

#endif /* SETTINGS_GUI_H */
