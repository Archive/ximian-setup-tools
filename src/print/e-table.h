/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/* e-table.h: this file is part of printers-admin, a ximian-setup-tool frontend 
 * for printer administration.
 * 
 * Copyright (C) 2001 Ximian, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Tambet Ingo <tambet@ximian.com>
 */

#ifndef __E_TABLE_H
#define __E_TABLE_H

#include <gnome.h>
#include <gal/e-table/e-table-memory.h>

#include "settings.h"

enum {
	COL_PRINTER_NAME,
	COL_PRINTER_TYPE,
	COL_PRINTER_DEVICE,
	COL_PRINTER_PAPERSIZE,
	COL_PRINTER_RESOLUTION,
	COL_PRINTER_SHARE,

	COL_PRINTER_LAST,
};

GtkWidget       *table_create         (void);
void             table_populate       (xmlNodePtr root);
void             table_set_state      (gboolean state);
PrinterSettings *table_get_selected   (void);
void             table_add_row        (PrinterSettings *settings);
void             table_update_current (void);
void             table_delete_current (void);

#endif /* E_TABLE_H */

