/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/* settings.h: this file is part of users-admin, a ximian-setup-tool frontend 
 * for user administration.
 * 
 * Copyright (C) 2000-2001 Ximian, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Tambet Ingo <tambet@ximian.com>
 */

#ifndef __SETTINGS_H
#define __SETTINGS_H

#include <gnome.h>
#include <gnome-xml/tree.h>

typedef enum {	
	PRINTER_LOCAL,
	PRINTER_LPD,
	PRINTER_SMB,
	PRINTER_NCP,
	PRINTER_DIRECT,

	PRINTER_NONE,
} XstPrinterType;

typedef struct {
	xmlNodePtr node;
	gboolean   new;

	XstPrinterType  type;
	gchar          *name;

	/* Connection */
	gchar          *host;
	gchar          *queue;
	gchar          *workgroup;
	gchar          *user;
	gchar          *password;
	gchar          *port;
	gchar          *device;

	/* Driver */
	gchar          *resolution;
	gchar          *papersize;
	
	/* Options */
	gboolean       trans_crlf;
	gboolean       send_eof;
	gboolean       fasttext;
	gint           pps;
	
} PrinterSettings;


PrinterSettings *printer_settings_new         (xmlNodePtr root, XstPrinterType type);
PrinterSettings *printer_settings_get_by_node (xmlNodePtr node);
GSList          *printer_settings_get_all     (xmlNodePtr root);
void             printer_settings_save        (PrinterSettings *settings);
void             printer_settings_delete      (PrinterSettings *settings);
void             printer_settings_destroy     (PrinterSettings *settings);

gchar           *printer_type_to_string       (XstPrinterType type);
XstPrinterType   printer_type_from_string     (const gchar *type_name);

#endif /* SETTINGS_H */

