/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/* main.c: this file is part of print-admin, a ximian-setup-tool frontend 
 * for printers administration.
 * 
 * Copyright (C) 2000-2001 Ximian, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Tambet Ingo <tambet@ximian.com>
 */


#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <gnome.h>
#include <gnome-xml/tree.h>
#include <gnome-xml/parser.h>
#include <gdk-pixbuf/gdk-pixbuf.h>
#include <glade/glade.h>

#include <time.h>
#include <stdlib.h>

#include "xst.h"

#include "e-table.h"
#include "callbacks.h"

XstTool *tool;

void quit_cb (XstTool *tool, gpointer data);

static XstDialogSignal signals[] = {
	{ "add_printer",   "clicked",  on_add_printer_clicked },
	{ "settings",      "clicked",  on_settings_clicked },
	{ "delete",        "clicked",  on_delete_clicked },
	{ NULL }
};

static const XstWidgetPolicy policies[] = {
	/* Name                     Basic                        Advanced                   Root   User */
	{ "add_printer",            XST_WIDGET_MODE_SENSITIVE,   XST_WIDGET_MODE_SENSITIVE, TRUE,  TRUE },
	{ "settings",               XST_WIDGET_MODE_SENSITIVE,   XST_WIDGET_MODE_SENSITIVE, TRUE,  FALSE },
	{ "delete",                 XST_WIDGET_MODE_SENSITIVE,   XST_WIDGET_MODE_SENSITIVE, TRUE,  FALSE },
	{ NULL }
};

static void
update_complexity (void)
{
	table_set_state (tool->main_dialog->complexity == XST_DIALOG_ADVANCED);
}

static void
connect_signals (void)
{
	gtk_signal_connect (GTK_OBJECT (tool->main_dialog), "complexity_change",
			    GTK_SIGNAL_FUNC (update_complexity),
			    NULL);

	xst_dialog_connect_signals (tool->main_dialog, signals);
}

static void
transfer_xml_to_gui (XstTool *tool, gpointer data)
{
	xmlNodePtr root;

	root = xst_xml_doc_get_root (tool->config);

	table_populate (root);
}

static void
transfer_gui_to_xml (XstTool *tool, gpointer data)
{
	xmlNodePtr root;

	root = xst_xml_doc_get_root (tool->config);

	g_warning ("Does nothing");
}

int
main (int argc, char *argv[])
{
	xst_init ("print-admin", argc, argv, NULL);
	tool = xst_tool_new ();
	xst_tool_construct (tool, "print", _("Print tool"));

	xst_tool_set_xml_funcs  (tool, transfer_xml_to_gui, transfer_gui_to_xml, NULL);

	connect_signals ();

	xst_dialog_enable_complexity (tool->main_dialog);
	update_complexity ();
	xst_dialog_set_widget_policies (tool->main_dialog, policies);

	xst_tool_main (tool, FALSE);

	return 0;
}
