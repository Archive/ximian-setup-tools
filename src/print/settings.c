/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/* settings.c: this file is part of print-admin, a ximian-setup-tool frontend 
 * for printer administration.
 * 
 * Copyright (C) 2001 Ximian, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Tambet Ingo <tambet@ximian.com>
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include "xst.h"
#include "settings.h"

typedef enum {
	GET,
	SET,
} XstPrintDirective;

typedef struct {
	gchar          *text;
	gchar          *xml_name;
	XstPrinterType  type;	
} PrinterTypeTable;

static PrinterTypeTable printer_type_table[] = {
	{ N_("Local"),     "LOCAL",  PRINTER_LOCAL },
	{ N_("UNIX"),      "LPD",    PRINTER_LPD },
	{ N_("Windows"),   "SMB",    PRINTER_SMB },
	{ N_("Novell"),    "NCP",    PRINTER_NCP },
	{ N_("JetDirect"), "DIRECT", PRINTER_DIRECT },

	{ N_("Unknown"),   "UNKNOWN",PRINTER_NONE },
	{ NULL, NULL, -1 },
};

gchar *
printer_type_to_string (XstPrinterType type)
{
	gint i;
	gchar *string = NULL;

	for (i = 0; printer_type_table[i].text; i++) {
		if (printer_type_table[i].type == type) {
			string = g_strdup (printer_type_table[i].text);
			break;
		}
	}

	return string;
}

XstPrinterType
printer_type_from_string (const gchar *type_name)
{
	gint i;
	XstPrinterType type = PRINTER_NONE;

	g_return_val_if_fail (type_name != NULL, type);

	for (i = 0; printer_type_table[i].text; i++) {
		if (!strcmp (printer_type_table[i].text, type_name)) {
			type = printer_type_table[i].type;
			break;
		}
	}

	return type;
}

static gchar *
printer_type_to_xml (XstPrinterType type)
{
	gint i;
	gchar *string = NULL;

	for (i = 0; printer_type_table[i].text; i++) {
		if (printer_type_table[i].type == type) {
			string = g_strdup (printer_type_table[i].xml_name);
			break;
		}
	}

	return string;
}

static XstPrinterType
printer_type_from_xml (const gchar *type_name)
{
	gint i;
	XstPrinterType type = PRINTER_NONE;

	g_return_val_if_fail (type_name != NULL, type);

	for (i = 0; printer_type_table[i].text; i++) {
		if (!strcmp (printer_type_table[i].xml_name, type_name)) {
			type = printer_type_table[i].type;
			break;
		}
	}

	return type;		
}

static gchar *
printer_general (PrinterSettings *settings, XstPrintDirective directive,
		 const gchar *field, const gchar *value)
{
	xmlNodePtr  n;
	gchar      *ret = NULL;

	g_return_val_if_fail (settings != NULL, NULL);
	g_return_val_if_fail (field != NULL, NULL);
	
	switch (directive) {
	case GET:
		n = xst_xml_element_find_first (settings->node, field);
		if (n)
			ret = xst_xml_element_get_content (n);
		else
			ret = g_strdup ("");
		break;
		
	case SET:
		xst_xml_set_child_content (settings->node, field, value);
		break;

	default:
		g_warning ("printer_general: unknown directive.");
		break;
	}

	return ret;
}

static gboolean
printer_general_bool (PrinterSettings *settings, XstPrintDirective directive,
		      const gchar *field, gboolean value)
{
	gchar    *buf;
	gboolean  ret = FALSE;
	
	switch (directive) {
	case GET:
		buf = printer_general (settings, directive, field, NULL);
		if (buf == NULL)
			return value;

		if ((strcmp (buf, "0")) != 0)
			ret = TRUE;
	
		g_free (buf);
		break;		
	case SET:
		if (value)
			buf = g_strdup ("1");
		else
			buf = g_strdup ("0");
		printer_general (settings, directive, field, buf);
		g_free (buf);
		break;
	default:
		g_warning ("printer_genereal_bool");
		break;
	}

	return ret;
}

static gboolean
printer_general_int (PrinterSettings *settings, XstPrintDirective directive,
		     const gchar *field, gint value)
{
	gchar *ret_buf;
	gchar *buf;
	gint   ret = 0;

	buf = g_strdup_printf ("%d", settings->pps);
	ret_buf = printer_general (settings, directive, field, buf);
	g_free (buf);

	if (ret_buf) {
		ret = atoi (ret_buf);
		g_free (ret_buf);
	}
	return ret;
}


static XstPrinterType
printer_type (PrinterSettings *settings, XstPrintDirective directive)
{
	gchar          *buf;
	XstPrinterType  type = PRINTER_NONE;

	switch (directive) {
	case GET:
		buf = printer_general (settings, directive, "type", NULL);
		if (buf == NULL)
			return type;

		type = printer_type_from_xml (buf);
		g_free (buf);
		break;
	case SET:
		buf = printer_type_to_xml (settings->type);
		if (buf == NULL)
			return type;

		printer_general (settings, directive, "type", buf);
		g_free (buf);
		type = settings->type;
		break;		
	default:
		g_warning ("printer_type: unknown directive");
		break;
	}

	return type;
}

static gchar *
printer_name (PrinterSettings *settings, XstPrintDirective directive)
{
	return printer_general (settings, directive, "name", settings->name);
}

static gchar *
printer_host (PrinterSettings *settings, XstPrintDirective directive)
{
	gchar *field, *ret;

	field = ret = NULL;

	switch (settings->type) {
	case PRINTER_SMB:    field = g_strdup ("smb_ip"); break;
	case PRINTER_LPD:    field = g_strdup ("lpd_server"); break;
	case PRINTER_DIRECT: field = g_strdup ("jetdirect_ip"); break;
	case PRINTER_NCP: 

	default: return NULL;
	}

	ret = printer_general (settings, directive, field, settings->host);
	g_free (field);

	return ret;
}

static gchar *
printer_queue (PrinterSettings *settings, XstPrintDirective directive)
{
	gchar *field, *ret;

	field = ret = NULL;

	switch (settings->type) {
	case PRINTER_SMB: field = g_strdup ("smb_share"); break;
	case PRINTER_LPD: field = g_strdup ("lpd_queue"); break;
	case PRINTER_NCP:

	default: return NULL;
	}

	ret = printer_general (settings, directive, field, settings->queue);
	g_free (field);

	return ret;
}

static gchar *
printer_workgroup (PrinterSettings *settings, XstPrintDirective directive)
{
	gchar *field, *ret;

	field = ret = NULL;

	switch (settings->type) {
	case PRINTER_SMB: field = g_strdup ("smb_workgroup"); break;

	default: return NULL;
	}

	ret = printer_general (settings, directive, field, settings->workgroup);
	g_free (field);

	return ret;
}

static gchar *
printer_user (PrinterSettings *settings, XstPrintDirective directive)
{
	gchar *ret;
	gchar *field = NULL;

	switch (settings->type) {
	case PRINTER_SMB: field = g_strdup ("smb_user"); break;
	case PRINTER_NCP: 

	default: return NULL;
	}

	ret = printer_general (settings, directive, field, settings->user);
	g_free (field);

	return ret;
}

static gchar *
printer_password (PrinterSettings *settings, XstPrintDirective directive)
{
	gchar *ret;
	gchar *field = NULL;

	switch (settings->type) {
	case PRINTER_SMB: field = g_strdup ("smb_password"); break;
	case PRINTER_NCP: 

	default: return NULL;
	}

	ret = printer_general (settings, directive, field, settings->password);
	g_free (field);

	return ret;
}

static gchar *
printer_port (PrinterSettings *settings, XstPrintDirective directive)
{
	gchar *ret;
	gchar *field = NULL;

	switch (settings->type) {
	case PRINTER_DIRECT: field = g_strdup ("jetdirect_port"); break;

	default: return NULL;
	}
	
	ret = printer_general (settings, directive, field, settings->port);
	g_free (field);

	return ret;
}

static gchar *
printer_device (PrinterSettings *settings, XstPrintDirective directive)
{
	gchar *ret;
	gchar *field = NULL;

	switch (settings->type) {
	case PRINTER_LOCAL: field = g_strdup ("device"); break;

	default: return NULL;
	}
	
	ret = printer_general (settings, directive, field, settings->device);
	g_free (field);

	return ret;
}

static gchar *
printer_resolution (PrinterSettings *settings, XstPrintDirective directive)
{
	return printer_general (settings, directive, "resolution", settings->resolution);
}

static gchar *
printer_papersize (PrinterSettings *settings, XstPrintDirective directive)
{
	return printer_general (settings, directive, "papersize", settings->papersize);
}

static gboolean
printer_trans_crlf (PrinterSettings *settings, XstPrintDirective directive)
{
	return printer_general_bool (settings, directive, "trans_crlf", settings->trans_crlf);
}

static gboolean
printer_send_eof (PrinterSettings *settings, XstPrintDirective directive)
{
	return printer_general_bool (settings, directive, "text_send_eof", settings->send_eof);
}

static gboolean
printer_fasttext (PrinterSettings *settings, XstPrintDirective directive)
{
/*	return printer_general_bool (settings, directive, "fasttext", settings->fasttext); */
	return FALSE;
}

static gint
printer_pps (PrinterSettings *settings, XstPrintDirective directive)
{
	return printer_general_int (settings, directive, "pages_per_sheet", settings->pps);
}

/* End of get/set functions */

static void
printer_settings_getset (PrinterSettings *settings, XstPrintDirective d)
{
	settings->type = printer_type (settings, d);
	
	settings->name =      printer_name (settings, d);
	settings->host =      printer_host (settings, d);
	settings->queue =     printer_queue (settings, d);
	settings->workgroup = printer_workgroup (settings, d);
	settings->user =      printer_user (settings, d);
	settings->password =  printer_password (settings, d);
	settings->port =      printer_port (settings, d);
	settings->device =    printer_device (settings, d);

	settings->resolution = printer_resolution (settings, d);
	settings->papersize =  printer_papersize (settings, d);

	settings->trans_crlf = printer_trans_crlf (settings, d);
	settings->send_eof =   printer_send_eof (settings, d);
	settings->fasttext =   printer_fasttext (settings, d);
	settings->pps =        printer_pps (settings, d);
}

PrinterSettings *
printer_settings_new (xmlNodePtr root, XstPrinterType type)
{
	PrinterSettings *settings;
	
	settings = g_new0 (PrinterSettings, 1);

	settings->node = root;
	settings->new  = TRUE;
	settings->type = type;

	return settings;
}

PrinterSettings *
printer_settings_get_by_node (xmlNodePtr node)
{
	PrinterSettings *settings;
	
	g_return_val_if_fail (node != NULL, NULL);

	settings = g_new0 (PrinterSettings, 1);

	settings->node = node;
	settings->new = FALSE;

	printer_settings_getset (settings, GET);
	
	return settings;
}

GSList *
printer_settings_get_all (xmlNodePtr root)
{
	PrinterSettings *settings;
	xmlNodePtr       node;
	GSList          *list = NULL;

	g_return_val_if_fail (root != NULL, NULL);

	node = xst_xml_element_find_first (root, "printer");
	while (node) {
		settings = printer_settings_get_by_node (node);

		if (settings)
			list = g_slist_append (list, settings);
		else
			g_warning ("Couldn't get settings.");

		node = xst_xml_element_find_next (node, "printer");
	}

	return list;
}

void
printer_settings_save (PrinterSettings *settings)
{
	xmlNodePtr node;
	
	g_return_if_fail (settings != NULL);

	node = settings->node;
	if (settings->new)
		settings->node = node = xst_xml_element_add (node, "printer");	

	/* TODO: check!!! */

	printer_settings_getset (settings, SET);
}

void
printer_settings_delete (PrinterSettings *settings)
{
	g_return_if_fail (settings != NULL);

	xst_xml_element_destroy (settings->node);
	printer_settings_destroy (settings);
}

void
printer_settings_destroy (PrinterSettings *settings)
{
	g_return_if_fail (settings != NULL);

	if (settings->name)      g_free (settings->name);
	if (settings->host)      g_free (settings->host);
	if (settings->queue)     g_free (settings->queue);
	if (settings->workgroup) g_free (settings->workgroup);
	if (settings->user)      g_free (settings->user);
	if (settings->password)  g_free (settings->password);
	if (settings->port)      g_free (settings->port);
	if (settings->device)    g_free (settings->device);

	g_free (settings);
}
