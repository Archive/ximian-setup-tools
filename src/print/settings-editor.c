/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Tambet Ingo <tambet@ximian.com>
 *
 *  Copyright 2001 Ximian, Inc. (www.ximian.com)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include "xst.h"
#include "settings-editor.h"
#include "e-table.h"

static void printer_settings_editor_class_init (PrinterSettingsEditorClass *class);
static void printer_settings_editor_finalize   (GtkObject *obj);

static GnomeDialogClass *parent_class;


GtkType
printer_settings_editor_get_type ()
{
	static GtkType type = 0;

	if (!type) {
		GtkTypeInfo type_info = {
			"PrinterSettingsEditor",
			sizeof (PrinterSettingsEditor),
			sizeof (PrinterSettingsEditorClass),
			(GtkClassInitFunc) printer_settings_editor_class_init,
			(GtkObjectInitFunc) NULL,
			(GtkArgSetFunc) NULL,
			(GtkArgGetFunc) NULL
		};

		type = gtk_type_unique (gnome_dialog_get_type (), &type_info);
	}

	return type;
}

static void
printer_settings_editor_class_init (PrinterSettingsEditorClass *class)
{
	GtkObjectClass *object_class;

	object_class = (GtkObjectClass *) class;
	parent_class = gtk_type_class (gnome_dialog_get_type ());

	object_class->finalize = printer_settings_editor_finalize;
}

static void
printer_settings_editor_finalize (GtkObject *obj)
{
	PrinterSettingsEditor *editor = (PrinterSettingsEditor *) obj;

	printer_settings_gui_destroy (editor->gui);
        ((GtkObjectClass *)(parent_class))->finalize (obj);
}

static void
ok_clicked (GtkWidget *widget, gpointer data)
{
	PrinterSettingsEditor *editor = data;

	if (printer_settings_gui_save (editor->gui)) {
		PrinterSettings *settings = editor->gui->settings;
		
		printer_settings_save (settings);

		/* Now that we have updated data, we have to update ETable too
		   making data GtkObject would be nicer I think, so we could
		   just send signals out. */

		if (settings->new)
			table_add_row (settings);
		else
			table_update_current ();
		
		gtk_widget_destroy (GTK_WIDGET (editor));
	}
}

static void
cancel_clicked (GtkWidget *widget, gpointer data)
{
	PrinterSettingsEditor *editor = data;

	gtk_widget_destroy (GTK_WIDGET (editor));
}

static gboolean
construct (PrinterSettingsEditor *editor, PrinterSettings *settings)
{
	GtkWidget *w;
	
	editor->gui = printer_settings_gui_new (settings, GTK_WIDGET (editor));
	g_return_val_if_fail (editor->gui != NULL, FALSE);

	w = glade_xml_get_widget (editor->gui->xml, "settings_editor");
	gtk_widget_reparent (w, GNOME_DIALOG (editor)->vbox);
	
	/* give our dialog an OK button and title */
	gtk_window_set_title (GTK_WINDOW (editor), _("Printer Settings Editor"));
	gtk_window_set_policy (GTK_WINDOW (editor), FALSE, TRUE, TRUE);
	gtk_window_set_modal (GTK_WINDOW (editor), FALSE);
	gnome_dialog_append_buttons (GNOME_DIALOG (editor),
				     GNOME_STOCK_BUTTON_OK,
				     GNOME_STOCK_BUTTON_CANCEL,
				     NULL);

	gnome_dialog_button_connect (GNOME_DIALOG (editor), 0 /* OK */,
				     GTK_SIGNAL_FUNC (ok_clicked),
				     editor);
	gnome_dialog_button_connect (GNOME_DIALOG (editor), 1 /* CANCEL */,
				     GTK_SIGNAL_FUNC (cancel_clicked),
				     editor);

	printer_settings_gui_setup (editor->gui, GNOME_DIALOG (editor)->vbox);

	return TRUE;
}

PrinterSettingsEditor *
printer_settings_editor_new (PrinterSettings *settings)
{
	PrinterSettingsEditor *new;

	g_return_val_if_fail (settings != NULL, NULL);
	
	new = (PrinterSettingsEditor *) gtk_type_new (printer_settings_editor_get_type ());
	if (construct (new, settings))
		return new;
	else {
		gtk_widget_destroy (GTK_WIDGET (new));
		return NULL;
	}
}
