/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/* settings.c: this file is part of print-admin, a ximian-setup-tool frontend 
 * for printer administration.
 * 
 * Copyright (C) 2001 Ximian, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Tambet Ingo <tambet@ximian.com>
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include "xst.h"
#include "settings-gui.h"
#include "e-table.h"

extern XstTool *tool;

/* Some GtkOptionMenu mappings, boy this widget sucks. */
typedef struct {
	gchar *text;
	gchar *xml_label;
	gint id;
} XstOptionMenuItem;

/* Data for resolution OptionMenu */
enum {
	RES_300x300,
	RES_400x400,
	RES_600x600,
	RES_1200x600,
	RES_1200x1200,
	RES_NONE,
};
static XstOptionMenuItem reso_items[] = {
	{ N_("300x300"),   "300x300",   RES_300x300 },
	{ N_("400x400"),   "400x400",   RES_400x400 },
	{ N_("600x600"),   "600x600",   RES_600x600 },
	{ N_("1200x600"),  "1200x600",  RES_1200x600 },
	{ N_("1200x1200"), "1200x1200", RES_1200x1200 },
	{ NULL, NULL, -1 },
};

/* Data for paper size OptionMenu */
enum {
	PAPER_LETTER,
	PAPER_LEGAL,
	PAPER_A4,
	PAPER_A3,
	PAPER_NONE,
};
static XstOptionMenuItem paper_items[] = {
	{ N_("Letter"), "letter", PAPER_LETTER },
	{ N_("Legal"),  "legal",  PAPER_LEGAL },
	{ N_("A4"),     "a4",     PAPER_A4 },
	{ N_("A3"),     "a3",     PAPER_A3 },
	{ NULL, NULL, -1 },
};

/* Data for colors OptionMenu */
enum {
	COLOR_16,
	COLOR_256,
	COLOR_HIGH,
	COLOR_TRUE,
	COLOR_NONE,
};
static XstOptionMenuItem color_items[] = {
	{ N_("16 colors"),  "16",   COLOR_16 },
	{ N_("256 colors"), "256",  COLOR_256 },
	{ N_("High color"), "high", COLOR_HIGH },
	{ N_("True color"), "true", COLOR_TRUE },
	{ NULL, NULL, -1 },
};

/* Data for device OptionMenu */
enum {
	DEV_LP0,
	DEV_LP1,
	DEV_TTYS0,
	DEV_TTYS1,
	DEV_TTYS2,
	DEV_TTYS3,
	DEV_NONE,
};
static XstOptionMenuItem device_items[] = {
	{ N_("/dev/lp0 (LPT1)"),   "/dev/lp0",   DEV_LP0 },
	{ N_("/dev/lp1 (LPT2)"),   "/dev/lp1",   DEV_LP1 },
	{ N_("/dev/ttyS0 (COM1)"), "/dev/ttyS0", DEV_TTYS0 },
	{ N_("/dev/ttyS1 (COM2)"), "/dev/ttyS1", DEV_TTYS1 },
	{ N_("/dev/ttyS2 (COM3)"), "/dev/ttyS2", DEV_TTYS2 },
	{ N_("/dev/ttyS3 (COM4)"), "/dev/ttyS3", DEV_TTYS3 },
	{ NULL, NULL, -1 },
};

static void
gui_connect_signals (PrinterSettingsGui *gui)
{
	
}

PrinterSettingsGui *
printer_settings_gui_new (PrinterSettings *settings, GtkWidget *parent)
{
	PrinterSettingsGui *gui;
	
	g_return_val_if_fail (settings != NULL, NULL);

	gui = g_new0 (PrinterSettingsGui, 1);
	gui->settings = settings;
	gui->xml = glade_xml_new (tool->glade_path, NULL);

	/* Connection frame */
	gui->conn_name =      GTK_ENTRY (glade_xml_get_widget (gui->xml, "settings_conn_name"));
	gui->conn_host =      GTK_ENTRY (glade_xml_get_widget (gui->xml, "settings_conn_host"));
	gui->conn_queue =     GTK_ENTRY (glade_xml_get_widget (gui->xml, "settings_conn_queue"));
	gui->conn_workgroup = GTK_ENTRY (glade_xml_get_widget (gui->xml, "settings_conn_workgroup"));
	gui->conn_user =      GTK_ENTRY (glade_xml_get_widget (gui->xml, "settings_conn_user"));
	gui->conn_password =  GTK_ENTRY (glade_xml_get_widget (gui->xml, "settings_conn_password"));
	gui->conn_port =      GTK_ENTRY (glade_xml_get_widget (gui->xml, "settings_conn_port"));
	gui->conn_device =    GTK_OPTION_MENU (glade_xml_get_widget (gui->xml, "settings_conn_device"));

	/* Driver frame */
	gui->driver_list =       GTK_CLIST (glade_xml_get_widget (gui->xml, "settings_driver_list"));
	gui->driver_resolution = GTK_OPTION_MENU (glade_xml_get_widget (gui->xml, "settings_driver_resolution"));
	gui->driver_papersize =  GTK_OPTION_MENU (glade_xml_get_widget (gui->xml, "settings_driver_papersize"));
	gui->driver_color =      GTK_OPTION_MENU (glade_xml_get_widget (gui->xml, "settings_driver_color"));

	/* Options frame */
	gui->option_eof =      GTK_TOGGLE_BUTTON (glade_xml_get_widget (gui->xml, "settings_option_eof"));
	gui->option_lf =       GTK_TOGGLE_BUTTON (glade_xml_get_widget (gui->xml, "settings_option_lf"));
	gui->option_fasttext = GTK_TOGGLE_BUTTON (glade_xml_get_widget (gui->xml, "settings_option_fasttext"));
	
	gui_connect_signals (gui);
	
	return gui;
}

static void
gui_settings_widget_show (PrinterSettingsGui *gui, GtkWidget *w)
{
	gchar *buf;
	GtkWidget *label;

	g_return_if_fail (w != NULL);

	buf = g_strdup_printf ("%slabel", w->name);
	label = glade_xml_get_widget (gui->xml, buf);
	g_free (buf);

	if (label)
		gtk_widget_show (label);
	
	gtk_widget_show (w);
}

static void
gui_setup_option_menu (GtkOptionMenu *om, XstOptionMenuItem *items, const gchar *string)
{
	GtkWidget *menu;
	gint active_item = 0;
	gint i;

	g_return_if_fail (om != NULL);
	g_return_if_fail (GTK_IS_OPTION_MENU (om));
	g_return_if_fail (items != NULL);

	gtk_option_menu_remove_menu (om);
	menu = gtk_menu_new ();	
	
	for (i = 0; items[i].id != -1; i++) {
		GtkWidget *item;

		if (items[i].text)
			item = gtk_menu_item_new_with_label (items[i].text);
		else
			item = gtk_menu_item_new ();

		gtk_widget_show (item);
		gtk_menu_append (GTK_MENU (menu), item);
		gtk_object_set_data (GTK_OBJECT (item), "Id", GINT_TO_POINTER (items[i].id));

		if (string && items[i].xml_label &&
		    strcmp (string, items[i].xml_label) == 0)
			active_item = items[i].id;
	}
	
	gtk_option_menu_set_menu (om, menu);
	gtk_option_menu_set_history (om, active_item);
}

typedef struct {
	gchar *widget_name;
	gint   id;
} XstRadioButtonItem; /* I know, name sucks */

static XstRadioButtonItem page_items[] = {
	{ "settings_option_mpage1", 1 },
	{ "settings_option_mpage2", 2 },
	{ "settings_option_mpage4", 4 },
	{ "settings_option_mpage8", 8 },
	{ NULL, -1 },
};
	
static void
gui_setup_radio_button (PrinterSettingsGui *gui, XstRadioButtonItem *items, gint active_item)
{
	gint i;

	g_return_if_fail (gui != NULL);
	g_return_if_fail (items != NULL);

	for (i = 0; items[i].id != -1; i++) {
		if (items[i].id == active_item) {
			GtkWidget *w = glade_xml_get_widget (gui->xml, items[i].widget_name);

			g_return_if_fail (w != NULL);
			g_return_if_fail (GTK_IS_RADIO_BUTTON (w));

			gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (w), TRUE);
			break;
		}
	}
}

static void
gui_setup_local (PrinterSettingsGui *gui)
{
	g_return_if_fail (gui != NULL);

	gui_setup_option_menu (gui->conn_device, device_items, gui->settings->device);
	gui_settings_widget_show (gui, GTK_WIDGET (gui->conn_device));
}

static void
gui_setup_lpd (PrinterSettingsGui *gui)
{
	g_return_if_fail (gui != NULL);

	gui_settings_widget_show (gui, GTK_WIDGET (gui->conn_host));
	gui_settings_widget_show (gui, GTK_WIDGET (gui->conn_queue));
}

static void
gui_setup_smb (PrinterSettingsGui *gui)
{
	g_return_if_fail (gui != NULL);

	gui_settings_widget_show (gui, GTK_WIDGET (gui->conn_host));
	gui_settings_widget_show (gui, GTK_WIDGET (gui->conn_queue));
	gui_settings_widget_show (gui, GTK_WIDGET (gui->conn_workgroup));
	gui_settings_widget_show (gui, GTK_WIDGET (gui->conn_user));
	gui_settings_widget_show (gui, GTK_WIDGET (gui->conn_password));	
}

static void
gui_setup_novell (PrinterSettingsGui *gui)
{
	g_return_if_fail (gui != NULL);

	gui_settings_widget_show (gui, GTK_WIDGET (gui->conn_host));
	gui_settings_widget_show (gui, GTK_WIDGET (gui->conn_queue));
	gui_settings_widget_show (gui, GTK_WIDGET (gui->conn_user));
	gui_settings_widget_show (gui, GTK_WIDGET (gui->conn_password));	
}

static void
gui_setup_direct (PrinterSettingsGui *gui)
{
	g_return_if_fail (gui != NULL);

	gui_settings_widget_show (gui, GTK_WIDGET (gui->conn_host));
	gui_settings_widget_show (gui, GTK_WIDGET (gui->conn_port));
}

void
printer_settings_gui_setup (PrinterSettingsGui *gui, GtkWidget *top)
{
	PrinterSettings *settings;
	
	g_return_if_fail (gui != NULL);

	settings = gui->settings;
	g_return_if_fail (settings != NULL);

	/* Show right combination of widgets */
	gtk_widget_show (GTK_WIDGET (gui->conn_name));
			 
	switch (settings->type) {
	case PRINTER_LOCAL: gui_setup_local (gui); break;
	case PRINTER_LPD:   gui_setup_lpd (gui); break;
	case PRINTER_SMB:   gui_setup_smb (gui); break;
	case PRINTER_NCP: gui_setup_novell (gui); break;
	case PRINTER_DIRECT: gui_setup_direct (gui); break;

	default:
		g_warning ("Unknown printer type '%d'.", settings->type);
		return;
		break;
	}	
	
	/* Populate widgets with data */

	/* Connection frame */
	xst_ui_entry_set_text (gui->conn_name, settings->name);
	xst_ui_entry_set_text (gui->conn_host, settings->host);
	xst_ui_entry_set_text (gui->conn_queue, settings->queue);
	xst_ui_entry_set_text (gui->conn_workgroup, settings->workgroup);
	xst_ui_entry_set_text (gui->conn_user, settings->user);
	xst_ui_entry_set_text (gui->conn_password, settings->password);
	xst_ui_entry_set_text (gui->conn_port, settings->port);

	/* Driver frame */
	gui_setup_option_menu (gui->driver_resolution, reso_items, settings->resolution);
	gui_setup_option_menu (gui->driver_papersize, paper_items, settings->papersize);
/*	gui_setup_option_menu (gui->driver_color, color_items, settings->color); */

	/* Options frame */
	gtk_toggle_button_set_active (gui->option_eof, settings->send_eof);
	gtk_toggle_button_set_active (gui->option_lf, settings->trans_crlf);
	gtk_toggle_button_set_active (gui->option_fasttext, settings->fasttext);

	gui_setup_radio_button (gui, page_items, settings->pps);
}

static void
gui_save_entry (void *widget, gchar **slot)
{
	g_return_if_fail (widget != NULL);
	g_return_if_fail (GTK_IS_WIDGET (widget));
	g_return_if_fail (GTK_IS_EDITABLE (widget));
	
	if (*slot)
		g_free (*slot);

	*slot = gtk_editable_get_chars (GTK_EDITABLE (widget), 0, -1);
}

static gint
option_menu_get_history (GtkWidget *omenu)
{
	GtkWidget *menu, *menu_item;
	GList *children, *child;

	if (omenu) {
		menu = gtk_option_menu_get_menu (GTK_OPTION_MENU (omenu));
		menu_item = GTK_OPTION_MENU (omenu)->menu_item;
		if (menu) {
			children = GTK_MENU_SHELL (menu)->children;
			child = g_list_find(children, menu_item);
			if (child) {
				return g_list_position(children, child);
			}
		}
	}

	return -1;
}

static void
gui_save_option_menu (GtkOptionMenu *om, XstOptionMenuItem *items, gchar **slot)
{
	gint i, active_item;
	
	g_return_if_fail (om != NULL);
	g_return_if_fail (GTK_IS_OPTION_MENU (om));
	
	if (*slot)
		g_free (*slot);
	
	active_item = option_menu_get_history (GTK_WIDGET (om));
	g_return_if_fail (active_item != -1);

	for (i = 0; items[i].id != -1; i++) {
		if (items[i].id == active_item) {
			*slot =  g_strdup (items[i].xml_label);
			break;
		}
	}
}

static void
gui_save_radio_button (PrinterSettingsGui *gui, XstRadioButtonItem *items, gint *slot)
{
	gint i;

	g_return_if_fail (gui != NULL);
	g_return_if_fail (items != NULL);

	for (i = 0; items[i].id != -1; i++) {
		GtkWidget *w = glade_xml_get_widget (gui->xml, items[i].widget_name);

		g_return_if_fail (w != NULL);
		g_return_if_fail (GTK_IS_RADIO_BUTTON (w));

		if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (w))) {
			*slot = items[i].id;
			break;
		}
	}

	if (items[i].id == -1)
		g_warning ("gui_save_radio_button: Couldn't find active item number ");

}


gboolean
printer_settings_gui_save (PrinterSettingsGui *gui)
{
	PrinterSettings *settings;
	
	g_return_val_if_fail (gui != NULL, FALSE);
	
	settings = gui->settings;
	g_return_val_if_fail (settings != NULL, FALSE);

	/* Connection frame */
	gui_save_entry (gui->conn_name, &settings->name);
	gui_save_entry (gui->conn_host, &settings->host);
	gui_save_entry (gui->conn_queue, &settings->queue);
	gui_save_entry (gui->conn_workgroup, &settings->workgroup);
	gui_save_entry (gui->conn_user, &settings->user);
	gui_save_entry (gui->conn_password, &settings->password);
	gui_save_entry (gui->conn_port, &settings->port);

	/* Driver frame */
	gui_save_option_menu (gui->conn_device, device_items, &settings->device);
	gui_save_option_menu (gui->driver_resolution, reso_items, &settings->resolution);
	gui_save_option_menu (gui->driver_papersize, paper_items, &settings->papersize);
/* 	gui_save_option_menu (gui->driver_color, color_items, &settings->color); */

	/* Options frame */
	settings->send_eof =   gtk_toggle_button_get_active (gui->option_eof);
	settings->trans_crlf = gtk_toggle_button_get_active (gui->option_lf);
	settings->fasttext =   gtk_toggle_button_get_active (gui->option_fasttext);

	gui_save_radio_button (gui, page_items, &settings->pps);
	
	xst_dialog_modify (tool->main_dialog);
	
	return TRUE;
}
	
void
printer_settings_gui_destroy (PrinterSettingsGui *gui)
{
	g_return_if_fail (gui != NULL);

	printer_settings_destroy (gui->settings);
	gtk_object_unref (GTK_OBJECT (gui->xml));
	g_free (gui);
}
