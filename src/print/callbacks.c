/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/* callbacks.c: this file is part of print-admin, a ximian-setup-tool frontend 
 * for printers administration.
 * 
 * Copyright (C) 2001 Ximian, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Tambet Ingo <tambet@ximian.com>
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <ctype.h>
#include <gnome.h>

#include "xst.h"
#include "callbacks.h"
#include "e-table.h"
#include "settings.h"
#include "settings-editor.h"

XstTool *tool;

static gint reply;

static void
reply_cb (gint val, gpointer data)
{
	reply = val;
	gtk_main_quit ();
}

static XstPrinterType
ask_new_printer_type (void)
{	
	GtkWidget *d;
	gint res, i;
	XstPrinterType type = PRINTER_NONE;
	struct {
		gchar          *radio_button;
		XstPrinterType  type;
	} radio_buttons[] = {
		{ "new_local",  PRINTER_LOCAL },
		{ "new_smb",    PRINTER_SMB },
		{ "new_lpd",    PRINTER_LPD },
		{ "new_ncp",    PRINTER_NCP },
		{ "new_direct", PRINTER_DIRECT },
		{ NULL,         PRINTER_NONE }
	};
		
	g_return_val_if_fail (xst_tool_get_access (tool), type);

	d = xst_dialog_get_widget (tool->main_dialog,
				   "new_printer_type_dialog");
	g_return_val_if_fail (d != NULL, type);

	res = gnome_dialog_run_and_close (GNOME_DIALOG (d));
	if (res)
		return type;

	for (i = 0; radio_buttons[i].radio_button; i++) {
		GtkWidget *w;
		gboolean state;

		w = xst_dialog_get_widget (tool->main_dialog,
					   radio_buttons[i].radio_button);

		state = gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (w));
		if (state)			
			break;
	}

	type = radio_buttons[i].type;

	return type;
}

void
on_add_printer_clicked (GtkButton *button, gpointer user_data)
{
	PrinterSettings *settings;
	PrinterSettingsEditor *editor;
	xmlNodePtr root;
	XstPrinterType type;
	
	g_return_if_fail (xst_tool_get_access (tool));

	type = ask_new_printer_type ();
	if (type == PRINTER_NONE)
		return;

	root = xst_xml_doc_get_root (tool->config);
	settings = printer_settings_new (root, type);
	g_return_if_fail (settings != NULL);

	editor = printer_settings_editor_new (settings);
	g_return_if_fail (editor != NULL);

	gtk_widget_show (GTK_WIDGET (editor));
}

void
on_settings_clicked (GtkButton *button, gpointer user_data)
{
	PrinterSettings *settings;
	PrinterSettingsEditor *editor;

	settings = table_get_selected ();
	g_return_if_fail (settings != NULL);

	editor = printer_settings_editor_new (settings);
	g_return_if_fail (editor != NULL);

	gtk_widget_show (GTK_WIDGET (editor));
}

void
on_delete_clicked (GtkButton *button, gpointer user_data)
{
	PrinterSettings *settings;
	gchar *buf;
	GtkWidget *d;

	g_return_if_fail (xst_tool_get_access (tool));

	settings = table_get_selected ();
	g_return_if_fail (settings != NULL);

	buf = g_strdup_printf (N_("Delete %s?"), settings->name);
	d = gnome_question_dialog_parented (buf, reply_cb, NULL,
					    GTK_WINDOW (tool->main_dialog));
	gnome_dialog_run_and_close (GNOME_DIALOG (d));
	g_free (buf);

	if (reply)
		return;

	/* TODO: delete */
	printer_settings_delete (settings);
	table_delete_current ();
	
	xst_dialog_modify (tool->main_dialog);

	gtk_widget_set_sensitive (xst_dialog_get_widget (
		tool->main_dialog, "settings"), FALSE);
	gtk_widget_set_sensitive (xst_dialog_get_widget (
		tool->main_dialog, "delete"), FALSE);
}
