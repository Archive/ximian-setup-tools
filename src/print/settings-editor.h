/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/* e-table.h: this file is part of printers-admin, a ximian-setup-tool frontend 
 * for printer administration.
 * 
 * Copyright (C) 2001 Ximian, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Tambet Ingo <tambet@ximian.com>
 */

#ifndef PRINTER_SETTINGS_EDITOR_H
#define PRINTER_SETTINGS_EDITOR_H

#ifdef __cplusplus
extern "C" {
#pragma }
#endif /* __cplusplus */

#include <libgnomeui/gnome-dialog.h>
#include "settings-gui.h"

#define PRINTER_SETTINGS_EDITOR_TYPE        (printer_settings_editor_get_type ())
#define PRINTER_SETTINGS_EDITOR(o)          (GTK_CHECK_CAST ((o), PRINTER_SETTINGS_EDITOR_TYPE, PrinterSettingsEditor))
#define PRINTER_SETTINGS_EDITOR_CLASS(k)    (GTK_CHECK_CLASS_CAST((k), PRINTER_SETTINGS_EDITOR_TYPE, PrinterSettingsEditorClass))
#define IS_PRINTER_SETTINGS_EDITOR(o)       (GTK_CHECK_TYPE ((o), PRINTER_SETTINGS_EDITOR_TYPE))
#define IS_PRINTER_SETTINGS_EDITOR_CLASS(k) (GTK_CHECK_CLASS_TYPE ((k), PRINTER_SETTINGS_EDITOR_TYPE))

struct _PrinterSettingsEditor {
	GnomeDialog parent;
	
	PrinterSettingsGui *gui;
};

typedef struct _PrinterSettingsEditor PrinterSettingsEditor;

typedef struct {
	GnomeDialogClass parent_class;
	
	/* signals */
	
} PrinterSettingsEditorClass;

GtkType printer_settings_editor_get_type (void);

PrinterSettingsEditor *printer_settings_editor_new (PrinterSettings *settings);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* PRINTER_SETTINGS_EDITOR_H */
