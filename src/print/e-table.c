/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/* e-table.c: this file is part of printers-admin, a ximian-setup-tool frontend 
 * for print administration.
 * 
 * Copyright (C) 2000-2001 Ximian, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Tambet Ingo <tambet@ximian.com>
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <gnome.h>
#include "xst.h"
#include <gal/e-table/e-table-scrolled.h>
#include <gal/e-table/e-table-memory.h>
#include <gal/e-table/e-table-memory-callbacks.h>
#include <gal/e-table/e-cell-text.h>
#include <gal/e-paned/e-hpaned.h>

#include "e-table.h"

#define PRINT_TABLE_SPEC "print.etspec"

extern XstTool *tool;

GtkWidget *print_table = NULL;

/* e-table states. */

const gchar *adv_print_state = "\
<ETableState> \
  <column source=\"0\"/> \
  <column source=\"1\"/> \
  <column source=\"2\"/> \
  <column source=\"3\"/> \
  <column source=\"4\"/> \
  <grouping> \
    <leaf column=\"0\" ascending=\"true\"/> \
  </grouping> \
</ETableState>";

const gchar *basic_print_state = "\
<ETableState> \
  <column source=\"0\"/> \
  <column source=\"1\"/> \
  <column source=\"2\"/> \
  <grouping> \
    <leaf column=\"0\" ascending=\"true\"/> \
  </grouping> \
</ETableState>";

static int
print_col_count (ETableModel *etm, void *data)
{
        return COL_PRINTER_LAST;
}

static void *
duplicate_value (ETableModel *etm, int col, const void *value, void *data)
{
        return g_strdup (value);
}

static void
free_value (ETableModel *etm, int col, void *value, void *data)
{
        g_free (value);
}

static void *
initialize_value (ETableModel *etm, int col, void *data)
{
        return g_strdup ("");
}

static gboolean
value_is_empty (ETableModel *etm, int col, const void *value, void *data)
{
        return !(value && *(char *)value);
}

static char *
value_to_string (ETableModel *etm, int col, const void *value, void *data)
{
	return (gchar *)value;
}

static void
print_set_value_at (ETableModel *etm, int col, int row, const void *val, void *data)
{
}

static gboolean
is_editable (ETableModel *etm, int col, int row, void *model_data)
{
	return FALSE;
}

static void *
print_value_at (ETableModel *etm, int col, int row, void *model_data)
{
	xmlNodePtr node;
	PrinterSettings *settings;
	gchar *buf = NULL;

	node = e_table_memory_get_data (E_TABLE_MEMORY (etm), row);
	g_return_val_if_fail (node != NULL, NULL);
	
	settings = printer_settings_get_by_node (node);
	g_return_val_if_fail (settings != NULL, NULL);

	switch (col) {
	case COL_PRINTER_NAME: buf = g_strdup (settings->name); break;
	case COL_PRINTER_TYPE: buf = g_strdup (printer_type_to_string (settings->type)); break;
	case COL_PRINTER_DEVICE: buf = g_strdup (settings->device); break;
	case COL_PRINTER_PAPERSIZE: buf = g_strdup (settings->papersize); break;
	case COL_PRINTER_RESOLUTION: buf = g_strdup (settings->resolution); break;
	case COL_PRINTER_SHARE: break;

	default:
		g_warning ("print_value_at: unknown col '%d'.", col);
		break;
	}

	printer_settings_destroy (settings);
	/* TODO: we leak buf every time print_value_at is called.
	   Unfortunately it gets called A LOT. */
	
	return buf;
}

static void
cursor_change (ETable *et, gint row, gpointer print_data)
{
	if (xst_tool_get_access (tool) && row >= 0) {
		XstDialog *xd = tool->main_dialog;
		
		gtk_widget_set_sensitive (xst_dialog_get_widget (xd, "settings"), TRUE);
		gtk_widget_set_sensitive (xst_dialog_get_widget (xd, "delete"), TRUE);
	}
}

GtkWidget *
table_create (void)
{
	ETable      *table;
	ETableModel *model;
	gchar       *spec;

	if (print_table)
		return NULL;
	
	model = e_table_memory_callbacks_new (print_col_count,
					      print_value_at,
					      print_set_value_at,
					      is_editable,
					      duplicate_value,
					      free_value,
					      initialize_value,
					      value_is_empty,
					      value_to_string,
					      NULL);

	spec = xst_conf_get_string (tool, "spec");
	if (!spec) {
		spec = xst_ui_load_etspec (tool->etspecs_common_path, PRINT_TABLE_SPEC);
		if (!spec)
			g_error ("create_print_table: Couldn't make table.");
		xst_conf_set_string (tool, "spec", spec);
	}
	
	print_table = e_table_scrolled_new (E_TABLE_MODEL (model), NULL, spec,
					   basic_print_state);

	g_free(spec);

	table = e_table_scrolled_get_table (E_TABLE_SCROLLED (print_table));
	gtk_signal_connect (GTK_OBJECT (table), "cursor_change", cursor_change, NULL);

/* hmm... on_settings_clicked is defined in callbacks.c but we don't want to depend on it	
   gtk_signal_connect (GTK_OBJECT (table), "double_click", on_settings_clicked, NULL); */

	return print_table;
}

void
table_populate (xmlNodePtr root)
{
	ETable          *table;
	ETableModel     *model;
	GSList          *list, *tmp_list;
	PrinterSettings *settings;

	g_return_if_fail (root != NULL);

	table = e_table_scrolled_get_table (
		E_TABLE_SCROLLED (print_table));
	g_return_if_fail (table != NULL);

	model = table->model;
	g_return_if_fail (model != NULL);

	list = printer_settings_get_all (root);
	
	e_table_memory_freeze (E_TABLE_MEMORY (model));
       
	tmp_list = list;
	while (tmp_list) {
		settings = tmp_list->data;
		tmp_list = tmp_list->next;

		e_table_memory_insert (E_TABLE_MEMORY (model), -1, settings->node);
		printer_settings_destroy (settings);
	}
	g_slist_free (list);
	
	e_table_memory_thaw (E_TABLE_MEMORY (model));
}

void
table_set_state (gboolean state)
{
	ETable *table = e_table_scrolled_get_table (E_TABLE_SCROLLED (print_table));

	g_return_if_fail (table != NULL);
	g_return_if_fail (E_IS_TABLE (table));

	if (state)
		e_table_set_state (table, adv_print_state);
	else
		e_table_set_state (table, basic_print_state);
}

PrinterSettings *
table_get_selected (void)
{
	gint             row;
	xmlNodePtr       node;
	PrinterSettings *settings;
	ETable          *table = e_table_scrolled_get_table (E_TABLE_SCROLLED (print_table));

	g_return_val_if_fail (table != NULL, NULL);
	g_return_val_if_fail (E_IS_TABLE (table), NULL);

	row = e_table_get_cursor_row (table);
	if (row < 0) {
		g_warning ("table_get_selected: unknown row '%d'.", row);
		return NULL;
	}

	node = e_table_memory_get_data (E_TABLE_MEMORY (table->model), row);
	g_return_val_if_fail (node != NULL, NULL);
	
	settings = printer_settings_get_by_node (node);
	if (settings == NULL)
		g_warning ("table_get_selected: couldn't get data from row '%d'.", row);

	return settings;
}

void
table_add_row (PrinterSettings *settings)
{
	ETable *table;
	
	g_return_if_fail (settings != NULL);

	table = e_table_scrolled_get_table (E_TABLE_SCROLLED (print_table));
	g_return_if_fail (table != NULL);
	g_return_if_fail (E_IS_TABLE (table));

	e_table_memory_insert (E_TABLE_MEMORY (table->model), -1, settings->node);
}

void
table_update_current (void)
{
	gint    row;
	ETable *table = e_table_scrolled_get_table (E_TABLE_SCROLLED (print_table));

	g_return_if_fail (table != NULL);
	g_return_if_fail (E_IS_TABLE (table));

	row = e_table_get_cursor_row (table);
	if (row >= 0)
		e_table_model_row_changed (table->model, row);
	else
		g_warning ("table_update_current: can't update row '%d'.", row);
}

void
table_delete_current (void)
{
	gint    row;
	ETable *table = e_table_scrolled_get_table (E_TABLE_SCROLLED (print_table));

	g_return_if_fail (table != NULL);
	g_return_if_fail (E_IS_TABLE (table));

	row = e_table_get_cursor_row (table);
	if (row >= 0)
		e_table_memory_remove (E_TABLE_MEMORY (table->model), row);
	else
		g_warning ("table_delete_current: can't delete row '%d'.", row);
}
