/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
#include "config.h"

#include <stdio.h>
#include <gnome.h>
#include <glade/glade.h>
#include <gdk-pixbuf/gdk-pixbuf.h>
#include <gdk-pixbuf/gnome-canvas-pixbuf.h>

#include "xst.h"

#include "callbacks.h"
#include "transfer.h"
#include "pixmaps.h"

XstTool *tool = NULL;

static XstDialogSignal signals[] = {
	{ "partition_ok",    "clicked",        xst_dialog_modify_cb },
	{ "disks",           "select_row",     on_disks_select_row },
	{ "disks",           "unselect_row",   on_disks_unselect_row },
	{ "new_launch",      "clicked",        on_partition_new_launched },
	{ "mounted_toggle",  "toggled",        xst_dialog_modify_cb },
	{ "mounted_toggle",  "toggled",        on_mounted_toggled },
	{ "settings_launch", "clicked",        on_partition_settings_launched },
	{ "listed_toggle",   "toggled",        xst_dialog_modify_cb },
	{ "listed_toggle",   "toggled",        on_listed_toggled },
	/* { "partition_type",  "selection-done", on_partition_type_selected }, */
	{ NULL }
};

int
main (int argc, char *argv[])
{
	GtkWidget *w0;

	xst_init ("disks-admin", argc, argv, NULL);
	tool = xst_tool_new ();
	xst_tool_construct (tool, "disks", _("Disk Partitions"));

	xst_tool_set_xml_funcs (tool, transfer_xml_to_gui, transfer_gui_to_xml, NULL);
	xst_dialog_connect_signals (tool->main_dialog, signals);

	pixmaps_init ();

	w0 = xst_dialog_get_widget (tool->main_dialog, "disks");
	gtk_ctree_set_indent (GTK_CTREE (w0), 16);

	xst_tool_main (tool, FALSE);

	return 0;
}
