/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/* Copyright (C) 2000-2001 Ximian, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Hans Petter Jansson <hpj@ximian.com>.
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <stdlib.h>
#include <string.h>
#include <gnome.h>

#include "xst.h"

#include "callbacks.h"
#include "transfer.h"
#include "pixmaps.h"

extern XstTool *tool;

GtkCTreeNode *disks_node_selected = NULL;


/* --- */


static gchar *
path_nice (gchar *path0)
{
	char *path1, *p0;

	/* Remove leading and trailing blanks */

	g_strstrip (path0);

	if (!*path0) return(path0);  /* User meant it to be empty */

	/* Remove leading slashes (both kinds) */

	for (p0 = path0; *p0; p0++)
		if (*p0 != '\\' && *p0 != '/') break;
	
	if (p0 != path0) memmove(path0, p0, strlen(p0) + 1);
	
	/* Remove trailing slashes (both kinds) */
	
	for (p0 = path0 + strlen(path0) - 1; p0 >= path0; p0--)
	{
		if (*p0 != '\\' && *p0 != '/') break;
		*p0 = '\0';
	}

	/* Ensure a leading slash */

	if (path0[0] != '/')
	{
		path1 = g_strconcat ("/", path0, NULL);
		g_free (path0);
		path0 = path1;
	}

	return path0;
}


struct
{
	int id;
	char *string;
	char *desc_sub;
}
file_system_id_map[] =
{
	{ 0, "auto" },
	{ 1, "ext2" },
	{ 2, "reiserfs" },
	{ 3, "ufs" },
	{ 4, "fat" },
	{ 5, "vfat" },
	{ 6, "fat32" },
	{ 7, "ntfs" },
	{ 0, NULL }
};


#if 0
static char *
file_system_id_to_string (int id)
{
	int i;
	
	for (i = 0; file_system_id_map [i].string; i++)
	{
		if (file_system_id_map [i].id == id)
			return(file_system_id_map [i].string);
	}
	
	return(NULL);
}
#endif


static char *
file_system_description_to_string (char *desc)
{
	int i, len = 0;
	char *s = NULL;

	/* Find the longest match */
  
	for (i = 0; file_system_id_map [i].string; i++)
	{
		if (xst_util_strcasestr (desc, file_system_id_map [i].string))
		{
			if (strlen(file_system_id_map [i].string) > len)
			{
				s = file_system_id_map [i].string;
				len = strlen(s);
			}
		}
	}
  
	return(s);
}


static int
file_system_string_to_id (char *string)
{
	int i;
  
	for (i = 0; file_system_id_map [i].string; i++)
	{
		if (!strcasecmp (file_system_id_map [i].string, string))
			return file_system_id_map [i].id;
	}
	
	return(0);
}


/* --- */


void
on_listed_toggled (GtkToggleButton *toggle, gpointer data)
{
	GtkCTree *ct;

	ct = GTK_CTREE (xst_dialog_get_widget (tool->main_dialog, "disks"));

	xst_ui_ctree_set_checkmark (ct, disks_node_selected, 2, gtk_toggle_button_get_active (toggle));
}


void
on_mounted_toggled (GtkToggleButton *toggle, gpointer data)
{
	GtkCTree *ct;

	ct = GTK_CTREE (xst_dialog_get_widget (tool->main_dialog, "disks"));

	xst_ui_ctree_set_checkmark (ct, disks_node_selected, 3, gtk_toggle_button_get_active (toggle));
}


void
on_disks_select_row (GtkCList *clist, gint row, gint column, GdkEvent *event, gpointer user_data)
{
	if (!gtk_ctree_node_get_selectable (GTK_CTREE (clist), gtk_ctree_node_nth (GTK_CTREE (clist), row)))
		return;
	
	disks_node_selected = gtk_ctree_node_nth (GTK_CTREE (clist), row);
	
	xst_dialog_freeze (tool->main_dialog);
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (xst_dialog_get_widget (tool->main_dialog, "listed_toggle")), xst_ui_ctree_get_checkmark (GTK_CTREE (clist), disks_node_selected, 2));
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (xst_dialog_get_widget (tool->main_dialog, "mounted_toggle")), xst_ui_ctree_get_checkmark (GTK_CTREE (clist), disks_node_selected, 3));

	gtk_widget_set_sensitive (xst_dialog_get_widget (tool->main_dialog, "listed_toggle"), TRUE);
	gtk_widget_set_sensitive (xst_dialog_get_widget (tool->main_dialog, "mounted_toggle"), TRUE);
	gtk_widget_set_sensitive (xst_dialog_get_widget (tool->main_dialog, "settings_launch"), TRUE);
	xst_dialog_thaw (tool->main_dialog);
}


void
on_disks_unselect_row (GtkCList *clist, gint row, gint column, GdkEvent *event, gpointer user_data)
{
	disks_node_selected = NULL;
	gtk_widget_set_sensitive (xst_dialog_get_widget (tool->main_dialog, "listed_toggle"), FALSE);
	gtk_widget_set_sensitive (xst_dialog_get_widget (tool->main_dialog, "mounted_toggle"), FALSE);
	gtk_widget_set_sensitive (xst_dialog_get_widget (tool->main_dialog, "settings_launch"), FALSE);
}


/* --- */


void
on_partition_type_selected (GtkMenuShell *menu, gpointer data)
{
	gchar *s0;

	gtk_label_get (GTK_LABEL
		       (GTK_BIN
			(xst_dialog_get_widget (tool->main_dialog, "partition_type"))->child), &s0);
#if 0
	if (strstr (s0, "NFS"))
	{
		gtk_widget_set_sensitive (xst_dialog_get_widget (tool->main_dialog, "import_user"), FALSE);
		gtk_widget_set_sensitive (xst_dialog_get_widget (tool->main_dialog, "import_password"), FALSE);
	}
	else
	{
		gtk_widget_set_sensitive (xst_dialog_get_widget (tool->main_dialog, "import_user"), TRUE);
		gtk_widget_set_sensitive (xst_dialog_get_widget (tool->main_dialog, "import_password"), TRUE);
	}
#endif
}


static void
partition_dialog_prepare_from_selected ()
{
	xmlNodePtr node = NULL, n0;
	GtkCTree *ctree;
	GtkCTreeNode *ctn;
	GList *glist = NULL;
	gboolean is_leaf;
	gchar *text;

	ctree = GTK_CTREE (xst_dialog_get_widget (tool->main_dialog, "disks"));

	if (disks_node_selected)
		node = gtk_ctree_node_get_row_data (ctree, disks_node_selected);

	if (node)
	{
		/* Get partition type from XML */

		gtk_option_menu_set_history (
			GTK_OPTION_MENU (xst_dialog_get_widget (tool->main_dialog, "partition_type")), 0);

		if ((n0 = xst_xml_element_find_first (node, "type")) &&
		    (text = xst_xml_element_get_content (n0)))
		{
			gtk_option_menu_set_history (
				GTK_OPTION_MENU (
					xst_dialog_get_widget (tool->main_dialog, "partition_type")),
				file_system_string_to_id(text));
		}

		/* Round up the known partitions and add them to the option list of
		 * the device combo */

		for (ctn = gtk_ctree_node_nth (ctree, 0); ctn; ctn = GTK_CTREE_NODE_NEXT (ctn))
		{
			gtk_ctree_get_node_info (ctree, ctn, NULL, NULL, NULL, NULL, NULL, NULL, &is_leaf, NULL);
			if (!is_leaf)
				continue;	/* Skip disk nodes */

			gtk_ctree_get_node_info (ctree, ctn, &text, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
			if (text)
				glist = g_list_append (glist, text);
		}

		if (glist)
			gtk_combo_set_popdown_strings (GTK_COMBO (xst_dialog_get_widget (tool->main_dialog, "partition_device")), glist);

		/* Set the active partition device from the currently selected item */
#if 0
		for (ctn = disks_node_selected; ctn; ctn = GTK_CTREE_NODE_PREV (ctn))
		{
			gtk_ctree_get_node_info (ctree, ctn, NULL, NULL, NULL, NULL, NULL, NULL, &is_leaf, NULL);
			if (is_leaf)
				break;	/* Found parent node */
		}
#endif
		gtk_ctree_get_node_info (ctree, disks_node_selected, &text, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
		if (text)
			gtk_entry_set_text (GTK_ENTRY (GTK_COMBO (xst_dialog_get_widget (tool->main_dialog, "partition_device"))->entry), text);

		/* Get mount point from ctree */

		text = NULL;
		gtk_editable_delete_text (GTK_EDITABLE (gnome_file_entry_gtk_entry (GNOME_FILE_ENTRY (xst_dialog_get_widget (tool->main_dialog, "partition_point")))), 0, -1);
		gtk_ctree_node_get_text (ctree, disks_node_selected, 7, &text);
		if (text)
			gtk_entry_set_text (GTK_ENTRY (gnome_file_entry_gtk_entry (GNOME_FILE_ENTRY (xst_dialog_get_widget (tool->main_dialog, "partition_point")))), text);
		else
			gtk_entry_set_text (GTK_ENTRY (gnome_file_entry_gtk_entry (GNOME_FILE_ENTRY (xst_dialog_get_widget (tool->main_dialog, "partition_point")))), "");
		
		/* Get check-for-errors status from ctree */
		
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (xst_dialog_get_widget (tool->main_dialog, "partition_check")), xst_ui_ctree_get_checkmark (ctree, disks_node_selected, 6));
	}
}


static void
partition_dialog_affect_selected ()
{
	xmlNodePtr node = NULL;
	GtkCTree *ctree;
	GtkCTreeNode *ctn;
	gboolean is_leaf;
	gchar *text, *s0, *s1;
	gchar *textrow [32];
	int i;

	ctree = GTK_CTREE (xst_dialog_get_widget (tool->main_dialog, "disks"));

	if (disks_node_selected)
		node = gtk_ctree_node_get_row_data (ctree, disks_node_selected);

	if (!node)
		return;		/* Should never happen */

	s0 = path_nice (g_strdup (gtk_entry_get_text (GTK_ENTRY (GTK_COMBO (xst_dialog_get_widget (tool->main_dialog, "partition_device"))->entry))));
	if (!strlen (s0))
	{
		/* If path is empty, don't apply any changes */
		
		g_free (s0);
		return;
	}
	
	s1 = tool_text_description_from_device (s0);
	g_free (s0);
	
	/* Check if the parent disk has changed. If it has, we need to move the ctree
	 * node. */

	for (ctn = disks_node_selected; ctn; ctn = GTK_CTREE_NODE_PREV (ctn))
	{
		gtk_ctree_get_node_info (ctree, ctn, NULL, NULL, NULL, NULL, NULL, NULL, &is_leaf, NULL);
		if (!is_leaf)
			break;	/* Found parent node */
	}

	if (ctn)
	{
		gtk_ctree_get_node_info (ctree, ctn, &s0, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

		if (s0 && s1)
		{
			if (strcasecmp (s0, s1))
			{
				GtkCTreeNode *oldhost = ctn;

				/* We need to move. First, see if the target disk is already in our
				 * tree */

				for (ctn = gtk_ctree_node_nth (ctree, 0); ctn; ctn = GTK_CTREE_NODE_NEXT (ctn))
				{
					gtk_ctree_get_node_info (ctree, ctn, NULL, NULL, NULL, NULL, NULL, NULL, &is_leaf, NULL);
					if (is_leaf)
						continue;	/* Skip import nodes */

					gtk_ctree_get_node_info (ctree, ctn, &text, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
					if (text && !strcasecmp (text, s1))
						break;	/* Found it */
				}

				for (i = 0; i < 32; i++)
					textrow [i] = NULL;

				if (!ctn)
				{
					/* We didn't find a matching disk entry; create it */

					textrow [0] = s1;
					ctn = gtk_ctree_insert_node (ctree, NULL, NULL, textrow, 0, disk_pixmap, disk_mask, disk_pixmap, disk_mask, FALSE, TRUE);

					gtk_ctree_node_set_selectable (ctree, ctn, FALSE);
				}

				gtk_ctree_move (ctree, disks_node_selected, ctn, GTK_CTREE_NODE_NEXT (ctn));

				/* Check if old parent disk is out of children; remove it if so */

				ctn = GTK_CTREE_NODE_NEXT (oldhost);
				if (!ctn)
					gtk_ctree_remove_node (ctree, oldhost);
				else
				{
					gtk_ctree_get_node_info (ctree, ctn, NULL, NULL, NULL, NULL, NULL, NULL, &is_leaf, NULL);
					if (!is_leaf)
						gtk_ctree_remove_node (ctree, oldhost);
				}
			}
		}
	}

	g_free (s1);

	/* Set device in ctree */

	s0 = path_nice (g_strdup (gtk_entry_get_text (GTK_ENTRY (GTK_COMBO (xst_dialog_get_widget (tool->main_dialog, "partition_device"))->entry))));
	gtk_ctree_node_set_pixtext (ctree, disks_node_selected, 0, s0, 0, partition_pixmap, partition_mask);
	g_free (s0);
	
	/* Set mount point in ctree */
	
	s0 = path_nice (g_strdup (gtk_entry_get_text (GTK_ENTRY (gnome_file_entry_gtk_entry (GNOME_FILE_ENTRY (xst_dialog_get_widget (tool->main_dialog, "partition_point")))))));
	gtk_ctree_node_set_text (ctree, disks_node_selected, 7, s0);
	g_free (s0);

	/* Set type in XML */

	gtk_label_get (GTK_LABEL (GTK_BIN (xst_dialog_get_widget (tool->main_dialog, "partition_type"))->child), &s0);
	s0 = file_system_description_to_string (s0);
	
	xst_xml_element_add_with_content (node, "type", s0);
	
	/* Set check-for-error status in ctree */
	
	xst_ui_ctree_set_checkmark (ctree, disks_node_selected, 6,
			     gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(xst_dialog_get_widget (tool->main_dialog, "partition_check"))));
}


void
on_partition_settings_launched (GtkButton * button, gpointer data)
{
	GnomeDialog *dialog;

	partition_dialog_prepare_from_selected ();
	dialog = GNOME_DIALOG (xst_dialog_get_widget (tool->main_dialog, "partition_settings_dialog"));
	
	switch (gnome_dialog_run_and_close (dialog))
	{
		case 0:
		        partition_dialog_affect_selected ();
		        break;

		case 1:
		case -1:
		default:
			break;
	}
}


static void
partition_dialog_prepare_new ()
{
	xmlNodePtr node = NULL;
	GtkCTree *ctree;
	GList *glist = NULL;

	ctree = GTK_CTREE (xst_dialog_get_widget (tool->main_dialog, "disks"));

	if (disks_node_selected)
		node = gtk_ctree_node_get_row_data (ctree, disks_node_selected);

	/* Set default type */

	gtk_option_menu_set_history (GTK_OPTION_MENU (xst_dialog_get_widget (tool->main_dialog, "partition_type")), 0);	/* SMB */

	/* Clear the partition list - no use in adding a partition that's already
	 * listed. */
  
	glist = g_list_append (glist, "");
	gtk_combo_set_popdown_strings (GTK_COMBO (xst_dialog_get_widget (tool->main_dialog, "partition_device")), glist);

	/* Set selected device to none */

	gtk_entry_set_text (GTK_ENTRY (GTK_COMBO (xst_dialog_get_widget (tool->main_dialog, "partition_device"))->entry), "");

	/* Set mount point to none */

	gtk_editable_delete_text (GTK_EDITABLE (gnome_file_entry_gtk_entry (GNOME_FILE_ENTRY (xst_dialog_get_widget (tool->main_dialog, "partition_point")))), 0, -1);

	/* Set check toggle to on */

	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON(xst_dialog_get_widget (tool->main_dialog,"partition_check")), TRUE);
}


static void
partition_dialog_affect_new ()
{
	xmlNodePtr disk_node, node = NULL;
	GtkCTree *ctree;
	GtkCTreeNode *ctn;
	gboolean is_leaf;
	gchar *text;
	gchar *textrow [32];
	char *partition_device, *disk_device, *disk_name;
	int i;

	ctree = GTK_CTREE (xst_dialog_get_widget (tool->main_dialog, "disks"));

	partition_device = g_strdup (gtk_entry_get_text (GTK_ENTRY (GTK_COMBO (xst_dialog_get_widget (tool->main_dialog, "partition_device"))->entry)));
	if (!strlen (partition_device))
	{
		/* If device name is empty, do nothing */
		
		g_free (partition_device);
		return;
	}

	disk_name = tool_text_description_from_device (partition_device);
	disk_device = g_strdup (partition_device);

	if (strlen (disk_device) > 3)
	{
		while (*(disk_device + strlen (disk_device) - 1) - '0' < 10 &&
		       *(disk_device + strlen (disk_device) - 1) - '0' >= 0)
			*(disk_device + strlen (disk_device) - 1) = '\0';
	}

	/* Find or make a suitable disk node to attach to.
	 First, see if the target disk is already in our tree */

	for (ctn = gtk_ctree_node_nth (ctree, 0); ctn; ctn = GTK_CTREE_NODE_NEXT (ctn))
	{
		gtk_ctree_get_node_info (ctree, ctn, NULL, NULL, NULL, NULL, NULL, NULL, &is_leaf, NULL);
		if (is_leaf)
			continue;	/* Skip partition nodes */

		gtk_ctree_get_node_info (ctree, ctn, &text, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
		if (text && !strcasecmp (text, disk_name))
			break;	/* Found it */
	}
	
	for (i = 0; i < 32; i++)
		textrow [i] = NULL;

	if (!ctn)
	{
		/* We didn't find a matching host entry; create it */

		textrow [0] = disk_name;
		ctn = gtk_ctree_insert_node (ctree, NULL, NULL, textrow, 6, disk_pixmap, disk_mask, disk_pixmap, disk_mask, FALSE, TRUE);
		gtk_ctree_node_set_selectable (ctree, ctn, FALSE);

		disk_node = xst_xml_element_add (xst_xml_doc_get_root (tool->config), "disk");
		xst_xml_element_add_with_content (disk_node, "device", disk_device);
		if (!disk_node)
			return;  /* Should never happen */
		gtk_ctree_node_set_row_data (ctree, ctn, disk_node);
	}
	
	disk_node = gtk_ctree_node_get_row_data (ctree, ctn);

	/* Pull strings from dialog */

	textrow [0] = partition_device;
	textrow [7] = path_nice (g_strdup (gtk_entry_get_text (GTK_ENTRY (gnome_file_entry_gtk_entry (GNOME_FILE_ENTRY (xst_dialog_get_widget (tool->main_dialog, "partition_point")))))));

	/* Create the partition's ctree node */

	ctn = gtk_ctree_insert_node (ctree, ctn, NULL, textrow, 0, partition_pixmap, partition_mask, partition_pixmap, partition_mask, TRUE, TRUE);

	xst_ui_ctree_set_checkmark (ctree, ctn, 1, FALSE);
	xst_ui_ctree_set_checkmark (ctree, ctn, 2, TRUE);
	xst_ui_ctree_set_checkmark (ctree, ctn, 3, TRUE);
	xst_ui_ctree_set_checkmark (ctree, ctn, 5, FALSE);
	xst_ui_ctree_set_checkmark (ctree, ctn, 6, gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(xst_dialog_get_widget (tool->main_dialog,"partition_check"))));

	/* Create partition's XML node */

	node = xst_xml_element_add (disk_node, "partition");
	if (!node)
		return;  /* Should never happen */

	xst_xml_element_add_with_content (node, "device", partition_device);
	xst_xml_element_add_with_content (node, "point", textrow[7]);

	/* Set type in XML */

	gtk_label_get (GTK_LABEL (GTK_BIN (xst_dialog_get_widget (tool->main_dialog, "partition_type"))->child), &text);
	text = file_system_description_to_string(text);

	xst_xml_element_add_with_content(node, "type", text);

	/* Attach XML node to ctree node */

	gtk_ctree_node_set_row_data (ctree, ctn, node);

	g_free (textrow [7]);
	g_free (partition_device);
	g_free (disk_name);
}


void
on_partition_new_launched (GtkButton * button, gpointer data)
{
	GnomeDialog *dialog;

	partition_dialog_prepare_new ();
	dialog = GNOME_DIALOG (xst_dialog_get_widget (tool->main_dialog, "partition_settings_dialog"));

	switch (gnome_dialog_run_and_close (dialog))
	{
		case 0:
			partition_dialog_affect_new ();
			break;

		case 1:
		case -1:
		default:
			break;
	}
}
