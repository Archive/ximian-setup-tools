/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/* Functions for transferring information between XML tree and UI */

#include <ctype.h>
#include <gnome.h>
#include <gnome-xml/tree.h>
#include <gnome-xml/parser.h>
#include <glade/glade.h>
#include <gdk-pixbuf/gdk-pixbuf.h>

#include "xst.h"

#include "transfer.h"
#include "pixmaps.h"

extern XstTool *tool;

/* Requires the specific configuration of CTree we're using. This code is by no
 * means generic. */

#define DISKS_CTREE_COLS_NUM 8

static void
transfer_partitions_ctree_xml_to_gui (xmlNode *root)
{
	xmlNodePtr disk_node, partition_node, n0, n1, n2, n3, n4;
	GtkCTree *ct;
	GtkCTreeNode *ctn0, *ctn1;
	char *disktext [DISKS_CTREE_COLS_NUM];
	char *disk_device, *disk_size;
	char *str;
	int i;

	/* Initialize */

	ct = GTK_CTREE (xst_dialog_get_widget (tool->main_dialog, "disks"));

	/* Populate */

	for (disk_node = xst_xml_element_find_first (root, "disk"); disk_node;
	     disk_node = xst_xml_element_find_next (disk_node, "disk"))
	{
		n0 = xst_xml_element_find_first (disk_node, "device");
		n1 = xst_xml_element_find_first (disk_node, "size");

		if (!(n0 && (disk_device = xst_xml_element_get_content (n0))))
			continue;
    
		if ((str = tool_text_description_from_device (disk_device)))
			disk_device = str;
    
		if (n1)
		{
			disk_size = xst_xml_element_get_content (n1);
			if ((str = tool_text_size_from_kb (atoi (disk_size))))
				disk_size = str;
		}
		else disk_size = NULL;

		/* Create the disk device entry */

		ctn0 = NULL;

		for (i = 0; i < DISKS_CTREE_COLS_NUM; i++) disktext [i] = NULL;
		disktext[0] = disk_device;
		disktext[4] = disk_size;

		ctn0 = gtk_ctree_insert_node (ct, NULL, NULL, disktext, 6, disk_pixmap, disk_mask, disk_pixmap, disk_mask, FALSE, TRUE);
		gtk_ctree_node_set_selectable (ct, ctn0, FALSE);
		gtk_ctree_node_set_row_data (ct, ctn0, disk_node);

		/* Create partition entries for this disk */

		for (partition_node = xst_xml_element_find_first (disk_node, "partition");
		     partition_node;
		     partition_node = xst_xml_element_find_next (partition_node, "partition"))
		{
			gchar *partition_row_text [DISKS_CTREE_COLS_NUM];

			for (i = 0; i < DISKS_CTREE_COLS_NUM; i++)
				partition_row_text [i] = NULL;

			n2 = xst_xml_element_find_first (partition_node, "device");
			n3 = xst_xml_element_find_first (partition_node, "point");
			n4 = xst_xml_element_find_first (partition_node, "size");
			
			if (!n2)
				continue;
			
			partition_row_text [0] = xst_xml_element_get_content (n2);
			if (n3)
				partition_row_text [7] = xst_xml_element_get_content (n3);
			if (n4)
			{
				partition_row_text [4] = xst_xml_element_get_content (n4);
				if ((str = tool_text_size_from_kb (atoi (partition_row_text [4]))))
					partition_row_text [4] = str;
			}
			
			ctn1 = gtk_ctree_insert_node (ct, ctn0, NULL, partition_row_text, 0, partition_pixmap, partition_mask, partition_pixmap, partition_mask, TRUE, TRUE);
			
			gtk_ctree_node_set_row_data (ct, ctn1, partition_node);
			xst_ui_ctree_set_checkmark (ct, ctn1, 1, xst_xml_element_get_state (partition_node, "detected"));
			xst_ui_ctree_set_checkmark (ct, ctn1, 2, xst_xml_element_get_state (partition_node, "listed"));
			xst_ui_ctree_set_checkmark (ct, ctn1, 3, xst_xml_element_get_state (partition_node, "mounted"));
			xst_ui_ctree_set_checkmark (ct, ctn1, 5, xst_xml_element_get_state (partition_node, "bootable"));
			xst_ui_ctree_set_checkmark (ct, ctn1, 6, xst_xml_element_get_state (partition_node, "integritycheck"));

			for (i = 0; i < DISKS_CTREE_COLS_NUM; i++)
				g_free (partition_row_text [i]);
		}
		
		g_free (disk_device);
	}
}


static void
transfer_partitions_ctree_gui_to_xml (xmlNodePtr root)
{
	GtkCTree *ct;
	GtkCTreeNode *ctn;
	xmlNodePtr disk_node = NULL, partition_node;
	gchar *text, *disk_device;
	gboolean is_leaf;
	GdkPixmap *pixmap;
	GdkBitmap *mask;
	guint8 spacing;

	ct = GTK_CTREE (xst_dialog_get_widget (tool->main_dialog, "disks"));

	for (ctn = gtk_ctree_node_nth (ct, 0); ctn; ctn = GTK_CTREE_NODE_NEXT (ctn))
	{
		/* If disk node, save or create disk XML node for coming entries */

		gtk_ctree_get_node_info (ct, ctn, NULL, NULL, NULL, NULL, NULL, NULL, &is_leaf, NULL);
		if (!is_leaf)
		{
			if (!gtk_ctree_node_get_pixtext (ct, ctn, 0, &disk_device, &spacing, &pixmap, &mask))
				disk_device = NULL;
			
			disk_node = gtk_ctree_node_get_row_data (ct, ctn);
			if (!disk_node && !disk_device)
				continue;  /* Illegal */
			if (!disk_node)
			{
				/* Create an XML disk node if it's not already there */
				
				disk_node = xst_xml_element_add (root, "disk");
				xst_xml_element_add_with_content (disk_node, "device", disk_device);
			}

			continue;
		}

		/* If we don't have a disk node, we ignore the partition */
		
		if (!disk_node)
			continue;
		
		partition_node = gtk_ctree_node_get_row_data (ct, ctn);
		
		if (!partition_node)
		{
			/* Create a new XML partition node under current disk node */
			
			partition_node = xst_xml_element_add (disk_node, "partition");
		}
		
		xst_xml_element_set_state (partition_node, "listed",
				       xst_ui_ctree_get_checkmark(ct, ctn, 2));
		xst_xml_element_set_state (partition_node, "mounted",
				       xst_ui_ctree_get_checkmark(ct, ctn, 3));
		xst_xml_element_set_state (partition_node, "bootable",
				       xst_ui_ctree_get_checkmark(ct, ctn, 5));
		xst_xml_element_set_state (partition_node, "integritycheck",
				       xst_ui_ctree_get_checkmark(ct, ctn, 6));

		gtk_ctree_node_get_pixtext (ct, ctn, 0, &text, &spacing, &pixmap, &mask);
		xst_xml_element_add_with_content (partition_node, "device", text);
		
		if (gtk_ctree_node_get_text (ct, ctn, 7, &text)
		    && strlen (text))
			xst_xml_element_add_with_content (partition_node, "point", text);
		else
			xst_xml_element_destroy_children_by_name (partition_node, "point");
	}
}


void
transfer_xml_to_gui (XstTool *tool, gpointer data)
{
	transfer_partitions_ctree_xml_to_gui (xst_xml_doc_get_root (tool->config));
}


void
transfer_gui_to_xml (XstTool *tool, gpointer data)
{
	transfer_partitions_ctree_gui_to_xml (xst_xml_doc_get_root (tool->config));
}
