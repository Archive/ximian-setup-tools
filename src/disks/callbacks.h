/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
#include <gnome.h>
#include <gnome-xml/tree.h>


extern xmlDocPtr doc;


void
on_imports_tree_select_row          (GtkCList        *clist,
                                        gint             row,
                                        gint             column,
                                        GdkEvent        *event,
                                        gpointer         user_data);

void
on_imports_tree_unselect_row        (GtkCList        *clist,
                                        gint             row,
                                        gint             column,
                                        GdkEvent        *event,
                                        gpointer         user_data);

void on_listed_toggled (GtkToggleButton *toggle, gpointer data);
void on_mounted_toggled (GtkToggleButton *toggle, gpointer data);

void on_help_clicked(GtkButton *button, gpointer data);
void on_ok_clicked(GtkButton *button, gpointer data);
void on_apply_clicked(GtkButton *button, gpointer data);
void on_cancel_clicked(GtkButton *button, gpointer data);

void on_partition_settings_launched(GtkButton *button, gpointer data);
void on_partition_new_launched(GtkButton *button, gpointer data);

void on_partition_type_selected(GtkMenuShell *menushell, gpointer user_data);

void on_disks_unselect_row (GtkCList * clist, gint row, gint column, GdkEvent * event, gpointer user_data);
void on_disks_select_row (GtkCList * clist, gint row, gint column, GdkEvent * event, gpointer user_data);
