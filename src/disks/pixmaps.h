/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
#include <gnome.h>
#include <gdk-pixbuf/gdk-pixbuf.h>

extern GdkPixmap *disk_pixmap, *partition_pixmap;
extern GdkBitmap *disk_mask, *partition_mask;

void pixmaps_init(void);
