/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
#include <gnome.h>
#include <gdk-pixbuf/gdk-pixbuf.h>
#include "pixmaps.h"

#include "disk.xpm"
#include "partition.xpm"


GdkPixmap *disk_pixmap = NULL, *partition_pixmap = NULL;
GdkBitmap *disk_mask = NULL, *partition_mask = NULL;


void
pixmaps_init ()
{
	GdkPixbuf *pixbuf;

	if (!disk_pixmap)
	{
		pixbuf = gdk_pixbuf_new_from_xpm_data ((const char **) disk_xpm);
		gdk_pixbuf_render_pixmap_and_mask (pixbuf, &disk_pixmap, &disk_mask, 1);
	}

	if (!partition_pixmap)
	{
		pixbuf = gdk_pixbuf_new_from_xpm_data ((const char **) partition_xpm);
		gdk_pixbuf_render_pixmap_and_mask (pixbuf, &partition_pixmap, &partition_mask, 1);
	}
}
