/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
#include <gnome.h>
#include <glade/glade.h>

#ifndef XST_UI_H
#define XST_UI_H

GtkWidget *xst_ui_list_get_list_item_by_name      (GtkList *list, const gchar *label);
void       xst_ui_combo_remove_by_label           (GtkCombo *combo, const gchar *label);
GtkWidget *xst_ui_image_widget_create             (gchar *name, gchar *string1, gchar *string2,
						   gint int1, gint int2);
void       xst_ui_image_set_pix                   (GtkWidget *widget, gchar *filename);
GtkWidget *xst_ui_image_widget_get                (GladeXML *gui, gchar *name);

gint       xst_ui_option_menu_get_selected_row    (GtkOptionMenu *option_menu);
gchar     *xst_ui_option_menu_get_selected_string (GtkOptionMenu *option_menu);
void       xst_ui_option_menu_set_selected_string (GtkOptionMenu *option_menu, const gchar *string);
GtkWidget *xst_ui_option_menu_add_string          (GtkOptionMenu *option_menu, const gchar *string);
void       xst_ui_option_menu_remove_string       (GtkOptionMenu *option_menu, const gchar *string);
void       xst_ui_option_menu_clear               (GtkOptionMenu *option_menu);

void       xst_ui_clist_set_checkmark             (GtkCList *clist, gint row, gint column, gboolean state);
gboolean   xst_ui_clist_get_checkmark             (GtkCList *clist, gint row, gint column);

void       xst_ui_ctree_set_checkmark             (GtkCTree *ctree, GtkCTreeNode *node,
						   gint column, gboolean state);
gboolean   xst_ui_ctree_get_checkmark             (GtkCTree *ctree, GtkCTreeNode *node, gint column);
gchar     *xst_ui_load_etspec                     (const gchar *common_path, const gchar *name);
void       xst_ui_entry_set_text                  (void *entry, const gchar *str);
gboolean   xst_ui_logout_dialog                   (const gchar *message);

void       xst_ui_text_view_add_text              (GtkTextView *view, const gchar *text);
gchar     *xst_ui_text_view_get_text              (GtkTextView *view);
void       xst_ui_text_view_clear                 (GtkTextView *view);

#endif /* XST_UI_H */
