/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/* Copyright (C) 2001 Ximian, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Israel Escalante <israel@ximian.com> based on code by :
 *          Arturo Espinosa <arturo@ximian.com>
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <gnome.h>
#include <glade/glade.h>

#include "xst.h"
#include "ishare.h"

XST_TOOL_MAKE_TYPE(ishare, Ishare)

static void
xst_ishare_tool_type_init (XstIshareTool *tool)
{
	memset (&tool->input,  0, sizeof (XstIshareInput));
	memset (&tool->output, 0, sizeof (XstIshareOutput));
}

XstTool *
ishare_tool_new (void)
{
	XstTool *tool; 
	
	tool = XST_TOOL (gtk_type_new (XST_TYPE_ISHARE_TOOL));

	return tool;
}
	
