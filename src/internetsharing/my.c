/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/* Copyright (C) 2001 Ximian, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Author: Arturo Espinosa <arturo@ximian.com>
 *
 */

#include <gnome.h>
#include <glade/glade.h>

#include "my.h"

GtkWidget * 
my_get_widget (GladeXML *glade, const gchar *name)
{
	GtkWidget *widget;
		
	g_return_val_if_fail (glade != NULL, NULL);
	
	widget = glade_xml_get_widget (glade, name);
	
	if (!widget)
		g_warning ( "my_get_widget: Unexistent widget %s ", name);
	
	return widget;
}

