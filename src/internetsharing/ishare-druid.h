/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/* Copyright (C) 2000-2001 Ximian, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Israel Escalante <israel@ximian.com> and Arturo Espinosa <arturo@ximian.com>.
 */

#ifndef __ISHARE_DRUID_H__
#define __ISHARE_DRUID_H__

#include <gnome.h>
#include <glade/glade.h>

#include "xst.h"
#include "ishare.h"

typedef enum  {
	FIRST_PAGE              = 0,
	LOCAL_NETWORK_CREATE    = 1,
	LOCAL_NETWORK_ACTIVATE  = 2,
	FIREWALL_OVERWRITE      = 3,
	DHCP_SERVICE            = 4,
	DHCP_REQUIREMENTS       = 5,
	DHCP_OVERWRITE          = 6,
	LAST_PAGE               = 7
} IshareDruidPage;

typedef struct _XstIshareDruid IshareDruid;
struct _XstIshareDruid {

        GladeXML      *glade;
        XstIshareTool *tool;
	
        GtkWidget  *win;
        GnomeDruid *druid;

	gboolean        launch_network;
        gboolean        error_state;
        IshareDruidPage current_page;

	/* Error */
	const gchar *error_message;
};

extern IshareDruid *ishare_druid_new         (XstIshareTool *tool);
extern void         ishare_druid_show        (IshareDruid *druid);
extern void         ishare_druid_free        (IshareDruid *druid);
extern void         ishare_druid_gui_to_xml  (XstTool *tool, gpointer data);
extern void         ishare_druid_xml_to_gui  (XstTool *tool, gpointer data);
    
#endif /*  __ISHARE_DRUID_H__  */

