/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/* Copyright (C) 2000-2001 Ximian, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Israel Escalante <israel@ximian.com> and Arturo Espinosa <arturo@ximian.com>.
 */

#ifndef __ISHARE_H__
#define __ISHARE_H__

#include <gnome.h>

#include "xst.h"

/* Structures for XML transfer. */

typedef struct _XstIshareInput     XstIshareInput;
typedef struct _XstIshareOutput    XstIshareOutput;

struct _XstIshareInput {

	gboolean  firewall_installed_flag;      /* I receive TRUE in this flag if firewall is installed */ 
	gboolean  firewall_configured_flag;     /* This flag detects if i have a previous configuration of
						 * masquerading rules */
	gboolean  lan_exists_flag;              /* This flag indicates if local_network is installed if it's
						 * not installed i will launch network-admin or user will do
						 * this manually */
	gboolean  lan_active_flag;              /* This flag indicates if local network is active */
	gboolean  dhcp_installed_flag;          /* This flag indicates if i have the requirements to configure
						 * DHCP service */
	gboolean  dhcp_configured_flag;         /* This flag will be set if i have a previous configuration
						 * of DHCP service */
};


struct _XstIshareOutput {

	gboolean  firewall_activate;             /* If user wants to activate the firewall rules */
	gboolean  firewall_overwrite;            /* If user wants to overwrite the firewall configuration */
	gboolean  lan_activate;                  /* If user wants to activate local network */
	gboolean  dhcp_activate;                 /* User wants dhcp service */
	gboolean  dhcp_overwrite;                /* Overwrite dhcp configuration */
};

/* Our XstTool inherited class */

#define XST_TYPE_ISHARE_TOOL            (xst_ishare_tool_get_type ())
#define XST_ISHARE_TOOL(obj)            (GTK_CHECK_CAST ((obj), XST_TYPE_ISHARE_TOOL, XstIshareTool))
#define XST_ISHARE_TOOL_CLASS(class)    (GTK_CHECK_CLASS_CAST ((class), XST_TYPE_ISHARE_TOOL, XstIshareToolClass))
#define XST_IS_ISHARE_TOOL(obj)         (GTK_CHECK_TYPE ((obj), XST_TYPE_ISHARE_TOOL))
#define XST_IS_ISHARE_TOOL_CLASS(class) (GTK_CHECK_CLASS_TYPE ((class), XST_TYPE_ISHARE_TOOL))

typedef struct _XstIshareTool      XstIshareTool;
typedef struct _XstIshareToolClass XstIshareToolClass;

struct _XstIshareTool {
	XstTool         tool;
	
	XstIshareInput  input;
	XstIshareOutput output;
};

struct _XstIshareToolClass {

	XstToolClass parent_class;
};

GtkType  xst_ishare_tool_get_type (void);
XstTool *ishare_tool_new (void);

#endif /*  __ISHARE_H__  */
