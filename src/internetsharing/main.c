/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/* Copyright (C) 2000-2001 Ximian, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Hans Petter Jansson <hpj@ximian.com>.
 *          Arturo Espinosa <arturo@ximian.com>
 */


#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <gnome.h>
#include <gnome-xml/tree.h>
#include <gnome-xml/parser.h>
#include <glade/glade.h>

#include "xst.h"

#include "transfer.h"
#include "callbacks.h"
#include "ishare.h"
#include "ishare-druid.h"

int
main (int argc, char *argv[])
{
	XstTool *tool;

	XstWidgetPolicy policies[] = {
		/* Name           Basic                        Advanced                   Root   User */
		{ "enable_frame", XST_WIDGET_MODE_HIDDEN,      XST_WIDGET_MODE_SENSITIVE, TRUE,  FALSE },
		{ NULL },
	};

	XstDialogSignal signals[] = {
		{ "configure_button",              "clicked",         on_configure_button_clicked },
		{ "disable_button",                "clicked",         on_disable_button_clicked   },
		{ NULL }
	};

	xst_init ("internetsharing-admin", argc, argv, NULL);

	tool = ishare_tool_new ();
	xst_tool_construct (tool, "internetsharing", _("Internet Sharing"));
	xst_tool_set_xml_funcs (tool, transfer_xml_to_gui, transfer_gui_to_xml, NULL);
/*	        xst_dialog_enable_complexity   (tool->main_dialog);*/
	xst_dialog_set_widget_policies (tool->main_dialog, policies);
	xst_dialog_connect_signals (tool->main_dialog, signals);

	xst_tool_main (tool, FALSE);
	
	return 0;
}

