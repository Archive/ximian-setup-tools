/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/* Copyright (C) 2001 Ximian, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Israel Escalante <israel@ximian.com> based on code by :
 *          Arturo Espinosa <arturo@ximian.com>
 */


#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <string.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/wait.h>

#include <gnome.h>
#include <glade/glade.h>

#include "xst.h"
#include "ishare.h"
#include "ishare-druid.h"
#include "my.h"

#define ISHARE_DRUID_DEFAULT_FINISH_MESSAGE N_("Congratulations! Your computer will now be\n" \
					       "configured to share its internet connection.")

static void ishare_druid_xml_lan_to_gui (xmlNodePtr root, XstIshareInput *input);

static void 
ishare_druid_exit (IshareDruid *druid)
{
	g_return_if_fail (druid != NULL);

	gtk_widget_hide (GTK_WIDGET (druid->win));

	gtk_main_quit ();
}

static void 
ishare_druid_on_window_delete_event (GtkWidget *w, gpointer data)
{
	IshareDruid *druid = (IshareDruid *) data;

	druid->error_state = TRUE;
	ishare_druid_exit (druid);
}

static void 
ishare_druid_on_cancel (GtkWidget *w, gpointer data)
{
	IshareDruid *druid = data;

	druid->error_state = TRUE;
	ishare_druid_exit (druid);
}

static void
ishare_druid_set_finish_message (IshareDruid *druid, gchar *message)
{
	GnomeDruidPageFinish *last_page;
	
	last_page = g_list_nth_data (druid->druid->children, LAST_PAGE);
	
	gnome_druid_page_finish_set_text (last_page, message);
}

static void
ishare_druid_set_default_finish_message (IshareDruid *druid)
{
	ishare_druid_set_finish_message (druid, ISHARE_DRUID_DEFAULT_FINISH_MESSAGE);
}

static void
ishare_druid_set_page_finish (IshareDruid *druid)
{
	GnomeDruidPage       *page;
	
	page = g_list_nth_data (druid->druid->children, LAST_PAGE);
	gnome_druid_set_page (druid->druid, page);
}

static void
ishare_druid_set_error (IshareDruid *druid, gchar *message)
{
	ishare_druid_set_finish_message (druid, message);

	druid->error_state  = TRUE;
	druid->current_page = LAST_PAGE;
}
	
static void
ishare_druid_reget_interfaces (XstIshareTool *tool)
{
	xmlDocPtr  doc;
	xmlNodePtr getroot, root, node, node_tmp;

	doc = xst_tool_run_get_directive (XST_TOOL (tool), _("Retrieving network interface configuration"),
					  "get_interfaces", NULL);

	g_return_if_fail (doc != NULL);

	getroot = xst_xml_doc_get_root (doc);
	root    = xst_xml_doc_get_root (XST_TOOL (tool)->config);

	xst_xml_element_destroy_children_by_name (root, "interface");

	for (node = xst_xml_element_find_first (getroot, "interface"); node;
	     node = node_tmp) {
		node_tmp = xst_xml_element_find_next  (node, "interface");
		xst_xml_element_reparent (node, root);
	}

	xst_xml_doc_destroy (doc);

	ishare_druid_xml_lan_to_gui (root, &tool->input);
}

static gint
ishare_druid_wait_child (gpointer data)
{
	pid_t pid = (pid_t) data;
	pid_t res;

	res = waitpid (pid, NULL, WNOHANG);
	if (res != 0) {
		gtk_main_quit ();
		return FALSE;
	}

	return TRUE;
}

static void
ishare_druid_run_network_tool (IshareDruid *druid)
{
	guint  idle;
	pid_t  pid;

	pid = fork ();
	if (pid) {
		g_return_if_fail (pid != -1);

		gtk_widget_set_sensitive (druid->win, FALSE);
		idle = gtk_idle_add (ishare_druid_wait_child, (gpointer) pid);
		gtk_main ();
		gtk_idle_remove (idle);
		ishare_druid_reget_interfaces (druid->tool);
		gtk_widget_set_sensitive (druid->win, TRUE);
	} else
		execl (FRONTEND_DIR "/bin/network-admin", FRONTEND_DIR "/bin/network-admin", NULL);
}

/**
 * ishare_druid_set_page:
 * @druid: 
 * 
 * This function takes care of setting the next page that should be displayed in the druid
 * it modified druid->current_page.
 **/

static void
ishare_druid_set_page_next (IshareDruid *druid)
{
	XstIshareInput  *input;
	XstIshareOutput *output;

	input  = &druid->tool->input;
	output = &druid->tool->output;
	
	switch (druid->current_page) {
	case FIRST_PAGE:
		if (input->firewall_installed_flag) {
			if (input->lan_exists_flag) {
				if (input->lan_active_flag) {
					if (!input->firewall_configured_flag) {
						output->firewall_overwrite = TRUE;
						druid->current_page = DHCP_SERVICE;
					} else
						druid->current_page = FIREWALL_OVERWRITE;
				} else
					druid->current_page = LOCAL_NETWORK_ACTIVATE;
			} else {
				druid->current_page = LOCAL_NETWORK_CREATE;
				druid->launch_network = TRUE;
			}
		} else {
			ishare_druid_set_error (druid,
						_("No firewall support was found, or it was incomplete."));
			ishare_druid_set_page_finish (druid);
		}
		
		break;

	case LOCAL_NETWORK_CREATE:
		if (druid->launch_network) {
			ishare_druid_run_network_tool (druid);
			druid->launch_network = FALSE;
			druid->current_page = FIRST_PAGE;
			ishare_druid_set_page_next (druid);
		}
		break;
	case LOCAL_NETWORK_ACTIVATE:
		if (input->firewall_configured_flag)
			druid->current_page = FIREWALL_OVERWRITE;
		else {
			output->firewall_overwrite = TRUE;
			druid->current_page = DHCP_SERVICE;
		}
		break;
	case FIREWALL_OVERWRITE:
		druid->current_page = DHCP_SERVICE;
		break;					
	case DHCP_SERVICE:
		if (output->dhcp_activate) {
			if (input->dhcp_installed_flag) {
				if (input->dhcp_configured_flag)
					druid->current_page = DHCP_OVERWRITE;
				else {
					output->dhcp_overwrite = TRUE;
					druid->current_page = LAST_PAGE;
				}
			} else
				druid->current_page = DHCP_REQUIREMENTS;
		} else
			druid->current_page = LAST_PAGE;
		break;
	case DHCP_REQUIREMENTS:
		break;
	case DHCP_OVERWRITE:
		if (output->dhcp_activate &&
		    input->dhcp_installed_flag &&
		    input->dhcp_configured_flag)
			druid->current_page = LAST_PAGE;
		break;
	case LAST_PAGE:
		output->firewall_activate = TRUE;
		break;
	default:
	} 	
} 

/**
 * ishare_druid_set_page_back:
 * @druid: 
 * 
 * This function takes care of setting the back page that should be displayed in the druid
 * it modified druid->current_page.
 **/ 
static void
ishare_druid_set_page_back (IshareDruid *druid)
{
	XstIshareInput  *input;
	
	input  = &druid->tool->input;
	
	switch (druid->current_page) {
	case LOCAL_NETWORK_CREATE:
		druid->current_page = FIRST_PAGE;
		break;

	case LOCAL_NETWORK_ACTIVATE:
		if (input->lan_exists_flag)
			druid->current_page = FIRST_PAGE;
		else
			druid->current_page = LOCAL_NETWORK_CREATE;
		break;

	case FIREWALL_OVERWRITE:
		if (input->lan_exists_flag) {
			if (input->lan_active_flag) 
				druid->current_page = FIRST_PAGE;
			else 
				druid->current_page = LOCAL_NETWORK_ACTIVATE;
		} else
			druid->current_page = LOCAL_NETWORK_CREATE;
		break;

	case DHCP_SERVICE:
		if (!input->firewall_configured_flag) {
			if (input->lan_exists_flag) {
				if (input->lan_active_flag) 
					druid->current_page = FIRST_PAGE;
				else 
					druid->current_page = LOCAL_NETWORK_ACTIVATE;
			} else
				druid->current_page = LOCAL_NETWORK_CREATE;
		} else
			druid->current_page = FIREWALL_OVERWRITE;
		
		break;
	case DHCP_REQUIREMENTS:
	case DHCP_OVERWRITE:
		druid->current_page = DHCP_SERVICE;
		break;
	case LAST_PAGE:
		if (input->lan_exists_flag) {
			if (input->dhcp_installed_flag) {
				if (input->dhcp_configured_flag)
					druid->current_page = DHCP_SERVICE;
				else
					druid->current_page = DHCP_OVERWRITE;
			} else
				druid->current_page = DHCP_REQUIREMENTS;
		} else
			druid->current_page = LOCAL_NETWORK_CREATE;
		break;
	case FIRST_PAGE:
		g_warning ("can't go back from the first page!");
		break;
	default:
		/*druid->current_page = LAST_PAGE; */
		g_warning ("default case: shouldn't be here");
	}
}


static GnomeDruidPage *
ishare_druid_get_page (IshareDruid *druid, gint page_num)
{
	GnomeDruidPage *page;
	
	page = g_list_nth_data (druid->druid->children, page_num);

	return page;
}

static gboolean
ishare_druid_on_page_next (GtkWidget *w, gpointer arg1, gpointer data)
{
	IshareDruid *druid = (IshareDruid *) data;
	GnomeDruidPage *page;

	ishare_druid_set_page_next (druid);   
	page = ishare_druid_get_page (druid, druid->current_page);

	gnome_druid_set_page (druid->druid, page);
		
	return TRUE;
}


static gboolean
ishare_druid_on_page_back (GtkWidget *w, gpointer arg1, gpointer data)
{
	IshareDruid *druid = (IshareDruid *) data;
	GnomeDruidPage *page;
	
	ishare_druid_set_page_back (druid); 
	page = ishare_druid_get_page (druid, druid->current_page);
	
	gnome_druid_set_page (druid->druid, page);
	
	return TRUE;
}

static gboolean
ishare_druid_on_page_finish (GtkWidget *w, gpointer arg1, gpointer data)
{
	IshareDruid *druid = data;

	ishare_druid_exit (druid);
	return FALSE;
}

static void
ishare_druid_on_network_create_radio_toggled (GtkWidget *w, gpointer data)
{
	IshareDruid *druid = (IshareDruid *) data;
	gboolean     active;

     	active = gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (w));
	
	if (active) {
		druid->current_page = LOCAL_NETWORK_CREATE;
		ishare_druid_set_default_finish_message (druid);
		
		druid->launch_network = TRUE;
	} else
		ishare_druid_set_error (druid,
					_("You can configure a local network interface\n"
					  "manually and come back later to finish\n"
					  "the internet sharing configuration."));
}

static void
ishare_druid_on_radio_toggled (GtkWidget *w, gpointer data)
{
	XstIshareInput  *input;
	XstIshareOutput *output;
	IshareDruid *druid = (IshareDruid *) data;
	gboolean state_button;

	input  = &druid->tool->input;
	output = &druid->tool->output;

     	state_button = gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (w));

	switch (druid->current_page)
	{
	case LOCAL_NETWORK_CREATE:
		g_warning ("Shouldn't be here: should be in on_network_create_radio_toggled.");
		break;
	case LOCAL_NETWORK_ACTIVATE:
		output->lan_activate = state_button;
		input->lan_active_flag = output->lan_activate;
		break;
	case FIREWALL_OVERWRITE:
		output->firewall_overwrite = state_button;
		break;
	case DHCP_SERVICE:
		output->dhcp_activate = state_button;
		break;
	case DHCP_REQUIREMENTS:
		if (state_button) {
			output->dhcp_activate  = FALSE;
			output->dhcp_overwrite = FALSE;
		} else
			ishare_druid_set_error (druid,
						_("You may install a DHCP server into your\n"
						  "system and come back later to finish\n"
						  "the internet sharing configuration."));
		break;
	case DHCP_OVERWRITE:
		output->dhcp_overwrite = !state_button;
		break;
	case FIRST_PAGE:
	case LAST_PAGE:
		break;
	default:
		g_warning ("ishare_druid_on_button_toggled: shouldn't be here.");
	}
}

static void
ishare_druid_connect_signals (IshareDruid *druid, XstDialogSignal *signals)
{
          gint i;
	  gchar *s;
	  
	  g_return_if_fail (druid != NULL);

	  for (i = 0; signals[i].widget; i++) {
		  GtkWidget *widget;

		  s = g_strdup_printf ("ishare_admin_druid_%s", signals[i].widget);
		  widget = my_get_widget (druid->glade, s);
		  g_free (s);
		  if (!widget)
			  continue;
		  gtk_signal_connect_after (GTK_OBJECT (widget), signals[i].signal_name,
					    GTK_SIGNAL_FUNC (signals[i].func),
					    druid);

	  }
}

IshareDruid *
ishare_druid_new (XstIshareTool *tool)
{
	IshareDruid *druid;
	XstDialogSignal signals[] = {
		{ "window" ,                   "delete_event", ishare_druid_on_window_delete_event },
		{ "druid" ,                    "cancel",       ishare_druid_on_cancel },
		{ "page0" ,                    "next",         GTK_SIGNAL_FUNC (ishare_druid_on_page_next) },
		{ "page1" ,                    "next",         GTK_SIGNAL_FUNC (ishare_druid_on_page_next) },
		{ "page2" ,                    "next",         GTK_SIGNAL_FUNC (ishare_druid_on_page_next) },
		{ "page3" ,                    "next",         GTK_SIGNAL_FUNC (ishare_druid_on_page_next) },
		{ "page4" ,                    "next",         GTK_SIGNAL_FUNC (ishare_druid_on_page_next) },
		{ "page5" ,                    "next",         GTK_SIGNAL_FUNC (ishare_druid_on_page_next) },
		{ "page6" ,                    "next",         GTK_SIGNAL_FUNC (ishare_druid_on_page_next) },
		{ "page1" ,                    "back",         GTK_SIGNAL_FUNC (ishare_druid_on_page_back) },
		{ "page2" ,                    "back",         GTK_SIGNAL_FUNC (ishare_druid_on_page_back) },
		{ "page3" ,                    "back",         GTK_SIGNAL_FUNC (ishare_druid_on_page_back) },
		{ "page4" ,                    "back",         GTK_SIGNAL_FUNC (ishare_druid_on_page_back) },
		{ "page5" ,                    "back",         GTK_SIGNAL_FUNC (ishare_druid_on_page_back) },
		{ "page6" ,                    "back",         GTK_SIGNAL_FUNC (ishare_druid_on_page_back) },
		{ "lastpage" ,                 "back",         GTK_SIGNAL_FUNC (ishare_druid_on_page_back) },
		{ "lastpage" ,                 "finish",       GTK_SIGNAL_FUNC (ishare_druid_on_page_finish) },

		{ "launchnetworktool",         "toggled",      ishare_druid_on_network_create_radio_toggled },
		
		{ "activatelocalnetwork",       "toggled",      ishare_druid_on_radio_toggled },
		{ "firewall_configure_keep",    "toggled",      ishare_druid_on_radio_toggled },
		{ "dhcp_configure_yes",         "toggled",      ishare_druid_on_radio_toggled },
		{ "dhcp_requirement_nevermind", "toggled",      ishare_druid_on_radio_toggled },
		{ "dhcp_configure_keep",        "toggled",      ishare_druid_on_radio_toggled },
		{ NULL }
		
	};
	gchar *glade_file;

	glade_file   = g_concat_dir_and_file (INTERFACES_DIR, "internetsharing.glade");
	druid        = g_new0 (IshareDruid, 1);
	druid->glade = glade_xml_new (glade_file, "ishare_admin_druid_window");
	
	g_return_val_if_fail (druid->glade != NULL, NULL);
	g_free (glade_file);
	
	druid->tool           = tool;
	druid->error_state    = FALSE;
	druid->launch_network = TRUE;
	druid->current_page   = 0;
	druid->win            = my_get_widget (druid->glade, "ishare_admin_druid_window"); 
	druid->druid          = GNOME_DRUID (my_get_widget (druid->glade, "ishare_admin_druid_druid"));
	
	ishare_druid_connect_signals (druid, signals);
	  
	return druid;
}

void 
ishare_druid_free (IshareDruid *druid)
{
	gtk_object_unref (GTK_OBJECT (druid->glade));
	g_free (druid);
}
	  
void 
ishare_druid_show (IshareDruid *druid)
{
          g_return_if_fail (druid != NULL);

          gtk_widget_show_all (druid->win);
}

static void
ishare_druid_gui_to_xml_lan (xmlNodePtr root, XstIshareOutput *output)
{
	xmlNodePtr interface, fwrules, dhcp;
	gchar *bootproto, *dev;
	gint cmp, i;
	gchar *landevs[] = {"eth", "wvlan", "plip", "irlan", NULL};

	fwrules = xst_xml_element_find_first (root, "fwrules");
	xst_xml_element_destroy_children_by_name (fwrules, "landev");
	dhcp = xst_xml_element_find_first (root, "dhcp");
	xst_xml_element_destroy_children_by_name (dhcp, "dev");

	for (interface = xst_xml_element_find_first (root, "interface");
	     interface; interface = xst_xml_element_find_next (interface, "interface")) {
		bootproto = xst_xml_get_child_content (interface, "bootproto");
		cmp = bootproto? strcmp (bootproto, "none"): 1;
		g_free (bootproto);
		if (!cmp) {
			dev = xst_xml_get_child_content (interface, "dev");
			for (i = 0; landevs[i]; i++)
				if (strstr (dev, landevs[i])) {
					if (output->lan_activate)
						xst_xml_element_set_boolean (interface, "enabled", TRUE);
					xst_xml_element_add_with_content (fwrules, "landev", dev);
					xst_xml_element_add_with_content (dhcp,    "dev",    dev);
					break;
				}
			g_free (dev);
		}
	}
}

void
ishare_druid_gui_to_xml (XstTool *tool, gpointer data)
{
	XstIshareTool  *ishare;
	XstIshareOutput *output;

	xmlNodePtr root;
	xmlNodePtr fwrules;
	xmlNodePtr dhcp;

	ishare = XST_ISHARE_TOOL (tool);
	root = xst_xml_doc_get_root (tool->config);
	fwrules   = xst_xml_element_find_first (root, "fwrules");
	dhcp      = xst_xml_element_find_first (root, "dhcp");

	output = &ishare->output;

	if (!fwrules)
		fwrules = xst_xml_element_add (root, "fwrules");
	xst_xml_element_set_boolean (fwrules, "active",    output->firewall_activate);
	xst_xml_element_set_boolean (fwrules, "overwrite", output->firewall_overwrite);
	
	if (!dhcp)
		dhcp = xst_xml_element_add (root, "dhcp");
	xst_xml_element_set_boolean (dhcp, "active",    output->dhcp_activate);
	xst_xml_element_set_boolean (dhcp, "overwrite", output->dhcp_overwrite);
	
	ishare_druid_gui_to_xml_lan (root, output);
}

static void
ishare_druid_xml_lan_to_gui (xmlNodePtr root, XstIshareInput *input)
{
	xmlNodePtr interface;
	gchar *bootproto, *dev;
	gint cmp, i;
	gchar *landevs[] = {"eth", "wvlan", "plip", "irlan", NULL};

	input->lan_exists_flag = input->lan_active_flag = FALSE;

	for (interface = xst_xml_element_find_first (root, "interface");
	     interface; interface = xst_xml_element_find_next (interface, "interface")) {
		bootproto = xst_xml_get_child_content (interface, "bootproto");
		cmp = bootproto? strcmp (bootproto, "none"): 1;
		g_free (bootproto);
		if (!cmp) {
			dev = xst_xml_get_child_content (interface, "dev");
			for (i = 0; landevs[i]; i++)
				if (strstr (dev, landevs[i])) {
					g_free (dev);
					input->lan_exists_flag = TRUE;
					input->lan_active_flag =
						xst_xml_element_get_boolean (interface, "enabled");
					return;
				}
			g_free (dev);
		}
	}
}

void
ishare_druid_xml_to_gui (XstTool *tool, gpointer data)
{
	XstIshareTool  *ishare;
	XstIshareInput *input;
		
	xmlNodePtr root;
	xmlNodePtr fwrules;
	xmlNodePtr dhcp;

	ishare = XST_ISHARE_TOOL (tool);
	root = xst_xml_doc_get_root (tool->config);
	fwrules   = xst_xml_element_find_first (root, "fwrules");
	dhcp      = xst_xml_element_find_first (root, "dhcp");

	input = &ishare->input;

	if (fwrules) {
		input->firewall_installed_flag =
			(xst_xml_element_get_boolean (fwrules, "forwarding") &&
			 xst_xml_element_get_boolean (fwrules, "kerneltool") &&
			 xst_xml_element_get_boolean (fwrules, "masquerading") &&
			 xst_xml_element_get_boolean (fwrules, "tools"));
		
		input->firewall_configured_flag =
			(xst_xml_element_get_boolean (fwrules, "configured") &&
			 xst_xml_element_get_boolean (fwrules, "active"));
	} else {
		g_warning ("ishare_druid_xml_to_gui: fwrules == NULL");
	}

	if (dhcp) {
		input->dhcp_installed_flag =
			xst_xml_element_get_boolean (dhcp, "installed");
		input->dhcp_configured_flag =
			(xst_xml_element_get_boolean (dhcp, "configured") &&
			 xst_xml_element_get_boolean (dhcp, "active"));
	} else {
		g_warning ("ishare_druid_xml_to_gui: dhcp == NULL");
	}

	ishare_druid_xml_lan_to_gui (root, input);
}

