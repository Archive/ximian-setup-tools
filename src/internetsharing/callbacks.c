/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/* 
 * Copyright (C) 2000-2001 Ximian, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Hans Petter Jansson <hpj@ximian.com>
 *          Arturo Espinosa <arturo@ximian.com>
 *          Jacob Berkman <jacob@ximian.com>
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <ctype.h>

#include <gnome.h>

#include "xst.h"
#include "callbacks.h"
#include "transfer.h"
#include "ishare.h"
#include "ishare-druid.h"
#include "my.h"

void
on_configure_button_clicked (GtkWidget *button, XstDialog *dialog)
{
	IshareDruid *druid;
	XstDialogComplexity complexity;
	XstIshareTool *tool = XST_ISHARE_TOOL (dialog->tool);

	g_return_if_fail (XST_IS_ISHARE_TOOL (tool));
	g_return_if_fail (GTK_IS_BUTTON (button));

	complexity = xst_dialog_get_complexity (dialog);
	
	if (complexity == XST_DIALOG_BASIC) {
		XstIshareInput  input;
		XstIshareOutput output;

		input  = tool->input;
		output = tool->output;

		tool->output.firewall_activate  = TRUE;
		tool->output.firewall_overwrite = TRUE;
		tool->output.lan_activate       = TRUE;
		tool->output.dhcp_activate      = TRUE;
		tool->output.dhcp_overwrite     = TRUE;
		
		druid = ishare_druid_new (tool);
		g_return_if_fail (druid != NULL);
		
		ishare_druid_show (druid);
		gtk_main ();
		if (druid->error_state) {
			tool->input  = input;
			tool->output = output;
		} else {
			xst_dialog_modify (dialog);
			gtk_widget_set_sensitive (my_get_widget (dialog->gui, "disable_button"), TRUE);
		}
				
		ishare_druid_free (druid);
	}
}

void
on_disable_button_clicked (GtkWidget *button, XstDialog *dialog)
{
	XstIshareTool *tool = XST_ISHARE_TOOL (dialog->tool);

	tool->output.firewall_activate = FALSE;
	gtk_widget_set_sensitive (button, FALSE);
	xst_dialog_modify (dialog);
}
