2002-04-09  Tambet Ingo  <tambet@ximian.com>

	* Ported to gnome2. 

2002-02-11  Seth Nickell  <snickell@stanford.edu>

	reviewed by: <delete if not using a buddy>

	* boot.desktop.in.in:

2002-02-10  Chema Celorio  <chema@celorio.com>

	* Port to the GNOME 2.0 platform

2002-02-08  Chema Celorio  <chema@celorio.com>

	* configure.in: 0.12.0 here we go
2001-12-11  Israel Escalante <israel@ximian.com>

	* Release 0.10.0.

2001-12-13  Arturo Espinosa Aldama  <arturo@ximian.com>

	* boot-image.c (boot_image_valid_name_chars): Verify this information
	with the backend: this is platform-dependent stuff.
	(boot_image_valid_label): use valid_name_chars now.
	(boot_image_valid_chars): warning squished.

2001-12-12  Arturo Espinosa Aldama  <arturo@ximian.com>

	* e-table.c (boot_table_add): Create a new key for the added node.
	(boot_table_get_new_key): Generates the key.
	(boot_table_add): add the node after we have searched for the key.

2001-12-11  Israel Escalante  <israel@ximian.com>

	* transfer.c (transfer_globals_xml_to_gui): Small modification
	 to compile /src/boot.	

2001-12-07  Arturo Espinosa Aldama  <arturo@ximian.com>

	* transfer.c (transfer_globals_xml_to_gui): deal with prompt
	as a 0/1 xml node.

2001-12-02  Chema Celorio  <chema@celorio.com>

	* Makefile.am: move more stuff into CommonMakefile

2001-11-26  Arturo Espinosa Aldama  <arturo@ximian.com>

	* e-table.c (boot_value_type): XstImageType -> XstEntryType.
	(boot_value_set_type): same.

2001-09-21  Tambet Ingo  <tambet@ximian.com>

	* e-table.c (table_create): Small modification to get new GAL
	compilant.

2001-09-17  Tambet Ingo  <tambet@ximian.com>

	* main.c: tool still has to be a global variable.

2001-08-21  Arturo Espinosa Aldama  <arturo@ximian.com>

	* callbacks.c (on_main_dialog_update_complexity): moved from
	main.c (was update_complexity). There are still some other
	functions using nothing but globals laying in callbacks.c (the
	helpers).

	* main.c (connect_signals): Put the signals table here and pass
	the tool structure, instead of using global.
	(main): Moved stuff around, removed globals.

	* callbacks.c (callbacks_conf_read_failed_hook): Free the generated
	text for the error message.

2001-08-17  Arturo Espinosa Aldama  <arturo@ximian.com>

	* main.c (main): add the hook to the boot_conf_read_failed report.

	* callbacks.c (callbacks_actions_set_sensitive): renamed.
	(callbacks_buttons_set_visibility): same.
	(callbacks_conf_read_failed_hook): report hook that shows error message.

2001-07-30  Tambet Ingo  <tambet@ximian.com>

	* e-table.c: XstPartitionType -> XstImageType.
	(table_create): Removed debug message.

	* callbacks.c (on_boot_delete_clicked): Check that user won't delete last image.

2001-07-26  Tambet Ingo  <tambet@ximian.com>

	* e-table.c (boot_table_update_state): Made more robust.
	(table_create): Ditto.

2001-07-25  Tambet Ingo  <tambet@ximian.com>

	* e-table.c (boot_row_count): Return 0 if data model isn't inited yet.
	(table_create): Just create the table, don't try to populate it (no data yet).
	(table_populate): Renamed function name, call model_changed to let etable know
	it has some data.

	* boot-image.c (boot_image_valid_chars): Implement.
	(boot_image_label_exists): Implement.
	(boot_image_file_exists): Implement.
	(boot_image_valid_label): Implement.
	(boot_image_valid_device): Implement.
	(boot_image_valid_root): Implement.
	(boot_image_check): Implement.

	* boot-druid.c: Added checks, fixes bugs #3112, and #4892.

2001-07-23  Tambet Ingo  <tambet@ximian.com>

	* e-table.c (boot_value_set_image): Set 'image' or 'other' correctly,
	fixes bug #5040.

2001-07-24  Zbigniew Chyla  <cyba@gnome.pl>

	* boot-image.c
	(type_labels_list): Use translated version of the label.
	Added missing #include <config.h>.

	* boot-settings.c (boot_settings_gui_setup): Use translated version of
	the label.

2001-06-22  Tambet Ingo  <tambet@ximian.com>

	* boot-settings.c (on_boot_add_clicked): Give error if MAX_COUNT is reached.

	* boot-image.c (boot_image_new): Check if MAX_COUNT isn't reached, fixes bug #3449
	(boot_image_count): Implement.

	* e-table.c (boot_value_device): Implement, split from (boot_value_image).
	Fixes bugs #3446 and #3447.
	(boot_value_image): More robust.

	* boot-image.c (boot_image_get_by_node): Get image right in case of non-linux.

	* e-table.c (boot_is_linux): Implement to have on common check.

2001-06-06  Tambet Ingo  <tambet@ximian.com>

	* boot-druid.c: Finished. It's working, needs extra checkings though.
	(construct): Allow Expand and Fill for reparented widgets. 

2001-06-05  Tambet Ingo  <tambet@ximian.com>

	* Makefile.am (boot_admin_SOURCES): Add new files to build.

	* boot-settings.c: Almost a rewrite to support addition of images and to
	make current code reusable.

	* boot-image.c: New file.

	* boot-image-editor.c: New file.

	* boot-druid.c: New file (not finished yet).

2001-05-31  Tambet Ingo  <tambet@ximian.com>

	* boot-settings.c: Use xst_ui_entry_set_text instead of my_gtk_entry_set_text.

	(boot_settings_prepare): Removed some obsolete stuff.

	* callbacks.c: Fixed style.

	* transfer.c (transfer_globals_gui_to_xml): Fixed typo.

	* main.c (set_access_sensitivity): Removed unused (and empty) function,
	fixing bug #3128.

2001-05-28  Tambet Ingo  <tambet@ximian.com>

	* e-table.c (create_table): Use xst_ui_load_etspec.

2001-05-26  Carlos Perell� Mar�n <carlos@gnome-db.org>

	* e-table.c: Extracted the ETableSpecification to an external file.

2001-05-25  Tambet Ingo  <tambet@ximian.com>

	* transfer.c (transfer_globals_xml_to_gui): Fixes bug 3007.

2001-05-24  Arturo Espinosa Aldama  <arturo@ximian.com>

	* 0.5 RELEASE

2001-05-23  Tambet Ingo  <tambet@ximian.com>

	* e-table.c (boot_value_set_append): Destroy xml node only if needed.
	(boot_value_default): Implement.
	(boot_value_type): use (boot_value_default).
	(boot_value_set_label): Set new default if default label is changed.

2001-05-21  Carlos Perell� Mar�n <carlos@gnome-db.org>

	* e-table.c: Marked to translate the ETableSpecification labels and other strings.

2001-05-18  Hans Petter Jansson  <hpj@ximian.com>

	* *.pam: Modified PAM configuration so that invoking as root works.

2001-05-17  Hans Petter Jansson  <hpj@ximian.com>

	* *.pam: Modified PAM configuration so that console-helper will ask
	  for root auth.

2001-05-10  Chema Celorio  <chema@celorio.com>

	* main.c (main): upd to the new init method.

2001-05-09  Arturo Espinosa Aldama  <arturo@ximian.com>

	* 0.4 RELEASE

2001-04-27  Arturo Espinosa Aldama  <arturo@ximian.com>

	* e-table.h: changed order for COL enum.

	* e-table.c (boot_value_type): Changed type "Unknown" to "Other".
	(create_extras): justify left, not center.
	Changed column order, to Name|Type, which is more expected.

	* callbacks.c (on_boot_prompt_toggled): change sensitivity on
	the label to make it mor apparent that the feature is not activated.
	Not too pretty.

2001-04-26  Chema Celorio  <chema@celorio.com>

	* Makefile.am: add $(srcdir) to security.in

2001-04-02  Tambet Ingo  <tambet@ximian.com>

	* e-table.c (table_structure_change): New. Save new ETable state.

	* callbacks.c (buttons_set_visibility): New.

	* main.c (main): set initial visibility of buttons which depends
	on complexity.

2001-04-01  Tambet Ingo  <tambet@ximian.com>

	* e-table.c (boot_table_update_state): get ETable state from conf.
	(create_table): Ditto.

2001-03-30  Tambet Ingo  <tambet@ximian.com>

	* Makefile.am: Add gconf cflags.

2001-02-28  Hans Petter Jansson  <hpj@ximian.com>

	* Makefile.am: Disable control-center entry.

2001-02-27  Chema Celorio  <chema@celorio.com>

	* Makefile.am (EXTRA_DIST): distcheck fixes

2001-02-26  Tambet Ingo  <tambet@ximian.com>

	* transfer.c (transfer_check_data): New function.
	(transfer_check_default): Check if "default" tag is valid, remove it if not.

	* e-table.c (boot_value_set_append): delete node if new val is empty, quote if neccesary.
	(boot_value_append): Don't show surrounding quotes.

	* transfer.c (transfer_globals_xml_to_gui): Don't discard timeout val even if prompt
	is not specified.

2001-02-23  Tambet Ingo  <tambet@ximian.com>

	* e-table.c (boot_value_set_append): New function.
	(boot_value_append): New function.

	* *.c : Renamed our "propertary" tag.

2001-02-19  Tambet Ingo  <tambet@ximian.com>

	* e-table.c (boot_value_label_default): Removed.
	(boot_value_type): Add (default) to default row.
	(boot_value_image):
	(boot_value_dev): Deal with extra spaces.

2001-02-17  Chema Celorio  <chema@celorio.com>

	* main.c (main): pass NULL as options

2001-02-16  Tambet Ingo  <tambet@ximian.com>

	* e-table.c (boot_value_dev): Add two spaces before value.
	(boot_value_image): Same.

	* callbacks.c (on_boot_default_clicked): Update ETable, modify dialog

	* boot-settings.c (on_boot_add_clicked): Show "Not Implemented".

	* e-table.c (boot_value_at): Show (default).

2001-02-13  Tambet Ingo  <tambet@ximian.com>

	* boot-settings.c: New file.
	* *.[ch] Started using new galde files.

2001-02-09  Tambet Ingo  <tambet@ximian.com>

	* callbacks.c (boot_settings_dialog_complexity): New function.
	(actions_set_sensitive): Hide advanced stuff when needed.

	* e-table.c: Wrote saving functions.

	* callbacks.c: Enabled "Add" and "Settings" buttons.

2001-02-08  Tambet Ingo  <tambet@ximian.com>

	* main.c (update_complexity): update ETable.
	(main): Enable complexity.
	(connect_signals): Connect signals using xst_dialog_connect_signals.

	* e-table.c: Split ETableSpec to spec and state.
	(boot_value_image): Show ONLY images.
	(boot_value_dev): New function, show devices.
	(boot_table_update_state): New function. Change ETable state.

