/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/* Copyright 2000-2001 Ximian, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Tambet Ingo <tambet@ximian.com>.
 */

/* Functions for transferring information between XML tree and UI */

#include <gnome.h>
#include <gnome-xml/tree.h>
#include <gnome-xml/parser.h>
#include <glade/glade.h>
#include "xst.h"

#include "transfer.h"
#include "callbacks.h"

extern XstTool *tool;

void transfer_config_saved (xmlNodePtr root);

static void
transfer_string_clist_xml_to_gui (xmlNodePtr root)
{
	gint i;
	gint row = -1;
	GtkCList *clist;
	gchar *text[SWAP_AREAS_CLIST_COLS_NUM];
	xmlNodePtr node, n0, n1, n2;
	
	/* Intitalize */
	
	for (i = 0; i < SWAP_AREAS_CLIST_COLS_NUM; i++)
		text[i] = NULL;
	clist = GTK_CLIST (xst_dialog_get_widget (tool->main_dialog, "memory_list"));

	/* Populate */

	for (node = xst_xml_element_find_first (root, "swapdev");
			node;
		       	node = xst_xml_element_find_next (node, "swapdev"))
	{
		for (i = 0; i < SWAP_AREAS_CLIST_COLS_NUM; i++)
		{
			if (text[i])
			{
				g_free (text[i]);
				text[i] = NULL;
			}
		}

		n0 = xst_xml_element_find_first (node, "device");
		n1 = xst_xml_element_find_first (node, "priority");
		n2 = xst_xml_element_find_first (node, "size");

		if (!(n0 && (text[1] = xst_xml_element_get_content (n0))))
			continue;
		if (!(n1 && (text[2] = xst_xml_element_get_content (n1))))
		{
			g_free (n0);
			continue;
		}
		
		if (!(n2 && (text[3] = xst_xml_element_get_content (n2))))
			continue;
		
		row = gtk_clist_append (clist, text);
		gtk_clist_set_row_data (clist, row, node);
		xst_ui_clist_set_checkmark (clist, row, 0, xst_xml_element_get_state (node, "enabled"));
		gtk_clist_select_row (clist, row, 0);
	}

	/* Set GTKCList's justifaction to center and autosize it. */
	for (i = 0; i < SWAP_AREAS_CLIST_COLS_NUM; i++)
		gtk_clist_set_column_justification (clist, i, GTK_JUSTIFY_CENTER);

	gtk_clist_columns_autosize (clist);

	if (row == -1)
		memory_actions_set_sensitive (FALSE);
}

static void
transfer_partitions_xml_to_gui (xmlNodePtr root)
{
	gint i, row;
	GtkCList *clist;
	gchar *text[2];
	xmlNodePtr node, n0, n1;
	
	/* Intitalize */
	
	for (i = 0; i < 2; i++)
		text[i] = NULL;
	clist = GTK_CLIST (xst_dialog_get_widget (tool->main_dialog, "browse_list"));

	/* Populate */

	for (node = xst_xml_element_find_first (root, "partition");
			node;
		       	node = xst_xml_element_find_next (node, "partition"))
	{
		for (i = 0; i < 2; i++)
		{
			if (text[i])
			{
				g_free (text[i]);
				text[i] = NULL;
			}
		}

		n0 = xst_xml_element_find_first (node, "device");
		n1 = xst_xml_element_find_first (node, "size");

		if (!(n0 && (text[0] = xst_xml_element_get_content (n0))))
			continue;
		if (!(n1 && (text[1] = xst_xml_element_get_content (n1))))
		{
			g_free (n0);
			continue;
		}
		
		row = gtk_clist_append (clist, text);
		gtk_clist_set_row_data (clist, row, node);
		gtk_clist_select_row (clist, row, 0);
	}
}

static void
transfer_memory_xml_to_gui (xmlNodePtr root)
{
	xmlNodePtr node;
	gchar *txt;
	
	node = xst_xml_element_find_first (root, "memsize");
	
	if (node)
	{
		txt = xst_xml_element_get_content (node);
		gtk_label_set_text (GTK_LABEL (xst_dialog_get_widget (tool->main_dialog, "memory_total_label")), txt);
	}
	
	node = xst_xml_element_find_first (root, "swapsize");

	if (node)
	{
		txt = xst_xml_element_get_content (node);
		gtk_label_set_text (GTK_LABEL (xst_dialog_get_widget (tool->main_dialog, "memory_swap_label")), txt);
	}

}

void
transfer_config_saved (xmlNodePtr root)
{
	xmlNodePtr node;
	GtkCList *clist;
	gint row;

	clist = GTK_CLIST (xst_dialog_get_widget (tool->main_dialog, "memory_list"));
	for (row = 0;; row++)
	{
		node = gtk_clist_get_row_data (clist, row);
		if (!node)
			break;
		xst_xml_element_set_state (node, "isnew", FALSE);
	}
}


void
transfer_xml_to_gui (XstTool *tool, gpointer data)
{
	xmlNode *root;

	root = xst_xml_doc_get_root (tool->config);

	transfer_string_clist_xml_to_gui (root);
	transfer_memory_xml_to_gui (root);
	transfer_partitions_xml_to_gui (root);
}

void
transfer_gui_to_xml (XstTool *tool, gpointer data)
{
}
