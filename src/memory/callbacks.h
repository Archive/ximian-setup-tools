/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
#include <gnome.h>
#include <gnome-xml/tree.h>

extern void
on_cancel_clicked (GtkButton *button, gpointer user_data);

extern void
on_apply_clicked (GtkButton *button, gpointer user_data);

extern void
create_cb (GtkButton *button, gpointer user_data);

extern void
select_row_cb (GtkCList *clist, gint row, gint col, GdkEventButton *event, gpointer user_data);

extern void
unselect_row_cb (GtkCList *clist, gint row, gint col, GdkEventButton *event, gpointer user_data);

extern void
settings_cb (GtkButton *button, gpointer user_data);

extern void
remove_cb (GtkButton *button, gpointer user_data);

/* Create dialog */

extern void
on_create_dialog_show (GtkWidget *widget, gpointer user_data);
extern void
on_create_cancel_clicked (GtkButton *button, gpointer user_data);
extern void
on_create_dialog_delete_event (GtkWidget *widget, gpointer user_data);
extern void
on_create_ok_clicked (GtkButton *button, gpointer user_data);

extern void
on_browse_button_clicked (GtkButton *button, gpointer user_data);

extern void
on_create_type_clicked (GtkButton *button, gpointer user_data);

extern void
on_create_priority_toggled (GtkWidget *widget, gpointer user_data);

extern void
file_cancel_cb (GtkButton *button, gpointer user_data);

extern void
file_ok_cb (GtkButton *button, gpointer user_data);

/* Browse dialog */

extern void
on_browse_cancel_clicked (GtkButton *button, gpointer user_data);

extern void
on_browse_ok_clicked (GtkButton *button, gpointer user_data);

/* Helper functions */

void memory_actions_set_sensitive (gboolean state);

