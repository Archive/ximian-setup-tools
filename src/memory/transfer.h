/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
#include <glade/glade.h>
#include <gnome-xml/tree.h>
#include <gnome-xml/parser.h>

#define SWAP_AREAS_CLIST_COLS_NUM 4

void transfer_config_saved(xmlNodePtr root);
void transfer_xml_to_gui(XstTool *tool, gpointer data);
void transfer_gui_to_xml(XstTool *tool, gpointer data);

