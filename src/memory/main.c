/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/* Copyright (C) 2000-2001 Ximian, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Tambet Ingo <tambet@ximian.com>.
 */


#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <gtk/gtk.h>
#include <gnome.h>
#include <gnome-xml/tree.h>
#include <gnome-xml/parser.h>
#include <gdk-pixbuf/gdk-pixbuf.h>
#include <glade/glade.h>

#include "xst.h"

#include "transfer.h"
#include "callbacks.h"

XstTool *tool = NULL;

static XstDialogSignal signals[] = {
	{ "browse_ok",        "clicked",        xst_dialog_modify_cb },
	{ "browse_ok",        "clicked",        on_browse_ok_clicked },
	{ "browse_list",      "select_row",     select_row_cb },
	{ "file_ok",          "clicked",        file_ok_cb },
	{ "file_cancel",      "clicked",        file_cancel_cb },
	{ "settings_ok",      "clicked",        xst_dialog_modify_cb },
	{ "create_ok",        "clicked",        on_create_ok_clicked },
	{ "create_cancel",    "clicked",        on_create_cancel_clicked },
	{ "create_type",      "clicked",        on_create_type_clicked },
	{ "create_browse",    "clicked",        on_browse_button_clicked },
	{ "memory_list",      "select_row",     select_row_cb },
	{ "memory_list",      "unselect_row",   unselect_row_cb },
	{ "memory_create",    "clicked",        create_cb },
	{ "memory_remove",    "clicked",        remove_cb },
	{ "memory_settings",  "clicked",        settings_cb },
	{ NULL }
};

static void 
set_access_sensitivity (void)
{
	gchar *access_no[] = {"memory_create", "memory_remove", "create_ok", "settings_ok", NULL};
	gint i;

	for (i = 0; access_no[i]; i++)
		gtk_widget_set_sensitive (xst_dialog_get_widget (tool->main_dialog, access_no[i]), xst_tool_get_access (tool));
}

static void
create_type_cb (GtkWidget *widget, gpointer user_data)
{
	gtk_signal_connect (GTK_OBJECT (widget), "activate", on_create_type_clicked, NULL);
}



static void
connect_cb (void)
{
	GtkContainer *w0;

	w0 = GTK_CONTAINER (GTK_OPTION_MENU (xst_dialog_get_widget (tool->main_dialog, "create_type"))->menu);
	gtk_container_foreach(w0, create_type_cb, NULL);
}


int
main (int argc, char *argv[])
{
	xst_init ("memory-admin", argc, argv, NULL);
	tool = xst_tool_new ();
	xst_tool_construct (tool, "memory", _("Swap Management"));

	xst_tool_set_xml_funcs (tool,  transfer_xml_to_gui, transfer_gui_to_xml, NULL);
	xst_dialog_connect_signals (tool->main_dialog, signals);

	/* Add callbacks to create_type (because You can't add callbacks to OptionMenu's 
	 * childs in glade */

	connect_cb ();

	/* Set accessibility. */
	set_access_sensitivity ();

	xst_tool_main (tool, FALSE);

	return 0;
}
