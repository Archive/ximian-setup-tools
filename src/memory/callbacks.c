/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/* Copyright (C) 2000-2001 Ximian, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Tambet Ingo <tambet@ximian.com>.
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <gnome.h>
#include <sys/stat.h>
#include <unistd.h>
#include "xst.h"

#include "callbacks.h"
#include "transfer.h"

extern XstTool *tool;

static gint reply;

/* Prototypes */

static void settings_affect (void);
static void settings_prepare (void);
static gboolean create_affect (void);
static void create_prepare (void);
static gboolean create_check (void);
static void reply_cb (gint val, gpointer data);
void memory_actions_set_sensitive (gboolean state);

extern void
create_cb (GtkButton *button, gpointer user_data)
{
	g_return_if_fail (xst_tool_get_access (tool));

	create_prepare ();
}

extern void
on_create_dialog_show (GtkWidget *widget, gpointer user_data)
{
	/* Set focus to path entry. */
	gtk_widget_grab_focus (xst_dialog_get_widget (tool->main_dialog, "create_path"));
}

static void
create_dialog_close (void)
{
	gtk_widget_hide (xst_dialog_get_widget (tool->main_dialog, "create_dialog"));
}

extern void
on_create_cancel_clicked (GtkButton *button, gpointer user_data)
{
	create_dialog_close ();
}

extern void
on_create_dialog_delete_event (GtkWidget *widget, gpointer user_data)
{
	create_dialog_close ();
}

extern void
on_create_ok_clicked (GtkButton *button, gpointer user_data)
{
	if (create_affect ())
	{
		create_dialog_close ();
		xst_dialog_modify (tool->main_dialog);
	}
}

extern void
select_row_cb (GtkCList *clist, gint row, gint col, GdkEventButton *event, gpointer user_data)
{
	memory_actions_set_sensitive (TRUE);
}

extern void
unselect_row_cb (GtkCList *clist, gint row, gint col, GdkEventButton *event, gpointer user_data)
{
	memory_actions_set_sensitive (FALSE);
}
extern void
settings_cb (GtkButton *button, gpointer user_data)
{
	GnomeDialog *dialog;

	settings_prepare ();
	dialog = GNOME_DIALOG (xst_dialog_get_widget (tool->main_dialog, "settings_dialog"));
	
	switch (gnome_dialog_run_and_close (dialog))
	{
		case 0:
			settings_affect ();
			break;

		case 1:
		case -1:
		default:
			break;
	}
}

static void
settings_prepare (void)
{
	GtkWidget *w0;
	GtkCList *clist;
	xmlNodePtr node;
	gchar *device, *priority, *size;
	gboolean enabled;
	gboolean is_file;
	gint row;

	w0 = xst_dialog_get_widget (tool->main_dialog, "settings_dialog");
	gtk_widget_show (w0);

	/* read from ui and populate dialog */

	clist = GTK_CLIST (xst_dialog_get_widget (tool->main_dialog, "memory_list"));
	row = GPOINTER_TO_INT (clist->selection->data);

	enabled = xst_ui_clist_get_checkmark (clist, row, 0);
	gtk_clist_get_text (clist, row, 1, &device);
	gtk_clist_get_text (clist, row, 2, &priority);
	gtk_clist_get_text (clist, row, 3, &size);

	node = gtk_clist_get_row_data (clist, row);
	is_file = xst_xml_element_get_state (node, "isfile");

	w0 = xst_dialog_get_widget (tool->main_dialog, "settings_device_label");
	gtk_label_set_text (GTK_LABEL (w0), device);

	w0 = xst_dialog_get_widget (tool->main_dialog, "settings_enable");
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (w0), enabled);

	w0 = xst_dialog_get_widget (tool->main_dialog, "settings_priority");
	gtk_spin_button_set_value (GTK_SPIN_BUTTON (w0), atoi(priority));

	w0 = xst_dialog_get_widget (tool->main_dialog, "settings_size");
	gtk_spin_button_set_value (GTK_SPIN_BUTTON (w0), g_strtod (size, NULL));

	if (is_file)
		gtk_widget_set_sensitive (w0, TRUE);
	else
		gtk_widget_set_sensitive (w0, FALSE);

	w0 = xst_dialog_get_widget (tool->main_dialog, "settings_dialog");
	gtk_widget_show (w0);
}

extern void
remove_cb (GtkButton *button, gpointer user_data)
{
	GtkCList *clist;
	xmlNodePtr node;
	gchar *txt, *name;
	GtkWindow *parent;
	GnomeDialog *dialog;
	gint row;

	g_return_if_fail (xst_tool_get_access (tool));

	clist = GTK_CLIST (xst_dialog_get_widget (tool->main_dialog, "memory_list"));
	row = GPOINTER_TO_INT (clist->selection->data);

	if (row < 0)
		return;

	gtk_clist_get_text (clist, row, 1, &name);
	txt = g_strdup_printf (_("Are You sure you want to remove %s?"), name);

	parent = GTK_WINDOW (tool->main_dialog);
	dialog = GNOME_DIALOG (gnome_question_dialog_parented (txt, reply_cb, NULL, parent));
	gnome_dialog_run (dialog);
	g_free (txt);

	if (reply)
		return;

	node = gtk_clist_get_row_data (clist, row);
	xst_xml_element_destroy (node);
	gtk_clist_remove (clist, row);

	if (clist->rows < 1)
		memory_actions_set_sensitive (FALSE);

	else
		gtk_clist_select_row (clist, 0, 0);

	xst_dialog_modify (tool->main_dialog);
}


static void
create_prepare (void)
{
	GtkWidget *w0;

	g_return_if_fail (xst_tool_get_access (tool));

	/* Set path to none */
	w0 = xst_dialog_get_widget (tool->main_dialog, "create_path");
	gtk_entry_set_text (GTK_ENTRY (w0), "");
	gtk_widget_grab_focus (w0);

	/* Set enabled boolean on */

	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (xst_dialog_get_widget (tool->main_dialog, "create_enable")), TRUE);

	/* Show create_dialog. */
	gtk_widget_show (xst_dialog_get_widget (tool->main_dialog, "create_dialog"));
}
	

static gboolean
create_affect (void)
{
	GtkCList *clist;
	GtkMenu *menu;
	GtkWidget *active_item;
	xmlNodePtr node;
	gchar *text;
	gchar *rowtext[SWAP_AREAS_CLIST_COLS_NUM];
	gint i;
	gint size;
	gboolean state;
	gboolean is_file = FALSE;
	gint row;

	g_return_val_if_fail (xst_tool_get_access (tool), FALSE);

	if (!create_check ())
		return FALSE;

	clist = GTK_CLIST (xst_dialog_get_widget (tool->main_dialog, "memory_list"));
	
	menu = GTK_MENU (gtk_option_menu_get_menu (GTK_OPTION_MENU
			(xst_dialog_get_widget (tool->main_dialog, "create_type"))));

	active_item = gtk_menu_get_active (menu);

	/* Add empty row to clist */

	for (i = 0; i < SWAP_AREAS_CLIST_COLS_NUM; i++)
		rowtext[i] = NULL;

	row = gtk_clist_append (clist, rowtext);


	/* Create top-level XML node */

	node = xst_xml_element_add (xst_xml_doc_get_root (tool->config), "swapdev");
	if (!node)
		return FALSE;		/* should never happen */

	/* Add childs to top-level node and to clist */
	/* Add path */

	text = gtk_entry_get_text (GTK_ENTRY (xst_dialog_get_widget (tool->main_dialog, "create_path")));
	gtk_clist_set_text (clist, row, 1, text);
	xst_xml_element_add_with_content (node, "device", text);

	/* Add priority */

	text = g_strdup_printf ("%d", gtk_spin_button_get_value_as_int (GTK_SPIN_BUTTON 
			(xst_dialog_get_widget (tool->main_dialog, "create_priority"))));

	gtk_clist_set_text (clist, row, 2, text);
	xst_xml_element_add_with_content (node, "priority", text);
	g_free (text);

	/* Add enabled/disabled checkmark */

	state = gtk_toggle_button_get_active
		(GTK_TOGGLE_BUTTON (xst_dialog_get_widget (tool->main_dialog, "create_enable")));

	xst_ui_clist_set_checkmark (clist, row, 0, state);
	xst_xml_element_set_state (node, "enabled", state);

	/* Set size */

	size = gtk_spin_button_get_value_as_int (GTK_SPIN_BUTTON
			(xst_dialog_get_widget (tool->main_dialog, "create_size")));

	text = tool_text_size_from_mb (size);

	gtk_clist_set_text (clist, row, 3, text);

	xst_xml_element_set_state (node, "isfile", TRUE);
	xst_xml_element_add_with_content (node, "size", text);

	g_free (text);

	/* Set is_file */

	if (active_item != GTK_MENU_SHELL (menu)->children->data)
		is_file = TRUE;

	xst_xml_element_set_state (node, "isfile", is_file);

	/* Add isnew */

	xst_xml_element_set_state (node, "isnew", TRUE);

	/* Assign XML node to clist row */

	gtk_clist_set_row_data (clist, row, node);

	gtk_clist_columns_autosize (clist);

	gtk_clist_select_row (clist, row, 0);
	memory_actions_set_sensitive (TRUE);

	return TRUE;
}

extern void
on_browse_button_clicked (GtkButton *button, gpointer user_data)
{
	GtkWidget *w0;
	GtkMenu *menu;
	GtkWidget *active_item;

	menu = GTK_MENU (gtk_option_menu_get_menu (GTK_OPTION_MENU
		(xst_dialog_get_widget (tool->main_dialog, "create_type"))));

	active_item = gtk_menu_get_active (menu);

	if (active_item != GTK_MENU_SHELL (menu)->children->data)
	{
		/* File */
		w0 = xst_dialog_get_widget (tool->main_dialog, "file_selection");
		gtk_file_selection_set_filename (GTK_FILE_SELECTION (w0), "/");
		gtk_widget_show (w0);
	}
	else
	{
		gtk_widget_show (xst_dialog_get_widget (tool->main_dialog, "browse_dialog"));
	}
}

extern void
on_create_type_clicked (GtkButton *button, gpointer user_data)
{
	GtkWidget *w0;
	GtkMenu *menu;
	GtkWidget *active_item;
	gboolean is_file = FALSE;
	
	menu = GTK_MENU (gtk_option_menu_get_menu (GTK_OPTION_MENU
		(xst_dialog_get_widget (tool->main_dialog, "create_type"))));

	active_item = gtk_menu_get_active (menu);

	w0 = xst_dialog_get_widget (tool->main_dialog, "create_size");
	if (active_item != GTK_MENU_SHELL (menu)->children->data)
		is_file = TRUE;

	gtk_widget_set_sensitive (w0, is_file);
}

extern void
on_create_priority_toggled (GtkWidget *widget, gpointer user_data)
{
	gboolean default_priority = FALSE;
	GtkWidget *w0;

	if (GTK_TOGGLE_BUTTON (widget)->active)
		default_priority = TRUE;

	w0 = xst_dialog_get_widget (tool->main_dialog, "create_priority");
	gtk_widget_set_sensitive (w0, default_priority);
}

extern void
file_cancel_cb (GtkButton *button, gpointer user_data)
{
	gtk_widget_hide (xst_dialog_get_widget (tool->main_dialog, "file_selection"));
}

extern void
file_ok_cb (GtkButton *button, gpointer user_data)
{
	GtkFileSelection *filesel;
	GtkWidget *w0;
	gchar *fname;
	struct stat *buf;
	gint errnr;
	long size = 128; /* Default size */

	buf = g_new0 (struct stat, 1);
	filesel = GTK_FILE_SELECTION (xst_dialog_get_widget (tool->main_dialog, "file_selection"));
		
	fname = g_strdup (gtk_file_selection_get_filename (filesel));
	errnr = stat (fname, buf);

	/* TODO: Should check errors too */
	if (!errnr)
	{
		size = (long)buf->st_size;
		size = size / (1024 * 1024);	/* Convert it from bytes to Mb */
	}

	w0 = xst_dialog_get_widget (tool->main_dialog, "create_path");
	gtk_entry_set_text (GTK_ENTRY (w0), fname);

	w0 = xst_dialog_get_widget (tool->main_dialog, "create_size");
	gtk_spin_button_set_value (GTK_SPIN_BUTTON (w0), (gfloat)size);
	
	gtk_widget_hide (GTK_WIDGET (filesel));
	g_free (fname);
	g_free (buf);
}
	

/* Browse dialog */

extern void
on_browse_cancel_clicked (GtkButton *button, gpointer user_data)
{
	gtk_widget_hide (xst_dialog_get_widget (tool->main_dialog, "browse_dialog"));
}

extern void
on_browse_ok_clicked (GtkButton *button, gpointer user_data)
{
	GtkCList *clist;
	GtkWidget *w0;
	xmlNodePtr node, n0;
	gchar *txt;
	gint row;

	clist = GTK_CLIST (xst_dialog_get_widget (tool->main_dialog, "browse_list"));
	row = GPOINTER_TO_INT (clist->selection->data);

	node = gtk_clist_get_row_data (clist, row);

	n0 = xst_xml_element_find_first (node, "device");
	if (n0)
	{
		txt = xst_xml_element_get_content (n0);
		w0 = xst_dialog_get_widget (tool->main_dialog, "create_path");
		gtk_entry_set_text (GTK_ENTRY (w0), txt);
		g_free (txt);
	}

	n0 = xst_xml_element_find_first (node, "size");
	if (n0)
	{
		txt = xst_xml_element_get_content (n0);
		w0 = xst_dialog_get_widget (tool->main_dialog, "create_size");
		gtk_spin_button_set_value (GTK_SPIN_BUTTON (w0), atoi (txt));
		g_free (txt);
	}

	gtk_widget_hide (xst_dialog_get_widget (tool->main_dialog, "browse_dialog"));
}


/* Settings dialog */

static void
settings_affect (void)
{
	GtkWidget *w0;
	GtkCList *clist;
	gboolean enabled = FALSE;
	gchar *text;
	xmlNodePtr node, n0;
	gint row;

	g_return_if_fail (xst_tool_get_access (tool));

	clist = GTK_CLIST (xst_dialog_get_widget (tool->main_dialog, "memory_list"));
	row = GPOINTER_TO_INT (clist->selection->data);

	node = gtk_clist_get_row_data(clist, row);

	/* Set enabled/disabled */

	w0 = xst_dialog_get_widget (tool->main_dialog, "settings_enable");
	if (GTK_TOGGLE_BUTTON (w0)->active)
		enabled = TRUE;

	xst_ui_clist_set_checkmark (clist, row, 0, enabled);
	xst_xml_element_set_state (node, "enabled", enabled);

	/* Set priority */

	w0 = xst_dialog_get_widget (tool->main_dialog, "settings_priority");
	text = g_strdup_printf ("%d",
		gtk_spin_button_get_value_as_int (GTK_SPIN_BUTTON (w0)));

	gtk_clist_set_text (clist, row, 2, text);
	n0 = xst_xml_element_find_first (node, "priority");
	xst_xml_element_set_content (n0, text);
	g_free (text);

	/* Set size */

	w0 = xst_dialog_get_widget (tool->main_dialog, "settings_size");
	text = tool_text_size_from_mb (gtk_spin_button_get_value_as_int (GTK_SPIN_BUTTON (w0)));

	gtk_clist_set_text (clist, row, 3, text);
	n0 = xst_xml_element_find_first (node, "size");
	xst_xml_element_set_content (n0, text);
	g_free (text);
}

/* Helper functions */

static gboolean
create_check (void)
{
	xmlNodePtr root, n0;
	gchar *text, *text2;
	GtkWindow *win;
	GtkWidget *entry;
	GnomeDialog *dialog;
	gchar *buf = NULL;

	entry = xst_dialog_get_widget (tool->main_dialog, "create_path");
	text = gtk_entry_get_text (GTK_ENTRY (entry));

	if (strlen (text) < 1)
	{
		/* No name, no swap */
		buf = g_strdup (_("Path to new swap location is empty."));
	}

	else
	{
		root = xst_xml_doc_get_root (tool->config);

		for (n0 = xst_xml_element_find_first (root, "swapdev"); n0;
			       n0 = xst_xml_element_find_next (n0, "swapdev"))
		{
			text2 = xst_xml_get_child_content (n0, "device");
			if (!text2)
				continue;
			
			if (!strcmp (text, text2))
			{
				/* It's already in there, can't add */
				buf = g_strdup_printf (_("Swap space %s already exists."), text);
				g_free (text2);
				break;
			}
			g_free (text2);
		}
	}

	if (buf)
	{
		win = GTK_WINDOW (xst_dialog_get_widget (tool->main_dialog, "create_dialog"));
		dialog = GNOME_DIALOG (gnome_error_dialog_parented (buf, win));
		gnome_dialog_run (dialog);
		g_free (buf);
		gtk_widget_grab_focus (entry);
		return FALSE;
	}

	return TRUE;
}

static void
reply_cb (gint val, gpointer data)
{
	reply = val;
}

void
memory_actions_set_sensitive (gboolean state)
{
	if (xst_tool_get_access (tool)) {
		gtk_widget_set_sensitive (xst_dialog_get_widget (tool->main_dialog, "memory_create"), TRUE);
		gtk_widget_set_sensitive (xst_dialog_get_widget (tool->main_dialog, "memory_remove"), state);
	}

	gtk_widget_set_sensitive (xst_dialog_get_widget (tool->main_dialog, "memory_settings"), state);
}

