/*
 * Archivo de cadenas traducibles generado por Glade.
 * A�ada este archivo a su POTFILES.in de su proyecto.
 * NO lo compile como parte de su aplicaci�n.
 */

gchar *s = N_("Group Settings");
gchar *s = N_("_Group name:");
gchar *s = N_("G_id:");
gchar *s = N_("Members");
gchar *s = N_("All _users");
gchar *s = N_("Group _members");
gchar *s = N_("_Add >>");
gchar *s = N_("<< _Remove");
gchar *s = N_("window1");
gchar *s = N_("This is a list of the users available in the system. Create new users or edit existing user settings with this dialog.");
gchar *s = N_("_Add user...");
gchar *s = N_("_Settings...");
gchar *s = N_("_Delete");
gchar *s = N_("_Profiles...");
gchar *s = N_("Users");
gchar *s = N_("This is the list of the groups available in the system. Create new groups or edit existing groups with this dialog.");
gchar *s = N_("_Add group...");
gchar *s = N_("_Settings...");
gchar *s = N_("_Delete");
gchar *s = N_("Groups");
gchar *s = N_("_Show all users and groups");
gchar *s = N_("window2");
gchar *s = N_("EXPERIMENTAL NIS support.\n"
              "It's only available if You have NIS\n"
              "and when You click on \"More Options\".");
gchar *s = N_("New User");
gchar *s = N_("New Group");
gchar *s = N_("Delete");
gchar *s = N_("Settings for selected user and group");
gchar *s = N_("Change the settings and options for the\n"
              "selected user or group, like a users password\n"
              "or a groups name or members.");
gchar *s = N_("Settings");
gchar *s = N_("label79");
gchar *s = N_("On this page, you can select scripts and files to be copied from your account into new users' accounts.");
gchar *s = N_("Edit profile:");
gchar *s = N_("Add");
gchar *s = N_("Delete");
gchar *s = N_("label152");
gchar *s = N_("User Account Wizard");
gchar *s = N_("User Creation Druid");
gchar *s = N_("This druid is designed to help you quickly \n"
              "and easily add new users to your system.\n"
              "\n"
              "To begin, click \"Next\". ");
gchar *s = N_("User Identity  (step 1 of 2)");
gchar *s = N_("Please enter a username for the new user in the space below. Optionally, you can fill in more information to identify the user. ");
gchar *s = N_("Click \"Next\" to continue");
gchar *s = N_("Password options (step 2 of 2)");
gchar *s = N_("Please enter a password for the new user. ");
gchar *s = N_("Click \"Next\" to continue");
gchar *s = N_("Finished");
gchar *s = N_("Congratulations, the new user has been created.\n"
              "Please click \"Finish\" to save your changes. ");
gchar *s = N_("New Profile");
gchar *s = N_("Use this dialog to create a new profile.");
gchar *s = N_("_Name:");
gchar *s = N_("_Comment:");
gchar *s = N_("C_opy settings from:");
gchar *s = N_("window3");
gchar *s = N_("User profiles can be used a templates to facilitate the creation of new users. To edit a profile, change the settings in the following tabs.");
gchar *s = N_("Add...");
gchar *s = N_("Copy...");
gchar *s = N_("Delete");
gchar *s = N_("Profiles");
gchar *s = N_("On this page, you can define the system defaults which will be assigned to new users.");
gchar *s = N_("Edit profile:");
gchar *s = N_("System defaults");
gchar *s = N_("Home directory:");
gchar *s = N_("Default shell:");
gchar *s = N_("Default group:");
gchar *s = N_("Automatic UID/GID");
gchar *s = N_("Minimum UID:");
gchar *s = N_("Minimum GID:");
gchar *s = N_("Maximum GID:");
gchar *s = N_("Maximum UID:");
gchar *s = N_("System");
gchar *s = N_("On this page, you can define security options, such as how frequently users must change their passwords.");
gchar *s = N_("Edit profile:");
gchar *s = N_("Password options");
gchar *s = N_("Days between warning and password expiration:");
gchar *s = N_("Assign a random password by default");
gchar *s = N_("Number of days that a password may be used:");
gchar *s = N_("Minimum days allowed between password changes:");
gchar *s = N_("Security");
gchar *s = N_("Basic settings");
gchar *s = N_("Real _name:");
gchar *s = N_("_Username:");
gchar *s = N_("Contact information");
gchar *s = N_("_Office location:");
gchar *s = N_("_Work phone:");
gchar *s = N_("_Home phone:");
gchar *s = N_("Set password b_y hand");
gchar *s = N_("Generate _random password");
gchar *s = N_("Password");
gchar *s = N_("Ne_w:");
gchar *s = N_("Check password _quality");
gchar *s = N_("C_onfirm:");
gchar *s = N_("label148");
gchar *s = N_("Password set to:");
gchar *s = N_("New password");
gchar *s = N_("label149");
gchar *s = N_("_Account");
gchar *s = N_("_Profile:");
gchar *s = N_("_Edit profiles...");
gchar *s = N_("Advanced settings");
gchar *s = N_("User _ID:");
gchar *s = N_("Home _directory:");
gchar *s = N_("_Shell:");
gchar *s = N_("Optional settings");
gchar *s = N_("Ma_ximum days a password may be used:");
gchar *s = N_("Mi_nimum days between password changes:");
gchar *s = N_("_Days between warning and password expiration:");
gchar *s = N_("A_dvanced");
gchar *s = N_("Main _group:");
gchar *s = N_("Groups");
gchar *s = N_("A_vailable groups");
gchar *s = N_("Us_er's groups");
gchar *s = N_("_Add >>");
gchar *s = N_("<< _Remove ");
gchar *s = N_("_Groups");
