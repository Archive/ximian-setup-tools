/*
 * Translatable strings file generated by Glade.
 * Add this file to your project's POTFILES.in.
 * DO NOT compile it as part of your application.
 */

gchar *s = N_("DHCP Server");
gchar *s = N_("DHCP server configuration");
gchar *s = N_("DHCP server configuration ");
gchar *s = N_("E_nabled");
gchar *s = N_("Subnet _Mask :");
gchar *s = N_("Range _End :");
gchar *s = N_("Range _Start :");
gchar *s = N_("_Domain :");
gchar *s = N_("DNS_2 :");
gchar *s = N_("DNS_1 :");
gchar *s = N_("_Edit Hosts...");
gchar *s = N_("Router :");
gchar *s = N_("Advanced mode not yet implemented. Sorry.");
gchar *s = N_("Hosts Editor");
gchar *s = N_("Hosts :");
gchar *s = N_("host");
gchar *s = N_("Ip Address");
gchar *s = N_("Host Info : ");
gchar *s = N_("_Hostname :");
gchar *s = N_("_MAC address :");
gchar *s = N_("_IP address :");
gchar *s = N_("_Comment :");
gchar *s = N_("_Add");
gchar *s = N_("_Delete");
